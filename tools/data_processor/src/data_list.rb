require_relative 'templates'

class DataList
    def initialize(directory, catalog)
        @catalog = catalog
        @header = directory.join("#{@catalog}DataList.h").open("w+")
        @object = directory.join("#{@catalog}DataList.cpp").open("w+")
    end

    def self.finalize(data_list)
        data_list.close_files
    end

    def open_list
        @header.write(DATA_LIST_HEADER_BEGIN)
        @object.write(DATA_LIST_CODE_BEGIN.gsub(/%CATALOG%/, @catalog))

        text = DATA_LIST_CATALOG_OPEN.gsub(/%CATALOG%/, @catalog)
        @header.write(text)
        @object.write(text)
    end

    def add_list_entry(factory)
        @header.write(DATA_LIST_HEADER_ENTRY.gsub(/%FACTORY%/, factory))
        @object.write(DATA_LIST_CODE_ENTRY.gsub(/%FACTORY%/, factory)
                                          .gsub(/%CATALOG%/, @catalog))
    end

    def close_list
        @header.write(CLOSE_SCOPE)
        @header.write(CLOSE_SCOPE)
        @object.write(CLOSE_SCOPE)
        @object.write(CLOSE_SCOPE)
    end

private
    def close_files
        @header.close
        @object.close
    end
end
