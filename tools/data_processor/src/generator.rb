require 'set'

require_relative 'catalog'
require_relative 'data_list'
require_relative 'factory'
require_relative 'templates'

class Generator
    def initialize(directory, name, content)
        if content.nil?
            puts "ERROR: No data to parse for \'#{name}\'."
            exit
        end

        if content['Type'].nil?
            puts "ERROR: \'Type\' key not found in \'#{name}\'."
            exit
        end

        if content['Data'].nil?
            puts "ERROR: \'Data\' key not found in \'#{name}\'."
            exit
        end

        @name = name
        @content = content

        @catalog = Catalog.new(directory, name, content['Type'])
        @data_list = DataList.new(directory, name)
        @factory = Factory.new(directory, name, content['Type'])

        @parse_context = Class.new do
            attr_accessor :factory_name,
                          :factory_properties,
                          :static_ressource,
                          :includes

            def initialize
                @includes = Set.new
            end
        end.new
    end

    def generate_code
        begin_generate

        @content['Data'].each do |data|
            @parse_context.factory_name = data.first[0]
            @parse_context.factory_properties = data.first[1]

            if @parse_context.factory_properties.nil?
                @parse_context.static_ressource = false
            else
                @parse_context.static_ressource = @parse_context.factory_properties.delete('Singleton')
            end

            add_current_content
        end

        end_generate
    end

private
    def begin_generate
        @data_list.open_list
        write_include_section
        write_begin_section
    end

    def add_current_content
        @data_list.add_list_entry(@parse_context.factory_name)
        write_content_section
    end

    def end_generate
        write_end_section
        @factory.write_extra_include(@parse_context.includes)
        @data_list.close_list
    end

    def write_include_section
        @catalog.write_include_section(@content['Includes'], @parse_context.factory_name)
        @factory.write_include_section(@content['Includes'], @parse_context.factory_name)
    end

    def write_begin_section
        @catalog.write_begin_section(@parse_context.factory_name)
        @factory.write_begin_section(@parse_context.factory_name)
    end

    def write_content_section
        @catalog.write_content_section(@parse_context.factory_name)
        @parse_context.includes.merge(
            @factory.write_content_section(@parse_context.static_ressource,
                                           @parse_context.factory_properties,
                                           @parse_context.factory_name))
    end

    def write_end_section
        @catalog.write_end_section(@parse_context.factory_name)
        @factory.write_end_section(@parse_context.factory_name)
    end
end
