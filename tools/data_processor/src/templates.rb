ressource_var = "data"
NAMESPACE_NAME = "DataList"

GENERIC_INCLUDE = %{
    #include <%INCLUDE%>
}

CATALOG_INCLUDE = %{
    #include "%CATALOG%Catalog.h"
}

CLOSE_SCOPE = %{
    \}
}

CATALOG_HEADER_INCLUDE = %{
    #pragma once
    #include <forge/engine/data/DataCatalog.h>
    #include "%CATALOG%Factories.h"
    #include "%CATALOG%DataList.h"
}

CATALOG_HEADER_BEGIN = %{
    //----------------------------------------------------------------------------------------------------------------------
    class %CATALOG%Catalog : public frg::DataCatalog<%TYPE%\>
    \{
        FRG__DECL_CATALOG(%CATALOG%Catalog);

    public:
        %CATALOG%Catalog();
        ~%CATALOG%Catalog();

    private:
}

CATALOG_HEADER_CONTENT = %{
        frg::generated::%CATALOG%Factories::%FACTORY%Factory m_%FACTORY%Factory;
}

CATALOG_HEADER_END = %{
    \};
}

CATALOG_CODE_INCLUDE = %{
    #include <forge/Project.h>
    #include "%CATALOG%Catalog.h"
}

CATALOG_CODE_BEGIN = %{
    //----------------------------------------------------------------------------------------------------------------------
    %CATALOG%Catalog::%CATALOG%Catalog()
    \{
}

CATALOG_CODE_CONTENT = %{
        RegisterData(#{NAMESPACE_NAME}::%CATALOG%::%FACTORY%, m_%FACTORY%Factory);
}

CATALOG_CODE_END = %{
    \}

    //----------------------------------------------------------------------------------------------------------------------
    %CATALOG%Catalog::~%CATALOG%Catalog()
    \{
        m_Factories.clear();
    \}
}

FACTORY_HEADER_INCLUDE = %{
    #pragma once
    #include <forge/engine/data/DataFactory.h>
}

FACTORY_HEADER_BEGIN = %{
    namespace frg \{ namespace generated \{ namespace %CATALOG%Factories \{
}

FACTORY_HEADER_CONTENT = %{
    //----------------------------------------------------------------------------------------------------------------------
    class %FACTORY%Factory : public frg::AbstractDataFactory
    \{
    public:
        %TYPE%* Create() const override;

    \};
}

FACTORY_HEADER_CONTENT_STATIC = %{
    //----------------------------------------------------------------------------------------------------------------------
    class %FACTORY%Factory : public frg::AbstractDataFactory
    \{
    public:
        %TYPE%* Create() const override;

    private:
        static %TYPE%* m_Data;

    \};
}

FACTORY_HEADER_END = %{
    \}\}\}
}

FACTORY_CODE_INCLUDE = %{
    #include <forge/Project.h>
    #include <forge/engine/data/api/DataAPI.h>

    #include "%CATALOG%Factories.h"
    #include "%CATALOG%DataList.h"

    //extra_includes
}

FACTORY_CODE_BEGIN = %{
    namespace frg \{ namespace generated \{ namespace %CATALOG%Factories \{
}

FACTORY_CODE_CONTENT_BEGIN = %{
    //----------------------------------------------------------------------------------------------------------------------
    %TYPE%* %FACTORY%Factory::Create() const
    \{
        %TYPE%* #{ressource_var} = new %TYPE%();
        #{ressource_var}->SetDataNameId(#{NAMESPACE_NAME}::%CATALOG%::%FACTORY%);
}

FACTORY_CODE_CONTENT_STATIC_BEGIN = %{
    //----------------------------------------------------------------------------------------------------------------------
    %TYPE%* %FACTORY%Factory::m_Data = nullptr;

    //----------------------------------------------------------------------------------------------------------------------
    %TYPE%* %FACTORY%Factory::Create() const
    \{
    if (m_Data == nullptr)
    \{
        %TYPE%* #{ressource_var} = new %TYPE%();
        m_Data = #{ressource_var};
        #{ressource_var}->SetInstanceAddr(&m_Data);
        #{ressource_var}->SetDataNameId(#{NAMESPACE_NAME}::%CATALOG%::%FACTORY%);
}

FACTORY_CODE_CONTENT_END = %{
        return #{ressource_var};
    \}
}

FACTORY_CODE_CONTENT_STATIC_END = %{
    \}

        return m_Data;
    \}
}

FACTORY_CODE_END = %{
    \}\}\}
}

SET_ATTR_DATA = %{
        #{ressource_var}->Set%ATTR%(frg::DataAPI::GetDataFrom<%CATALOG%Catalog>(#{NAMESPACE_NAME}::%CATALOG%::%FACTORY%));
}

SET_ATTR_ARGS = %{
        #{ressource_var}->Set%ATTR%(%ARGS%);
}

DATA_LIST_HEADER_BEGIN = %{
    #pragma once
    #include <forge/engine/data/Data.h>

    namespace #{NAMESPACE_NAME}
    \{
}

DATA_LIST_CODE_BEGIN = %{
    #include <forge/Project.h>
    #include "%CATALOG%DataList.h"

    namespace #{NAMESPACE_NAME}
    \{
}

DATA_LIST_CATALOG_OPEN = %{
    namespace %CATALOG%
    \{
}

DATA_LIST_HEADER_ENTRY = %{
        extern const frg::DataNameId %FACTORY%;
}

DATA_LIST_CODE_ENTRY = %{
        constexpr frg::DataNameId %FACTORY% = frg::DataNameId("%CATALOG%::%FACTORY%");
}

def format_template(template, type, catalog, factory)
    format = template.gsub(/%TYPE%/, type)
                     .gsub(/%CATALOG%/, catalog)

    unless factory.nil?
        format = format.gsub(/%FACTORY%/, factory)
    end

    return format
end

def format_arg(arg)
    return arg.is_a?(String) ? "\"#{arg}\"" : arg.to_s
end

def format_set_data(attr, value)
    return SET_ATTR_DATA.gsub(/%ATTR%/, attr)
                        .gsub(/%CATALOG%/, value['from'])
                        .gsub(/%FACTORY%/, value['name'])
end

def format_set_multi(attr, value)
    args = format_arg(value[0])

    for i in 1..value.length - 1
        args += ", " + format_arg(value[i])
    end

    return SET_ATTR_ARGS.gsub(/%ATTR%/, attr)
                        .gsub(/%ARGS%/, args)
end

def format_set_single(attr, value)
    return SET_ATTR_ARGS.gsub(/%ATTR%/, attr)
                        .gsub(/%ARGS%/, format_arg(value))
end
