require_relative 'templates'

class Factory
    def initialize(directory, catalog, type)
        @catalog = catalog
        @type = type
        @header = directory.join("#{@catalog}Factories.h").open("w+")
        @object = directory.join("#{@catalog}Factories.cpp").open("w+")
    end

    def self.finalize(factory)
        factory.close_files
    end

    def write_include_section(includes, factory)
        @header.write(format_template(FACTORY_HEADER_INCLUDE, @type, @catalog, factory))
        @object.write(format_template(FACTORY_CODE_INCLUDE, @type, @catalog, factory))

        unless includes.nil?
            includes.each do |incl|
                @header.write(GENERIC_INCLUDE.gsub(/%INCLUDE%/, incl))
            end
        end
    end

    def write_extra_include(includes)
        extra_includes = ""

        includes.each do |incl|
            extra_includes += CATALOG_INCLUDE.gsub(/%CATALOG%/, incl) + "\n"
        end

        if extra_includes.length > 0
            @object.seek(0, :SET)
            new_content = @object.read.gsub(/\/\/extra_includes/, extra_includes)
            @object.seek(0, :SET)
            @object.write(new_content)
        end
    end

    def write_begin_section(factory)
        @header.write(format_template(FACTORY_HEADER_BEGIN, @type, @catalog, factory))
        @object.write(format_template(FACTORY_CODE_BEGIN, @type, @catalog, factory))
    end

    def write_content_section(is_static, properties, factory)
        extra_includes = Set.new

        fac_c_content_end = is_static ? FACTORY_CODE_CONTENT_STATIC_END : FACTORY_CODE_CONTENT_END
        fac_h_content = is_static ? FACTORY_HEADER_CONTENT_STATIC : FACTORY_HEADER_CONTENT
        fac_c_content_begin = is_static ? FACTORY_CODE_CONTENT_STATIC_BEGIN : FACTORY_CODE_CONTENT_BEGIN

        @header.write(format_template(fac_h_content, @type, @catalog, factory))
        @object.write(format_template(fac_c_content_begin, @type, @catalog, factory))

        unless properties.nil?
            properties.each do |key, value|
                if key == 'Verbatim'
                    @object.write(value)
                elsif value.respond_to?('key')
                    extra_includes.add(value['from'])
                    @object.write(format_set_data(key, value))
                elsif value.is_a?(Array)
                    @object.write(format_set_multi(key, value))
                else
                    @object.write(format_set_single(key, value))
                end
            end
        end

        @object.write(fac_c_content_end)

        return extra_includes
    end

    def write_end_section(factory)
        @header.write(format_template(FACTORY_HEADER_END, @type, @catalog, factory))
        @object.write(format_template(FACTORY_CODE_END, @type, @catalog, factory))
    end

private
    def close_files
        @header.close
        @object.close
    end
end
