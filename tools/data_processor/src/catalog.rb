require_relative 'templates'

class Catalog
    def initialize(directory, name, type)
        @name = name
        @type = type
        @header = directory.join("#{@name}Catalog.h").open("w+")
        @object = directory.join("#{@name}Catalog.cpp").open("w+")
    end

    def self.finalize(catalog)
        catalog.close_files
    end

    def write_include_section(includes, factory)
        @header.write(format_template(CATALOG_HEADER_INCLUDE, @type, @name, factory))
        @object.write(format_template(CATALOG_CODE_INCLUDE, @type, @name, factory))

        unless includes.nil?
            includes.each do |incl|
                @header.write(GENERIC_INCLUDE.gsub(/%INCLUDE%/, incl))
            end
        end
    end

    def write_begin_section(factory)
        @header.write(format_template(CATALOG_HEADER_BEGIN, @type, @name, factory))
        @object.write(format_template(CATALOG_CODE_BEGIN, @type, @name, factory))
    end

    def write_content_section(factory)
        @header.write(format_template(CATALOG_HEADER_CONTENT, @type, @name, factory))
        @object.write(format_template(CATALOG_CODE_CONTENT, @type, @name, factory))
    end

    def write_end_section(factory)
        @header.write(format_template(CATALOG_HEADER_END, @type, @name, factory))
        @object.write(format_template(CATALOG_CODE_END, @type, @name, factory))
    end

private
    def close_files
        @header.close
        @object.close
    end
end
