#!/bin/ruby

require 'fileutils'
require 'optparse'
require 'pathname'
require 'yaml'

require_relative 'src/generator'
require_relative 'src/data_list'

output_dir = "."

OptionParser.new do |opt|
    opt.on("-o", "--output OUTPUT_DIR", "Output directory for generated files") do |dir|
        output_dir = dir
    end

    opt.on("-h", "--help", "Prints this help") do
        puts opt
        exit
    end
end.parse!

od = Pathname.new(output_dir)
unless od.exist?
    od.mkdir
end

od_tmp = Pathname.new("#{od.join('_tmp')}")
unless od_tmp.exist?
    od_tmp.mkdir
end

ARGV.each do |arg|
    p "Processing: #{arg}"

    yaml = YAML.load_file(arg)

    if yaml
        yaml.each do |name, content|
            if content.nil?
                puts "ERROR: No data to parse for \"#{name}\"."
                exit
            end

            generator = Generator.new(od_tmp, name, content)
            generator.generate_code
        end
    else
        puts "ERROR: Invalid YAML file \"#{arg}\"."
        exit
    end
end

# close the files
GC.start

od_tmp.each_child(false) do |f|
    if not od.join(f).exist? or not FileUtils.identical?(od.join(f), od_tmp.join(f))
        FileUtils.cp(od_tmp.join(f), od)
    end
end

FileUtils.rm_rf(od_tmp)
FileUtils.touch od.join("data_report")
