#include <forge/Project.h>
#include <forge/builtin/presentation/behaviors/ClickableBehavior.h>

#include <forge/engine/presentation/api/PresentationAPI.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

namespace static_utils
{
    Vector2i GetClickPadding(GUIWidget* _widget)
    {
        Vector2i padding { 2, 2 };

        if ((_widget->GetGravity() & GUIGravity::Right) != 0)
        {
            padding.x *= -1;
        }

        if ((_widget->GetGravity() & GUIGravity::Bottom) != 0)
        {
            padding.y *= -1;
        }

        return padding;
    }
}

//----------------------------------------------------------------------------------------------------------------------
ClickableBehavior::ClickableBehavior(const BehaviorCallback& _callback)
    : AbstractGUIBehavior(_callback)
    , m_IsPressed(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
bool ClickableBehavior::ShouldCaptureEvent(const SystemEvent& _event)
{
    bool shouldCapture = false;
    Vector2f guiScaledCoord = PresentationAPI::ScreenToGUIPixel({ static_cast<f32>(_event.MouseButton.x),
                                                                  static_cast<f32>(_event.MouseButton.y) });

    if (_event.Type == SystemEvent::MouseButtonPressed && GetOwner()->ContainsPixel(guiScaledCoord))
    {
        m_IsPressed = true;
    }
    else if (_event.Type == SystemEvent::MouseButtonReleased)
    {
        if (GetOwner()->ContainsPixel(guiScaledCoord))
        {
            shouldCapture = m_IsPressed;
        }

        m_IsPressed = false;
    }

    return shouldCapture;
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
