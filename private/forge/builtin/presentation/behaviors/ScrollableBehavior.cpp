#include <forge/Project.h>
#include <forge/builtin/presentation/behaviors/ScrollableBehavior.h>

#include <forge/engine/presentation/api/PresentationAPI.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
ScrollableBehavior::ScrollableBehavior(const BehaviorCallback& _callback)
    : AbstractGUIBehavior(_callback)
{
}

//----------------------------------------------------------------------------------------------------------------------
bool ScrollableBehavior::ShouldCaptureEvent(const SystemEvent& _event)
{
    Vector2f guiScaledCoord = PresentationAPI::ScreenToGUIPixel({ static_cast<f32>(_event.MouseWheelScroll.x),
                                                                  static_cast<f32>(_event.MouseWheelScroll.y) });

    if (_event.Type == SystemEvent::MouseWheelScrolled && GetOwner()->ContainsPixel(guiScaledCoord))
    {
        return true;
    }

    return false;
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
