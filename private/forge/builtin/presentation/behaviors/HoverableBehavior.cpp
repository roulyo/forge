#include <forge/Project.h>
#include <forge/builtin/presentation/behaviors/HoverableBehavior.h>

#include <forge/engine/presentation/api/PresentationAPI.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
HoverableBehavior::HoverableBehavior(const BehaviorCallback& _callback)
    : AbstractGUIBehavior(_callback)
    , m_IsHovered(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
bool HoverableBehavior::ShouldCaptureEvent(const SystemEvent& _event)
{
    Vector2f guiScaledCoord = PresentationAPI::ScreenToGUIPixel({ static_cast<f32>(_event.MouseMove.x),
                                                                  static_cast<f32>(_event.MouseMove.y) });

    if (_event.Type == SystemEvent::MouseMoved)
    {
        if (GetOwner()->ContainsPixel(guiScaledCoord))
        {
            if (!m_IsHovered)
            {
                SystemEvent enterEvent = _event;
                enterEvent.Type = SystemEvent::MouseEntered;

                Trigger(enterEvent);
                m_IsHovered = true;
            }
        }
        else
        {
            if (m_IsHovered)
            {
                SystemEvent exitEvent = _event;
                exitEvent.Type = SystemEvent::MouseLeft;

                Trigger(exitEvent);
                m_IsHovered = false;
            }
        }
    }

    return false;
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
