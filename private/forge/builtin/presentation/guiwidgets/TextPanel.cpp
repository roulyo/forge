#include <forge/Project.h>
#include <forge/builtin/presentation/guiwidgets/TextPanel.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);
FRG__OPEN_NAMESPACE(gui);

//----------------------------------------------------------------------------------------------------------------------
AbstractTextPanel::AbstractTextPanel()
{
    AddDrawable(&m_Text);
}

FRG__CLOSE_NAMESPACE(gui);
FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
