#include <forge/Project.h>
#include <forge/builtin/presentation/guiwidgets/Panel.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);
FRG__OPEN_NAMESPACE(gui);

//----------------------------------------------------------------------------------------------------------------------
AbstractPanel::~AbstractPanel()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<>
void SpecializedPanel<QuadShape>::OnRenderedGeometryComputed(const Vector2f& _position,
                                                             const Vector2f& _size)
{
    (*static_cast<GUIDrawable<QuadShape>*>(m_Background))->SetSize(_size);
}

//----------------------------------------------------------------------------------------------------------------------
template<>
void SpecializedPanel<CircleShape>::OnRenderedGeometryComputed(const Vector2f& _position,
                                                               const Vector2f& _size)
{
    (*static_cast<GUIDrawable<CircleShape>*>(m_Background))->SetRadius(_size.h * 0.5f);
}

FRG__CLOSE_NAMESPACE(gui);
FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
