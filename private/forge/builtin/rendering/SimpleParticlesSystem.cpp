#include <forge/Project.h>
#include <forge/builtin/rendering/SimpleParticlesSystem.h>

#include <forge/engine/camera/api/CameraAPI.h>
#include <forge/engine/rendering/drawable/Drawable.h>
#include <forge/engine/time/api/TimeAPI.h>
#include <forge/engine/math/Utils.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

namespace static_utils { //---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
static f32 GetRand(f32 _upperBorn)
{
    FRG__RETURN_IF(_upperBorn == 0.0f, 0.0f);

    return fmod(rand(), _upperBorn * 100.0f) * 0.01f;
}

//----------------------------------------------------------------------------------------------------------------------
static f32 GetRandomVariation(f32 _baseVariation)
{
    return (100.0f + GetRand(_baseVariation) * 2.0f - _baseVariation) * 0.01f;
}

} // namespace static_utils

//----------------------------------------------------------------------------------------------------------------------
static constexpr f32 k_spatialOffset = 10.0f;

//----------------------------------------------------------------------------------------------------------------------
SimpleParticlesSystem::SimpleParticlesSystem()
{
    if (CameraAPI::IsDimetric())
    {
        m_zOffset = k_spatialOffset / sqrt(1.25f);
    }
    else
    {
        m_zOffset = k_spatialOffset;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleParticlesSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
    Entity& entity = *_entity.get();

    ParticlesComponent& particlesComp = entity.GetComponent<ParticlesComponent>();
    List<Particle>& particles = particlesComp.Particles();
    u64 timeMs = TimeAPI::GetGameTimeMilliseconds();

    if (particlesComp.GetIsActive())
    {
        if (particlesComp.GetStartTimeMs() == 0)
        {
            // particlesComp.SetIsEmitting(true);
            particlesComp.SetStartTimeMs(timeMs);

            particles.push_back(CreateParticle(particlesComp));
            particlesComp.SetLastSpawnTimeMs(timeMs);
        }
        else
        {
            u64 elapsedSpawnTimeMs = timeMs - particlesComp.GetLastSpawnTimeMs();
            u32 spawnCount =
                  static_cast<f32>(elapsedSpawnTimeMs) * 0.001f
                * particlesComp.GetDensity() * static_utils::GetRandomVariation(particlesComp.GetDensityVariation());

            for (u32 i = 0; i < spawnCount; ++i)
            {
                particles.push_back(CreateParticle(particlesComp));
                particlesComp.SetLastSpawnTimeMs(timeMs);
            }
        }
    }

    auto particleIt = particles.begin();

    while (particleIt != particles.end())
    {
        if (timeMs - particleIt->StartTimeMs < particleIt->TTLMs)
        {
            HandleParticle(*particleIt, particlesComp);

            const Vector3f& pos = entity.GetPosition();
            PushToRender(particleIt->TransformNode,
                         FloatBox { { pos.x + k_spatialOffset, pos.y + k_spatialOffset, pos.h + m_zOffset },
                                    Vector3f::Zero
                         });

            ++particleIt;
        }
        else
        {
            particleIt = particles.erase(particleIt);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
Particle SimpleParticlesSystem::CreateParticle(const ParticlesComponent& _comp) const
{
    const f32 angle = fmod(360.0f + static_utils::GetRand(_comp.GetSpread()) - _comp.GetSpread() * 0.5f, 360.0f);
    const f32 speed = _comp.GetSpeed() * static_utils::GetRandomVariation(_comp.GetSpeedVariation());
    Particle p;

    p.StartTimeMs = TimeAPI::GetGameTimeMilliseconds();

    p.Speed.x = speed * std::sin(math_utils::Radian(angle));
    p.Speed.y = speed * std::cos(math_utils::Radian(angle));

    p.TTLMs = _comp.GetTTLMs() * static_utils::GetRandomVariation(_comp.GetTTLMsVariation());
    p.Size = _comp.GetSize() * static_utils::GetRandomVariation(_comp.GetSizeVariation());
    p.Fading = _comp.GetFading() * static_utils::GetRandomVariation(_comp.GetFadingVariation());

    p.Offset.x = static_utils::GetRand(_comp.GetSpawnAreaSize().w) - _comp.GetSpawnAreaSize().w * 0.5f;
    p.Offset.y = static_utils::GetRand(_comp.GetSpawnAreaSize().h) - _comp.GetSpawnAreaSize().h * 0.5f;

    p.TransformNode = WorldTransformRenderNode::MakePtr();
    p.TransformNode->AddChild(_comp.GetParticleTreeFactory()());
    p.TransformNode->SetScale({ p.Size, p.Size });

    return p;
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleParticlesSystem::HandleParticle(const Particle& _particle, const ParticlesComponent& _comp) const
{
    f32 dtMs = static_cast<f32>(TimeAPI::GetGameTimeMilliseconds() - _particle.StartTimeMs);
    f32 x = _particle.Speed.x * dtMs * 0.001f;
    f32 y = _particle.Speed.y * dtMs * 0.001f;

    if (_comp.GetGravity() != 0)
    {
        y += (-_comp.GetGravity() * std::pow(dtMs * 0.001f, 2)) * 0.5f; // ut + gt^2 * 0.5
    }

    _particle.TransformNode->SetTranslation({  (_comp.GetSpawnAreaOffset().x + _particle.Offset.x + x),
                                              -(_comp.GetSpawnAreaOffset().y + _particle.Offset.y + y) });
    // _particle.TransformNode->SetScale({ _particle.Size, _particle.Size });
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
