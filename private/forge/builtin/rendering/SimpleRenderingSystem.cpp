#include <forge/Project.h>
#include <forge/builtin/rendering/SimpleRenderingSystem.h>

#include <forge/engine/camera/api/CameraAPI.h>
#include <forge/engine/rendering/drawable/Sprite.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
void SimpleRenderingSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
    Entity& entity = *_entity.get();

    RenderableComponent& renderComp = entity.GetComponent<RenderableComponent>();

    PushToRender(renderComp.GetRenderTreeRoot(), FloatBox{ entity.GetPosition(), entity.GetSize() });
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
