#include <forge/Project.h>
#include <forge/builtin/rendering/ParticlesComponent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_COMPONENT(ParticlesComponent);

//----------------------------------------------------------------------------------------------------------------------
ParticlesComponent::ParticlesComponent()
    : m_ParticleTreeFactory(nullptr)
    , m_SpawnAreaSize{ 0.0f, 0.0f }
    , m_SpawnAreaOffset{ 0.0f, 0.0f }
    , m_Spread(0.0f)
    , m_Density(1.0f)
    , m_DensityVariation(0.0f)
    , m_TTLMs(500)
    , m_TTLMsVariation(0.0f)
    , m_Fading(0.0f)
    , m_FadingVariation(0.0f)
    , m_Size(1.0f)
    , m_SizeVariation(0.0f)
    , m_Speed(1.0f)
    , m_SpeedVariation(0.0f)
    , m_Gravity(0)
    , m_StartTimeMs(0)
    , m_LastSpawnTimeMs(0)
    , m_IsActive(true)
{
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
