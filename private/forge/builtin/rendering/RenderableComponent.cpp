#include <forge/Project.h>
#include <forge/builtin/rendering/RenderableComponent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
namespace static_utils
{
    void RemovePrefixRec(const AbstractRenderTree::CPtr& _node,
                         AbstractRenderTree::Ptr& _tree)
    {

        FRG__RETURN_IF(_tree->GetTag() == "scene_root");

        AbstractRenderTree::Ptr child = _node->GetChildren()[0];

        if (_node == _tree)
        {
            _tree->RemoveChildren();
            _tree = child;
        }
        else
        {
            RemovePrefixRec(_node, child);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_COMPONENT(RenderableComponent);

//----------------------------------------------------------------------------------------------------------------------
RenderableComponent::RenderableComponent()
{
    m_SceneTree = BasicRenderNode::MakePtr("scene_root");
    m_PrefixTree = m_SceneTree;
}

//----------------------------------------------------------------------------------------------------------------------
void RenderableComponent::AddPrefixNode(const AbstractRenderTree::Ptr& _node)
{
    _node->AddChild(m_PrefixTree);
    m_PrefixTree = _node;
}

//----------------------------------------------------------------------------------------------------------------------
void RenderableComponent::RemovePrefixNode(const AbstractRenderTree::CPtr& _node)
{
    static_utils::RemovePrefixRec(_node, m_PrefixTree);
}

//----------------------------------------------------------------------------------------------------------------------
AbstractRenderTree::CPtr RenderableComponent::GetRenderTreeRoot() const
{
    return m_PrefixTree;
}


FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
