#include <forge/Project.h>
#include <forge/builtin/3Cs/PlayerControlSystem.h>

#include <cmath>

#include <forge/engine/camera/api/CameraAPI.h>

#include <forge/builtin/3Cs/InteractableComponent.h>
#include <forge/builtin/3Cs/events/MoveEvent.h>
#include <forge/builtin/3Cs/events/MouseEvent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
static constexpr f32 k_DefaultPlayerSpeed_UpS = 5.0f;
static constexpr f32 k_DeadZoneAngle  = 22.5f;

//----------------------------------------------------------------------------------------------------------------------
PlayerControlSystem::OnEntityMovedHook PlayerControlSystem::ms_OnEntityMovedHook = nullptr;

//----------------------------------------------------------------------------------------------------------------------
void PlayerControlSystem::SetOnEntityMovedHook(const OnEntityMovedHook& _hook)
{
    ms_OnEntityMovedHook = _hook;
}

//----------------------------------------------------------------------------------------------------------------------
PlayerControlSystem::PlayerControlSystem()
    : m_DefaultNavOptions()
    , m_NavOptions()
    , m_NavRequest{ NAN, NAN }
    , m_WasMoving(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
PlayerControlSystem::~PlayerControlSystem()
{
}

//----------------------------------------------------------------------------------------------------------------------
void PlayerControlSystem::OnStart()
{
    FRG__MK_EVT_HANDLR_M(EntityClickedEvent, &PlayerControlSystem::OnEntityClickedEvent, this);
    FRG__MK_EVT_HANDLR_M(NavigationRequestEvent, &PlayerControlSystem::OnNavigationRequestEvent, this);
}

//----------------------------------------------------------------------------------------------------------------------
void PlayerControlSystem::OnStop()
{
    FRG__RM_EVT_HANDLR_M(NavigationRequestEvent, &PlayerControlSystem::OnNavigationRequestEvent, this);
    FRG__RM_EVT_HANDLR_M(EntityClickedEvent, &PlayerControlSystem::OnEntityClickedEvent, this);
}

//----------------------------------------------------------------------------------------------------------------------
void PlayerControlSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
    NavigatorComponent& navComp = _entity->GetComponent<NavigatorComponent>();
    PhysicableComponent& physicsComp = _entity->GetComponent<PhysicableComponent>();

    if (!std::isnan(m_NavRequest.x))
    {
        const bool wasNavigating = !navComp.GetPath().empty();
        const NavNode lastKnownPosition = wasNavigating ? navComp.GetPath().back() : NavNode();

        m_NavOptions.LookupArea.Center = _entity->GetPosition().xy();

        if (m_NavigatorToolkit.Navigate(_entity, m_NavRequest, m_NavOptions))
        {
            if (wasNavigating)
            {
                NavigationStoppedEvent::Broadcast(lastKnownPosition, true);
            }

            NavigationStartedEvent::Broadcast(m_NavRequest);
            m_WasMoving = true;
        }

        m_NavRequest = { NAN, NAN };
    }

    UpdateDirection(_entity);
    UpdateSpeed(_entity);

    if (ms_OnEntityMovedHook != nullptr && (physicsComp.Speed != Vector2f::Zero || m_WasMoving))
    {
        ms_OnEntityMovedHook(_entity, physicsComp.Speed);

        if (physicsComp.Speed == Vector2f::Zero)
        {
            NavigationStoppedEvent::Broadcast(navComp.GetLastKnownPosition().xy(), false);
        }
    }

    m_WasMoving = physicsComp.Speed != Vector2f::Zero;
}

//----------------------------------------------------------------------------------------------------------------------
void PlayerControlSystem::OnEntityClickedEvent(const EntityClickedEvent& _event)
{
    FRG__RETURN_IF(_event.GetIsPressed());
    FRG__RETURN_IF(_event.GetEntity()->HasComponent<frg::bi::InteractableComponent>());

    m_NavRequest = _event.GetEntity()->GetPosition().xy();
    m_NavOptions = m_DefaultNavOptions;
}

//----------------------------------------------------------------------------------------------------------------------
void PlayerControlSystem::OnNavigationRequestEvent(const NavigationRequestEvent& _event)
{
    m_NavRequest = _event.GetDestination();
    m_NavOptions = _event.GetHasOptions() ? _event.GetOptions() : m_DefaultNavOptions;
}

//----------------------------------------------------------------------------------------------------------------------
void PlayerControlSystem::UpdateDirection(const Entity::Ptr& _navEntity) const
{
    FRG__RETURN_IF(_navEntity->GetComponent<NavigatorComponent>().GetPath().empty());

    m_NavigatorToolkit.UpdateNavigation(_navEntity);
}

//----------------------------------------------------------------------------------------------------------------------
void PlayerControlSystem::UpdateSpeed(const Entity::Ptr& _navEntity) const
{
    NavigatorComponent& navComp = _navEntity->GetComponent<NavigatorComponent>();
    PhysicableComponent& physicsComp = _navEntity->GetComponent<PhysicableComponent>();

    physicsComp.Speed.x = navComp.GetDirection().x * navComp.GetMaxSpeed();
    physicsComp.Speed.y = navComp.GetDirection().y * navComp.GetMaxSpeed();
}


FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
