#include <forge/Project.h>
#include <forge/builtin/3Cs/NavigatorComponent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_COMPONENT(NavigatorComponent);

//----------------------------------------------------------------------------------------------------------------------
NavigatorComponent::NavigatorComponent()
    : m_Direction{ 0.0f, 0.0f }
    , m_LastKnownPosition{ 0.0f, 0.0f }
    , m_Path{}
{
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
