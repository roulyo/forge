#include <forge/Project.h>
#include <forge/builtin/3Cs/SimpleControlSystem.h>

#include <cmath>

#include <forge/engine/camera/api/CameraAPI.h>

#include <forge/builtin/3Cs/events/MoveEvent.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

namespace static_utils
{
//----------------------------------------------------------------------------------------------------------------------
    static void Normalize(frg::Vector2f& _vec)
    {
        if (_vec.x != 0.0f || _vec.y != 0.0f)
        {
            f32 length = sqrt((_vec.x * _vec.x) + (_vec.y * _vec.y));

            _vec.x /= length;
            _vec.y /= length;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
f32 SimpleControlSystem::MoveSpeedUpS = 5.0f;

//----------------------------------------------------------------------------------------------------------------------
SimpleControlSystem::SimpleControlSystem()
    : m_Direction(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
SimpleControlSystem::~SimpleControlSystem()
{
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleControlSystem::OnStart()
{
    MoveEvent::Handlers += MoveEvent::Handler(this, &SimpleControlSystem::OnMoveEvent);
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleControlSystem::OnStop()
{
    MoveEvent::Handlers -= MoveEvent::Handler(this, &SimpleControlSystem::OnMoveEvent);
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleControlSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
    PhysicableComponent& physicsComp = _entity->GetComponent<PhysicableComponent>();

    UpdateMoveVector();
    UpdateSpeed(_entity);
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleControlSystem::OnMoveEvent(const MoveEvent& _event)
{
    u8 direction = static_cast<u8>(_event.GetDirection());

    if (_event.IsMoving())
    {
        m_Direction |= static_cast<u8>(_event.GetDirection());
    }
    else
    {
        m_Direction ^= static_cast<u8>(_event.GetDirection());
    }
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleControlSystem::UpdateMoveVector()
{
    const bool isDimetric = CameraAPI::IsDimetric();

    m_MoveVector = { 0.0f, 0.0f };

    if (m_Direction & static_cast<u8>(MoveDirection::Up))
    {
        m_MoveVector.y = -1.0f;

        if (isDimetric)
        {
            m_MoveVector.x = -1.0f;
        }
    }

    if (m_Direction & static_cast<u8>(MoveDirection::Down))
    {
        m_MoveVector.y = +1.0f;

        if (isDimetric)
        {
            m_MoveVector.x = +1.0f;
        }
    }

    if (m_Direction & static_cast<u8>(MoveDirection::Left))
    {
        m_MoveVector.x = -1.0f;

        if (isDimetric)
        {
            m_MoveVector.y = +1.0f;
        }
    }

    if (m_Direction & static_cast<u8>(MoveDirection::Right))
    {
        m_MoveVector.x = +1.0f;

        if (isDimetric)
        {
            m_MoveVector.y = -1.0f;
        }
    }

    static_utils::Normalize(m_MoveVector);
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleControlSystem::UpdateSpeed(const Entity::Ptr& _navEntity) const
{
    PhysicableComponent& physicsComp = _navEntity->GetComponent<PhysicableComponent>();

    physicsComp.Speed.x = m_MoveVector.x * MoveSpeedUpS;
    physicsComp.Speed.y = m_MoveVector.y * MoveSpeedUpS;
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
