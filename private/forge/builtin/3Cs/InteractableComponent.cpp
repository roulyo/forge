#include <forge/Project.h>
#include <forge/builtin/3Cs/InteractableComponent.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_COMPONENT(InteractableComponent);

//----------------------------------------------------------------------------------------------------------------------
InteractableComponent::InteractableComponent()
    : m_AlwaysUseDefault(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
void InteractableComponent::AddInteraction(const AbstractInteraction::CPtr& _interaction)
{
    m_Interactions.push_back(_interaction);
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
