#include <forge/Project.h>
#include <forge/builtin/3Cs/SimpleInteractionSystem.h>

#include <forge/builtin/3Cs/events/MouseEvent.h>
#include <forge/builtin/3Cs/events/MoveEvent.h>
#include <forge/builtin/3Cs/events/InteractionEvents.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
static NavigationOptions s_NavOptions = NavigationOptions();

//----------------------------------------------------------------------------------------------------------------------
SimpleInteractionSystem::SimpleInteractionSystem()
{
    s_NavOptions.StopAheadStepCount = 1;
}

//----------------------------------------------------------------------------------------------------------------------
SimpleInteractionSystem::~SimpleInteractionSystem()
{
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::OnStart()
{
    FRG__MK_EVT_HANDLR_M(EntityClickedEvent, &SimpleInteractionSystem::OnEntityClickedEvent, this);
    FRG__MK_EVT_HANDLR_M(EntityHoveredEnterEvent, &SimpleInteractionSystem::OnEntityHoveredEnterEvent, this);
    FRG__MK_EVT_HANDLR_M(EntityHoveredExitEvent, &SimpleInteractionSystem::OnEntityHoveredExitEvent, this);
    FRG__MK_EVT_HANDLR_M(InteractionChosenEvent, &SimpleInteractionSystem::OnInteractionChosenEvent, this);
    FRG__MK_EVT_HANDLR_M(NavigationStartedEvent, &SimpleInteractionSystem::OnNavigationStartedEvent, this);
    FRG__MK_EVT_HANDLR_M(NavigationStoppedEvent, &SimpleInteractionSystem::OnNavigationStoppedEvent, this);
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::OnStop()
{
    FRG__RM_EVT_HANDLR_M(NavigationStoppedEvent, &SimpleInteractionSystem::OnNavigationStoppedEvent, this);
    FRG__RM_EVT_HANDLR_M(NavigationStartedEvent, &SimpleInteractionSystem::OnNavigationStartedEvent, this);
    FRG__RM_EVT_HANDLR_M(InteractionChosenEvent, &SimpleInteractionSystem::OnInteractionChosenEvent, this);
    FRG__RM_EVT_HANDLR_M(EntityHoveredExitEvent, &SimpleInteractionSystem::OnEntityHoveredExitEvent, this);
    FRG__RM_EVT_HANDLR_M(EntityHoveredEnterEvent, &SimpleInteractionSystem::OnEntityHoveredEnterEvent, this);
    FRG__RM_EVT_HANDLR_M(EntityClickedEvent, &SimpleInteractionSystem::OnEntityClickedEvent, this);
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::ExecuteInteraction(const Entity::CPtr& _interactable,
                                                 const AbstractInteraction::CPtr& _interaction)
{
    _interaction->Interact(_interactable);

    if (_interaction->GetConsumesEntityOnInteract())
    {
        RequestRemoveEntity(_interactable);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::OnEntityClickedEvent(const EntityClickedEvent& _event)
{
    FRG__RETURN_IF(_event.GetIsPressed());
    FRG__RETURN_IF(!_event.GetEntity()->HasComponent<InteractableComponent>());

    const InteractableComponent& comp = _event.GetEntity()->GetComponent<InteractableComponent>();

    if (comp.GetDefaultInteraction() != nullptr && (_event.GetIsLeftClick() || comp.GetAlwaysUseDefault()))
    {
        InteractionChosenEvent::Broadcast(_event.GetEntity(), comp.GetDefaultInteraction());
    }
    else
    {
        InteractableClickedEvent::Broadcast(_event.GetEntity(), comp.GetInteractions());
    }
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::OnEntityHoveredEnterEvent(const EntityHoveredEnterEvent& _event)
{
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::OnEntityHoveredExitEvent(const EntityHoveredExitEvent& _event)
{
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::OnInteractionChosenEvent(const InteractionChosenEvent& _event)
{
    if (_event.GetInteraction()->GetNeedsCloseRange())
    {
        m_Context.Interactable = _event.GetInteractable();
        m_Context.Interaction = _event.GetInteraction();
        m_Context.Destination = _event.GetInteractable()->GetPosition().xy();

        NavigationRequestEvent::Broadcast(m_Context.Destination, s_NavOptions);
    }
    else
    {
        ExecuteInteraction(_event.GetInteractable(), _event.GetInteraction());
    }
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::OnNavigationStartedEvent(const NavigationStartedEvent& _event)
{
    if (_event.GetDestination() != m_Context.Destination)
    {
        m_Context.Reset();
    }
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleInteractionSystem::OnNavigationStoppedEvent(const NavigationStoppedEvent& _event)
{
    if (m_Context.Interaction != nullptr && !_event.GetInterrupted())
    {
        ExecuteInteraction(m_Context.Interactable, m_Context.Interaction);

        m_Context.Reset();
    }
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
