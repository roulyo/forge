#include <forge/Project.h>
#include <forge/builtin/3Cs/events/MoveEvent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
MoveEvent::MoveEvent(bool _isMoving, MoveDirection _direction)
    : m_IsMoving(_isMoving)
    , m_Direction(_direction)
{}

//----------------------------------------------------------------------------------------------------------------------
bool MoveEvent::IsMoving() const
{
    return m_IsMoving;
}

//----------------------------------------------------------------------------------------------------------------------
MoveDirection MoveEvent::GetDirection() const
{
    return m_Direction;
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
