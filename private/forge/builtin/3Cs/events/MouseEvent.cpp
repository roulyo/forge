#include <forge/Project.h>
#include <forge/builtin/3Cs/events/MouseEvent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
MouseClickEvent::MouseClickEvent(i32 _x, i32 _y, bool _isPressed, Mouse::Button _button)
    : m_IsPressed(_isPressed)
    , m_X(_x)
    , m_Y(_y)
    , m_MouseButton(_button)
{
}

//----------------------------------------------------------------------------------------------------------------------
MouseWheelEvent::MouseWheelEvent(i32 _x, i32 _y, f32 _delta)
    : m_Delta(_delta)
{
}

//----------------------------------------------------------------------------------------------------------------------
MouseMoveEvent::MouseMoveEvent(i32 _x, i32 _y)
    : m_X(_x)
    , m_Y(_y)
{
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
