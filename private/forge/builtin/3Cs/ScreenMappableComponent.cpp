#include <forge/Project.h>
#include <forge/builtin/3Cs/ScreenMappableComponent.h>

#include <forge/builtin/3Cs/ScreenMappingSystem.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_COMPONENT(ScreenMappableComponent);

//----------------------------------------------------------------------------------------------------------------------
ScreenMappableComponent::ScreenMappableComponent()
    : m_MaterialNode(ShaderMaterialRenderNode::MakePtr())
{
    ShaderUniform param;
    param.Name = "color";
    param.Type = ShaderUniform::Type::Color;

    ShaderMaterial material;
    material.Program = nullptr;
    material.Parameters[param.Name] = param;

    m_MaterialNode->SetShaderMaterial(material);
    m_MaterialNode->SetOverrideNextMaterials(true);
}

//----------------------------------------------------------------------------------------------------------------------
void ScreenMappableComponent::SetRenderable(const AbstractRenderTree::Ptr& _renderTree)
{
    m_MaterialNode->AddChild(_renderTree);
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
