#include <forge/Project.h>
#include <forge/builtin/3Cs/PlayableCharacterComponent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_COMPONENT(PlayableCharacterComponent);

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
