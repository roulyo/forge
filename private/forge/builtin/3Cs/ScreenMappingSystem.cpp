#include <forge/Project.h>
#include <forge/builtin/3Cs/ScreenMappingSystem.h>

#include <forge/builtin/3Cs/events/MouseEvent.h>

#include <iostream>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
const f32 k_ScreenMapRatio = 1.f / 16.f;
const u32 k_RefreshRateHz = 5;

//----------------------------------------------------------------------------------------------------------------------
constexpr char vertexShader[] =
"\
uniform float   scale;\
uniform vec4    color;\
\
void main()\
{\
    vec2 vertexXY = gl_Vertex.xy * scale;\
    gl_Position = gl_ModelViewProjectionMatrix * vec4(vertexXY, gl_Vertex.zw);\
    gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;\
    gl_FrontColor = color;\
}\
";

//----------------------------------------------------------------------------------------------------------------------
constexpr char pixelShader[] =
"\
uniform sampler2D   currentTexture;\
\
void main()\
{\
    vec4 pixel = texture2D(currentTexture, gl_TexCoord[0].xy);\
    if (pixel.a == 0.0f)\
        gl_FragColor = vec4(gl_Color.rgb, 0);\
    else\
        gl_FragColor = vec4(gl_Color.rgb, 1);\
}\
";

//----------------------------------------------------------------------------------------------------------------------
Shader::Ptr ScreenMappingSystem::ms_ScreenMappingShader = nullptr;
frg::TextureHandle ScreenMappingSystem::ms_ScreenMapHandle = -1;
frg::TextureHandle ScreenMappingSystem::ms_ScreenMapBackBuffer = -1;

//----------------------------------------------------------------------------------------------------------------------
ScreenMappingSystem::ScreenMappingSystem()
    : m_State(State::Waiting)
    , m_ScreenMap(nullptr)
    , m_CurrentColor(1)
    , m_HoveredEntity(nullptr)
{
    if (ms_ScreenMapHandle == -1)
    {
        Vector2f targetRes = GetTargetResolution();
        TextureOptions texOpts({ static_cast<u32>(targetRes.w * k_ScreenMapRatio),
                                 static_cast<u32>(targetRes.h * k_ScreenMapRatio) }
                               , frg::PlainColor::Black);

        ms_ScreenMapHandle = CreateTexture(texOpts);
        ms_ScreenMapBackBuffer = CreateTexture(texOpts);
    }

    if (ms_ScreenMappingShader == nullptr)
    {
        ms_ScreenMappingShader = Shader::MakePtr();
        ms_ScreenMappingShader->LoadCodes(vertexShader, pixelShader);

        ShaderUniform param;
        param.Name = "scale";
        param.Type = ShaderUniform::Type::Float;
        param.Value.Float = k_ScreenMapRatio;

        ms_ScreenMappingShader->SetUniform(param);
    }

    m_EntityMap.fill(nullptr);
}

//----------------------------------------------------------------------------------------------------------------------
ScreenMappingSystem::~ScreenMappingSystem()
{
}

//----------------------------------------------------------------------------------------------------------------------
bool ScreenMappingSystem::IsStandingBy() const
{
    return !IsInState(State::Mapping);
}

//----------------------------------------------------------------------------------------------------------------------
void ScreenMappingSystem::OnStart()
{
    MouseClickEvent::Handlers += MouseClickEvent::Handler(this, &ScreenMappingSystem::OnMouseClickEvent);
    MouseMoveEvent::Handlers += MouseMoveEvent::Handler(this, &ScreenMappingSystem::OnMouseMoveEvent);
}

//----------------------------------------------------------------------------------------------------------------------
void ScreenMappingSystem::OnStop()
{
    MouseClickEvent::Handlers -= MouseClickEvent::Handler(this, &ScreenMappingSystem::OnMouseClickEvent);
    MouseMoveEvent::Handlers -= MouseMoveEvent::Handler(this, &ScreenMappingSystem::OnMouseMoveEvent);
}

//----------------------------------------------------------------------------------------------------------------------
void ScreenMappingSystem::OnFrameBegin()
{
    if (IsInState(State::Waiting))
    {
        if (m_DLReqTimer.IsElapsed())
        {
            m_DLReqTimer.Start(1000 / k_RefreshRateHz);
            SetState(State::Mapping);
        }
    }
    else if (IsInState(State::Mapping))
    {
        m_DLReq = DownloadRAMTexture(ms_ScreenMapHandle);
        std::swap(ms_ScreenMapHandle, ms_ScreenMapBackBuffer);
        SetState(State::Downloading);
    }
    else if (IsInState(State::Downloading))
    {
        if (m_DLReq.IsReady() && m_DLReq.GetResult().IsSuccessful())
        {
            m_ScreenMap = m_DLReq.GetReturnValue();
            SetState(State::Waiting);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
void ScreenMappingSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
    if (IsInState(State::Mapping))
    {
        const Vector3f& entitySize = _entity->GetSize();
        const Vector3f& entityPosition = _entity->GetPosition();
        ShaderMaterialRenderNode::Ptr& materialNode = _entity->GetComponent<ScreenMappableComponent>().MaterialNode();

        [[unlikely]]
        if (materialNode->GetShaderMaterial().Program == nullptr)
        {
            materialNode->ShaderMaterial().Program = ms_ScreenMappingShader;
        }

        materialNode->ShaderMaterial().Parameters["color"].Value.RGBA = {
            (u8) 0,
            (u8) ((m_CurrentColor >> 8) & 0xff),
            (u8) (m_CurrentColor        & 0xff)
        };

        PushToTexture(ms_ScreenMapHandle, materialNode, FloatBox{ entityPosition, entitySize });

        m_EntityMap[m_CurrentColor - 1] = _entity;
        m_CurrentColor = m_CurrentColor == UINT16_MAX ? 1 : m_CurrentColor + 1;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void ScreenMappingSystem::OnMouseClickEvent(const MouseClickEvent& _event)
{
    static Entity::CPtr clickedEntity = nullptr;

    FRG__RETURN_IF(m_ScreenMap == nullptr);

    if (_event.GetIsPressed())
    {
        u32 x = static_cast<u32>(_event.GetX() * k_ScreenMapRatio);
        u32 y = static_cast<u32>(_event.GetY() * k_ScreenMapRatio);

        FRG__RETURN_IF(   x >= m_ScreenMap->GetSize().x
                       || y >= m_ScreenMap->GetSize().y);

        Color color = m_ScreenMap->GetPixelColor({ x, y });
        u32 u32color = 0x00000000 | (((u32)color.r << 16) + ((u32)color.g << 8) + (u32)color.b);

        clickedEntity = u32color != 0x00000000 ? m_EntityMap[u32color - 1] : nullptr;
    }

    if (clickedEntity != nullptr)
    {
        EntityClickedEvent::Broadcast(_event.GetIsPressed(),
                                      _event.GetMouseButton() == Mouse::Button::Right,
                                      clickedEntity);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void ScreenMappingSystem::OnMouseMoveEvent(const MouseMoveEvent& _event)
{
    u32 x = static_cast<u32>(_event.GetX() * k_ScreenMapRatio);
    u32 y = static_cast<u32>(_event.GetY() * k_ScreenMapRatio);

    FRG__RETURN_IF(m_ScreenMap == nullptr);
    FRG__RETURN_IF(x >= m_ScreenMap->GetSize().x || y >= m_ScreenMap->GetSize().y);

    Color color = m_ScreenMap->GetPixelColor({ x, y });
    u32 u32color = 0x00000000 | (((u32)color.r << 16) + ((u32)color.g << 8) + (u32)color.b);

    const frg::Entity::Ptr& entity = u32color == 0x00000000 ? nullptr : m_EntityMap[u32color - 1];

    if (entity != m_HoveredEntity)
    {
        if (m_HoveredEntity != nullptr)
        {
            EntityHoveredExitEvent::Broadcast(m_HoveredEntity);
        }

        m_HoveredEntity = entity;

        if (m_HoveredEntity != nullptr)
        {
            EntityHoveredEnterEvent::Broadcast(m_HoveredEntity);
        }
    }
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
