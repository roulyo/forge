#include <forge/Project.h>
#include <forge/builtin/3Cs/CameraFollowSystem.h>

#include <cmath>

#include <forge/engine/camera/api/CameraAPI.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
static constexpr bool k_UseDynamics = true;

static constexpr f32 k_MaxSpeedUpS = 5.0f;
static constexpr f32 k_AccelerationDistance = 1.0f;

//----------------------------------------------------------------------------------------------------------------------
CameraFollowSystem::CameraFollowSystem()
    : m_Speed{ 0.0f, 0.0f, 0.0f }
{
    MouseWheelEvent::Handlers += MouseWheelEvent::Handler(this, &CameraFollowSystem::OnMouseWheelEvent);
    m_DefaultFOV = CameraAPI::GetFieldOfView() - CameraAPI::GetLookAt().z;
}

//----------------------------------------------------------------------------------------------------------------------
CameraFollowSystem::~CameraFollowSystem()
{
    MouseWheelEvent::Handlers -= MouseWheelEvent::Handler(this, &CameraFollowSystem::OnMouseWheelEvent);
}

//----------------------------------------------------------------------------------------------------------------------
void CameraFollowSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
    Entity& entity = *_entity.get();

    if (k_UseDynamics)
    {
        f32 dtSeconds = _dt * 0.000001f;

        Vector3f lookAt = CameraAPI::GetLookAt();
        Vector3f direction = { entity.GetPosition().x - lookAt.x,
                               entity.GetPosition().y - lookAt.y,
                               entity.GetPosition().z - lookAt.z };

        if (direction.x != 0.0f || direction.y != 0.0f)
        {
            m_Speed.x = std::min(std::abs(direction.x), k_AccelerationDistance) / k_AccelerationDistance * k_MaxSpeedUpS;
            m_Speed.y = std::min(std::abs(direction.y), k_AccelerationDistance) / k_AccelerationDistance * k_MaxSpeedUpS;

            f32 directionLength = sqrt(  pow(direction.x, 2.f)
                                       + pow(direction.y, 2.f));

            direction.x *= m_Speed.x * dtSeconds / directionLength;
            direction.y *= m_Speed.y * dtSeconds / directionLength;
        }

        if (direction.z != 0.0f)
        {
            m_Speed.z = std::min(std::abs(direction.z), k_AccelerationDistance) / k_AccelerationDistance * k_MaxSpeedUpS;
            direction.z *= m_Speed.z * dtSeconds / std::abs(direction.z);
        }

        CameraAPI::SetLookAt( { lookAt.x + direction.x,
                                lookAt.y + direction.y,
                                lookAt.z + direction.z });
        CameraAPI::SetFieldOfView(m_DefaultFOV + lookAt.z);
    }
    else
    {
        Vector3f pc = { entity.GetPosition().x + entity.GetSize().w / 2.0f,
                        entity.GetPosition().y + entity.GetSize().d / 2.0f,
                        entity.GetPosition().z + entity.GetSize().h / 2.0f };

        CameraAPI::SetLookAt(pc);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void CameraFollowSystem::OnMouseWheelEvent(const MouseWheelEvent& _event)
{
    f32 distance = CameraAPI::GetDistance();
    distance -= _event.GetDelta();
    CameraAPI::SetDistance(distance);
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
