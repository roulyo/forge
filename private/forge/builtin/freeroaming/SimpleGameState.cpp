#include <forge/Project.h>
#include <forge/builtin/freeroaming/SimpleGameState.h>

#include <forge/engine/game/Game.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
Vector<AbstractSystem*>          SimpleGameState::ms_SystemPlugins;
Vector<AbstractViewController*>  SimpleGameState::ms_ViewControllerPlugins;

//----------------------------------------------------------------------------------------------------------------------
void SimpleGameState::AddPluginSystem(AbstractSystem* _system)
{
    ms_SystemPlugins.push_back(_system);
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleGameState::AddPluginViewController(AbstractViewController* _vc)
{
    ms_ViewControllerPlugins.push_back(_vc);
}

//----------------------------------------------------------------------------------------------------------------------
SimpleGameState::SimpleGameState(const AbstractForgeGame& _game)
    : AbstractGameState(_game)
{
    AddSystem(&m_PlayerControlSystem);
    AddSystem(&m_PhysicsSystem);
    AddSystem(&m_RenderingSystem);

    for (AbstractSystem* system : ms_SystemPlugins)
    {
        AddSystem(system);
    }

    for (AbstractViewController* viewCtrl : ms_ViewControllerPlugins)
    {
        AddViewController(viewCtrl);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleGameState::OnStart()
{
    SetInputMapping(m_FreeRoamingInputScheme);
}

//----------------------------------------------------------------------------------------------------------------------
void SimpleGameState::OnStop()
{
    ResetInputMapping();
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
