#include <forge/Project.h>
#include <forge/builtin/freeroaming/FreeRoamingState.h>

#include <forge/engine/game/Game.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
Vector<AbstractSystem*>          FreeRoamingState::ms_SystemPlugins;
Vector<AbstractViewController*>  FreeRoamingState::ms_ViewControllerPlugins;

//----------------------------------------------------------------------------------------------------------------------
void FreeRoamingState::AddPluginSystem(AbstractSystem* _system)
{
    ms_SystemPlugins.push_back(_system);
}

//----------------------------------------------------------------------------------------------------------------------
void FreeRoamingState::AddPluginViewController(AbstractViewController* _vc)
{
    ms_ViewControllerPlugins.push_back(_vc);
}

//----------------------------------------------------------------------------------------------------------------------
FreeRoamingState::FreeRoamingState(const AbstractForgeGame& _game)
    : AbstractGameState(_game)
{
    AddSystem(&m_PlayerControlSystem);
    AddSystem(&m_PhysicsSystem);

    AddSystem(&m_CameraFollowSystem);
    AddSystem(&m_RenderingSystem);
    AddSystem(&m_ScreenMappingSystem);

    for (AbstractSystem* system : ms_SystemPlugins)
    {
        AddSystem(system);
    }

    for (AbstractViewController* viewCtrl : ms_ViewControllerPlugins)
    {
        AddViewController(viewCtrl);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void FreeRoamingState::OnStart()
{
    SetInputMapping(m_FreeRoamingInputScheme);
}

//----------------------------------------------------------------------------------------------------------------------
void FreeRoamingState::OnStop()
{
    ResetInputMapping();
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
