#include <forge/Project.h>
#include <forge/builtin/freeroaming/FreeRoamingInputs.h>

#include <forge/engine/input/InputTypes.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
FreeRoamingInputScheme::FreeRoamingInputScheme()
{
    RegisterInputBinder(Keyboard::Key::Z, &m_MoveUpBinder);
    RegisterInputBinder(Keyboard::Key::W, &m_MoveUpBinder);
    RegisterInputBinder(Keyboard::Key::Q, &m_MoveLeftBinder);
    RegisterInputBinder(Keyboard::Key::A, &m_MoveLeftBinder);
    RegisterInputBinder(Keyboard::Key::S, &m_MoveDownBinder);
    RegisterInputBinder(Keyboard::Key::D, &m_MoveRightBinder);

    RegisterInputBinder(Mouse::Button::Left, &m_LeftClickBinder);
    RegisterInputBinder(Mouse::Button::Right, &m_RightClickBinder);
    RegisterInputBinder(Mouse::Move::Move, &m_MouseMoveBinder);
    RegisterInputBinder(Mouse::Wheel::VerticalWheel, &m_CameraZoomBinder);
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
