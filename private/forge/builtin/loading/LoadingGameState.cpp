#include <forge/Project.h>
#include <forge/builtin/loading/LoadingGameState.h>

#include <forge/builtin/loading/events/LoadingEvents.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
Vector<AbstractSystem*>          LoadingGameState::ms_SystemPlugins;
Vector<AbstractViewController*>  LoadingGameState::ms_ViewControllerPlugins;

//----------------------------------------------------------------------------------------------------------------------
void LoadingGameState::AddPluginSystem(AbstractSystem* _system)
{
    ms_SystemPlugins.push_back(_system);
}

//----------------------------------------------------------------------------------------------------------------------
void LoadingGameState::AddPluginViewController(AbstractViewController* _vc)
{
    ms_ViewControllerPlugins.push_back(_vc);
}

//----------------------------------------------------------------------------------------------------------------------
LoadingGameState::LoadingGameState(const AbstractForgeGame& _game)
    : AbstractGameState(_game)
{
    AddSystem(&m_LoadingSystem);

    for (AbstractSystem* system : ms_SystemPlugins)
    {
        AddSystem(system);
    }

    for (AbstractViewController* viewCtrl : ms_ViewControllerPlugins)
    {
        AddViewController(viewCtrl);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void LoadingGameState::OnStart()
{
}

//----------------------------------------------------------------------------------------------------------------------
void LoadingGameState::OnStop()
{
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
