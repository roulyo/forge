#include <forge/Project.h>
#include <forge/builtin/loading/LoadingComponent.h>


FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_COMPONENT(LoadingComponent);

//----------------------------------------------------------------------------------------------------------------------
u32 LoadingComponent::AddTask(const BaseAsyncTask::Ptr& _task)
{
    if (!m_Locked)
    {
        m_Tasks.push_back(_task);
        return static_cast<u32>(m_Tasks.size() - 1);
    }

    return 0xffffffff;
}

//----------------------------------------------------------------------------------------------------------------------
bool LoadingComponent::HasTasks() const
{
    return m_Tasks.size() != 0;
}

//----------------------------------------------------------------------------------------------------------------------
BaseAsyncTask::Ptr LoadingComponent::GetNextTask()
{
    BaseAsyncTask::Ptr task = m_Tasks.front();

    m_Tasks.pop_front();

    return task;
}

//----------------------------------------------------------------------------------------------------------------------
void LoadingComponent::Lock()
{
    m_Locked = true;
}

//----------------------------------------------------------------------------------------------------------------------
void LoadingComponent::Unlock()
{
    m_Locked = false;
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
