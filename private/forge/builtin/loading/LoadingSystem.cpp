#include <forge/Project.h>
#include <forge/builtin/loading/LoadingSystem.h>

#include <forge/builtin/loading/events/LoadingEvents.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
void LoadingSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
    LoadingComponent& loadComp = _entity->GetComponent<LoadingComponent>();

    if (loadComp.HasTasks())
    {
        Load(loadComp);
    }

    HandleLoading(loadComp);
}

//----------------------------------------------------------------------------------------------------------------------
void LoadingSystem::Load(LoadingComponent& _loadComp)
{
    _loadComp.Lock();

    while (_loadComp.HasTasks())
    {
        m_Contexts.push_back(PushAsyncTask(_loadComp.GetNextTask()));
    }

}

//----------------------------------------------------------------------------------------------------------------------
void LoadingSystem::HandleLoading(LoadingComponent& _loadComp)
{
    if (IsLoadingComplete())
    {
        HandleLoadingCompleted(_loadComp);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void LoadingSystem::HandleLoadingCompleted(LoadingComponent& _loadComp)
{
    Vector<AsyncResult> results;

    for (const AsyncContext::Ptr& context : m_Contexts)
    {
        results.push_back(context->GetResult());
    }

    LoadingTasksDoneEvent::Broadcast(_loadComp.GetPostLoadingState(), results);
    _loadComp.Unlock();
}

//----------------------------------------------------------------------------------------------------------------------
bool LoadingSystem::IsLoadingComplete() const
{
    for (const AsyncContext::Ptr& context : m_Contexts)
    {
        if (context->GetState() != AsyncState::Complete)
        {
            return false;
        }
    }

    return true;
}


FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
