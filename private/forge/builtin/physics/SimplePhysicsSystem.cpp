#include <forge/Project.h>
#include <forge/builtin/physics/SimplePhysicsSystem.h>

#include <forge/engine/math/Types.h>
#include <forge/engine/math/spacetree/Quadtree.h>
#include <forge/engine/world/events/EntityEvents.h>

#include <forge/builtin/physics/events/SimplePhysicsEvents.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

namespace static_utils { //---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
static FloatQuad GetAABB(const Entity::CPtr& _entity)
{
    Vector2f position = _entity->GetPosition().xy();
    position.x -= _entity->GetSize().w * 0.5f;
    position.y -= _entity->GetSize().d * 0.5f;

    return { position, _entity->GetSize().xy() };
}

} // namespace static_utils

//----------------------------------------------------------------------------------------------------------------------
void SimplePhysicsSystem::OnStart()
{
    m_PhysicsWorld = new Quadtree<Entity::CPtr>({ { FLT_MIN * 0.5f, FLT_MIN * 0.5f }, { FLT_MAX, FLT_MAX } });

    FRG__MK_EVT_HANDLR_M(EntityAddedEvent, &SimplePhysicsSystem::OnEntityAdded, this);
    FRG__MK_EVT_HANDLR_M(EntityRemovedEvent, &SimplePhysicsSystem::OnEntityRemoved, this);
}

//----------------------------------------------------------------------------------------------------------------------
void SimplePhysicsSystem::OnStop()
{
    FRG__RM_EVT_HANDLR_M(EntityAddedEvent, &SimplePhysicsSystem::OnEntityAdded, this);
    FRG__RM_EVT_HANDLR_M(EntityRemovedEvent, &SimplePhysicsSystem::OnEntityRemoved, this);

    delete m_PhysicsWorld;
}

//----------------------------------------------------------------------------------------------------------------------
void SimplePhysicsSystem::Execute(const u64& _dt, const Entity::Ptr& _entity)
{
    const f32 dtSeconds = _dt * 0.000001f;
    Entity& entity = *_entity.get();

    PhysicableComponent& physicComp = entity.GetComponent<PhysicableComponent>();
    Vector2f deltaPos{ physicComp.Speed.x * dtSeconds, physicComp.Speed.y * dtSeconds };

    if (deltaPos != Vector2f::Zero)
    {
        Vector3f newPos = entity.GetPosition();
        newPos.x += deltaPos.x;
        newPos.y += deltaPos.y;

        if (physicComp.SuffersGravity)
        {
            newPos.z = GetGroundElevation(newPos.x, newPos.y);

            FRG__ASSERT(!std::isnan(newPos.z));
        }

        Vector<Entity::CPtr> collidedEntities = GetCollisions({ newPos.xy(), entity.GetSize().xy() });
        bool canMove = true;

        for (const Entity::CPtr& collided : collidedEntities)
        {
            FRG__CONTINUE_IF(collided == _entity);

            const PhysicableComponent& physicsComp = collided->GetComponent<PhysicableComponent>();

            SimplePhysicsCollisionEvent::Broadcast(_entity, collided);

            if (physicsComp.IsBlocking || (collided->GetSize().h > 0.0f && physicComp.Speed == Vector2f::Zero))
            {
                canMove = false;
            }
        }

        if (canMove)
        {
            entity.SetPosition(newPos);
            physicComp.IsDirty = true;
        }
    }

    if (physicComp.IsDirty)
    {
        FloatQuad aabb = static_utils::GetAABB(_entity);

        if (!m_PhysicsWorld->Update(_entity, aabb))
        {
            m_PhysicsWorld->Insert(_entity, aabb);
        }

        RequestUpdateEntity(_entity);
        physicComp.IsDirty = false;
    }
}

//----------------------------------------------------------------------------------------------------------------------
f32 SimplePhysicsSystem::GetGroundElevation(f32 _x, f32 _y) const
{
    Vector<RaycastResult<Entity::Ptr>> results;
    LineEquation ray = { { _x, _y, 0.0f },
                         { 0.0f, 0.0f, 1.0f } };

    if (GetAllRaycastCollisions(ray, results))
    {
        Entity::Ptr lowestElt = results[0].CollidedElement;

        for (i32 i = 1; i < results.size(); ++i)
        {
            const Entity::Ptr& cmpElt = results[i].CollidedElement;

            if (  cmpElt->GetPosition().z + cmpElt->GetSize().h
                < lowestElt->GetPosition().z + lowestElt->GetSize().h)
            {
                lowestElt = cmpElt;
            }
        }

        return lowestElt->GetPosition().z;
    }

    return NAN;
}

//----------------------------------------------------------------------------------------------------------------------
Vector<Entity::CPtr> SimplePhysicsSystem::GetCollisions(const FloatQuad& _aabb)
{
    Vector<Entity::CPtr> collidedEntities;

    m_PhysicsWorld->QueryRange(_aabb, collidedEntities);

    return collidedEntities;
}

//----------------------------------------------------------------------------------------------------------------------
void SimplePhysicsSystem::OnEntityAdded(const EntityAddedEvent& _event)
{
    const Entity::CPtr& entity = _event.GetEntity();

    if (entity->HasComponent<PhysicableComponent>())
    {
        m_PhysicsWorld->Insert(entity, static_utils::GetAABB(entity));
    }
}

//----------------------------------------------------------------------------------------------------------------------
void SimplePhysicsSystem::OnEntityRemoved(const EntityRemovedEvent& _event)
{
    const Entity::CPtr& entity = _event.GetEntity();

    if (entity->HasComponent<PhysicableComponent>())
    {
        m_PhysicsWorld->Remove(entity, static_utils::GetAABB(entity));
    }
}

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
