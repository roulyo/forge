#include <forge/Project.h>
#include <forge/builtin/physics/PhysicableComponent.h>


FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_COMPONENT(PhysicableComponent);

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
