#include <forge/Project.h>
#include <forge/engine/ecs/System.h>

#include <forge/engine/camera/CameraManager.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
System<>::System()
    : m_Signature(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
void System<>::Start()
{
    OnStart();
}

//----------------------------------------------------------------------------------------------------------------------
void System<>::Stop()
{
    OnStop();
}


//----------------------------------------------------------------------------------------------------------------------
void System<>::ExecuteForEach(const u64& _dt, const Vector<Entity::Ptr>& _entities)
{
    for (const Entity::Ptr& entity : _entities)
    {
        Entity* rawEntity = static_cast<Entity*>(entity.get());

        if (DoesTarget(rawEntity->GetSignature()))
        {
            Execute(_dt, entity);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
void System<>::ExecuteForEach(const u64& _dt, const EntityMapType<ComponentSignature, Vector<Entity::Ptr>>& _entityMap)
{
    for (const Pair<ComponentSignature, Vector<Entity::Ptr>>& mapEntry : _entityMap)
    {
        const ComponentSignature& signature = mapEntry.first;

        if (DoesTarget(signature))
        {
            const Vector<Entity::Ptr>& entities = mapEntry.second;

            for (const Entity::Ptr& entity : entities)
            {
                Execute(_dt, entity);
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
bool System<>::IsStandingBy() const
{
    return false;
}

//----------------------------------------------------------------------------------------------------------------------
void System<>::OnStart()
{
}

//----------------------------------------------------------------------------------------------------------------------
void System<>::OnFrameBegin()
{
}

//----------------------------------------------------------------------------------------------------------------------
void System<>::OnFrameEnd()
{
}

//----------------------------------------------------------------------------------------------------------------------
void System<>::OnStop()
{
}

//----------------------------------------------------------------------------------------------------------------------
bool System<>::DoesTarget(const ComponentSignature& _signature) const
{
    return (m_Signature & _signature) == m_Signature;
}

FRG__CLOSE_NAMESPACE(frg);
