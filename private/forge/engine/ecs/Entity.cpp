#include <forge/Project.h>
#include <forge/engine/ecs/Entity.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
u32 EntityRuntimeIdAuthority::m_Id  = 0;

//----------------------------------------------------------------------------------------------------------------------
u32 EntityRuntimeIdAuthority::GetNewRuntimeId()
{
    return m_Id++;
}

//----------------------------------------------------------------------------------------------------------------------
Entity::Entity()
    : m_DisplayName("J. Doe")
    , m_Signature(0)
    , m_RuntimeId(EntityRuntimeIdAuthority::GetNewRuntimeId())
{
}

//----------------------------------------------------------------------------------------------------------------------
AbstractComponent& Entity::GetComponentById(ComponentId _id) const
{
    Map<ComponentId, AbstractComponent*>::const_iterator it = m_Components.find(_id);

    FRG__ASSERT(it != m_Components.end());

    return *it->second;
}

//----------------------------------------------------------------------------------------------------------------------
void Entity::SetPosition(f32 _x, f32 _y, f32 _z)
{
    m_Position.x = _x;
    m_Position.y = _y;
    m_Position.z = _z;
}

//----------------------------------------------------------------------------------------------------------------------
void Entity::SetSize(f32 _width, f32 _depth, f32 _height)
{
    m_Size.w = _width;
    m_Size.d = _depth;
    m_Size.h = _height;
}

FRG__CLOSE_NAMESPACE(frg);
