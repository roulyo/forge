#include <forge/Project.h>
#include <forge/engine/data/DataManager.h>

FRG__OPEN_NAMESPACE(frg);
//----------------------------------------------------------------------------------------------------------------------
void DataManager::Update()
{
    for (const Pair<CatalogId, const AbstractDataCatalog*>& catalogEntry : m_Catalogs)
    {
        const_cast<AbstractDataCatalog*>(catalogEntry.second)->CleanStaticResources();
    }
}

//----------------------------------------------------------------------------------------------------------------------
void DataManager::RegisterCatalog(const CatalogId& _id, const AbstractDataCatalog* _catalog)
{
    FRG__ASSERT(m_Catalogs.find(_id) == m_Catalogs.end());

    m_Catalogs[_id] = _catalog;
}

//----------------------------------------------------------------------------------------------------------------------
const AbstractDataCatalog* DataManager::GetCatalog(const CatalogId& _id) const
{
    Map<CatalogId, const AbstractDataCatalog*>::const_iterator catalogIt = m_Catalogs.find(_id);

    if (catalogIt != m_Catalogs.end())
    {
        return catalogIt->second;
    }
    else
    {
        return nullptr;
    }
}

FRG__CLOSE_NAMESPACE(frg);
