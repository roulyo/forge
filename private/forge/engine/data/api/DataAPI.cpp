#include <forge/Project.h>
#include <forge/engine/data/api/DataAPI.h>

#include <forge/engine/data/DataManager.h>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
const AbstractDataCatalog* DataAPI::GetCatalog(const CatalogId& _catalogId)
{
    return DataManager::GetInstance().GetCatalog(_catalogId);
}

FRG__CLOSE_NAMESPACE(frg)
