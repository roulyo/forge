#include <forge/Project.h>
#include <forge/engine/data/DataCatalog.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void AbstractDataCatalog::RegisterData(const DataNameId& _dataId, const AbstractDataFactory& _factory)
{
    m_Factories[_dataId] = &_factory;
}

FRG__CLOSE_NAMESPACE(frg);
