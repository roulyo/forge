#pragma once

#include <forge/engine/data/DataCatalog.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractDataCatalog;

//----------------------------------------------------------------------------------------------------------------------
class DataManager : public Singleton<DataManager>
{
    FRG__DECL_SINGLETON(DataManager);

public:
    void Update();

    void RegisterCatalog(const CatalogId& _id, const AbstractDataCatalog* _catalog);
    const AbstractDataCatalog* GetCatalog(const CatalogId& _id) const;

private:
    Map<CatalogId, const AbstractDataCatalog*>  m_Catalogs;

};

FRG__CLOSE_NAMESPACE(frg);

//#include "DataManager.inl"
