#include <forge/Project.h>
#include <forge/engine/data/Data.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
const DataNameId& AbstractData::GetDataNameId() const
{
    return *m_NameId;
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractData::SetDataNameId(const DataNameId& _id)
{
   m_NameId = &_id;
}

FRG__CLOSE_NAMESPACE(frg);
