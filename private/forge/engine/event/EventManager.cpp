#include <forge/Project.h>
#include <forge/engine/event/EventManager.h>

#include <forge/engine/event/EventHandler.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void EventManager::Update()
{
    for (const AbstractEvent::Ptr event : m_Events)
    {
        const List<const AbstractEventHandler*>& handlers = m_HandlersMap[event->GetEventId()];

        for (const AbstractEventHandler* handler : handlers)
        {
            (*handler)(*event);
        }
    }

    m_Events.clear();
}

//----------------------------------------------------------------------------------------------------------------------
void EventManager::Push(const AbstractEvent::Ptr _event)
{
    m_Events.push_back(_event);
}

//----------------------------------------------------------------------------------------------------------------------
void EventManager::AddHandler(EventId _id, const AbstractEventHandler& _handler)
{
    List<const AbstractEventHandler*>& handlers = m_HandlersMap[_id];

#if defined(FRG_USE_DEBUG_INFO)
    List<const AbstractEventHandler*>::const_iterator it = handlers.cbegin();

    while (it != handlers.cend() && **it != _handler)
    {
        ++it;
    }

    FRG__ASSERT(it == handlers.cend());
#endif

    handlers.push_back(_handler.Clone());
}

//----------------------------------------------------------------------------------------------------------------------
void EventManager::RemoveHandler(EventId _id, const AbstractEventHandler& _handler)
{
    List<const AbstractEventHandler*>& handlers = m_HandlersMap[_id];
    List<const AbstractEventHandler*>::const_iterator it = handlers.cbegin();

    while (it != handlers.cend() && **it != _handler)
    {
        ++it;
    }

    FRG__ASSERT(it != handlers.cend());

    const AbstractEventHandler* foundElt = *it;

    handlers.erase(it);

    delete foundElt;
}

FRG__CLOSE_NAMESPACE(frg);
