#include <forge/Project.h>
#include <forge/engine/event/EventHandler.h>

#include <forge/engine/event/EventManager.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
// AbstractEventHandler
//----------------------------------------------------------------------------------------------------------------------
AbstractEventHandler::AbstractEventHandler()
    : m_Hash(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
AbstractEventHandler::~AbstractEventHandler()
{
}

//----------------------------------------------------------------------------------------------------------------------
bool AbstractEventHandler::operator==(const AbstractEventHandler& _h) const
{
    return _h.m_Hash == this->m_Hash;
}

//----------------------------------------------------------------------------------------------------------------------
bool AbstractEventHandler::operator!=(const AbstractEventHandler& _h) const
{
    return !(*this == _h);
}

//----------------------------------------------------------------------------------------------------------------------
AbstractEventHandler::AbstractEventHandler(uintptr_t _hash)
    : m_Hash(_hash)
{
}


//----------------------------------------------------------------------------------------------------------------------
// EventHandlersBroker
//----------------------------------------------------------------------------------------------------------------------
void EventHandlersBroker::RegisterHandler(EventId _id, const AbstractEventHandler& _handler) const
{
    EventManager::GetInstance().AddHandler(_id, _handler);
}

//----------------------------------------------------------------------------------------------------------------------
void EventHandlersBroker::UnregisterHandler(EventId _id, const AbstractEventHandler& _handler) const
{
    EventManager::GetInstance().RemoveHandler(_id, _handler);
}

FRG__CLOSE_NAMESPACE(frg);
