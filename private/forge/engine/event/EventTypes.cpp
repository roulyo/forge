#include <forge/Project.h>
#include <forge/engine/event/Event.h>

#include <forge/engine/event/EventManager.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void AbstractEvent::BroadcastRelay(AbstractEvent::Ptr _event)
{
    EventManager::GetInstance().Push(_event);
}

FRG__CLOSE_NAMESPACE(frg);
