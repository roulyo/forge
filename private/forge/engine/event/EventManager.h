#pragma once

#include <forge/engine/event/EventTypes.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractEventHandler;

//----------------------------------------------------------------------------------------------------------------------
class EventManager : public Singleton<EventManager>
{
    FRG__DECL_SINGLETON(EventManager);

    friend class EventHandlersBroker;

public:
    void Update();
    void Push(AbstractEvent::Ptr _event);

private:
    void AddHandler(EventId _id, const AbstractEventHandler& _handler);
    void RemoveHandler(EventId _id, const AbstractEventHandler& _handler);

private:
    Map<EventId, List<const AbstractEventHandler*>> m_HandlersMap;
    List<AbstractEvent::Ptr>                        m_Events;

};

FRG__CLOSE_NAMESPACE(frg);
