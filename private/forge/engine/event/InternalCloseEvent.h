#pragma once

#include <forge/engine/event/Event.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class InternalCloseEvent : public Event<InternalCloseEvent>
{
    FRG__DECL_EVENT(InternalCloseEvent);
};

FRG__CLOSE_NAMESPACE(frg);
