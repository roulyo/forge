#pragma once

#include <forge/engine/audio/playable/Music.h>
#include <forge/engine/audio/playable/Sound.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
typedef u32 AudioDesc;

//----------------------------------------------------------------------------------------------------------------------
class AudioManager : public Singleton<AudioManager>
{
    FRG__DECL_SINGLETON(AudioManager);

public:
    void InitInstance() override;

    void Update();

    AudioDesc PlayMusic(const Music::Ptr& _music, bool _repeat);
    void StopMusic(AudioDesc _musicDescriptor);
    void SetGlobalMusicVolume(f32 _volume);

    AudioDesc PlaySound(const Sound::Ptr& _sound, bool _repeat);
    void SetGlobalSoundVolume(f32 _volume);

private:
    void UpdateMusics();
    void UpdateSounds();

private:
    f32                 m_GlobalMusicVolume;
    f32                 m_GlobalSoundVolume;

    Vector<Music::Ptr>  m_Musics;
    Vector<Sound::Ptr>  m_Sounds;

};

FRG__CLOSE_NAMESPACE(frg);
