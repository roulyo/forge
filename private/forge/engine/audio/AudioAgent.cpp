#include <forge/Project.h>
#include <forge/engine/audio/AudioAgent.h>

#include <forge/engine/audio/AudioManager.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
AudioDesc AudioAgent::PlayMusic(const Music::Ptr& _music, bool _repeat /* = false */)
{
    return AudioManager::GetInstance().PlayMusic(_music, _repeat);
}

//----------------------------------------------------------------------------------------------------------------------
void AudioAgent::StopMusic(AudioDesc _musicDescriptor)
{
    AudioManager::GetInstance().StopMusic(_musicDescriptor);
}

//----------------------------------------------------------------------------------------------------------------------
void AudioAgent::SetGlobalMusicVolume(f32 _volume)
{
    AudioManager::GetInstance().SetGlobalMusicVolume(_volume);
}

//----------------------------------------------------------------------------------------------------------------------
AudioDesc AudioAgent::PlaySound(const Sound::Ptr& _sound, bool _repeat /* = false */)
{
    return AudioManager::GetInstance().PlaySound(_sound, _repeat);
}

//----------------------------------------------------------------------------------------------------------------------
void AudioAgent::SetGlobalSoundVolume(f32 _volume)
{
    AudioManager::GetInstance().SetGlobalSoundVolume(_volume);
}

FRG__CLOSE_NAMESPACE(frg);
