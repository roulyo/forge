#include <forge/Project.h>
#include <forge/engine/audio/resource/SoundBuffer.h>

#include <SFML/Audio/SoundBuffer.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
SoundBuffer::SoundBuffer()
{
}

//----------------------------------------------------------------------------------------------------------------------
SoundBuffer::SoundBuffer(const String& _bufferPath)
{
    SetFile(_bufferPath);
}

//----------------------------------------------------------------------------------------------------------------------
SoundBuffer::~SoundBuffer()
{
}

//----------------------------------------------------------------------------------------------------------------------
void SoundBuffer::SetFile(const String& _bufferPath)
{
    m_Impl->loadFromFile(_bufferPath);
}

FRG__CLOSE_NAMESPACE(frg);
