#include <forge/Project.h>
#include <forge/engine/audio/playable/Sound.h>

#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
Sound::Sound()
{
}

//----------------------------------------------------------------------------------------------------------------------
Sound::Sound(const SoundBuffer::Ptr& _soundBuffer)
    : m_BufferPtr(_soundBuffer)
{
    SetSoundBuffer(_soundBuffer);
}

//----------------------------------------------------------------------------------------------------------------------
Sound::~Sound()
{
    delete m_Impl;
}

//----------------------------------------------------------------------------------------------------------------------
void Sound::SetSoundBuffer(const SoundBuffer::Ptr& _soundBuffer)
{
    if (_soundBuffer != nullptr)
    {
        m_Impl->setBuffer(_soundBuffer->GetSFMLObject());
    }
}

//----------------------------------------------------------------------------------------------------------------------
void Sound::SetLoop(bool _loop) const
{
    m_Impl->setLoop(_loop);
}

//----------------------------------------------------------------------------------------------------------------------
void Sound::Play() const
{
    m_Impl->play();
}

//----------------------------------------------------------------------------------------------------------------------
f32 Sound::GetPitch() const
{
    return m_Impl->getPitch();
}

//----------------------------------------------------------------------------------------------------------------------
void Sound::SetPitch(f32 _p)
{
    m_Impl->setPitch(_p);
}

//----------------------------------------------------------------------------------------------------------------------
void Sound::SetVolume(f32 _volume)
{
    m_Impl->setVolume(_volume);
}

//----------------------------------------------------------------------------------------------------------------------
AudioStatus Sound::GetStatus() const
{
    switch (m_Impl->getStatus())
    {
    case sf::Sound::Stopped:
        return AudioStatus::Stopped;
    case sf::Sound::Playing:
        return AudioStatus::Playing;
    case sf::Sound::Paused:
        return AudioStatus::Paused;
    }

    return AudioStatus::Invalid;
}


FRG__CLOSE_NAMESPACE(frg);
