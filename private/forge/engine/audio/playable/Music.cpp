#include <forge/Project.h>
#include <forge/engine/audio/playable/Music.h>

#include <SFML/Audio/Music.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
Music::Music()
{
}

//----------------------------------------------------------------------------------------------------------------------
Music::Music(const String& _musicPath)
{
    m_Impl->openFromFile(_musicPath);
}

//----------------------------------------------------------------------------------------------------------------------
Music::~Music()
{
    delete m_Impl;
}

//----------------------------------------------------------------------------------------------------------------------
void Music::SetFile(const String& _musicPath)
{
    m_Impl->openFromFile(_musicPath);
}

//----------------------------------------------------------------------------------------------------------------------
void Music::SetLoop(bool _loop)
{
    m_Impl->setLoop(_loop);
}

//----------------------------------------------------------------------------------------------------------------------
void Music::SetVolume(f32 _volume)
{
    m_Impl->setVolume(_volume);
}

//----------------------------------------------------------------------------------------------------------------------
void Music::Play() const
{
    m_Impl->play();
}

//----------------------------------------------------------------------------------------------------------------------
void Music::Pause() const
{
    m_Impl->pause();
}

//----------------------------------------------------------------------------------------------------------------------
void Music::Stop() const
{
    m_Impl->stop();
}

//----------------------------------------------------------------------------------------------------------------------
AudioStatus Music::GetStatus() const
{
    switch (m_Impl->getStatus())
    {
    case sf::Music::Stopped:
        return AudioStatus::Stopped;
    case sf::Music::Playing:
        return AudioStatus::Playing;
    case sf::Music::Paused:
        return AudioStatus::Paused;
    }

    return AudioStatus::Invalid;
}

FRG__CLOSE_NAMESPACE(frg);
