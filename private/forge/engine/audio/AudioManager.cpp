#include <forge/Project.h>
#include <forge/engine/audio/AudioManager.h>

#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/Sound.hpp>

static constexpr bool k_NoAudio = false;

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void AudioManager::InitInstance()
{
    m_GlobalMusicVolume = 100.0f;
    m_GlobalSoundVolume = 100.0f;
}

//----------------------------------------------------------------------------------------------------------------------
void AudioManager::Update()
{
    UpdateSounds();
    UpdateMusics();
}

//----------------------------------------------------------------------------------------------------------------------
AudioDesc AudioManager::PlayMusic(const Music::Ptr& _music, bool _repeat)
{
    _music->SetLoop(_repeat);
    _music->SetVolume(m_GlobalMusicVolume);

    if (!k_NoAudio)
    {
        _music->Play();
    }

    m_Musics.push_back(_music);

    return static_cast<AudioDesc>(m_Musics.size() - 1);
}

//----------------------------------------------------------------------------------------------------------------------
void AudioManager::StopMusic(AudioDesc _musicDescriptor)
{
    if (!k_NoAudio)
    {
        m_Musics[_musicDescriptor]->Stop();
    }
}

//----------------------------------------------------------------------------------------------------------------------
void AudioManager::SetGlobalMusicVolume(f32 _volume)
{
    m_GlobalMusicVolume = _volume;

    for (Music::Ptr& music : m_Musics)
    {
        music->SetVolume(_volume);
    }
}

//----------------------------------------------------------------------------------------------------------------------
AudioDesc AudioManager::PlaySound(const Sound::Ptr& _sound, bool _repeat)
{
    _sound->SetLoop(_repeat);
    _sound->SetVolume(m_GlobalSoundVolume);

    if (!k_NoAudio)
    {
        _sound->Play();
    }

    m_Sounds.push_back(_sound);

    return static_cast<AudioDesc>(m_Sounds.size() - 1);
}

//----------------------------------------------------------------------------------------------------------------------
void AudioManager::SetGlobalSoundVolume(f32 _volume)
{
    m_GlobalSoundVolume = _volume;

    for (Sound::Ptr& sound : m_Sounds)
    {
        sound->SetVolume(_volume);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void AudioManager::UpdateSounds()
{
    Vector<Sound::Ptr>::iterator soundIt = m_Sounds.begin();

    while (soundIt != m_Sounds.end())
    {
        if ((*soundIt)->GetStatus() == AudioStatus::Stopped)
        {
            soundIt = m_Sounds.erase(soundIt);
        }
        else
        {
            ++soundIt;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
void AudioManager::UpdateMusics()
{
    Vector<Music::Ptr>::iterator musicIt = m_Musics.begin();

    while (musicIt != m_Musics.end())
    {
        if ((*musicIt)->GetStatus() == AudioStatus::Stopped)
        {
            musicIt = m_Musics.erase(musicIt);
        }
        else
        {
            ++musicIt;
        }
    }
}

FRG__CLOSE_NAMESPACE(frg);
