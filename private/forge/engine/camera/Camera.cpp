#include <forge/Project.h>
#include <forge/engine/camera/Camera.h>

#include <forge/engine/math/Utils.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
AbstractCamera::AbstractCamera(const Vector2f& _resolution)
    : m_FilmedWorld(nullptr)
    , m_Resolution(_resolution)
    , m_Height(10)
    , m_FoV(54)
{
    ComputeFrustum();
}

//----------------------------------------------------------------------------------------------------------------------
AbstractCamera::~AbstractCamera()
{
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractCamera::SetDistance(f32 _distance)
{
    m_Height = _distance;

    ComputeFrustum();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractCamera::SetFieldOfView(f32 _FoV)
{
    m_FoV = _FoV;

    ComputeFrustum();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractCamera::SetFocus(const Vector3f& _coordWorld)
{
    m_LookAt = _coordWorld;

    m_Frustum.Position().x = _coordWorld.x - (m_Frustum.GetSize().w * 0.5f);
    m_Frustum.Position().y = _coordWorld.y - (m_Frustum.GetSize().h * 0.5f);
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractCamera::ComputeFrustum()
{
    f32 theta = math_utils::Radian(90.0f - (m_FoV * 0.5f));
    f32 h = m_Height / sin(theta);

    m_Frustum.Size().x = h * 2.0f * cos(theta);
    m_Frustum.Size().y = m_Resolution.h / m_Resolution.w * m_Frustum.GetSize().x;

    m_Frustum.Position().x = m_LookAt.x - (m_Frustum.GetSize().w * 0.5f);
    m_Frustum.Position().y = m_LookAt.y - (m_Frustum.GetSize().h * 0.5f);

    m_PPU = (1.0f / (m_Height * 2.0f)) * m_Resolution.w;
}

FRG__CLOSE_NAMESPACE(frg);
