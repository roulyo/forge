#include <forge/Project.h>
#include <forge/engine/camera/api/CameraAPI.h>

#include <forge/engine/camera/CameraManager.h>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
void CameraAPI::SetFilmedWorld(const World& _world)
{
    CameraManager::GetInstance().GetMainCamera().SetFilmedWorld(&_world);
}

//----------------------------------------------------------------------------------------------------------------------
void CameraAPI::SetLookAt(const Vector3f& _coordWorld)
{
    CameraManager::GetInstance().GetMainCamera().SetFocus(_coordWorld);
}

//----------------------------------------------------------------------------------------------------------------------
Vector3f CameraAPI::GetLookAt()
{
    return CameraManager::GetInstance().GetMainCamera().GetLookAt();
}

//----------------------------------------------------------------------------------------------------------------------
void CameraAPI::SetDistance(f32 _distance)
{
    return CameraManager::GetInstance().GetMainCamera().SetDistance(_distance);
}

//----------------------------------------------------------------------------------------------------------------------
f32 CameraAPI::GetDistance()
{
    return CameraManager::GetInstance().GetMainCamera().GetHeight();
}

//----------------------------------------------------------------------------------------------------------------------
void CameraAPI::SetFieldOfView(f32 _fov)
{
    return CameraManager::GetInstance().GetMainCamera().SetFieldOfView(_fov);
}

//----------------------------------------------------------------------------------------------------------------------
f32 CameraAPI::GetFieldOfView()
{
    return CameraManager::GetInstance().GetMainCamera().GetFoV();
}

//----------------------------------------------------------------------------------------------------------------------
const Vector3f& CameraAPI::GetCameraPosition()
{
    return CameraManager::GetInstance().GetMainCamera().GetPosition();
}

//----------------------------------------------------------------------------------------------------------------------
f32 CameraAPI::GetPPU()
{
    return CameraManager::GetInstance().GetMainCamera().GetPPU();
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f CameraAPI::WorldPointToScreen(const Vector3f& _coordWorld)
{
    return CameraManager::GetInstance().GetMainCamera().WorldPointToScreen(_coordWorld);
}

//----------------------------------------------------------------------------------------------------------------------
Vector3f CameraAPI::ScreenPointToWorld(const Vector2f& _coordScreen)
{
    return CameraManager::GetInstance().GetMainCamera().ScreenPointToWorld(_coordScreen);
}

//----------------------------------------------------------------------------------------------------------------------
bool CameraAPI::IsDimetric()
{
    return CameraManager::GetInstance().GetMainCamera().GetCameraType() == CameraType::Dimetric;
}


FRG__CLOSE_NAMESPACE(frg)
