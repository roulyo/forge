#include <forge/Project.h>
#include <forge/engine/camera/DimetricCamera.h>

#include <forge/engine/math/Utils.h>
#include <forge/engine/world/World.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
static constexpr f32    k_IsometricAngle = 30.0f;
static const f32        k_LookAtLineZ = sqrt(2.0f) * tan(k_IsometricAngle * PI32 / 180.f);
static const f32        k_ZScale = sqrt(1.25f);
static const Vector3f   k_LookAtVector { 1.0f, 1.0f, k_LookAtLineZ * k_ZScale };


//----------------------------------------------------------------------------------------------------------------------
DimetricCamera::DimetricCamera(const Vector2f& _resolution)
    : AbstractCamera(_resolution)
{
    ComputeFrustum();
}

//----------------------------------------------------------------------------------------------------------------------
DimetricCamera::~DimetricCamera()
{
}

//----------------------------------------------------------------------------------------------------------------------
CameraType DimetricCamera::GetCameraType() const
{
    return CameraType::Dimetric;
}

//----------------------------------------------------------------------------------------------------------------------
void DimetricCamera::SetFocus(const Vector3f& _lookAt)
{
    AbstractCamera::SetFocus(_lookAt);

    ComputePosition();
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f DimetricCamera::WorldPointToScreen(const Vector3f& _coordWorld) const
{
    Vector2f isoCoord = FrustumToIso(WorldToFrustum(_coordWorld));

    isoCoord.x += m_ProjectionCenteringVector.x - m_FrustumCenteringVector.x;
    isoCoord.y += m_ProjectionCenteringVector.y - m_FrustumCenteringVector.y;

    return isoCoord;
}

//----------------------------------------------------------------------------------------------------------------------
Vector3f DimetricCamera::ScreenPointToWorld(const Vector2f& _coordRenderTarget) const
{
    FRG__ASSERT(m_FilmedWorld != nullptr);

    Vector2f uncenteredIsoCoord = { _coordRenderTarget.x - m_ProjectionCenteringVector.x,
                                    _coordRenderTarget.y };

    uncenteredIsoCoord.x += m_FrustumCenteringVector.x;
    uncenteredIsoCoord.y += m_FrustumCenteringVector.y;

    Vector3f raycastDest = FrustumToWorld(IsoToFrustumGround(uncenteredIsoCoord));
    Vector<RaycastResult<Entity::Ptr>> raycastResults;

    if (m_FilmedWorld->GetAllRaycastCollisions({ raycastDest, k_LookAtVector }, raycastResults))
    {
        RaycastResult<Entity::Ptr>& highestElt = raycastResults[0];

        for (i32 i = 1; i < raycastResults.size(); ++i)
        {
            const Entity::Ptr& cmpElt = raycastResults[i].CollidedElement;

            if (  cmpElt->GetPosition().z + cmpElt->GetSize().h
                > highestElt.CollidedElement->GetPosition().z + highestElt.CollidedElement->GetSize().h)
            {
                highestElt = raycastResults[i];
            }
        }

        return highestElt.CollisionPoint;
    }

    return raycastDest;
}

//----------------------------------------------------------------------------------------------------------------------
Vector3f DimetricCamera::WorldToFrustum(const Vector3f& _coordWorld) const
{
    return { _coordWorld.x - m_Frustum.GetPosition().x,
             _coordWorld.y - m_Frustum.GetPosition().y,
             _coordWorld.z - m_LookAt.z };
}

//----------------------------------------------------------------------------------------------------------------------
Vector3f DimetricCamera::FrustumToWorld(const Vector3f& _coordFrustum) const
{
    return { _coordFrustum.x + m_Frustum.GetPosition().x,
             _coordFrustum.y + m_Frustum.GetPosition().y,
             _coordFrustum.z + m_LookAt.z };
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f DimetricCamera::FrustumToIso(const Vector3f& _coordFrustum) const
{
    const Vector3f scaledCoord = { _coordFrustum.x * m_PPU,
                                   _coordFrustum.y * m_PPU,
                                   _coordFrustum.z * m_PPU * k_ZScale };

    return { scaledCoord.x - scaledCoord.y,
             ((scaledCoord.x + scaledCoord.y) * 0.5f) - scaledCoord.z };
}

//----------------------------------------------------------------------------------------------------------------------
Vector3f DimetricCamera::IsoToFrustumGround(const Vector2f& _coordIso) const
{
    const Vector2f unscaledCoord = { (0.5f * _coordIso.x + _coordIso.y),
                                     (-0.5f * _coordIso.x + _coordIso.y)};

    return { unscaledCoord.x / m_PPU,
             unscaledCoord.y / m_PPU,
             0.0f };
}

//----------------------------------------------------------------------------------------------------------------------
Vector3f DimetricCamera::IsoToFrustumLookAt(const Vector2f& _coordIso) const
{
    const Vector2f unscaledCoord = { (0.5f * _coordIso.x + _coordIso.y),
                                     (-0.5f * _coordIso.x + _coordIso.y)};

    return { unscaledCoord.x / m_PPU + m_LookAt.z,
             unscaledCoord.y / m_PPU + m_LookAt.z,
             m_LookAt.z };
}

//----------------------------------------------------------------------------------------------------------------------
void DimetricCamera::ComputePosition()
{

    // // V3/2 * V2/2 = 0.61234
    // f32 x = m_Height * 0.61234f + m_LookAt.x;
    // f32 y = m_Height * 0.61234f + m_LookAt.y;
    // f32 z = m_Height * 0.5f + m_LookAt.z;

    const f32 x = m_LookAt.x + m_Frustum.GetSize().w * 0.5f;
    const f32 y = m_LookAt.y + m_Frustum.GetSize().h * 0.5f;
    const f32 z = m_LookAt.z + m_Frustum.GetSize().w * 0.5f * k_LookAtLineZ;

    m_Position = { x, y, z };
}

//----------------------------------------------------------------------------------------------------------------------
void DimetricCamera::ComputeFrustum()
{
    f32 theta = math_utils::Radian(90.0f - (m_FoV * 0.5f));
    f32 h = m_Height / sin(theta);

    m_Frustum.Size().x = h * 2.0f * cos(theta);
    m_Frustum.Size().y = m_Frustum.GetSize().x;

    m_Frustum.Position().x = m_LookAt.x - (m_Frustum.GetSize().w * 0.5f);
    m_Frustum.Position().y = m_LookAt.y - (m_Frustum.GetSize().h * 0.5f);

    ComputePosition();

    m_PPU = (1.0f / (m_Height * 2.0f)) * m_Resolution.w;

    m_FrustumCenteringVector = { ((m_Frustum.GetSize().w * m_PPU) - m_Resolution.w) / 2.0f,
                                 ((m_Frustum.GetSize().h * m_PPU) - m_Resolution.h) / 2.0f };
    m_ProjectionCenteringVector = { (m_Frustum.GetSize().w * m_PPU) / 2.0f, 0.0f };
}

FRG__CLOSE_NAMESPACE(frg);
