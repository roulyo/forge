#pragma once

#include <forge/engine/camera/Camera.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class ElevationCamera : public AbstractCamera
{
public:
    ElevationCamera(const Vector2f& _resolution);
    ~ElevationCamera();

    void SetFocus(const Vector3f& _coordWorld) override;

    Vector2f WorldPointToScreen(const Vector3f& _coordWorld) const override;
    Vector3f ScreenPointToWorld(const Vector2f& _coordScreen) const override;

    CameraType GetCameraType() const override;
};

FRG__CLOSE_NAMESPACE(frg);
