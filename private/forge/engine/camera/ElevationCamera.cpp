#include <forge/Project.h>
#include <forge/engine/camera/ElevationCamera.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
ElevationCamera::ElevationCamera(const Vector2f& _resolution)
    : AbstractCamera(_resolution)
{
}


//----------------------------------------------------------------------------------------------------------------------
ElevationCamera::~ElevationCamera()
{
}

//----------------------------------------------------------------------------------------------------------------------
void ElevationCamera::SetFocus(const Vector3f& _lookAt)
{
    AbstractCamera::SetFocus(_lookAt);

    m_Position = m_LookAt;
    m_Position.z = m_Height;
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f ElevationCamera::WorldPointToScreen(const Vector3f& _coordWorld) const
{
    return { (_coordWorld.x - m_Frustum.GetPosition().x) * m_PPU,
             (_coordWorld.y - m_Frustum.GetPosition().y) * m_PPU };
}

//----------------------------------------------------------------------------------------------------------------------
Vector3f ElevationCamera::ScreenPointToWorld(const Vector2f& _coordRenderTarget) const
{
    return { _coordRenderTarget.x / m_PPU + m_Frustum.GetPosition().x,
             _coordRenderTarget.y / m_PPU + m_Frustum.GetPosition().y,
             0.0f };
}

//----------------------------------------------------------------------------------------------------------------------
CameraType ElevationCamera::GetCameraType() const
{
    return CameraType::Elevation;
}

FRG__CLOSE_NAMESPACE(frg);
