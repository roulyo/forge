#pragma once

#include <forge/engine/camera/Camera.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class DimetricCamera : public AbstractCamera
{
public:
    DimetricCamera(const Vector2f& _resolution);
    ~DimetricCamera();

    CameraType GetCameraType() const override;

    void SetFocus(const Vector3f& _coordWorld) override;

    Vector2f WorldPointToScreen(const Vector3f& _coordWorld) const override;
    Vector3f ScreenPointToWorld(const Vector2f& _coordRenderTarget) const override;

private:
    Vector3f WorldToFrustum(const Vector3f& _coordWorld) const;
    Vector3f FrustumToWorld(const Vector3f& _coordFrustum) const;

    Vector2f FrustumToIso(const Vector3f& _coordFrustum) const;
    Vector3f IsoToFrustumGround(const Vector2f& _coordIso) const;
    Vector3f IsoToFrustumLookAt(const Vector2f& _coordIso) const;

    void ComputePosition();
    void ComputeFrustum() override;

private:
    Vector2f m_FrustumCenteringVector;
    Vector2f m_ProjectionCenteringVector;

};

FRG__CLOSE_NAMESPACE(frg);
