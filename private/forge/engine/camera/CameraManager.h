#pragma once

#include <forge/engine/camera/Camera.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class CameraManager : public Singleton<CameraManager>
{
    FRG__DECL_SINGLETON(CameraManager);

public:
    void InitInstance() override;
    void DestroyInstance() override;

    void Update();

    AbstractCamera& CreateCamera(CameraType _type, const Vector2f& _resolution);
    void DeleteCamera();

    AbstractCamera& GetMainCamera();

private:
    AbstractCamera* m_Camera;

};

FRG__CLOSE_NAMESPACE(frg);
