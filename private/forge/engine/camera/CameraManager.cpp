#include <forge/Project.h>
#include <forge/engine/camera/CameraManager.h>

#include <forge/engine/camera/DimetricCamera.h>
#include <forge/engine/camera/ElevationCamera.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void CameraManager::InitInstance()
{
    m_Camera = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
void CameraManager::DestroyInstance()
{
    DeleteCamera();
}

//----------------------------------------------------------------------------------------------------------------------
void CameraManager::Update()
{
}

//----------------------------------------------------------------------------------------------------------------------
AbstractCamera& CameraManager::CreateCamera(CameraType _type, const Vector2f& _resolution)
{
    DeleteCamera();

    switch (_type)
    {
    case CameraType::Elevation:
        m_Camera = new ElevationCamera(_resolution);
        break;
    case CameraType::Dimetric:
        m_Camera = new DimetricCamera(_resolution);
        break;
    default:
        FRG__ASSERT(false);
        break;
    }

    return *m_Camera;
}

//----------------------------------------------------------------------------------------------------------------------
void CameraManager::DeleteCamera()
{
    if (m_Camera != nullptr)
    {
        delete m_Camera;
    }
}

//----------------------------------------------------------------------------------------------------------------------
AbstractCamera& CameraManager::GetMainCamera()
{
    return *m_Camera;
}

FRG__CLOSE_NAMESPACE(frg);
