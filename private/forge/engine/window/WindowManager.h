#pragma once

#include <forge/engine/window/Cursor.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class Event;
    class RenderWindow;
    class Window;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractDrawable;

//----------------------------------------------------------------------------------------------------------------------
class WindowManager : public Singleton<WindowManager>
{
    FRG__DECL_SINGLETON(WindowManager);

public:
    void InitInstance() override;

    void Update();

    const sf::Window& OpenWindow(const String& _name, const Vector2u& _res, bool _fullscreen = false);
    void ClearWindow();
    void RenderWindow();
    void CloseWindow();

    bool PollEvent(sf::Event& event);

    Vector2f GetCursorPosition() const;
    void SetCursor(const Cursor::CPtr& _cursor);

private:
    sf::RenderWindow*   m_Window;
    Cursor::CPtr        m_Cursor;

};

FRG__CLOSE_NAMESPACE(frg);
