#include <forge/Project.h>
#include <forge/engine/window/WindowManager.h>

#include <forge/engine/presentation/PresentationManager.h>
#include <forge/engine/rendering/RenderingManager.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Mouse.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void WindowManager::InitInstance()
{
    m_Window = nullptr;
    m_Cursor = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
void WindowManager::Update()
{
    ClearWindow();
}

//----------------------------------------------------------------------------------------------------------------------
const sf::Window& WindowManager::OpenWindow(const String& _name, const Vector2u& _res, bool _fullscreen /* = false */)
{
    String name = _name
#if defined(FRG_DEBUG)
        + " (DEBUG)"
#elif defined(FRG_DEBUG_OPTI)
        + " (O2 DEBUG)"
#endif
    ;

    u32 opts = _fullscreen ? sf::Style::Fullscreen : sf::Style::Default;
    sf::ContextSettings context;
    // context.antialiasingLevel = 2;
    // context.attributeFlags = sf::ContextSettings::Attribute::Core;

    m_Window = new sf::RenderWindow(sf::VideoMode(_res.w, _res.h),
                                    name,
                                    opts,
                                    context);

    m_Window->setVerticalSyncEnabled(false);
    m_Window->setKeyRepeatEnabled(false);

    PresentationManager::GetInstance().SetViewport(*m_Window);
    RenderingManager::GetInstance().SetViewport(*m_Window);

    return *m_Window;
}

//----------------------------------------------------------------------------------------------------------------------
void WindowManager::ClearWindow()
{
    m_Window->clear(sf::Color(255, 255, 255));
}

//----------------------------------------------------------------------------------------------------------------------
void WindowManager::RenderWindow()
{
    RenderingManager::GetInstance().RenderViewport();
    PresentationManager::GetInstance().RenderGUI();

    m_Window->display();
}

//----------------------------------------------------------------------------------------------------------------------
void WindowManager::CloseWindow()
{
    m_Window->close();

    delete m_Window;
}

//----------------------------------------------------------------------------------------------------------------------
bool WindowManager::PollEvent(sf::Event& _event)
{
    return m_Window->pollEvent(_event);
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f WindowManager::GetCursorPosition() const
{
    sf::Vector2i mousePos = sf::Mouse::getPosition(*m_Window);

    return { static_cast<f32>(mousePos.x), static_cast<f32>(mousePos.y) };
}

//----------------------------------------------------------------------------------------------------------------------
void WindowManager::SetCursor(const Cursor::CPtr& _cursor)
{
    m_Cursor = _cursor;

    if (!m_Cursor->IsLoaded())
    {
        m_Cursor->Load();
    }

    return m_Window->setMouseCursor(m_Cursor->GetSFMLObject());
}

FRG__CLOSE_NAMESPACE(frg);
