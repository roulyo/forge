#include <forge/Project.h>
#include <forge/engine/window/Cursor.h>

#include <SFML/Graphics/Image.hpp>
#include <SFML/Window/Cursor.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
const Cursor::CPtr Cursor::Arrow = Cursor::MakePtr(sf::Cursor::Arrow);
const Cursor::CPtr Cursor::Hand = Cursor::MakePtr(sf::Cursor::Hand);
const Cursor::CPtr Cursor::Cross = Cursor::MakePtr(sf::Cursor::Cross);

//----------------------------------------------------------------------------------------------------------------------
Cursor::Cursor()
    : m_Id(0xFFFFFFFF)
    , m_Pixels(nullptr)
    , m_Loaded(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
Cursor::Cursor(u32 _cursorId)
    : m_Id(_cursorId)
    , m_Pixels(nullptr)
    , m_Loaded(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
Cursor::~Cursor()
{
    delete m_Impl;
    delete[] m_Pixels;
}

//----------------------------------------------------------------------------------------------------------------------
void Cursor::SetFile(const String& _file)
{
    sf::Image img;
    img.loadFromFile(_file);
    delete[] m_Pixels;
    u32 pixelCount = img.getSize().x * img.getSize().y * 4;
    m_Pixels = new u8[pixelCount];
    std::memcpy(m_Pixels, img.getPixelsPtr(), pixelCount);
}

//----------------------------------------------------------------------------------------------------------------------
void Cursor::SetSize(u32 _x, u32 _y)
{
    m_Size = { _x, _y };
}

//----------------------------------------------------------------------------------------------------------------------
void Cursor::SetHotspot(u32 _x, u32 _y)
{
    m_Hotspot = { _x, _y };
}

//----------------------------------------------------------------------------------------------------------------------
void Cursor::Load() const
{
    if (m_Pixels != nullptr)
    {
        m_Impl->loadFromPixels(m_Pixels, { m_Size.x, m_Size.y }, { m_Hotspot.x, m_Hotspot.y });
    }
    else if (m_Id != 0xFFFFFFFF)
    {
         m_Impl->loadFromSystem(static_cast<sf::Cursor::Type>(m_Id));
    }
    else
    {
        FRG__LOG_ASSERT(false, "No cursor to load.");
    }

    m_Loaded = true;
}

//----------------------------------------------------------------------------------------------------------------------
bool Cursor::IsLoaded() const
{
    return m_Loaded;
}

FRG__CLOSE_NAMESPACE(frg);
