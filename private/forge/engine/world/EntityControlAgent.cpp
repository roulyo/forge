#include <forge/Project.h>
#include <forge/engine/world/EntityControlAgent.h>

#include <forge/engine/math/Types.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
World* EntityControlAgent::m_World = nullptr;

//----------------------------------------------------------------------------------------------------------------------
EntityControlAgent::EntityControlAgent()
{
    FRG__ASSERT(m_World);

    m_World->RegisterEntityControlAgent(*this);
}

//----------------------------------------------------------------------------------------------------------------------
EntityControlAgent::~EntityControlAgent()
{
    FRG__ASSERT(m_World);

    m_World->UnregisterEntityControlAgent(*this);
}

//----------------------------------------------------------------------------------------------------------------------
void EntityControlAgent::Clear()
{
    m_AddRequests.clear();
    m_RemoveRequests.clear();
    m_UpdateRequests.clear();
}

//----------------------------------------------------------------------------------------------------------------------
void EntityControlAgent::RequestAddEntity(const Entity::Ptr& _entity)
{
    m_AddRequests.push_back(_entity);
}

//----------------------------------------------------------------------------------------------------------------------
void EntityControlAgent::RequestUpdateEntity(const Entity::Ptr& _entity)
{
    m_UpdateRequests.push_back(_entity);
}

//----------------------------------------------------------------------------------------------------------------------
void EntityControlAgent::RequestRemoveEntity(const Entity::CPtr& _entity)
{
    m_RemoveRequests.push_back(_entity);
}

//----------------------------------------------------------------------------------------------------------------------
u32 EntityControlAgent::GetFirstRaycastCollision(const LineEquation& _ray, RaycastResult<Entity::Ptr>& _result) const
{
    return m_World->GetFirstRaycastCollision(_ray, _result);
}

//----------------------------------------------------------------------------------------------------------------------
u32 EntityControlAgent::GetAllRaycastCollisions(const LineEquation& _ray, Vector<RaycastResult<Entity::Ptr>>& _result) const
{
    return m_World->GetAllRaycastCollisions(_ray, _result);
}

//----------------------------------------------------------------------------------------------------------------------
void EntityControlAgent::SetActiveWorld(World* _world)
{
    m_World = _world;
}

//----------------------------------------------------------------------------------------------------------------------
void EntityControlAgent::Update()
{
    FRG__ASSERT(m_World);

    for (const Entity::CPtr& entity : m_RemoveRequests)
    {
        m_World->RemoveEntity(entity);
    }
    m_RemoveRequests.clear();

    for (const Entity::Ptr& entity : m_AddRequests)
    {
        m_World->AddEntity(entity);
    }
    m_AddRequests.clear();

    for (const Entity::Ptr& entity : m_UpdateRequests)
    {
        m_World->UpdateEntity(entity);
    }
    m_UpdateRequests.clear();
}

FRG__CLOSE_NAMESPACE(frg);
