#include <forge/Project.h>
#include <forge/engine/world/World.h>

#include <forge/engine/event/EventManager.h>
#include <forge/engine/math/Types.h>
#include <forge/engine/math/spacetree/Octree.h>
#include <forge/engine/world/EntityControlAgent.h>
#include <forge/engine/world/events/EntityEvents.h>

FRG__OPEN_NAMESPACE(frg);

namespace static_utils { //---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
static FloatBox GetAABB(const Entity::CPtr& _entity)
{
    Vector3f position = _entity->GetPosition();
    position.x -= _entity->GetSize().w * 0.5f;
    position.y -= _entity->GetSize().d * 0.5f;

    return { position, _entity->GetSize() };
}

} // namespace static_utils

//----------------------------------------------------------------------------------------------------------------------
static constexpr f32 k_MaximumHeight = 420.0f;

//----------------------------------------------------------------------------------------------------------------------
World::World()
    : m_Entities(nullptr)
    , m_Width(0.0f)
    , m_Depth(0.0f)
{
}

//----------------------------------------------------------------------------------------------------------------------
World::~World()
{
}

//----------------------------------------------------------------------------------------------------------------------
void World::Init(f32 _width, f32 _depth)
{
    m_Width = _width;
    m_Depth = _depth;

    m_Entities = new Octree<Entity::Ptr>(FloatBox(0.0f, 0.0f, 0.0f, m_Width, m_Depth, k_MaximumHeight));

    EntityControlAgent::SetActiveWorld(this);
}

//----------------------------------------------------------------------------------------------------------------------
void World::Fill(Octree<Entity::Ptr>* _spacetree)
{
    Destroy();
    m_Entities = _spacetree;
}

//----------------------------------------------------------------------------------------------------------------------
void World::Update()
{
    for (EntityControlAgent* eca : m_ECAs)
    {
        eca->Update();
    }
}

//----------------------------------------------------------------------------------------------------------------------
void World::Destroy()
{
    for (EntityControlAgent* eca : m_ECAs)
    {
        eca->Clear();
    }

    delete m_Entities;
    m_Entities = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
void World::RegisterEntityControlAgent(EntityControlAgent& _ecc)
{
    m_ECAs.push_back(&_ecc);
}

//----------------------------------------------------------------------------------------------------------------------
void World::UnregisterEntityControlAgent(EntityControlAgent& _ecc)
{
    m_ECAs.remove(&_ecc);
}

//----------------------------------------------------------------------------------------------------------------------
const Entity::Ptr& World::AddStaticEntity(const Entity::Ptr& _entity)
{
    m_StaticEntities.push_back(_entity);

    return _entity;
}

//----------------------------------------------------------------------------------------------------------------------
bool World::RemoveStaticEntity(const Entity::Ptr& _entity)
{
    auto elt = std::find(m_StaticEntities.cbegin(), m_StaticEntities.cend(), _entity);

    if (elt != m_StaticEntities.cend())
    {
        m_StaticEntities.erase(elt);

        return true;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------
const Entity::Ptr& World::AddEntity(const Entity::Ptr& _entity)
{
    FRG__ASSERT(m_Entities != nullptr);

    m_Entities->Insert(_entity, static_utils::GetAABB(_entity));

    EntityAddedEvent::Broadcast(_entity);

    return _entity;
}

//----------------------------------------------------------------------------------------------------------------------
bool World::UpdateEntity(const Entity::Ptr& _entity)
{
    FRG__ASSERT(m_Entities != nullptr);

    return m_Entities->Update(_entity, static_utils::GetAABB(_entity));
}

//----------------------------------------------------------------------------------------------------------------------
bool World::RemoveEntity(const Entity::CPtr& _entity)
{
    FRG__ASSERT(m_Entities != nullptr);

    if (m_Entities->Remove(std::const_pointer_cast<Entity>(_entity), static_utils::GetAABB(_entity)))
    {
        EntityRemovedEvent::Broadcast(_entity);

        return true;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------
u32 World::GetStaticEntities(AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const
{
    _result.AddRange(m_StaticEntities.begin(), m_StaticEntities.end());

    return static_cast<u32>(m_StaticEntities.size());
}

//----------------------------------------------------------------------------------------------------------------------
u32 World::GetAllEntities(AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const
{
    if (m_Entities == nullptr)
        return 0;

    return m_Entities->GetAllElements(_result);
}

//----------------------------------------------------------------------------------------------------------------------
u32 World::GetFirstRaycastCollision(const LineEquation& _ray, RaycastResult<Entity::Ptr>& _result) const
{
    if (m_Entities == nullptr)
        return 0;

    return m_Entities->GetFirstRaycastCollision(_ray, _result);
}

//----------------------------------------------------------------------------------------------------------------------
u32 World::GetAllRaycastCollisions(const LineEquation& _ray, Vector<RaycastResult<Entity::Ptr>>& _result) const
{
    if (m_Entities == nullptr)
        return 0;

    return m_Entities->GetAllRaycastCollisions(_ray, _result);
}

FRG__CLOSE_NAMESPACE(frg);
