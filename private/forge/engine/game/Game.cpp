
#include <forge/Project.h>
#include <forge/engine/game/Game.h>

#include <chrono>
#include <thread>

#include <SFML/Window/Window.hpp>

#include <forge/engine/async/AsyncTaskManager.h>
#include <forge/engine/audio/AudioManager.h>
#include <forge/engine/camera/CameraManager.h>
#include <forge/engine/data/DataManager.h>
#include <forge/engine/event/EventManager.h>
#include <forge/engine/event/InternalCloseEvent.h>
#include <forge/engine/game/GameState.h>
#include <forge/engine/input/InputManager.h>
#include <forge/engine/presentation/PresentationManager.h>
#include <forge/engine/rendering/RenderingManager.h>
#include <forge/engine/time/TimeManager.h>
#include <forge/engine/window/WindowManager.h>


FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
AbstractForgeGame::AbstractForgeGame()
    : m_World()
    , m_GameEntity(nullptr)
    , m_LastTick(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
AbstractForgeGame::~AbstractForgeGame()
{
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::Init(const ForgeGameParam& _param)
{
    m_Seed = static_cast<u32>(std::chrono::steady_clock::now().time_since_epoch().count());
    srand(m_Seed);

    EventManager::Instantiate();
    TimeManager::Instantiate();
    AudioManager::Instantiate();
    AsyncTaskManager::Instantiate();
    CameraManager::Instantiate();
    DataManager::Instantiate();
    InputManager::Instantiate();
    PresentationManager::Instantiate();
    RenderingManager::Instantiate();
    WindowManager::Instantiate();

    const sf::Window& window = WindowManager::GetInstance().OpenWindow(_param.GameName,
                                                                       _param.Resolution,
                                                                       _param.Fullsreen);

    Vector2f cameraRes;
    cameraRes.x = static_cast<f32>(_param.Resolution.x);
    cameraRes.y = static_cast<f32>(_param.Resolution.y);

    CameraManager::GetInstance().CreateCamera(_param.Camera, cameraRes);

    InternalCloseEvent::Handlers += InternalCloseEvent::Handler(this, &AbstractForgeGame::OnCloseEvent);

    ResetGameEntity();
    OnInit();

    m_LastTick = TimeManager::GetInstance().GetGameTimeUs();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::MainLoop()
{
    m_Running = true;

    while (m_Running)
    {
        TimeManager::GetInstance().Update();

        u64 now = TimeManager::GetInstance().GetGameTimeUs();
        m_DT = now - m_LastTick;
        m_LastTick = now;

        FRG__START_FRAME_PROFILING();

        BeginFrame(m_DT);
        PlayFrame(m_DT);
        EndFrame(m_DT);

        FRG__STOP_FAME_PROFILING();

        std::this_thread::yield();
    }
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::Quit()
{
    OnQuit();

    InternalCloseEvent::Handlers -= InternalCloseEvent::Handler(this, &AbstractForgeGame::OnCloseEvent);

    WindowManager::GetInstance().CloseWindow();

    WindowManager::Release();
    RenderingManager::Release();
    PresentationManager::Release();
    InputManager::Release();
    DataManager::Release();
    CameraManager::Release();
    AsyncTaskManager::Release();
    AudioManager::Release();
    TimeManager::Release();
    EventManager::Release();
}

//----------------------------------------------------------------------------------------------------------------------
u32 AbstractForgeGame::GetSeed() const
{
    return m_Seed;
}

//----------------------------------------------------------------------------------------------------------------------
const World& AbstractForgeGame::GetWorld() const
{
    return m_World;
}

//----------------------------------------------------------------------------------------------------------------------
const Entity::Ptr& AbstractForgeGame::GetGameEntity() const
{
    return m_GameEntity;
}


//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::BeginFrame(const u64& _dt)
{
    FRG__PROFILE_POINT(forge_BeginFrame);

    // TimeManager is updated in the MainLoop to compute dt
    InputManager::GetInstance().Update();
    EventManager::GetInstance().Update();
    AsyncTaskManager::GetInstance().Update();
    DataManager::GetInstance().Update();
    CameraManager::GetInstance().Update();
    AudioManager::GetInstance().Update();
    WindowManager::GetInstance().Update();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::PlayFrame(const u64& _dt)
{
    FRG__PROFILE_POINT(forge_PlayFrame);

    m_World.Update();
    HierarchicalAutomata::Tick();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::EndFrame(const u64& _dt)
{
    FRG__PROFILE_POINT(forge_EndFrame);

    WindowManager::GetInstance().RenderWindow();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::OnCloseEvent(const InternalCloseEvent& _event)
{
    Stop();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::Stop()
{
    m_Running = false;
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::ResetGameEntity()
{
    m_GameEntity = Entity::MakePtr();

    OnGameEntityCreated();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractForgeGame::RegisterCatalog(const CatalogId& _id, const AbstractDataCatalog* _catalog) const
{
    DataManager::GetInstance().RegisterCatalog(_id, _catalog);
}


FRG__CLOSE_NAMESPACE(frg);
