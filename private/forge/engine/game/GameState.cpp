#include <forge/Project.h>
#include <forge/engine/game/GameState.h>

#include <forge/engine/camera/CameraManager.h>
#include <forge/engine/ecs/System.h>
#include <forge/engine/game/Game.h>
#include <forge/engine/game/events/GameStateEvents.h>
#include <forge/engine/input/InputManager.h>
#include <forge/engine/presentation/ViewController.h>
#include <forge/engine/presentation/PresentationManager.h>

#if defined(FRG_USE_DEBUG_INFO)
#   include <forge/engine/world/events/EntityEvents.h>
#endif

FRG__OPEN_NAMESPACE(frg);


//----------------------------------------------------------------------------------------------------------------------
void EntityMapSQR::Add(const Entity::Ptr& _elt)
{
    m_EntityMap[_elt->GetSignature()].push_back(_elt);
}

//----------------------------------------------------------------------------------------------------------------------
void EntityMapSQR::ClearEntities()
{
    for (auto& pair : m_EntityMap)
    {
        pair.second.clear();
    }
}

//----------------------------------------------------------------------------------------------------------------------
const EntityMapType<ComponentSignature, Vector<Entity::Ptr>>& EntityMapSQR::GetEntityMap() const
{
    return m_EntityMap;
}

//----------------------------------------------------------------------------------------------------------------------
AbstractGameState::AbstractGameState(const AbstractForgeGame& _game)
    : m_Game(_game)
{
}

//----------------------------------------------------------------------------------------------------------------------
AbstractGameState::~AbstractGameState()
{
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::PlayFrame(const u64& _dt)
{
    u32 entityCount = FillEntityMap();

#if defined(FRG_USE_DEBUG_INFO)
    EntityDebugInfoEvent::Broadcast(static_cast<u32>(m_EntityMapSQR.GetEntityMap().size()),
                                    entityCount);
#endif

    for (AbstractSystem* system : m_Systems)
    {
        system->OnFrameBegin();
    }

    for (AbstractSystem* system : m_Systems)
    {
        if (!system->IsStandingBy())
        {
            system->ExecuteForEach(_dt, m_EntityMapSQR.GetEntityMap());
        }
    }

    PresentationManager::GetInstance().FillEntityDB(m_EntityMapSQR.GetEntityMap());

    for (AbstractViewController* viewController : m_ViewControllers)
    {
        viewController->Update();
    }

    for (AbstractSystem* system : m_Systems)
    {
        system->OnFrameEnd();
    }
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::Start()
{
    FillEntityMap();
    PresentationManager::GetInstance().FillEntityDB(m_EntityMapSQR.GetEntityMap());

    for (AbstractSystem* system : m_Systems)
    {
        system->Start();
    }

    for (AbstractViewController* viewController : m_ViewControllers)
    {
        viewController->Start();
    }

    GameStateStartedEvent::Broadcast(GetId());

    OnStart();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::Stop()
{
    for (AbstractSystem* system : m_Systems)
    {
        system->Stop();
    }

    for (AbstractViewController* viewController : m_ViewControllers)
    {
        viewController->Stop();
    }

    GameStateStoppedEvent::Broadcast(GetId());

    OnStop();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::OnStart()
{
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::OnStop()
{
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::AddSystem(AbstractSystem* _system)
{
    m_Systems.push_back(_system);
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::AddViewController(AbstractViewController* _viewController)
{
    m_ViewControllers.push_back(_viewController);
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::SetInputMapping(const InputMappingScheme& _inputMapping) const
{
    InputManager::GetInstance().SetInputMapping(&_inputMapping);
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGameState::ResetInputMapping() const
{
    InputManager::GetInstance().SetInputMapping(nullptr);
}

//----------------------------------------------------------------------------------------------------------------------
u32 AbstractGameState::FillEntityMap()
{
    u32 entityCount = 0;
    FloatQuad frustum = CameraManager::GetInstance().GetMainCamera().GetFrustum();

    m_EntityMapSQR.ClearEntities();

    if (CameraManager::GetInstance().GetMainCamera().GetCameraType() == CameraType::Dimetric)
    {
        static Vector<FloatManhattanCylinder> ranges(2);

        // We assume the frustum is square
        f32 radius = frustum.GetSize().w * 0.33f;
        f32 firstOffest = frustum.GetSize().w * 0.40f;
        f32 secondOffest = frustum.GetSize().w * 0.60f;

        ranges[0].SetBase(
            {
                frustum.GetPosition().x + firstOffest,
                frustum.GetPosition().y + firstOffest,
                0.0f
            });
        ranges[0].SetRadius(radius);

        ranges[1].SetBase(
            {
                frustum.GetPosition().x + secondOffest,
                frustum.GetPosition().y + secondOffest,
                0.0f
            });
        ranges[1].SetRadius(radius);

        entityCount += m_Game.GetWorld().GetEntitiesInRange(ranges, m_EntityMapSQR);
    }
    else
    {
        FloatBox aabb(frustum.GetPosition().x,
                      frustum.GetPosition().y,
                      0.0f,
                      frustum.GetSize().w,
                      frustum.GetSize().h,
                      420.0f);

        entityCount += m_Game.GetWorld().GetEntitiesInRange(aabb, m_EntityMapSQR);
    }

    entityCount += m_Game.GetWorld().GetStaticEntities(m_EntityMapSQR);

    m_EntityMapSQR.Add(m_Game.GetGameEntity());

    entityCount += 1;

    return entityCount;
}

FRG__CLOSE_NAMESPACE(frg);
