#include <forge/Project.h>
#include <forge/engine/async/AsyncContext.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
AsyncContext::AsyncContext()
    : m_State(AsyncState::Pending)
    , m_Result()
{

}

FRG__CLOSE_NAMESPACE(frg);
