#pragma once

#include <forge/engine/async/AsyncTask.h>
#include <forge/engine/async/AsyncContext.h>


FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class ThreadPool;

//----------------------------------------------------------------------------------------------------------------------
class AsyncTaskManager : public Singleton<AsyncTaskManager>
{
    FRG__DECL_SINGLETON(AsyncTaskManager);

public:
    void InitInstance() override;
    void DestroyInstance() override;

    void Update();

    AsyncContext::Ptr QueueTask(BaseAsyncTask::Ptr _task);

private:
    struct AsyncTaskHandler
    {
        BaseAsyncTask::Ptr  Task;
        AsyncContext::Ptr   Context;

        void StartTask() const;
        void UpdateTask() const;
        void StopTask() const;

    };


private:
    List<AsyncTaskHandler>  m_TaskHandlers;
    ThreadPool*             m_Workers;

};

FRG__CLOSE_NAMESPACE(frg);
