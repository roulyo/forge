#include <forge/Project.h>
#include <forge/engine/async/AsyncResult.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
AsyncResult::AsyncResult()
    : m_ReturnValue(nullptr)
    , m_Error()
{

}

//----------------------------------------------------------------------------------------------------------------------
bool AsyncResult::IsSuccessful() const
{
    return m_Error.value() == StatusCode::OK;
}

//----------------------------------------------------------------------------------------------------------------------
bool AsyncResult::WasCancelled() const
{
    return m_Error.value() == StatusCode::Cancelled;
}

//----------------------------------------------------------------------------------------------------------------------
void AsyncResult::SetErrorCode(u32 _errorCode)
{
    FRG__ASSERT(_errorCode != StatusCode::OK);

    m_Error.assign(_errorCode, std::generic_category());
}

FRG__CLOSE_NAMESPACE(frg);
