#include <forge/Project.h>
#include <forge/engine/async/ThreadPool.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
ThreadPool::ThreadPool(u32 _nbThread)
    : m_Shutdown(false)
{
    for (u32 i = 0; i < _nbThread; ++i)
    {
        m_Threads.push_back(std::thread(std::bind(&ThreadPool::ThreadUpdate, this)));
    }
}

//----------------------------------------------------------------------------------------------------------------------
ThreadPool::~ThreadPool()
{
    m_Shutdown.store(true);
    m_Sync.notify_all();

    for (std::thread& thread : m_Threads)
    {
        thread.join();
    }

    m_Threads.clear();
}

//----------------------------------------------------------------------------------------------------------------------
void ThreadPool::AddJob(const std::function<void()>& _job)
{
    {
        std::unique_lock<std::mutex> lock(m_JobMutex);
        m_Jobs.push(_job);
    }

    m_Sync.notify_one();
}

//----------------------------------------------------------------------------------------------------------------------
void ThreadPool::ThreadUpdate()
{
    while (true)
    {
        std::function<void()> job;

        {
            std::unique_lock<std::mutex> lock(m_JobMutex);

            m_Sync.wait(lock, [this]{ return !m_Jobs.empty() || m_Shutdown.load(); });

            // NOTE: This will not execute remaining jobs. Maybe more flexibility?
            FRG__RETURN_IF(m_Shutdown.load());

            job = m_Jobs.front();
            m_Jobs.pop();
        }

        job();
    }
}

FRG__CLOSE_NAMESPACE(frg);
