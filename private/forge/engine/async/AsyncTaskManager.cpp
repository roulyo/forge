#include <forge/Project.h>
#include <forge/engine/async/AsyncTaskManager.h>

#include <forge/engine/async/ThreadPool.h>

#include <functional>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void AsyncTaskManager::InitInstance()
{
    m_Workers = new ThreadPool(std::max(1, static_cast<i32>(std::thread::hardware_concurrency()) - 1));
}

//----------------------------------------------------------------------------------------------------------------------
void AsyncTaskManager::DestroyInstance()
{
    delete m_Workers;
    m_Workers = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
void AsyncTaskManager::Update()
{
    List<AsyncTaskHandler>::iterator it = m_TaskHandlers.begin();

    while (it != m_TaskHandlers.end())
    {
        const AsyncTaskHandler& handler = *it;
        const AsyncState& state = handler.Context->GetState();

        if (state == AsyncState::Pending)
        {
            handler.StartTask();
        }

        if (state == AsyncState::Running)
        {
            handler.UpdateTask();
        }

        if (state == AsyncState::Returning)
        {
            handler.StopTask();
            it = m_TaskHandlers.erase(it);
        }
        else
        {
            ++it;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
AsyncContext::Ptr AsyncTaskManager::QueueTask(BaseAsyncTask::Ptr _task)
{
    AsyncTaskHandler handler;

    handler.Context = AsyncContext::MakePtr();
    handler.Task = _task;

    m_TaskHandlers.push_back(handler);

    return handler.Context;
}

//----------------------------------------------------------------------------------------------------------------------
void AsyncTaskManager::AsyncTaskHandler::StartTask() const
{
    if (Task->GetTaskParam() != nullptr && Task->GetTaskParam()->UseWorkerThread)
    {
        AsyncTaskManager::GetInstance().m_Workers->AddJob(
            std::bind(&BaseAsyncTask::OnStart, Task)
        );
    }
    else
    {
        Task->OnStart();
    }

    Context->SetState(AsyncState::Running);
}

//----------------------------------------------------------------------------------------------------------------------
void AsyncTaskManager::AsyncTaskHandler::UpdateTask() const
{
    Task->OnUpdate();

    if (Task->IsDone())
    {
        Context->SetState(AsyncState::Returning);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void AsyncTaskManager::AsyncTaskHandler::StopTask() const
{
    Task->OnFinish();
    Context->SetResult(Task->GetResult());
    Context->SetState(AsyncState::Complete);
}

FRG__CLOSE_NAMESPACE(frg);
