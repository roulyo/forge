#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class ThreadPool
{
public:
    ThreadPool(u32 _nbThread);
    ~ThreadPool();

    void AddJob(const std::function<void()>& _job);

private:
    void ThreadUpdate();

private:
    Vector<std::thread>             m_Threads;
    Queue<std::function<void()>>    m_Jobs;

    std::condition_variable         m_Sync;
    std::mutex                      m_JobMutex;

    std::atomic_bool                m_Shutdown;

};

FRG__CLOSE_NAMESPACE(frg);
