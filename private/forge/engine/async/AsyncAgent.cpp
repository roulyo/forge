#include <forge/Project.h>
#include <forge/engine/async/AsyncAgent.h>

#include <forge/engine/async/AsyncTaskManager.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
AsyncContext::Ptr AsyncAgent::PushAsyncTask(const BaseAsyncTask::Ptr& _task) const
{
    return AsyncTaskManager::GetInstance().QueueTask(_task);
}

FRG__CLOSE_NAMESPACE(frg);
