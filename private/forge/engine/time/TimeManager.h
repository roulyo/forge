#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class PauseTimeRequestEvent;
class ResumeTimeRequestEvent;

//----------------------------------------------------------------------------------------------------------------------
class TimeManager : public Singleton<TimeManager>
{
    FRG__DECL_SINGLETON(TimeManager);

public:
    void InitInstance() override;
    void DestroyInstance() override;

    void Update();

private:
    void OnPauseTimeRequestEvent(const PauseTimeRequestEvent& _event);
    void OnResumeTimeRequestEvent(const ResumeTimeRequestEvent& _event);

private:
    FRG__CLASS_ATTR___(bool, IsPaused);
    FRG__CLASS_ATTR___(u64, LastUpdateNs);

    FRG__CLASS_ATTR_R_(u64, GameTimeNs);
    FRG__CLASS_ATTR_R_(u64, GameTimeUs);
    FRG__CLASS_ATTR_R_(u64, GameTimeMs);
    FRG__CLASS_ATTR_R_(u64, GameTimeS);

};

FRG__CLOSE_NAMESPACE(frg);
