#include <forge/Project.h>
#include <forge/engine/time/Chrono.h>

#include <forge/engine/time/api/TimeAPI.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
Chrono::Chrono()
    : m_DurationMs(0)
    , m_StartTimeMs(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
void Chrono::Start(u32 _durationMs)
{
    m_DurationMs = _durationMs;
    m_StartTimeMs = TimeAPI::GetGameTimeMilliseconds();
}

//----------------------------------------------------------------------------------------------------------------------
void Chrono::Stop()
{
    m_StartTimeMs = 0;
    m_DurationMs = 0;
}

//----------------------------------------------------------------------------------------------------------------------
bool Chrono::IsStarted() const
{
    return m_StartTimeMs != 0;
}

//----------------------------------------------------------------------------------------------------------------------
bool Chrono::IsElapsed() const
{
    return TimeAPI::GetGameTimeMilliseconds() > m_StartTimeMs + m_DurationMs;
}

//----------------------------------------------------------------------------------------------------------------------
u32 Chrono::GetElapsedTimeMs() const
{
    return static_cast<u32>(TimeAPI::GetGameTimeMilliseconds() - m_StartTimeMs);
}

//----------------------------------------------------------------------------------------------------------------------
u32 Chrono::GetRemainingTimeMs() const
{
    if ((m_StartTimeMs + m_DurationMs) < TimeAPI::GetGameTimeMilliseconds())
        return 0;

    return static_cast<u32>(  (m_StartTimeMs + m_DurationMs)
                            - TimeAPI::GetGameTimeMilliseconds());
}

//----------------------------------------------------------------------------------------------------------------------
f32 Chrono::GetElapsedRatio() const
{
    return static_cast<f32>(GetElapsedTimeMs()) / static_cast<f32>(m_DurationMs);
}

FRG__CLOSE_NAMESPACE(frg);