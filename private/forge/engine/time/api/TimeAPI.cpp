#include <forge/Project.h>
#include <forge/engine/time/api/TimeAPI.h>

#include <forge/engine/time/TimeManager.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
u64 TimeAPI::GetGameTimeNanoseconds()
{
    return TimeManager::GetInstance().GetGameTimeNs();
}

//----------------------------------------------------------------------------------------------------------------------
u64 TimeAPI::GetGameTimeMicroseconds()
{
    return TimeManager::GetInstance().GetGameTimeUs();
}

//----------------------------------------------------------------------------------------------------------------------
u64 TimeAPI::GetGameTimeMilliseconds()
{
    return TimeManager::GetInstance().GetGameTimeMs();
}

//----------------------------------------------------------------------------------------------------------------------
u64 TimeAPI::GetGameTimeSeconds()
{
    return TimeManager::GetInstance().GetGameTimeS();
}

FRG__CLOSE_NAMESPACE(frg);
