#include <forge/Project.h>
#include <forge/engine/time/TimeManager.h>

#include <forge/engine/debugging/Log.h>
#include <forge/engine/time/events/TimeEvents.h>

#include <chrono>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void TimeManager::InitInstance()
{
    FRG__MK_EVT_HANDLR_M(PauseTimeRequestEvent, &TimeManager::OnPauseTimeRequestEvent, this);
    FRG__MK_EVT_HANDLR_M(ResumeTimeRequestEvent, &TimeManager::OnResumeTimeRequestEvent, this);

    m_IsPaused = false;
    m_LastUpdateNs = std::chrono::steady_clock::now().time_since_epoch().count();
    m_GameTimeNs = 0;
    m_GameTimeUs = 0;
    m_GameTimeMs = 0;
    m_GameTimeS  = 0;
}

//----------------------------------------------------------------------------------------------------------------------
void TimeManager::DestroyInstance()
{
    FRG__RM_EVT_HANDLR_M(PauseTimeRequestEvent, &TimeManager::OnPauseTimeRequestEvent, this);
    FRG__RM_EVT_HANDLR_M(ResumeTimeRequestEvent, &TimeManager::OnResumeTimeRequestEvent, this);
}

//----------------------------------------------------------------------------------------------------------------------
void TimeManager::Update()
{
    FRG__RETURN_IF(m_IsPaused);

    u64 nowNs = std::chrono::steady_clock::now().time_since_epoch().count();
    u64 dtNs = nowNs - m_LastUpdateNs;

    m_LastUpdateNs = nowNs;

    // skip if > 1 sec
    // TODO: add some config for this value
    [[unlikely]]
    if (dtNs > 1'000'000'000)
    {
        FRG__LOG_WARNING(LogCategory("Forge.Time"), "Frame DT was abnormally high, skipping frame.");

        return;
    }

    m_GameTimeNs += dtNs;
    m_GameTimeUs = m_GameTimeNs * 0.001;
    m_GameTimeMs = m_GameTimeUs * 0.001;
    m_GameTimeS  = m_GameTimeMs * 0.001;

}


//----------------------------------------------------------------------------------------------------------------------
void TimeManager::OnPauseTimeRequestEvent(const PauseTimeRequestEvent& _event)
{
    m_IsPaused = true;
}

//----------------------------------------------------------------------------------------------------------------------
void TimeManager::OnResumeTimeRequestEvent(const ResumeTimeRequestEvent& _event)
{
    m_IsPaused = false;
    m_LastUpdateNs = std::chrono::steady_clock::now().time_since_epoch().count();
}

FRG__CLOSE_NAMESPACE(frg);
