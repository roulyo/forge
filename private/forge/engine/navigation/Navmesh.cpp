#include <forge/Project.h>
#include <forge/engine/navigation/Navmesh.h>

#include <cmath>
#include <algorithm>

#include <forge/engine/math/Utils.h>
#include <forge/engine/math/spacetree/SpacetreeQueryResult.h>
#include <forge/engine/world/World.h>

//------------------------------------------------------------------
namespace std
{
    template <> struct hash<frg::NavCell>
    {
        typedef frg::NavCell argument_type;
        typedef std::size_t result_type;

        std::size_t operator()(const frg::NavCell& id) const noexcept
        {
            return std::hash<frg::u32>()(id.x ^ (id.y << 4));
        }
    };
}

FRG__OPEN_NAMESPACE(frg);

//------------------------------------------------------------------
struct WeightedNavCell : public NavCell
{
    WeightedNavCell(const NavCell& _og, f32 _w)
        : NavCell(_og)
        , w(_w)
    {}

    // We want the lowest weight at the top of the priority queue
    bool operator<(const WeightedNavCell& _rhs) const
    {
        return this->w >= _rhs.w;
    }

    f32 w;

};

//------------------------------------------------------------------
Navmesh::Navmesh()
    : m_NavigationHook(nullptr)
    , m_AccessHook(nullptr)
    , m_RawMesh(nullptr)
    , m_CellSize(0.0f)
    , m_Width(0)
    , m_Height(0)
{
}

//------------------------------------------------------------------
Navmesh::~Navmesh()
{
    if (m_RawMesh != nullptr)
    {
        for (u32 i = 0; i < m_Height; ++i)
        {
            delete m_RawMesh[i];
        }

        delete m_RawMesh;
    }
}

//------------------------------------------------------------------
void Navmesh::Generate(const World* _world, f32 _cellSize)
{
    FRG__ASSERT(m_NavigationHook != nullptr);

    SetCellSize(_cellSize);
    SetWidth(static_cast<u32>(_world->GetWidth() / _cellSize));
    SetHeight(static_cast<u32>(_world->GetDepth() / _cellSize));

    SetRawMesh(new f32*[m_Height]);

    for (u32 i = 0; i < m_Height; ++i)
    {
        m_RawMesh[i] = new f32[m_Width];

        for (u32 j = 0; j < m_Width; ++j)
        {
            m_RawMesh[i][j] = NAN;
        }
    }

    VectorSQR<Entity::Ptr> entities;

    _world->GetAllEntities(entities);

    for (const Entity::Ptr& entity : entities.GetContainer())
    {
        if (!m_NavigationHook(entity->GetPosition()))
            continue;

        // FIX ME: need to check for full cell
        u32 navLeft = static_cast<u32>(entity->GetPosition().x / m_CellSize);
        u32 navTop = static_cast<u32>(entity->GetPosition().y / m_CellSize);
        u32 navWidth = static_cast<u32>(entity->GetSize().w / m_CellSize);
        u32 navDepth = static_cast<u32>(entity->GetSize().d / m_CellSize);

        for (u32 i = navTop; i < navTop + navDepth; ++i)
        {
            for (u32 j = navLeft; j < navLeft + navWidth; ++j)
            {
                m_RawMesh[j][i] = entity->GetPosition().z;
            }
        }
    }
}

//------------------------------------------------------------------
bool Navmesh::FindPath(const Vector2f& _start,
                       const Vector2f& _goal,
                       Vector<Vector2f>& _path,
                       const NavigationOptions& _options) const
{
    FRG__ASSERT(m_AccessHook != nullptr);
    FRG__ASSERT(m_RawMesh != nullptr);

    NavCell start = { static_cast<i32>(_start.x / m_CellSize), static_cast<i32>(_start.y / m_CellSize) };
    NavCell goal = { static_cast<i32>(_goal.x / m_CellSize), static_cast<i32>(_goal.y / m_CellSize) };

    _path.clear();

    FRG__RETURN_IF(goal.x < 0 || goal.x >= m_Width || goal.y < 0 || goal.y >= m_Height, false);
    FRG__RETURN_IF(math_utils::ManhattanDistance(start, goal) > _options.MaxStepCount, false);
    FRG__RETURN_IF(std::isnan(m_RawMesh[goal.x][goal.y]) && _options.StopAheadStepCount == 0, false);

    FRG__RETURN_IF(math_utils::ManhattanDistance(start, goal) <= _options.StopAheadStepCount, true);
    FRG__RETURN_IF(start == goal, true);

    Map<NavCell, NavCell> result;

    FRG__RETURN_IF(!AStar(start, goal, result, _options), false);

    NavCell current = goal;

    while (current != start)
    {
        _path.push_back({ static_cast<f32>(current.x) * m_CellSize, static_cast<f32>(current.y) * m_CellSize });
        current = result.at(current);
    }

    if (_path.size() > _options.MaxStepCount)
    {
        _path.clear();

        return false;
    }

    std::reverse(_path.begin(), _path.end());

    return true;
}

//------------------------------------------------------------------
bool Navmesh::AStar(const NavCell& _start,
                    NavCell& _goal,
                    Map<NavCell, NavCell>& _closed,
                    const NavigationOptions& _options) const
{
    std::priority_queue<WeightedNavCell>    open;
    Map<NavCell, f32>                       cost;

    open.emplace(_start, 0.0f);

    _closed[_start] = _start;
    cost[_start] = 0.0f;

    while (!open.empty())
    {
        WeightedNavCell current = open.top();
        open.pop();

        if (current != _goal && math_utils::ManhattanDistance(current, _goal) <= _options.StopAheadStepCount)
        {
            Vector3f currentPos = { current.x * m_CellSize,
                                    current.y * m_CellSize,
                                    m_RawMesh[current.x][current.y] };
            Vector3f goalPos = { _goal.x * m_CellSize,
                                 _goal.y * m_CellSize,
                                 m_RawMesh[_goal.x][_goal.y] };

            if (m_AccessHook(currentPos, goalPos))
            {
                _goal = current;
                break;
            }
        }

        for (NavCell next : Neighbors(current, _options))
        {
            f32 newCost = cost[current] + Distance(current, next);

            if (cost.find(next) == cost.end() || newCost < cost[next])
            {
                f32 priority = newCost + Heuristic(next, _goal);

                cost[next] = newCost;
                open.emplace(next, priority);
                _closed[next] = current;
            }
        }
    }

    return _closed.find(_goal) != _closed.end();
}

//------------------------------------------------------------------
Vector<NavCell> Navmesh::Neighbors(const NavCell& _node,
                                   const NavigationOptions& _options) const
{
    Vector<NavCell> neighbors;

    // Check center of the cell to avoid overlapping with neighboring cells
    Vector3f nodePos = { _node.x * m_CellSize /*+ (m_CellSize * 0.5f)*/,
                         _node.y * m_CellSize /*+ (m_CellSize * 0.5f)*/,
                         m_RawMesh[_node.x][_node.y] };

    for (i32 i = -1; i <= 1; ++i)
    {
        for (i32 j = -1; j <= 1; ++j)
        {
            if (!_options.CanMoveDiagonaly)
            {
                // Diagonals || Current node
                if (std::abs(i) == std::abs(j))
                    continue;
            }
            else
            {
                // Current node
                if (i == 0 && j == 0)
                    continue;
            }

            // Out of bound
            if (   _node.y + i < 0 || _node.y + i >= m_Height
                || _node.x + j < 0 || _node.x + j >= m_Width)
                continue;

            Vector3f neighborPos = { (_node.x + j) * m_CellSize /*+ (m_CellSize * 0.5f)*/,
                                     (_node.y + i) * m_CellSize /*+ (m_CellSize * 0.5f)*/,
                                     m_RawMesh[_node.x + j][_node.y + i] };

            // Out of lookup area
            if (!std::isnan(_options.LookupArea.Radius))
            {
                f32 distance = math_utils::Distance(neighborPos.xy(), _options.LookupArea.Center);

                if (distance > _options.LookupArea.Radius)
                    continue;
            }

            // Not navigable
            if (std::isnan(m_RawMesh[_node.x + j][_node.y + i]))
                continue;

            // Remove corners if edge is not navigable / accessible
            Vector3f edgePosV = { _node.x * m_CellSize /*+ (m_CellSize * 0.5f)*/,
                                  (_node.y + i) * m_CellSize /*+ (m_CellSize * 0.5f)*/,
                                  m_RawMesh[_node.x][_node.y + i] };
            Vector3f edgePosH = { (_node.x + j) * m_CellSize /*+ (m_CellSize * 0.5f)*/,
                                  _node.y * m_CellSize /*+ (m_CellSize * 0.5f)*/,
                                  m_RawMesh[_node.x + j][_node.y] };
            if (   std::isnan(m_RawMesh[_node.x][_node.y + i])
                || !m_AccessHook(nodePos, edgePosV)
                || std::isnan(m_RawMesh[_node.x + j][_node.y])
                || !m_AccessHook(nodePos, edgePosH))
                continue;

            // Access was denied by gameplay
            if (   !m_AccessHook(nodePos, neighborPos)
                || !(_options.IgnoreDynamicObstacle || m_FreeSpaceCheckHook(neighborPos)))
                continue;

            neighbors.push_back({ _node.x + j, _node.y + i });
        }
    }

    return neighbors;
}

//------------------------------------------------------------------
f32 Navmesh::Distance(const NavCell& _a, const NavCell& _b) const
{
    return math_utils::Distance(_a, _b);
}

//------------------------------------------------------------------
f32 Navmesh::Heuristic(const NavCell& _a, const NavCell& _b) const
{
    return static_cast<f32>(math_utils::ManhattanDistance(_a, _b));
}

FRG__CLOSE_NAMESPACE(frg);
