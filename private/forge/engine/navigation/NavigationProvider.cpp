#include <forge/Project.h>
#include <forge/engine/navigation/NavigationProvider.h>

#include <forge/engine/navigation/Navmesh.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
NavigationProvider::NavigationProvider()
    : m_Navmesh(new Navmesh())
{
}

//----------------------------------------------------------------------------------------------------------------------
NavigationProvider::~NavigationProvider()
{
    delete m_Navmesh;
}

//----------------------------------------------------------------------------------------------------------------------
void NavigationProvider::SetNavmeshHooks(const IsNavigableHook& _navHook,
                                         const IsAccessibleHook& _accessHook,
                                         const IsFreeHook& _freeSpaceCheckHook)
{
    m_Navmesh->SetNavigationHook(_navHook);
    m_Navmesh->SetAccessHook(_accessHook);
    m_Navmesh->SetFreeSpaceCheckHook(_freeSpaceCheckHook);
}

//----------------------------------------------------------------------------------------------------------------------
void NavigationProvider::GenerateNavmesh(const World* _world, f32 _cellSize)
{
    m_Navmesh->Generate(_world, _cellSize);
}

//----------------------------------------------------------------------------------------------------------------------
bool NavigationProvider::FindPath(const NavNode& _start,
                                  const NavNode& _goal,
                                  NavPath& _path,
                                  const NavigationOptions& _options) const
{
    return m_Navmesh->FindPath(_start, _goal, _path, _options);
}

FRG__CLOSE_NAMESPACE(frg);
