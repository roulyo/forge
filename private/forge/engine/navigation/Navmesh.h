#pragma once

#include <forge/engine/ecs/Entity.h>
#include <forge/engine/navigation/NavigationOptions.h>
#include <forge/engine/navigation/NavigationTypes.h>

FRG__OPEN_NAMESPACE(frg);

//------------------------------------------------------------------
class World;

//------------------------------------------------------------------
using NavCell = Vector2i;

//------------------------------------------------------------------
class Navmesh
{
public:
    Navmesh();
    ~Navmesh();

    void Generate(const World* _world, f32 _cellSize);
    bool FindPath(const Vector2f& _start,
                  const Vector2f& _goal,
                  Vector<Vector2f>& _path,
                  const NavigationOptions& _options) const;

private:
    bool AStar(const NavCell& _start,
               NavCell& _goal,
               Map<NavCell, NavCell>& _closed,
               const NavigationOptions& _options) const;

    Vector<NavCell> Neighbors(const NavCell& _node,
                              const NavigationOptions& _options) const;
    f32 Distance(const NavCell& _a, const NavCell& _b) const;
    f32 Heuristic(const NavCell& _a, const NavCell& _b) const;

private:
    FRG__CLASS_ATTR__W(IsAccessibleHook, AccessHook);
    FRG__CLASS_ATTR__W(IsNavigableHook, NavigationHook);
    FRG__CLASS_ATTR__W(IsFreeHook, FreeSpaceCheckHook);

    FRG__CLASS_ATTR___(f32**, RawMesh);
    FRG__CLASS_ATTR___(f32, CellSize);
    FRG__CLASS_ATTR___(u32, Width);
    FRG__CLASS_ATTR___(u32, Height);

};

FRG__CLOSE_NAMESPACE(frg);
