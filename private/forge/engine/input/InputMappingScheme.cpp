#include <forge/Project.h>
#include <forge/engine/input/InputMappingScheme.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
InputMappingScheme::InputMappingScheme()
{
    memset(m_InputBinders, 0, Input::Count * sizeof (BaseInputBinder*));
}

//----------------------------------------------------------------------------------------------------------------------
void InputMappingScheme::RegisterInputBinder(Keyboard::Key _key, const BaseInputBinder* _binder)
{
    RegisterInputBinder(static_cast<u32>(_key), _binder);
}

//----------------------------------------------------------------------------------------------------------------------
void InputMappingScheme::RegisterInputBinder(Mouse::Button _mouseButton, const BaseInputBinder* _binder)
{
    RegisterInputBinder(static_cast<u32>(_mouseButton), _binder);
}

//----------------------------------------------------------------------------------------------------------------------
void InputMappingScheme::RegisterInputBinder(Mouse::Wheel _mouseWheel, const BaseInputBinder* _binder)
{
    RegisterInputBinder(static_cast<u32>(_mouseWheel), _binder);
}

//----------------------------------------------------------------------------------------------------------------------
void InputMappingScheme::RegisterInputBinder(Mouse::Move _mouseMove, const BaseInputBinder* _binder)
{
    RegisterInputBinder(static_cast<u32>(_mouseMove), _binder);
}

//----------------------------------------------------------------------------------------------------------------------
void InputMappingScheme::RegisterInputBinder(u32 _inputIndex, const BaseInputBinder* _binder)
{
    m_InputBinders[_inputIndex] = _binder;
}

//----------------------------------------------------------------------------------------------------------------------
const BaseInputBinder* InputMappingScheme::GetInputBinder(u32 _inputIndex) const
{
    return m_InputBinders[_inputIndex];
}

FRG__CLOSE_NAMESPACE(frg);
