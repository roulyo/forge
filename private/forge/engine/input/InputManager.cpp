#include <forge/Project.h>
#include <forge/engine/input/InputManager.h>

#include <forge/engine/window/WindowManager.h>
#include <forge/engine/event/EventManager.h>
#include <forge/engine/event/SystemEvent.h>
#include <forge/engine/event/InternalCloseEvent.h>
#include <forge/engine/input/InputBinder.h>
#include <forge/engine/input/InputMappingScheme.h>
#include <forge/engine/presentation/PresentationManager.h>

#include <SFML/Window/Event.hpp>

FRG__OPEN_NAMESPACE(frg);

//------------------------------------------------------------------------
namespace static_utils
{
//------------------------------------------------------------------------
    u32 GetKeyIndex(sf::Keyboard::Key _sfKey)
    {
        return _sfKey;
    }

//------------------------------------------------------------------------
    u32 GetMouseButtonIndex(sf::Mouse::Button _sfMouseButton)
    {
        return _sfMouseButton + static_cast<u32>(Keyboard::Key::Count);
    }

//------------------------------------------------------------------------
    u32 GetMouseScrollIndex(sf::Mouse::Wheel _sfMouseWheel)
    {
        return _sfMouseWheel + static_cast<u32>(Mouse::Button::Count);
    }

//------------------------------------------------------------------------
    u32 GetMouseMoveIndex()
    {
        return static_cast<u32>(Mouse::Move::Move);
    }

}

//------------------------------------------------------------------------
union PolyEvent
{
    sf::Event sfml;
    SystemEvent forge;
};

//------------------------------------------------------------------------
void InputManager::InitInstance()
{
    m_InputMapping = nullptr;
}

//------------------------------------------------------------------------
void InputManager::SetInputMapping(const InputMappingScheme* _inputMapping)
{
    m_InputMapping = _inputMapping;
}

//------------------------------------------------------------------------
void InputManager::Update()
{
    PolyEvent polyEvent;

    while (WindowManager::GetInstance().PollEvent(polyEvent.sfml))
    {
        const sf::Event& event = polyEvent.sfml;

        FRG__CONTINUE_IF(   event.type != sf::Event::Closed
                         && event.key.code == sf::Keyboard::Unknown);
        FRG__CONTINUE_IF(PresentationManager::GetInstance().HandleSystemEvent(polyEvent.forge));

        switch (event.type)
        {
        case sf::Event::Closed:
            OnCloseEvent();
            break;

        case sf::Event::KeyPressed:
            OnKeyPress(static_utils::GetKeyIndex(event.key.code));
            break;

        case sf::Event::KeyReleased:
            OnKeyRelease(static_utils::GetKeyIndex(event.key.code));
            break;

        case sf::Event::MouseButtonPressed:
            OnMouseButtonPress(static_utils::GetMouseButtonIndex(event.mouseButton.button),
                               event.mouseButton.x,
                               event.mouseButton.y);
            break;

        case sf::Event::MouseButtonReleased:
            OnMouseButtonRelease(static_utils::GetMouseButtonIndex(event.mouseButton.button),
                                 event.mouseButton.x,
                                 event.mouseButton.y);
            break;

        case sf::Event::MouseWheelScrolled:
            OnMouseScroll(static_utils::GetMouseScrollIndex(event.mouseWheelScroll.wheel),
                          event.mouseWheelScroll.x,
                          event.mouseWheelScroll.y,
                          event.mouseWheelScroll.delta);
            break;

        case sf::Event::MouseMoved:
            OnMouseMove(event.mouseMove.x,
                        event.mouseMove.y);
            break;

        default:
            break;
        }
    }
}

//------------------------------------------------------------------------
void InputManager::OnCloseEvent() const
{
    InternalCloseEvent::Broadcast();
}

//------------------------------------------------------------------------
void InputManager::OnKeyPress(u32 _inputIndex) const
{
    [[unlikely]]
    FRG__RETURN_IF (_inputIndex == sf::Keyboard::F4 && sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt), OnCloseEvent());

    if (m_InputMapping != nullptr)
    {
        const BaseInputBinder* binder = m_InputMapping->GetInputBinder(_inputIndex);

        if (binder)
        {
            binder->OnKeyPress();
        }
    }
}

//------------------------------------------------------------------------
void InputManager::OnKeyRelease(u32 _inputIndex) const
{
    if (m_InputMapping != nullptr)
    {
        const BaseInputBinder* binder = m_InputMapping->GetInputBinder(_inputIndex);

        if (binder != nullptr)
        {
            binder->OnKeyRelease();
        }
    }
}

//------------------------------------------------------------------------
void InputManager::OnMouseButtonPress(u32 _inputIndex, i32 _x, i32 _y) const
{
    if (m_InputMapping != nullptr)
    {
        const BaseInputBinder* binder = m_InputMapping->GetInputBinder(_inputIndex);

        if (binder != nullptr)
        {
            binder->OnMouseButtonPress(_x, _y);
        }
    }
}

//------------------------------------------------------------------------
void InputManager::OnMouseButtonRelease(u32 _inputIndex, i32 _x, i32 _y) const
{
    if (m_InputMapping != nullptr)
    {
        const BaseInputBinder* binder = m_InputMapping->GetInputBinder(_inputIndex);

        if (binder != nullptr)
        {
            binder->OnMouseButtonRelease(_x, _y);
        }
    }
}

//------------------------------------------------------------------------
void InputManager::OnMouseScroll(u32 _inputIndex, i32 _x, i32 _y, f32 _delta) const
{
    if (m_InputMapping != nullptr)
    {
        const BaseInputBinder* binder = m_InputMapping->GetInputBinder(_inputIndex);

        if (binder != nullptr)
        {
            binder->OnMouseScroll(_x, _y, _delta);
        }
    }
}

//------------------------------------------------------------------------
void InputManager::OnMouseMove(i32 _x, i32 _y) const
{
    if (m_InputMapping != nullptr)
    {
        const BaseInputBinder* binder = m_InputMapping->GetInputBinder(static_utils::GetMouseMoveIndex());

        if (binder != nullptr)
        {
            binder->OnMouseMove(_x, _y);
        }
    }
}

FRG__CLOSE_NAMESPACE(frg);
