#include <forge/Project.h>
#include <forge/engine/input/InputBinder.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void BaseInputBinder::OnKeyPress() const
{
    FRG__ASSERT(false);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseInputBinder::OnKeyRelease() const
{
    FRG__ASSERT(false);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseInputBinder::OnMouseButtonPress(i32 _x, i32 _y) const
{
    FRG__ASSERT(false);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseInputBinder::OnMouseButtonRelease(i32 _x, i32 _y) const
{
    FRG__ASSERT(false);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseInputBinder::OnMouseScroll(i32 _x, i32 _y, f32 _delta) const
{
    FRG__ASSERT(false);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseInputBinder::OnMouseMove(i32 _x, i32 _y) const
{
    FRG__ASSERT(false);
}

FRG__CLOSE_NAMESPACE(frg);
