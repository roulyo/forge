#pragma once

#include <forge/engine/input/InputTypes.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class InputMappingScheme;

//----------------------------------------------------------------------------------------------------------------------
class InputManager : public Singleton<InputManager>
{
    FRG__DECL_SINGLETON(InputManager);

public:
    void InitInstance() override;

    void SetInputMapping(const InputMappingScheme* _inputMapping);
    void Update();

private:
    void OnCloseEvent() const;

    void OnKeyPress(u32 _inputIndex) const;
    void OnKeyRelease(u32 _inputIndex) const;

    void OnMouseButtonPress(u32 _inputIndex, i32 _x, i32 _y) const;
    void OnMouseButtonRelease(u32 _inputIndex, i32 _x, i32 _y) const;
    void OnMouseScroll(u32 _inputIndex, i32 _x, i32 _y, f32 _delta) const;
    void OnMouseMove(i32 _x, i32 _y) const;

private:
    const InputMappingScheme*   m_InputMapping;

};

FRG__CLOSE_NAMESPACE(frg);
