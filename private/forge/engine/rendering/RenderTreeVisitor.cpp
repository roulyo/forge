#include <forge/Project.h>
#include <forge/engine/rendering/RenderTreeVisitor.h>

#include <algorithm>

#include <SFML/Graphics/Sprite.hpp>

#include <forge/engine/camera/api/CameraAPI.h>
#include <forge/engine/time/api/TimeAPI.h>

#include <iostream>
#include <random>

FRG__OPEN_NAMESPACE(frg);

namespace static_utils { //---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
constexpr f32 k_roundingStep = 0.33f;

//----------------------------------------------------------------------------------------------------------------------
static f64 ComputeDistanceScore(const FloatBox& _spatialData, const Vector3f& _cam)
{
    const Vector3f& position = _spatialData.GetPosition();
    const Vector3f& size = _spatialData.GetSize();

    Vector3f p { std::round(position.x + k_roundingStep),
                 std::round(position.y + k_roundingStep),
                 position.z + size.h };

    if (frg::CameraAPI::IsDimetric())
    {
        return (_cam.x - p.x) + (_cam.y - p.y) + (_cam.z - p.z) * 0.5;
    }
    else
    {
        return std::abs(p.z - _cam.z);
    }
}

//----------------------------------------------------------------------------------------------------------------------
static FloatQuad GetTransformRect(const sf::Transform& _transform)
{
    FRG__RETURN_IF(_transform == sf::Transform::Identity, { { 0.0f, 0.0f }, { 1.0f, 1.0f } });

    const sf::FloatRect transformRect = _transform.transformRect({ 0.0f, 0.0f, 1.0f, 1.0f });

    return { { transformRect.getPosition().x,   transformRect.getPosition().y },
             { transformRect.getSize().x,       transformRect.getSize().y } };
}

//----------------------------------------------------------------------------------------------------------------------
static FloatBox ApplyWorldTransform(const sf::Transform& _transform, const FloatBox& _spaltialData, bool _dimetric)
{
    FRG__RETURN_IF(_transform == sf::Transform::Identity, _spaltialData);

    const sf::FloatRect transformRect = _transform.transformRect({ 0.0f, 0.0f, 1.0f, 1.0f });
    FloatBox result;

    result.Position().x = _spaltialData.GetPosition().x + transformRect.getPosition().x;
    result.Position().y = _spaltialData.GetPosition().y + transformRect.getPosition().y;
    result.Position().z = _spaltialData.GetPosition().z;

    if (_dimetric)
    {
        result.Size().w = _spaltialData.GetSize().w * transformRect.getSize().x;
        result.Size().d = _spaltialData.GetSize().d * transformRect.getSize().x;
        result.Size().h = _spaltialData.GetSize().h * transformRect.getSize().y;
    }
    else
    {
        result.Size().w = _spaltialData.GetSize().w * transformRect.getSize().x;
        result.Size().d = _spaltialData.GetSize().d * transformRect.getSize().y;
        result.Size().h = _spaltialData.GetSize().h;
    }

    return result;
}

} // namespace static_utils

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::Eval(const AbstractRenderTree::CPtr& _tree,
                             const FloatBox& _spatialData,
                             Multiset<DrawData>& _drawBuffer)
{
    Reset();
    m_DrawBuffer = &_drawBuffer;
    m_SpatialData = _spatialData;
    Visit(_tree.get());
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::Visit(const AbstractRenderTree* _tree)
{
    m_LocalTransformStack.push_back(m_LocalTransformStack.empty() ? sf::Transform() : m_LocalTransformStack.back());
    m_WorldTransformStack.push_back(m_WorldTransformStack.empty() ? sf::Transform() : m_WorldTransformStack.back());
    m_ScreenTransformStack.push_back(m_ScreenTransformStack.empty() ? sf::Transform() : m_ScreenTransformStack.back());

    _tree->Accept(*this, TraversalOrder::PreOrder);

    for (const AbstractRenderTree::Ptr& node : _tree->GetChildren())
    {
        Visit(node.get());
    }

    _tree->Accept(*this, TraversalOrder::PostOrder);

    m_LocalTransformStack.pop_back();
    m_WorldTransformStack.pop_back();
    m_ScreenTransformStack.pop_back();
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::Reset()
{
    m_DrawBuffer = nullptr;
    m_LocalTransformStack.clear();
    m_WorldTransformStack.clear();
    m_ScreenTransformStack.clear();
    m_MaterialStack.clear();
    m_FrameMod = { 0, 0 };
    m_SkippedMaterial = -1;
    m_CurrentWorldTranslation = { 0.0f, 0.0f, 0.0f };
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const BasicRenderNode* _node)
{
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const BasicRenderNode* _node)
{
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const SwitchRenderNode* _node)
{
    FRG__RETURN_IF(_node->IsSolved());

    const u32 x = m_SpatialData.GetPosition().x;
    const u32 y = m_SpatialData.GetPosition().y;
    const u32 z = m_SpatialData.GetPosition().z + std::ceil(m_SpatialData.GetSize().h);
    // const u32 seed =  ((x + y) ^ (y - h)) / 2;
    // Cantor Pairing
    u32 seed = (((x + y) * (x + y + 1)) / 2) + y;
    seed = (((seed + z) * (seed + z + 1)) / 2) + z;

    const_cast<SwitchRenderNode*>(_node)->SolveChild(std::minstd_rand(seed)());
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const SwitchRenderNode* _node)
{
    // const_cast<SwitchRenderNode*>(_node)->RemoveChildren();
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const LocalTransformRenderNode* _node)
{
    if (_node->GetTransform() != sf::Transform::Identity)
    {
        CurrentLocalTransform().combine(_node->GetTransform());
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const LocalTransformRenderNode* _node)
{}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const WorldTransformRenderNode* _node)
{
    if (_node->GetTransform() != sf::Transform::Identity)
    {
        CurrentWorldTransform().combine(_node->GetTransform());
        m_CurrentWorldTranslation.x += _node->GetTranslation3D().x;
        m_CurrentWorldTranslation.y += _node->GetTranslation3D().y;
        m_CurrentWorldTranslation.z += _node->GetTranslation3D().z;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const WorldTransformRenderNode* _node)
{
    if (_node->GetTransform() != sf::Transform::Identity)
    {
        m_CurrentWorldTranslation.x -= _node->GetTranslation3D().x;
        m_CurrentWorldTranslation.y -= _node->GetTranslation3D().y;
        m_CurrentWorldTranslation.z -= _node->GetTranslation3D().z;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const ScreenTransformRenderNode* _node)
{
    if (_node->GetTransform() != sf::Transform::Identity)
    {
        CurrentScreenTransform().combine(_node->GetTransform());
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const ScreenTransformRenderNode* _node)
{}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const ShaderMaterialRenderNode* _node)
{
    if (m_SkippedMaterial >= 0)
    {
        ++m_SkippedMaterial;
    }
    else
    {
        m_MaterialStack.push_back(&_node->GetShaderMaterial());
        m_SkippedMaterial = _node->GetOverrideNextMaterials() ? 0 : -1;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const ShaderMaterialRenderNode* _node)
{
    if (m_SkippedMaterial <= 0)
    {
        m_MaterialStack.pop_back();
    }

    if (m_SkippedMaterial >= 0)
    {
        --m_SkippedMaterial;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const BasicDrawableRenderNode* _node)
{
    FloatBox spatialData = m_SpatialData;
    const Vector3f& camPosition = CameraAPI::GetCameraPosition();
    const f32 ppu = CameraAPI::GetPPU();
    const AbstractDrawable::IPtr& drawable = _node->GetDrawable();
    const frg::Vector2f drawableSize = drawable->GetOriginalSize();
    const Vector3f treeSize = spatialData.GetSize();
    const Vector3f treePosition = spatialData.GetPosition();
    const Vector2f screenCoord = CameraAPI::WorldPointToScreen({ treePosition.x,
                                                                 treePosition.y,
                                                                 treePosition.z + treeSize.h });
    sf::Transform localTransform = CurrentLocalTransform();
    sf::Transform screenTransform = CurrentScreenTransform();

    f32 scalingRatio = 1.0f;

    if (treeSize.d != 0)
    {
        scalingRatio = ppu * treeSize.w / drawableSize.w;
    }

    // localTransform.translate(-drawableSize.w * 0.5f, -drawableSize.h + (originalSpritePPU * treeSize.d * 0.5f));
    localTransform.translate(-drawableSize.w * 0.5f, -drawableSize.h * 0.5f);

    screenTransform.translate(screenCoord.x, screenCoord.y);
    screenTransform.scale(scalingRatio, scalingRatio);

    DrawData data { drawable.get(), sf::RenderStates(), m_MaterialStack };
    data.RenderState.transform = screenTransform * localTransform;
    data.OrderingValue = static_utils::ComputeDistanceScore(spatialData, camPosition);

    m_DrawBuffer->insert(std::move(data));
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const BasicDrawableRenderNode* _node)
{}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const GUIDrawableRenderNode* _node)
{
    const AbstractDrawable::IPtr& drawable = _node->GetDrawable();
    sf::Transform localTransform = CurrentLocalTransform();
    sf::Transform screenTransform = CurrentScreenTransform();

    DrawData data { drawable.get(), sf::RenderStates(), m_MaterialStack };
    data.RenderState.transform = screenTransform * localTransform;
    data.OrderingValue = 0.0f;

    m_DrawBuffer->insert(std::move(data));
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const GUIDrawableRenderNode* _node)
{}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const ParticleRenderNode* _node)
{
    const f32 ppu = CameraAPI::GetPPU();
    const Vector3f& camPosition = CameraAPI::GetCameraPosition();
    const AbstractDrawable::IPtr& drawable = _node->GetDrawable();
    const frg::Vector2f drawableSize = drawable->GetOriginalSize();

    sf::Transform localTransform;
    sf::Transform screenTransform;

    const FloatQuad worldTransformRect = static_utils::GetTransformRect(CurrentWorldTransform());
    const Vector3f& treePosition = m_SpatialData.GetPosition();
    const Vector2f screenCoord = CameraAPI::WorldPointToScreen(treePosition);

    Vector2f particleSize = worldTransformRect.GetSize();
    particleSize.x *= drawableSize.x / drawableSize.y;

    frg::Vector2f scalingRatio{ ppu * particleSize.x / drawableSize.w,
                                ppu * particleSize.y / drawableSize.h };

    localTransform.translate(-drawableSize.x * 0.5f, -drawableSize.y * 0.5f);
    localTransform = CurrentLocalTransform() * localTransform;

    screenTransform.translate(worldTransformRect.GetPosition().x * ppu, worldTransformRect.GetPosition().y * ppu);
    screenTransform.translate(screenCoord.x, screenCoord.y);
    screenTransform.scale(scalingRatio.x, scalingRatio.y);
    screenTransform = CurrentScreenTransform() * screenTransform;

    DrawData data{ drawable.get(), sf::RenderStates(), m_MaterialStack };
    data.RenderState.transform = screenTransform * localTransform;
    data.OrderingValue = static_utils::ComputeDistanceScore(m_SpatialData, camPosition);

    m_DrawBuffer->insert(std::move(data));
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const ParticleRenderNode* _node)
{}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const SpriteRenderNode* _node)
{
    const f32 ppu = CameraAPI::GetPPU();
    const Vector3f& camPosition = CameraAPI::GetCameraPosition();
    const Sprite::Ptr& sprite = _node->GetSprite();

    FloatBox spatialData = m_SpatialData;
    sf::Transform localTransform;
    sf::Transform screenTransform;

    const FloatBox worldSpatialData = static_utils::ApplyWorldTransform(CurrentWorldTransform(),
                                                                        spatialData,
                                                                        CameraAPI::IsDimetric());
    const Vector3f& treeSize = worldSpatialData.GetSize();
    const Vector3f& treePosition = worldSpatialData.GetPosition();
    const Vector2f screenCoord = CameraAPI::WorldPointToScreen({ treePosition.x,
                                                                 treePosition.y,
                                                                 treePosition.z + treeSize.h });

    Vector2f scalingRatio{ 0.0f, 0.0f };

    sprite->SetKeyframeModifier(m_FrameMod);

    if (CameraAPI::IsDimetric())
    {
        // x/y scaling cannot be accuratly rendered in isometric so tweak
        // the regular width/height 2:1 ratio using only the entity's depth
        // and sprite's width.
        f32 effectiveSpriteWidth = sprite->GetOriginalSize().w - sprite->GetPadding().x * 2.0f;
        f32 halfSpriteWidth = effectiveSpriteWidth * 0.5f;
        f32 effectiveSpriteDepth = halfSpriteWidth;

        if (treeSize.w != 0 && treeSize.d != 0)
        {
            scalingRatio.x = ppu * treeSize.d / effectiveSpriteDepth;
            scalingRatio.y = ppu * treeSize.d / effectiveSpriteDepth;
        }

        f32 noDepthHeight = (sprite->GetOriginalSize().h - sprite->GetPadding().y * 2.0f) - effectiveSpriteDepth;

        noDepthHeight /= sqrt(1.25f);
        spatialData.Size().h = noDepthHeight * scalingRatio.y / ppu;

        // compute spatial data for transformed sprite
        {
            f32 zDiff = treeSize.h;

            if (CurrentLocalTransform() != sf::Transform::Identity)
            {
                zDiff -= CurrentLocalTransform().transformPoint(0.0f, 0.0f).y / sqrt(1.25f) * scalingRatio.y / ppu;
            }

            zDiff -= spatialData.GetSize().h;
            zDiff += m_CurrentWorldTranslation.z;

            spatialData.Position().z += zDiff;
        }

        // isometric offset
        // TODO: translate on y too (effectiveSpriteWidth / 2) ?
        localTransform.translate(-halfSpriteWidth, 0.0f);
    }
    else
    {
        f32 effectiveSpriteWidth = sprite->GetOriginalSize().w - sprite->GetPadding().x * 2.0f;
        f32 effectiveSpriteHeight = sprite->GetOriginalSize().h - sprite->GetPadding().y * 2.0f;
        f32 originalSpritePPU = effectiveSpriteWidth / treeSize.w;

        if (treeSize.d != 0)
        {
            scalingRatio.y = ppu * treeSize.w / effectiveSpriteWidth;
        }

        localTransform.translate(-effectiveSpriteWidth * 0.5f,
                                 -effectiveSpriteHeight + (originalSpritePPU * treeSize.d * 0.5f));
    }

    localTransform.translate(-sprite->GetPadding().x, -sprite->GetPadding().y);
    localTransform = CurrentLocalTransform() * localTransform;

    screenTransform.translate(screenCoord.x, screenCoord.y);
    screenTransform.scale(scalingRatio.x, scalingRatio.y);
    screenTransform = CurrentScreenTransform() * screenTransform;

    DrawData data{ sprite.get(), sf::RenderStates(), m_MaterialStack };
    data.RenderState.transform = screenTransform * localTransform;
    data.OrderingValue = static_utils::ComputeDistanceScore(spatialData, camPosition);

    m_DrawBuffer->insert(std::move(data));
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const SpriteRenderNode* _node)
{}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const AnimationRenderNode* _node)
{
    FRG__RETURN_IF(!_node->GetIsPlaying());

    u64 t = TimeAPI::GetGameTimeMilliseconds();

    if (_node->GetClock() == AnimationClock::Local)
    {
        t -= _node->GetStartTimeMs();
    }

    m_FrameMod = _node->GetFrameModifier(t);
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const AnimationRenderNode* _node)
{
    m_FrameMod = { 0, 0 };
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPreOrder(const DiscreteAnimationRenderNode* _node)
{
    DiscreteAnimationRenderNode* node
        = const_cast<DiscreteAnimationRenderNode*>(_node);
    u64 loopStartTimeMs = 0;

    if (node->GetIsLooping())
    {
        loopStartTimeMs = node->GetLoopStartTimeMs();

        if (node->GetTimer().IsElapsed())
        {
            node->StopLoop();
            node->StartIdleTimer();
        }
    }
    else
    {
        if (node->GetTimer().IsElapsed())
        {
            node->StartLoop();
            node->StartAnimationTimer();

            loopStartTimeMs = node->GetLoopStartTimeMs();
        }
    }

    if (loopStartTimeMs != 0)
    {
        u64 t = TimeAPI::GetGameTimeMilliseconds();

        if (node->GetClock() == AnimationClock::Local)
        {
            t -= loopStartTimeMs;
        }

        m_FrameMod = node->GetFrameModifier(t);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderTreeVisitor::VisitPostOrder(const DiscreteAnimationRenderNode* _node)
{
    m_FrameMod = { 0, 0 };
}

//----------------------------------------------------------------------------------------------------------------------
sf::Transform& RenderTreeVisitor::CurrentLocalTransform()
{
    return m_LocalTransformStack.back();
}

//----------------------------------------------------------------------------------------------------------------------
sf::Transform& RenderTreeVisitor::CurrentWorldTransform()
{
    return m_WorldTransformStack.back();
}

//----------------------------------------------------------------------------------------------------------------------
sf::Transform& RenderTreeVisitor::CurrentScreenTransform()
{
    return m_ScreenTransformStack.back();
}

FRG__CLOSE_NAMESPACE(frg);
