#include <forge/Project.h>
#include <forge/engine/rendering/resource/Texture.h>

#include <SFML/Graphics/Texture.hpp>

#include <mutex>
#include <thread>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
TextureOptions::TextureOptions()
    : Resolution{ 1024, 1024 }
    , ClearColor(frg::PlainColor::Black)
{}

//----------------------------------------------------------------------------------------------------------------------
TextureOptions::TextureOptions(const Vector2u& _resolution, const Color& _clearColor)
    : Resolution(_resolution)
    , ClearColor(_clearColor)
{}

//----------------------------------------------------------------------------------------------------------------------
RAMTexture::RAMTexture()
{
}

//----------------------------------------------------------------------------------------------------------------------
RAMTexture::~RAMTexture()
{
}

//----------------------------------------------------------------------------------------------------------------------
void RAMTexture::Copy(const sf::Image& _source)
{
    *m_Impl = _source;
}

//----------------------------------------------------------------------------------------------------------------------
void RAMTexture::Move(const sf::Image&& _source)
{
    *m_Impl = std::move(_source);
}

//----------------------------------------------------------------------------------------------------------------------
Color RAMTexture::GetPixelColor(const Vector2u& _pixelCoord) const
{
    sf::Color color = m_Impl->getPixel(_pixelCoord.x, _pixelCoord.y);

    return { color.r, color.g, color.b, color.a };
}

//----------------------------------------------------------------------------------------------------------------------
Vector2u RAMTexture::GetSize() const
{
    return { m_Impl->getSize().x, m_Impl->getSize().y };
}

//----------------------------------------------------------------------------------------------------------------------
Texture::Texture()
    : m_Bound(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
Texture::Texture(const Color& _color)
    : m_Bound(false)
{
    sf::Image img;
    img.create(1, 1, { _color.r, _color.g, _color.b, _color.a } );
    m_Impl->loadFromImage(img);
}

//----------------------------------------------------------------------------------------------------------------------
Texture::Texture(const String& _texPath)
    : m_Bound(false)
{
    SetFile(_texPath);
}

//----------------------------------------------------------------------------------------------------------------------
Texture::Texture(sf::Texture* _sfTexture)
    : m_Bound(false)
{
    Bind(_sfTexture);
}

//----------------------------------------------------------------------------------------------------------------------
Texture::~Texture()
{
    if (m_Bound)
        m_Impl = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
void Texture::Bind(sf::Texture*  _sfTexutre)
{
    delete m_Impl;

    m_Impl = _sfTexutre;
    m_Bound = true;
}

//----------------------------------------------------------------------------------------------------------------------
void Texture::SetFile(const String& _texPath)
{
    m_Impl->loadFromFile(_texPath);
}

//----------------------------------------------------------------------------------------------------------------------
void Texture::SetRepeated(bool _repeated)
{
    m_Impl->setRepeated(_repeated);
}

//----------------------------------------------------------------------------------------------------------------------
template class SFMLWrapper<sf::Image>;
template class SFMLWrapper<sf::Texture>;

FRG__CLOSE_NAMESPACE(frg);
