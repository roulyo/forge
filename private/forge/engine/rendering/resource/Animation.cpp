#include <forge/Project.h>
#include <forge/engine/rendering/resource/Animation.h>

#include <forge/engine/time/api/TimeAPI.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
Animation::Animation()
    : m_StartFrameCoord{ 0, 0 }
    , m_KeyframeCount(1)
    , m_DurationMs(0)
    , m_KeyframeDuration(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
Animation::Animation(u32 _x, u32 _y, u32 _keyframeCount, u32 _durationInMs)
    : m_StartFrameCoord{ _x, _y }
    , m_KeyframeCount(_keyframeCount)
    , m_DurationMs(_durationInMs)
    , m_KeyframeDuration(_durationInMs / _keyframeCount + 1)
{
}

//----------------------------------------------------------------------------------------------------------------------
Animation::~Animation()
{
}

//----------------------------------------------------------------------------------------------------------------------
void Animation::SetStartFrameCoord(u32 _x, u32 _y)
{
    m_StartFrameCoord = { _x, _y };
}

//----------------------------------------------------------------------------------------------------------------------
void Animation::SetKeyframeCount(u32 _count)
{
    m_KeyframeCount = _count;
    m_KeyframeDuration = m_DurationMs / m_KeyframeCount + 1;
}

//----------------------------------------------------------------------------------------------------------------------
void Animation::SetDurationMs(u32 _durationMs)
{
    m_DurationMs = _durationMs;
    m_KeyframeDuration = m_DurationMs / m_KeyframeCount + 1;
}

//----------------------------------------------------------------------------------------------------------------------
Vector2u Animation::GetKeyframe(u32 _t) const
{
    Vector2u coord = m_StartFrameCoord;

    if (m_KeyframeCount > 1)
    {
        coord.x += (_t % m_DurationMs) / m_KeyframeDuration;
    }

    return coord;
}

//----------------------------------------------------------------------------------------------------------------------
u32 Animation::GetDurationMs() const
{
    return m_DurationMs;
}

FRG__CLOSE_NAMESPACE(frg);
