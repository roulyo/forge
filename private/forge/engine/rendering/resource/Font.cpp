#include <forge/Project.h>
#include <forge/engine/rendering/resource/Font.h>

#include <SFML/Graphics/Font.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
Font::Font()
{
}

//----------------------------------------------------------------------------------------------------------------------
Font::Font(const String& _fontPath)
{
    m_Impl->loadFromFile(_fontPath);
}

//----------------------------------------------------------------------------------------------------------------------
Font::~Font()
{
    delete m_Impl;
}

//----------------------------------------------------------------------------------------------------------------------
void Font::SetFile(const String& _texPath)
{
    m_Impl->loadFromFile(_texPath);
}

//----------------------------------------------------------------------------------------------------------------------
Texture::CPtr Font::GetTexture(u32 _size) const
{
    Texture::Ptr texture = Texture::MakePtr();
    texture->Bind(const_cast<sf::Texture*>(&m_Impl->getTexture(_size)));

    return texture;
}

//----------------------------------------------------------------------------------------------------------------------
IntQuad Font::GetGlyphRect(char32_t _char, u32 _size, bool _isBold /* = false */, f32 _outlineThickness /* = 0 */) const
{
    const sf::IntRect& rect = m_Impl->getGlyph(_char, _size, _isBold, _outlineThickness).textureRect;

    return { rect.left, rect.top, rect.width, rect.height };
}

FRG__CLOSE_NAMESPACE(frg);
