#include <forge/Project.h>
#include <forge/engine/rendering/resource/Shader.h>

#include <SFML/Graphics/Shader.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
Shader::Shader()
{
}

//----------------------------------------------------------------------------------------------------------------------
Shader::~Shader()
{
    delete SFMLWrapper<sf::Shader>::m_Impl;
}

//----------------------------------------------------------------------------------------------------------------------
void Shader::SetVertexFile(const String& _string)
{
    m_Impl->loadFromFile(_string, sf::Shader::Vertex);
}

//----------------------------------------------------------------------------------------------------------------------
void Shader::SetPixelFile(const String& _string)
{
    m_Impl->loadFromFile(_string, sf::Shader::Fragment);
}

//----------------------------------------------------------------------------------------------------------------------
void Shader::SetFiles(const String& _vertex, const String& _pixel)
{
    m_Impl->loadFromFile(_vertex, _pixel);
}

//----------------------------------------------------------------------------------------------------------------------
void Shader::LoadVertexCode(const String& _string)
{
    m_Impl->loadFromMemory(_string, sf::Shader::Vertex);
}

//----------------------------------------------------------------------------------------------------------------------
void Shader::LoadPixelCode(const String& _string)
{
    m_Impl->loadFromMemory(_string, sf::Shader::Fragment);
}

//----------------------------------------------------------------------------------------------------------------------
void Shader::LoadCodes(const String& _vertex, const String& _pixel)
{
    m_Impl->loadFromMemory(_vertex, _pixel);
}

//----------------------------------------------------------------------------------------------------------------------
void Shader::SetUniform(const ShaderUniform& _param)
{
    if (_param.Type == ShaderUniform::Type::Color)
    {
        m_Impl->setUniform(_param.Name, sf::Glsl::Vec4((f32)_param.Value.RGBA.r / 255.f,
                                                       (f32)_param.Value.RGBA.g / 255.f,
                                                       (f32)_param.Value.RGBA.b / 255.f,
                                                       (f32)_param.Value.RGBA.a / 255.f));
    }
    else if (_param.Type == ShaderUniform::Type::Float)
    {
        m_Impl->setUniform(_param.Name, _param.Value.Float);
    }
}


//----------------------------------------------------------------------------------------------------------------------
void Shader::SetUniforms(const UniformMap& _params)
{
    for (const Pair<String, ShaderUniform>& param : _params)
    {
        SetUniform(param.second);
    }
}

//----------------------------------------------------------------------------------------------------------------------
template class SFMLWrapper<sf::Shader>;

FRG__CLOSE_NAMESPACE(frg);
