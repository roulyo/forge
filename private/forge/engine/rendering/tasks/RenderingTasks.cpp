#include <forge/Project.h>
#include <forge/engine/rendering/tasks/RenderingTasks.h>

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Image.hpp>

FRG__OPEN_NAMESPACE(frg);


//----------------------------------------------------------------------------------------------------------------------
void DownloadRAMTextureTask::OnStart()
{
    RAMTexture::Ptr ramTexture = RAMTexture::MakePtr();

    ramTexture->Move(FRG__TASK_PARAM.Source->GetSFMLObject().copyToImage());

    ReturnValue::Ptr rv = ReturnValue::MakePtr();
    rv->SetValue(ramTexture);

    Return(rv);
}


FRG__CLOSE_NAMESPACE(frg);
