#pragma once

#include <forge/engine/async/AsyncTask.h>

#include <forge/engine/rendering/resource/Texture.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
struct DownloadRAMTextureParam : public AsyncParam<DownloadRAMTextureParam>
{
    Texture::Ptr Source;
};

//----------------------------------------------------------------------------------------------------------------------
class DownloadRAMTextureTask : public AsyncTask<DownloadRAMTextureTask>
{
    FRG__SET_TASK_PARAM(DownloadRAMTextureParam);
    FRG__SET_TASK_RETURNVALUE(RAMTexture::Ptr);

public:
    void OnStart() override;

private:
    Texture::Ptr    m_Texture;

};

FRG__CLOSE_NAMESPACE(frg);
