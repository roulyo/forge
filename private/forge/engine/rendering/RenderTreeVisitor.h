#pragma once

#include <forge/engine/rendering/RenderTree.h>
#include <forge/engine/rendering/RenderTypes.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class RenderTreeVisitor
{
public:
    void Eval(const AbstractRenderTree::CPtr& _tree,
              const FloatBox& _spatialData,
              Multiset<DrawData>& _drawBuffer);
    void Visit(const AbstractRenderTree* _tree);
    void Reset();

    void VisitPreOrder(const BasicRenderNode* _node);
    void VisitPostOrder(const BasicRenderNode* _node);

    void VisitPreOrder(const SwitchRenderNode* _node);
    void VisitPostOrder(const SwitchRenderNode* _node);

    void VisitPreOrder(const LocalTransformRenderNode* _node);
    void VisitPostOrder(const LocalTransformRenderNode* _node);

    void VisitPreOrder(const WorldTransformRenderNode* _node);
    void VisitPostOrder(const WorldTransformRenderNode* _node);

    void VisitPreOrder(const ScreenTransformRenderNode* _node);
    void VisitPostOrder(const ScreenTransformRenderNode* _node);

    void VisitPreOrder(const ShaderMaterialRenderNode* _node);
    void VisitPostOrder(const ShaderMaterialRenderNode* _node);

    void VisitPreOrder(const BasicDrawableRenderNode* _node);
    void VisitPostOrder(const BasicDrawableRenderNode* _node);

    void VisitPreOrder(const GUIDrawableRenderNode* _node);
    void VisitPostOrder(const GUIDrawableRenderNode* _node);

    void VisitPreOrder(const ParticleRenderNode* _node);
    void VisitPostOrder(const ParticleRenderNode* _node);

    void VisitPreOrder(const SpriteRenderNode* _node);
    void VisitPostOrder(const SpriteRenderNode* _node);

    void VisitPreOrder(const AnimationRenderNode* _node);
    void VisitPostOrder(const AnimationRenderNode* _node);

    void VisitPreOrder(const DiscreteAnimationRenderNode* _node);
    void VisitPostOrder(const DiscreteAnimationRenderNode* _node);

private:
    sf::Transform& CurrentLocalTransform();
    sf::Transform& CurrentWorldTransform();
    sf::Transform& CurrentScreenTransform();

private:
    Multiset<DrawData>*             m_DrawBuffer;

    Vector<sf::Transform>           m_LocalTransformStack;
    Vector<sf::Transform>           m_WorldTransformStack;
    Vector<sf::Transform>           m_ScreenTransformStack;
    Vector<const ShaderMaterial*>   m_MaterialStack;
    Vector2u                        m_FrameMod;
    FloatBox                        m_SpatialData;
    i32                             m_SkippedMaterial;
    Vector3f                        m_CurrentWorldTranslation;

};

FRG__CLOSE_NAMESPACE(frg);
