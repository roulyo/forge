#include <forge/Project.h>
#include <forge/engine/rendering/drawable/Drawable.h>

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
AbstractDrawable::AbstractDrawable()
{
}

//----------------------------------------------------------------------------------------------------------------------
AbstractDrawable::~AbstractDrawable()
{
}

//----------------------------------------------------------------------------------------------------------------------
bool AbstractDrawable::operator<(const AbstractDrawable& _other) const
{
    Vector2f thisPos = GetRenderedPosition();
    Vector2f otherPos = _other.GetRenderedPosition();

    if (thisPos.y == otherPos.y)
        return thisPos.x < otherPos.x;

    return thisPos.y < otherPos.y;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Drawable<T>::~Drawable()
{
    delete SFMLWrapper<T>::m_Impl;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Drawable<T>::SetScreenCoord(f32 _x, f32 _y)
{
    SFMLWrapper<T>::m_Impl->setPosition(_x, _y);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Vector2f Drawable<T>::GetOriginalPosition() const
{
    Vector2f vec;

    vec.x = SFMLWrapper<T>::m_Impl->getLocalBounds().left;
    vec.y = SFMLWrapper<T>::m_Impl->getLocalBounds().top;

    return vec;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Vector2f Drawable<T>::GetRenderedPosition() const
{
    Vector2f vec;

    vec.x = SFMLWrapper<T>::m_Impl->getGlobalBounds().left;
    vec.y = SFMLWrapper<T>::m_Impl->getGlobalBounds().top;

    return vec;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Vector2f Drawable<T>::GetOriginalSize() const
{
    Vector2f vec;

    vec.x = SFMLWrapper<T>::m_Impl->getLocalBounds().width;
    vec.y = SFMLWrapper<T>::m_Impl->getLocalBounds().height;

    return vec;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Vector2f Drawable<T>::GetRenderedSize() const
{
    Vector2f vec;

    vec.x = SFMLWrapper<T>::m_Impl->getGlobalBounds().width;
    vec.y = SFMLWrapper<T>::m_Impl->getGlobalBounds().height;

    return vec;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
const AbstractDrawable::DrawableData& Drawable<T>::GetDrawableData() const
{
    return *SFMLWrapper<T>::m_Impl;
}

//----------------------------------------------------------------------------------------------------------------------
template class SFMLWrapper<sf::CircleShape>;
template class SFMLWrapper<sf::RectangleShape>;
template class SFMLWrapper<sf::Sprite>;
template class SFMLWrapper<sf::Text>;

template class Drawable<sf::CircleShape>;
template class Drawable<sf::RectangleShape>;
template class Drawable<sf::Sprite>;
template class Drawable<sf::Text>;


FRG__CLOSE_NAMESPACE(frg)
