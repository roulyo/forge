#include <forge/Project.h>
#include <forge/engine/rendering/drawable/Sprite.h>

#include <SFML/Graphics/Sprite.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
Sprite::Sprite()
    : m_SpriteCoord{ 0, 0 }
    , m_Padding{ 0, 0 }
    , m_KeyframeMod{ 0, 0 }
    , m_IsDirty(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
Sprite::Sprite(const Texture::Ptr _texture, const IntQuad& _texQuad)
    : m_Texture(_texture)
    , m_SpriteCoord{ _texQuad.GetPosition().x, _texQuad.GetPosition().y }
    , m_Padding{ 0, 0 }
    , m_KeyframeMod{ 0, 0 }
    , m_IsDirty(false)
{
    if (_texture != nullptr)
    {
        m_Impl->setTexture(_texture->GetSFMLObject());
        m_Impl->setTextureRect(sf::IntRect(_texQuad.GetPosition().x, _texQuad.GetPosition().y,
                                           _texQuad.GetSize().w, _texQuad.GetSize().h));
    }
}

//----------------------------------------------------------------------------------------------------------------------
Sprite::~Sprite()
{
}

//----------------------------------------------------------------------------------------------------------------------
void Sprite::SetTexture(const Texture::CPtr& _texture)
{
    if (_texture != nullptr)
    {
        m_Texture = _texture;
        m_Impl->setTexture(_texture->GetSFMLObject());
    }
}

//----------------------------------------------------------------------------------------------------------------------
const Texture::CPtr& Sprite::GetTexture() const
{
    return m_Texture;
}

//----------------------------------------------------------------------------------------------------------------------
void Sprite::SetTextureQuad(i32 _x, i32 _y, i32 _w, i32 _h)
{
    SetTextureQuad({ _x, _y, _w, _h });
}

//----------------------------------------------------------------------------------------------------------------------
void Sprite::SetTextureQuad(const IntQuad& _texQuad)
{
    m_SpriteCoord = { _texQuad.GetPosition().x, _texQuad.GetPosition().y };

    m_Impl->setTextureRect(sf::IntRect(_texQuad.GetPosition().x, _texQuad.GetPosition().y,
                                       _texQuad.GetSize().w, _texQuad.GetSize().h));
}

//----------------------------------------------------------------------------------------------------------------------
IntQuad Sprite::GetTextureQuad() const
{
    IntQuad quad(m_Impl->getTextureRect().left,
                 m_Impl->getTextureRect().top,
                 m_Impl->getTextureRect().width,
                 m_Impl->getTextureRect().height);

    return quad;
}

//----------------------------------------------------------------------------------------------------------------------
void Sprite::SetColor(const Color& _color)
{
    m_Impl->setColor({ _color.r, _color.g, _color.b, _color.a });
}

//----------------------------------------------------------------------------------------------------------------------
Color Sprite::GetColor() const
{
    sf::Color c = m_Impl->getColor();

    return { c.r, c.g, c.b, c.a };
}

//----------------------------------------------------------------------------------------------------------------------
void Sprite::SetOpacity(u8 _opacity)
{
    m_Impl->setColor({ 255, 255, 255, _opacity });
}

//----------------------------------------------------------------------------------------------------------------------
u8 Sprite::GetOpacity() const
{
    return m_Impl->getColor().a;
}

//----------------------------------------------------------------------------------------------------------------------
void Sprite::SetKeyframeModifier(const Vector2u& _keyframeMod)
{
    if (m_KeyframeMod != _keyframeMod)
    {
        m_KeyframeMod = _keyframeMod;
        m_IsDirty = true;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void Sprite::SetPadding(i32 _x, i32 _y)
{
    SetPadding({ _x, _y });
}

//----------------------------------------------------------------------------------------------------------------------
const Sprite::DrawableData& Sprite::GetDrawableData() const
{
    if (m_IsDirty)
    {
        IntQuad texQuad = GetTextureQuad();

        texQuad.Position().x = m_SpriteCoord.x + texQuad.GetSize().w * m_KeyframeMod.x;
        texQuad.Position().y = m_SpriteCoord.y + texQuad.GetSize().h * m_KeyframeMod.y;

        m_Impl->setTextureRect(
            sf::IntRect(texQuad.GetPosition().x, texQuad.GetPosition().y,
                        texQuad.GetSize().w, texQuad.GetSize().h));

        m_IsDirty = false;
    }

    return *m_Impl;
}

//----------------------------------------------------------------------------------------------------------------------
PlainSprite::PlainSprite(const Color& _color)
{
    SetTexture(Texture::MakePtr(_color));
}

//----------------------------------------------------------------------------------------------------------------------
const PlainSprite::Ptr PlainSprite::White   = nullptr;
const PlainSprite::Ptr PlainSprite::Black   = nullptr;

const PlainSprite::Ptr PlainSprite::Red     = nullptr;
const PlainSprite::Ptr PlainSprite::Green   = nullptr;
const PlainSprite::Ptr PlainSprite::Blue    = nullptr;

const PlainSprite::Ptr PlainSprite::Cyan    = nullptr;
const PlainSprite::Ptr PlainSprite::Purple  = nullptr;
const PlainSprite::Ptr PlainSprite::Yellow  = nullptr;

const PlainSprite::Ptr PlainSprite::Orange  = nullptr;
const PlainSprite::Ptr PlainSprite::Brown   = nullptr;

FRG__CLOSE_NAMESPACE(frg);
