#include <forge/Project.h>
#include <forge/engine/rendering/drawable/Text.h>

#include <SFML/Graphics/Text.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
Text::Text()
    : m_Font(nullptr)
{
}

//----------------------------------------------------------------------------------------------------------------------
Text::Text(const Font::Ptr _font)
    : m_Font(_font)
{
    m_Impl->setFont(_font->GetSFMLObject());
}


//----------------------------------------------------------------------------------------------------------------------
Text::~Text()
{
}

//----------------------------------------------------------------------------------------------------------------------
void Text::SetString(const String& _string)
{
    m_Impl->setString(_string);
}

//----------------------------------------------------------------------------------------------------------------------
void Text::SetString(const WString& _string)
{
    m_Impl->setString(_string);
}

//----------------------------------------------------------------------------------------------------------------------
void Text::SetFillColor(const Color& _color)
{
    m_Impl->setFillColor(sf::Color(_color.r, _color.g, _color.b, _color.a));
}

//----------------------------------------------------------------------------------------------------------------------
void Text::SetFont(const Font::CPtr& _font)
{
    m_Font = _font;
    m_Impl->setFont(m_Font->GetSFMLObject());
}

//----------------------------------------------------------------------------------------------------------------------
void Text::SetSize(u32 _size)
{
    m_Impl->setCharacterSize(_size);
}

//----------------------------------------------------------------------------------------------------------------------
void Text::SetStyle(Style _style)
{
    m_Impl->setStyle(static_cast<sf::Text::Style>(_style));
}

//----------------------------------------------------------------------------------------------------------------------
void Text::SetLineSpacing(f32 _spacing)
{
    m_Impl->setLineSpacing(_spacing);
}

FRG__CLOSE_NAMESPACE(frg);
