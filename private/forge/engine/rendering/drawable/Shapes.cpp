#include <forge/Project.h>
#include <forge/engine/rendering/drawable/Shapes.h>

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

//----------------------------------------------------------------------------------------------------------------------
#define FRG__IMPL_SHAPE(T)                                                                  \
    void T::SetTexture(const Texture::CPtr& _texture, bool _resetRect)                      \
    {                                                                                       \
        m_Texture = _texture;                                                               \
                                                                                            \
        if (_texture != nullptr)                                                            \
        {                                                                                   \
            m_Impl->setTexture(&(m_Texture->GetSFMLObject()), _resetRect);                  \
        }                                                                                   \
        else                                                                                \
        {                                                                                   \
            m_Impl->setTextureRect(sf::IntRect());                                          \
        }                                                                                   \
    }                                                                                       \
                                                                                            \
    void T::SetTextureQuad(const IntQuad& _texQuad)                                         \
    {                                                                                       \
        m_TexQuad = _texQuad;                                                               \
        m_Impl->setTextureRect(sf::IntRect(_texQuad.GetPosition().x,                        \
                                           _texQuad.GetPosition().y,                        \
                                           _texQuad.GetSize().w,                            \
                                           _texQuad.GetSize().h));                          \
    }                                                                                       \
                                                                                            \
    void T::SetSprite(const Sprite::CPtr& _sprite)                                          \
    {                                                                                       \
        if (_sprite != nullptr)                                                             \
        {                                                                                   \
            SetTexture(_sprite->GetTexture());                                              \
            SetTextureQuad(_sprite->GetTextureQuad());                                      \
            SetFillColor(_sprite->GetColor());                                              \
        }                                                                                   \
        else                                                                                \
        {                                                                                   \
            SetTexture(nullptr);                                                            \
            SetTextureQuad(IntQuad());                                                      \
            SetFillColor(frg::PlainColor::Transparent);                                     \
        }                                                                                   \
    }                                                                                       \
                                                                                            \
    void T::SetFillColor(const Color& _color)                                               \
    {                                                                                       \
        m_Impl->setFillColor(sf::Color(_color.r, _color.g,                                  \
                                       _color.b, _color.a));                                \
    }                                                                                       \
                                                                                            \
    void T::SetOutlineColor(const Color& _color)                                            \
    {                                                                                       \
        m_Impl->setOutlineColor(sf::Color(_color.r, _color.g,                               \
                                          _color.b, _color.a));                             \
    }                                                                                       \
                                                                                            \
    void T::SetOutlineThickness(f32 _thickness)                                             \
    {                                                                                       \
        m_Impl->setOutlineThickness(_thickness);                                            \
    }                                                                                       \
                                                                                            \
    Color T::GetFillColor() const                                                           \
    {                                                                                       \
        const sf::Color& color = m_Impl->getFillColor();                                    \
        return { color.r, color.g, color.b, color.a };                                      \
    }                                                                                       \
                                                                                            \
    Color T::GetOutlineColor() const                                                        \
    {                                                                                       \
        const sf::Color& color = m_Impl->getOutlineColor();                                 \
        return { color.r, color.g, color.b, color.a };                                      \
    }                                                                                       \
                                                                                            \
    f32 T::GetOutlineThickness() const                                                      \
    {                                                                                       \
        return m_Impl->getOutlineThickness();                                               \
    }



FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_SHAPE(CircleShape);

//----------------------------------------------------------------------------------------------------------------------
CircleShape::CircleShape()
{
}

//----------------------------------------------------------------------------------------------------------------------
CircleShape::~CircleShape()
{
}

//----------------------------------------------------------------------------------------------------------------------
void CircleShape::SetRadius(f32 _radius)
{
    m_Impl->setRadius(_radius);
}

//----------------------------------------------------------------------------------------------------------------------
f32 CircleShape::GetRadius() const
{
    return m_Impl->getRadius();
}

//----------------------------------------------------------------------------------------------------------------------
FRG__IMPL_SHAPE(QuadShape);

//----------------------------------------------------------------------------------------------------------------------
QuadShape::QuadShape()
{
}

//----------------------------------------------------------------------------------------------------------------------
QuadShape::~QuadShape()
{
}

//----------------------------------------------------------------------------------------------------------------------
void QuadShape::SetSize(const Vector2f& _size)
{
    m_Impl->setSize({ _size.x, _size.y });
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f QuadShape::GetSize() const
{
    return { m_Impl->getSize().x, m_Impl->getSize().y };
}

FRG__CLOSE_NAMESPACE(frg);
