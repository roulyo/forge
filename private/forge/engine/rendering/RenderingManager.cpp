#include <forge/Project.h>
#include <forge/engine/rendering/RenderingManager.h>

#include <forge/engine/camera/api/CameraAPI.h>
#include <forge/engine/rendering/drawable/Drawable.h>
#include <forge/engine/rendering/tasks/RenderingTasks.h>

#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/Sprite.hpp>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void RenderingManager::InitInstance()
{
    m_Viewport = nullptr;
    m_BlendNoneState.blendMode = sf::BlendNone;

    const_cast<PlainSprite::Ptr&>(PlainSprite::White) = Sharable<PlainSprite>::MakePtr(PlainColor::White);
    const_cast<PlainSprite::Ptr&>(PlainSprite::Black) = Sharable<PlainSprite>::MakePtr(PlainColor::Black);

    const_cast<PlainSprite::Ptr&>(PlainSprite::Red) = Sharable<PlainSprite>::MakePtr(PlainColor::Red);
    const_cast<PlainSprite::Ptr&>(PlainSprite::Green) = Sharable<PlainSprite>::MakePtr(PlainColor::Green);
    const_cast<PlainSprite::Ptr&>(PlainSprite::Blue) = Sharable<PlainSprite>::MakePtr(PlainColor::Blue);

    const_cast<PlainSprite::Ptr&>(PlainSprite::Cyan) = Sharable<PlainSprite>::MakePtr(PlainColor::Cyan);
    const_cast<PlainSprite::Ptr&>(PlainSprite::Purple) = Sharable<PlainSprite>::MakePtr(PlainColor::Purple);
    const_cast<PlainSprite::Ptr&>(PlainSprite::Yellow) = Sharable<PlainSprite>::MakePtr(PlainColor::Yellow);

    const_cast<PlainSprite::Ptr&>(PlainSprite::Orange) = Sharable<PlainSprite>::MakePtr(PlainColor::Orange);
    const_cast<PlainSprite::Ptr&>(PlainSprite::Brown) = Sharable<PlainSprite>::MakePtr(PlainColor::Brown);
}

//----------------------------------------------------------------------------------------------------------------------
void RenderingManager::SetViewport(sf::RenderTarget& _target)
{
    m_Viewport = &_target;

    m_Resolution.x = static_cast<f32>(_target.getSize().x);
    m_Resolution.y = static_cast<f32>(_target.getSize().y);

    TextureOptions texOpts({ _target.getSize().x, _target.getSize().y }, frg::PlainColor::Black);
    CreateTexture(texOpts);

    m_FrontBuffer.create(_target.getSize().x, _target.getSize().y);
    m_BackBuffer.create(_target.getSize().x, _target.getSize().y);
}

//----------------------------------------------------------------------------------------------------------------------
TextureHandle RenderingManager::CreateTexture(const TextureOptions& _options)
{
    TextureHolder texHolder;
    texHolder.RenderTexture = new sf::RenderTexture();
    texHolder.FrgTexture = Texture::MakePtr();
    texHolder.Options = _options;

    texHolder.RenderTexture->create(_options.Resolution.w, _options.Resolution.h);
    texHolder.FrgTexture->Bind(
        const_cast<sf::Texture*>(&texHolder.RenderTexture->getTexture()));

    m_RenderTextures.push_back(texHolder);
    m_DepthSortedData.emplace_back();
    m_RenderTrees.emplace_back();

    return static_cast<TextureHandle>(m_RenderTextures.size() - 1);
}

//----------------------------------------------------------------------------------------------------------------------
Texture::Ptr RenderingManager::GetTexture(TextureHandle _textureHandle)
{
    return m_RenderTextures[_textureHandle].FrgTexture;
}

//----------------------------------------------------------------------------------------------------------------------
Future<RAMTexture::Ptr> RenderingManager::DownloadRAMTexture(TextureHandle _textureHandle)
{
    Future<RAMTexture::Ptr> future;

    DownloadRAMTextureParam::Ptr param = DownloadRAMTextureParam::MakePtr();
    param->UseWorkerThread = true;
    param->Source = m_RenderTextures[_textureHandle].FrgTexture;

    DownloadRAMTextureTask::Ptr task = DownloadRAMTextureTask::MakePtr();
    task->SetTaskParam(param);

    future.Bind(PushAsyncTask(task));

    return future;
}

//----------------------------------------------------------------------------------------------------------------------
const Vector2f& RenderingManager:: GetRenderTargetResolution() const
{
    return m_Resolution;
}

//----------------------------------------------------------------------------------------------------------------------
void RenderingManager::PushRenderTree(TextureHandle _handle,
                                      const AbstractRenderTree::CPtr& _renderTree,
                                      const FloatBox& _spatialData)
{
    m_RenderTrees[_handle].push_back({ _renderTree, _spatialData });
}

//----------------------------------------------------------------------------------------------------------------------
void RenderingManager::RenderViewport()
{
    EvalRenderTrees();
    RenderDrawables();
    ClearRenderTrees();
}

//----------------------------------------------------------------------------------------------------------------------
bool RenderingManager::IsScreenBound(const AbstractDrawable& _drawable) const
{
    Vector2f pos = _drawable.GetRenderedPosition();
    Vector2f size = _drawable.GetRenderedSize();

    return pos.x + size.w >= 0.0f
        && pos.y + size.h >= 0.0f
        && pos.x <= m_Resolution.w
        && pos.y <= m_Resolution.h;
}

//----------------------------------------------------------------------------------------------------------------------
void RenderingManager::EvalRenderTrees()
{
    for (u32 i = 0; i < m_RenderTextures.size(); ++i)
    {
        for (const RenderTreeData& treeData : m_RenderTrees[i])
        {
            m_Visitor.Eval(treeData.Tree, treeData.SpatialData, m_DepthSortedData[i]);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderingManager::ClearRenderTrees()
{
    for (u32 i = 0; i < m_RenderTextures.size(); ++i)
    {
        m_RenderTrees[i].clear();
    }
}

//----------------------------------------------------------------------------------------------------------------------
void RenderingManager::RenderDrawables()
{
    sf::RenderTexture* front = &m_FrontBuffer;
    sf::RenderTexture* back = &m_BackBuffer;

    for (u32 i = 0; i < m_DepthSortedData.size(); ++i)
    {
        FRG__CONTINUE_IF(m_DepthSortedData[i].size() == 0);

        Multiset<DrawData>& depthSortedData = m_DepthSortedData[i];
        sf::RenderTexture& renderTexture = *(m_RenderTextures[i].RenderTexture);
        const Color& clearColor = m_RenderTextures[i].Options.ClearColor;
        sf::Color sfClearColor{ clearColor.r, clearColor.g, clearColor.b, clearColor.a };

        renderTexture.clear(sfClearColor);

        for (const DrawData& data : depthSortedData)
        {
            if (data.ShaderMaterials.size() <= 1)
            {
                if (!data.ShaderMaterials.empty())
                {
                    const ShaderMaterial& material = *data.ShaderMaterials[0];
                    material.Program->SetUniforms(material.Parameters);
                    const_cast<DrawData&>(data).RenderState.shader = &material.Program->GetSFMLObject();
                }

                renderTexture.draw(data.Drawable->GetDrawableData(), data.RenderState);
            }
            else
            {
                const sf::IntRect rect { static_cast<i32>(data.Drawable->GetRenderedPosition().x),
                                         static_cast<i32>(data.Drawable->GetRenderedPosition().y),
                                         static_cast<i32>(data.Drawable->GetRenderedSize().w),
                                         static_cast<i32>(data.Drawable->GetRenderedSize().h) };

                const ShaderMaterial& firstMaterial = *data.ShaderMaterials[0];
                firstMaterial.Program->SetUniforms(firstMaterial.Parameters);
                m_BlendNoneState.shader = &firstMaterial.Program->GetSFMLObject();
                back->clear({ 255, 255, 255, 0 });
                back->draw(data.Drawable->GetDrawableData(), m_BlendNoneState);
                back->display();

                for (u32 i = 1; i < data.ShaderMaterials.size() - 1; ++i)
                {
                    const ShaderMaterial& material = *data.ShaderMaterials[i];
                    material.Program->SetUniforms(material.Parameters);
                    m_BlendNoneState.shader = &material.Program->GetSFMLObject();
                    front->clear({ 255, 255, 255, 0 });
                    front->draw(sf::Sprite(back->getTexture(), rect), m_BlendNoneState);
                    front->display();

                    std::swap(back, front);
                }

                const ShaderMaterial& lastMaterial = *data.ShaderMaterials.back();
                lastMaterial.Program->SetUniforms(lastMaterial.Parameters);
                const_cast<DrawData&>(data).RenderState.shader = &lastMaterial.Program->GetSFMLObject();

                renderTexture.draw(sf::Sprite(back->getTexture(), rect), data.RenderState);
            }
        }

        renderTexture.display();

        depthSortedData.clear();
    }

#if defined(FRG_USE_DEBUG_INFO)
    if (m_RenderTextures.size() > 1)
    {
        sf::Sprite internalTexture(m_RenderTextures[0].RenderTexture->getTexture());
        m_Viewport->draw(internalTexture);
    }
    else
#endif
    {
        sf::Sprite internalTexture(m_RenderTextures[0].RenderTexture->getTexture());
        m_Viewport->draw(internalTexture);
    }
}

FRG__CLOSE_NAMESPACE(frg);
