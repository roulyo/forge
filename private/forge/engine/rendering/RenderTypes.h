#pragma once

#include <SFML/Graphics/RenderStates.hpp>

#include <forge/engine/rendering/RenderTree.h>
#include <forge/engine/rendering/resource/Shader.h>
#include <forge/engine/rendering/resource/Texture.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class RenderTexture;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractDrawable;

//----------------------------------------------------------------------------------------------------------------------
typedef u32 TextureHandle;

//----------------------------------------------------------------------------------------------------------------------
struct TextureHolder
{
    sf::RenderTexture*  RenderTexture = nullptr;
    Texture::Ptr        FrgTexture;
    TextureOptions      Options;
};

//----------------------------------------------------------------------------------------------------------------------
struct RenderTreeData
{
    AbstractRenderTree::CPtr    Tree = nullptr;
    FloatBox                    SpatialData;
};

//----------------------------------------------------------------------------------------------------------------------
struct DrawData
{
    const AbstractDrawable*         Drawable = nullptr;
    sf::RenderStates                RenderState;
    Vector<const ShaderMaterial*>   ShaderMaterials;
    f64                             OrderingValue = 0;

    bool operator<(const DrawData& _other) const
    {
        return this->OrderingValue > _other.OrderingValue;
    }
};

//----------------------------------------------------------------------------------------------------------------------
enum class TraversalOrder
{
    PreOrder,
    PostOrder,
};



FRG__CLOSE_NAMESPACE(frg);
