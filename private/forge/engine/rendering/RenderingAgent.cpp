#include <forge/Project.h>
#include <forge/engine/rendering/RenderingAgent.h>

#include <forge/engine/rendering/RenderingManager.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
const Vector2f& RenderingAgent::GetTargetResolution() const
{
    return RenderingManager::GetInstance().GetRenderTargetResolution();
}

//----------------------------------------------------------------------------------------------------------------------
TextureHandle RenderingAgent::CreateTexture(const TextureOptions& _options) const
{
    return RenderingManager::GetInstance().CreateTexture(_options);
}

//----------------------------------------------------------------------------------------------------------------------
Texture::Ptr RenderingAgent::GetTexture(TextureHandle _handle) const
{
    return RenderingManager::GetInstance().GetTexture(_handle);
}

//----------------------------------------------------------------------------------------------------------------------
Future<RAMTexture::Ptr> RenderingAgent::DownloadRAMTexture(TextureHandle _handle) const
{
    return RenderingManager::GetInstance().DownloadRAMTexture(_handle);
}



//----------------------------------------------------------------------------------------------------------------------
void RenderingAgent::PushToRender(const AbstractRenderTree::CPtr& _renderTree,
                                  const FloatBox& _spatialData) const
{
    RenderingManager::GetInstance().PushRenderTree(RenderingManager::RenderTargetHandle,
                                                   _renderTree,
                                                   _spatialData);
}

//----------------------------------------------------------------------------------------------------------------------
void RenderingAgent::PushToTexture(TextureHandle _handle,
                                   const AbstractRenderTree::CPtr& _renderTree,
                                   const FloatBox& _spatialData) const
{
    RenderingManager::GetInstance().PushRenderTree(_handle,
                                                   _renderTree,
                                                   _spatialData);
}

FRG__CLOSE_NAMESPACE(frg);
