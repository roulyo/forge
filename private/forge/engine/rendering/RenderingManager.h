#pragma once

#include <SFML/Graphics/RenderTexture.hpp>

#include <forge/engine/async/AsyncAgent.h>
#include <forge/engine/async/Future.h>
#include <forge/engine/math/Types.h>
#include <forge/engine/rendering/RenderTree.h>
#include <forge/engine/rendering/RenderTreeVisitor.h>
#include <forge/engine/rendering/RenderTypes.h>
#include <forge/engine/rendering/resource/Shader.h>
#include <forge/engine/rendering/resource/Texture.h>
#include <forge/engine/system/PtrLess.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class RenderTarget;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractDrawable;

//----------------------------------------------------------------------------------------------------------------------
class RenderingManager : public Singleton<RenderingManager>,
                         public AsyncAgent
{
    FRG__DECL_SINGLETON(RenderingManager);

public:
    static constexpr TextureHandle RenderTargetHandle = 0;

public:
    void InitInstance() override;

    void SetViewport(sf::RenderTarget& _target);

    TextureHandle CreateTexture(const TextureOptions& _options);
    Texture::Ptr GetTexture(TextureHandle _textureHandle);
    Future<RAMTexture::Ptr> DownloadRAMTexture(TextureHandle _textureHandle);

    const Vector2f& GetRenderTargetResolution() const;

    void PushRenderTree(TextureHandle _handle,
                        const AbstractRenderTree::CPtr& _renderTree,
                        const FloatBox& _spatialData);

    void RenderViewport();


private:
    bool IsScreenBound(const AbstractDrawable& _drawable) const;

    void EvalRenderTrees();
    void ClearRenderTrees();
    void RenderDrawables();

private:
    RenderTreeVisitor               m_Visitor;

    Vector<Vector<RenderTreeData>>  m_RenderTrees;
    Vector<Multiset<DrawData>>      m_DepthSortedData;

    Vector<TextureHolder>   m_RenderTextures;
    sf::RenderTarget*       m_Viewport;

    Vector2f                m_Resolution;

    sf::RenderTexture       m_FrontBuffer;
    sf::RenderTexture       m_BackBuffer;
    sf::RenderStates        m_BlendNoneState;

};

FRG__CLOSE_NAMESPACE(frg);
