#include <forge/Project.h>
#include <forge/engine/rendering/RenderTree.h>

#include <SFML/Graphics/Transformable.hpp>

#include <forge/engine/rendering/RenderTreeVisitor.h>
#include <forge/engine/time/api/TimeAPI.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
AbstractRenderTree::AbstractRenderTree(const String& _tag)
    : m_Tag(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
AbstractRenderTree::~AbstractRenderTree()
{
    RemoveChildren();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractRenderTree::AddChild(const AbstractRenderTree::Ptr& _node)
{
    _node->m_Parents.push_back(this);
    m_Children.push_back(_node);
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractRenderTree::RemoveChild(const AbstractRenderTree::Ptr& _child)
{
    auto child = std::find(m_Children.begin(), m_Children.end(), _child);

    if (child != m_Children.end())
    {
        (*child)->m_Parents.erase(std::find((*child)->m_Parents.begin(),
                                            (*child)->m_Parents.end(),
                                            this));
        m_Children.erase(child);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractRenderTree::RemoveChildren()
{
    for (AbstractRenderTree::Ptr& child : m_Children)
    {
        child->m_Parents.erase(std::find(child->m_Parents.begin(),
                                         child->m_Parents.end(),
                                         this));
    }

    m_Children.clear();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractRenderTree::InsertChild(const AbstractRenderTree::Ptr& _node)
{
    Vector<AbstractRenderTree::Ptr> children = m_Children;

    RemoveChildren();
    AddChild(_node);

    for (AbstractRenderTree::Ptr& grandChild : children)
    {
        _node->AddChild(grandChild);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractRenderTree::ExtractChild(const AbstractRenderTree::Ptr& _node)
{
    Vector<AbstractRenderTree::Ptr> children = _node->m_Children;

    _node->RemoveChildren();
    RemoveChild(_node);

    for (AbstractRenderTree::Ptr& child : children)
    {
        AddChild(child);
    }
}

//----------------------------------------------------------------------------------------------------------------------
const Vector<AbstractRenderTree::Ptr>& AbstractRenderTree::GetAllChildren() const
{
    return m_Children;
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractRenderTree::Accept(RenderTreeVisitor& _visitor,
                               TraversalOrder _order) const
{
    FRG__ASSERT(false);
}
//----------------------------------------------------------------------------------------------------------------------
template<class NodeType>
RenderTree<NodeType>::RenderTree(const String& _tag)
    : AbstractRenderTree(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class NodeType>
void RenderTree<NodeType>::Accept(RenderTreeVisitor& _visitor,
                                  TraversalOrder _order) const
{
    if (_order == TraversalOrder::PreOrder)
    {
        _visitor.VisitPreOrder(static_cast<const NodeType*>(this));
    }
    else
    {
        _visitor.VisitPostOrder(static_cast<const NodeType*>(this));
    }
}

//----------------------------------------------------------------------------------------------------------------------
BasicRenderNode::BasicRenderNode()
    : RenderTree<BasicRenderNode>("basic")
{
}

//----------------------------------------------------------------------------------------------------------------------
BasicRenderNode::BasicRenderNode(const String& _tag)
    : RenderTree<BasicRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
SwitchRenderNode::SwitchRenderNode()
    : RenderTree<SwitchRenderNode>("switch")
    , m_Solved(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
SwitchRenderNode::SwitchRenderNode(const String& _tag)
    : RenderTree<SwitchRenderNode>(_tag)
    , m_Solved(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
void SwitchRenderNode::AddChild(const AbstractRenderTree::Ptr& _node, u32 _weight)
{
    const u32 index = m_ChildrenCandidate.size();

    for (u32 i = 0; i < _weight; ++i)
    {
        m_WeightedIndex.push_back(index);
    }

    m_ChildrenCandidate.push_back(_node);
}

//----------------------------------------------------------------------------------------------------------------------
bool SwitchRenderNode::IsSolved() const
{
    return m_Solved;
}

//----------------------------------------------------------------------------------------------------------------------
void SwitchRenderNode::SolveChild(u32 _seed)
{
    m_Solved = true;

    AbstractRenderTree::AddChild(m_ChildrenCandidate[
                                     m_WeightedIndex[
                                         _seed % m_WeightedIndex.size()
                                     ]
                                 ]);
}

//----------------------------------------------------------------------------------------------------------------------
const Vector<AbstractRenderTree::Ptr>& SwitchRenderNode::GetAllChildren() const
{
    return m_ChildrenCandidate;
}

//----------------------------------------------------------------------------------------------------------------------
BaseTransformRenderNode::BaseTransformRenderNode()
    : m_Transform(*new sf::Transformable())
{
}

//----------------------------------------------------------------------------------------------------------------------
BaseTransformRenderNode::~BaseTransformRenderNode()
{
    delete &m_Transform;
}

//----------------------------------------------------------------------------------------------------------------------
void BaseTransformRenderNode::Rotate(f32 _rotation)
{
    m_Transform.rotate(_rotation);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseTransformRenderNode::Scale(const Vector2f& _scale)
{
    m_Transform.scale(_scale.x, _scale.y);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseTransformRenderNode::Translate(const Vector2f& _translation)
{
    m_Transform.move(_translation.x, _translation.y);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseTransformRenderNode::SetRotation(f32 _rotation)
{
    m_Transform.setRotation(_rotation);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseTransformRenderNode::SetScale(const Vector2f& _scale)
{
    m_Transform.setScale(_scale.x, _scale.y);
}

//----------------------------------------------------------------------------------------------------------------------
void BaseTransformRenderNode::SetTranslation(const Vector2f& _translation)
{
    m_Transform.setPosition(_translation.x, _translation.y);
}

//----------------------------------------------------------------------------------------------------------------------
f32 BaseTransformRenderNode::GetRotation() const
{
    return m_Transform.getRotation();
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f BaseTransformRenderNode::GetScale() const
{
    const sf::Vector2f& scale = m_Transform.getScale();

    return { scale.x, scale.y };
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f BaseTransformRenderNode::GetTranslation() const
{
    const sf::Vector2f& translation = m_Transform.getPosition();

    return { translation.x, translation.y };
}

//----------------------------------------------------------------------------------------------------------------------
const sf::Transform& BaseTransformRenderNode::GetTransform() const
{
    return m_Transform.getTransform();
}

//----------------------------------------------------------------------------------------------------------------------
LocalTransformRenderNode::LocalTransformRenderNode()
    : BaseTransformRenderNode()
    , RenderTree<LocalTransformRenderNode>("local_transform")
{
}

//----------------------------------------------------------------------------------------------------------------------
LocalTransformRenderNode::LocalTransformRenderNode(const String& _tag)
    : BaseTransformRenderNode()
    , RenderTree<LocalTransformRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
LocalTransformRenderNode::~LocalTransformRenderNode()
{
}

//----------------------------------------------------------------------------------------------------------------------
WorldTransformRenderNode::WorldTransformRenderNode()
    : BaseTransformRenderNode()
    , RenderTree<WorldTransformRenderNode>("world_transform")
{
}

//----------------------------------------------------------------------------------------------------------------------
WorldTransformRenderNode::WorldTransformRenderNode(const String& _tag)
    : BaseTransformRenderNode()
    , RenderTree<WorldTransformRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
WorldTransformRenderNode::~WorldTransformRenderNode()
{
}

//----------------------------------------------------------------------------------------------------------------------
void WorldTransformRenderNode::Translate(const Vector3f& _translation)
{
    const f32 tz = _translation.z * std::sqrt(1.25f);

    m_Transform.move(_translation.x - tz, _translation.y - tz);
    m_Translation3D.x += _translation.x;
    m_Translation3D.y += _translation.y;
    m_Translation3D.z += _translation.z;
}

//----------------------------------------------------------------------------------------------------------------------
void WorldTransformRenderNode::SetTranslation(const Vector3f& _translation)
{
    const f32 tz = _translation.z * std::sqrt(1.25f);

    m_Transform.setPosition(_translation.x - tz, _translation.y - tz);
    m_Translation3D = _translation;
}

//----------------------------------------------------------------------------------------------------------------------
ScreenTransformRenderNode::ScreenTransformRenderNode()
    : BaseTransformRenderNode()
    , RenderTree<ScreenTransformRenderNode>("screen_transform")
{
}

//----------------------------------------------------------------------------------------------------------------------
ScreenTransformRenderNode::ScreenTransformRenderNode(const String& _tag)
    : BaseTransformRenderNode()
    , RenderTree<ScreenTransformRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
ScreenTransformRenderNode::~ScreenTransformRenderNode()
{
}

//----------------------------------------------------------------------------------------------------------------------
ShaderMaterialRenderNode::ShaderMaterialRenderNode()
    : RenderTree<ShaderMaterialRenderNode>("shader_material")
    , m_OverrideNextMaterials(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
ShaderMaterialRenderNode::ShaderMaterialRenderNode(const String& _tag)
    : RenderTree<ShaderMaterialRenderNode>(_tag)
    , m_OverrideNextMaterials(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
BasicDrawableRenderNode::BasicDrawableRenderNode()
    : RenderTree<BasicDrawableRenderNode>("drawable")
{
}

//----------------------------------------------------------------------------------------------------------------------
BasicDrawableRenderNode::BasicDrawableRenderNode(const String& _tag)
    : RenderTree<BasicDrawableRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
GUIDrawableRenderNode::GUIDrawableRenderNode()
    : RenderTree<GUIDrawableRenderNode>("gui_drawable")
{
}

//----------------------------------------------------------------------------------------------------------------------
GUIDrawableRenderNode::GUIDrawableRenderNode(const String& _tag)
    : RenderTree<GUIDrawableRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
ParticleRenderNode::ParticleRenderNode()
    : RenderTree<ParticleRenderNode>("particle")
{
}

//----------------------------------------------------------------------------------------------------------------------
ParticleRenderNode::ParticleRenderNode(const String& _tag)
    : RenderTree<ParticleRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
SpriteRenderNode::SpriteRenderNode()
    : RenderTree<SpriteRenderNode>("sprite")
{
}

//----------------------------------------------------------------------------------------------------------------------
SpriteRenderNode::SpriteRenderNode(const String& _tag)
    : RenderTree<SpriteRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
BaseAnimationRenderNode::BaseAnimationRenderNode()
    : m_Clock(AnimationClock::Local)
    , m_IsPlaying(false)
    , m_StartTimeMs(0)
    , m_CurrentAnimation(nullptr)
{
}

//----------------------------------------------------------------------------------------------------------------------
void BaseAnimationRenderNode::AddAnimation(const Animation::Ptr& _animation)
{
    FRG__ASSERT(m_Animations.find(_animation->GetDataNameId()) == m_Animations.end());

    m_Animations[_animation->GetDataNameId()] = _animation;
}

//----------------------------------------------------------------------------------------------------------------------
void BaseAnimationRenderNode::PlayAnimation(const AnimationId& _animationId)
{
    if (   m_CurrentAnimation == nullptr
        || m_CurrentAnimation->GetDataNameId() != _animationId)
    {
        // FRG__ASSERT(m_Animations.find(_animationId) != m_Animations.end());
        FRG__RETURN_IF(m_Animations.find(_animationId) == m_Animations.end());

        m_CurrentAnimation = m_Animations.at(_animationId).get();
        m_IsPlaying = true;
        m_StartTimeMs = TimeAPI::GetGameTimeMilliseconds();
    }
}

//----------------------------------------------------------------------------------------------------------------------
void BaseAnimationRenderNode::StopAnimation()
{
    if (m_CurrentAnimation != nullptr)
    {
        m_CurrentAnimation = nullptr;
        m_IsPlaying = false;
        m_StartTimeMs = 0;
    }
}

//----------------------------------------------------------------------------------------------------------------------
Vector2u BaseAnimationRenderNode::GetFrameModifier(u64 _t) const
{
    return m_CurrentAnimation->GetKeyframe(_t);
}

//----------------------------------------------------------------------------------------------------------------------
u64 BaseAnimationRenderNode::GetAnimationTimeMs() const
{
    return m_CurrentAnimation->GetDurationMs();
}

//----------------------------------------------------------------------------------------------------------------------
AnimationRenderNode::AnimationRenderNode()
    : BaseAnimationRenderNode()
    , RenderTree<AnimationRenderNode>("animation")
{
}

//----------------------------------------------------------------------------------------------------------------------
AnimationRenderNode::AnimationRenderNode(const String& _tag)
    : BaseAnimationRenderNode()
    , RenderTree<AnimationRenderNode>(_tag)
{
}

//----------------------------------------------------------------------------------------------------------------------
DiscreteAnimationRenderNode::DiscreteAnimationRenderNode()
    : BaseAnimationRenderNode()
    , RenderTree<DiscreteAnimationRenderNode>("discrete_animation")
    , m_IsLooping(false)
    , m_LoopStartTimeMs(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
DiscreteAnimationRenderNode::DiscreteAnimationRenderNode(const String& _tag)
    : BaseAnimationRenderNode()
    , RenderTree<DiscreteAnimationRenderNode>(_tag)
    , m_IsLooping(false)
    , m_LoopStartTimeMs(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
void DiscreteAnimationRenderNode::PlayAnimation(const AnimationId& _animationId)
{
    StartIdleTimer();
    BaseAnimationRenderNode::PlayAnimation(_animationId);
}

//----------------------------------------------------------------------------------------------------------------------
void DiscreteAnimationRenderNode::StartLoop()
{
    m_IsLooping = true;
    m_LoopStartTimeMs = TimeAPI::GetGameTimeMilliseconds();
}

//----------------------------------------------------------------------------------------------------------------------
void DiscreteAnimationRenderNode::StopLoop()
{
    m_IsLooping = false;
    m_LoopStartTimeMs = 0;
}

//----------------------------------------------------------------------------------------------------------------------
void DiscreteAnimationRenderNode::StartAnimationTimer()
{
    m_Timer.Start(GetAnimationTimeMs());
}

//----------------------------------------------------------------------------------------------------------------------
void DiscreteAnimationRenderNode::StartIdleTimer()
{
    FRG__ASSERT(m_APM != 0);

    const f32 tpa = 60'000 / m_APM;
    const f32 P = static_cast<f32>(rand() % 100'000) / 100'000.0f;
    const u32 nextTimeMs = static_cast<u32>(-std::log(1 - P) * tpa);

    m_Timer.Start(nextTimeMs);
}

//----------------------------------------------------------------------------------------------------------------------
AbstractRenderTree::Ptr RenderTreeUtils::GetFirstNodeByTag(const AbstractRenderTree::Ptr& _tree,
                                                           const String& _tag)
{
    FRG__RETURN_IF(_tree->GetTag() == _tag, _tree);

    AbstractRenderTree::Ptr result = nullptr;

    for (const AbstractRenderTree::Ptr& child : _tree->GetAllChildren())
    {
        result = GetFirstNodeByTag(child, _tag);

        FRG__BREAK_IF(result != nullptr);
    }

    return result;
}

//----------------------------------------------------------------------------------------------------------------------
Vector<AbstractRenderTree::Ptr> RenderTreeUtils::GetNodesByTag(const AbstractRenderTree::Ptr& _tree,
                                                               const String& _tag)
{
    Vector<AbstractRenderTree::Ptr> result;

    _GetNodesByTag(_tree, _tag, result);

    return result;
}

void RenderTreeUtils::_GetNodesByTag(const AbstractRenderTree::Ptr& _tree,
                                     const String& _tag,
                                     Vector<AbstractRenderTree::Ptr>& _result)
{
    if (_tree->GetTag() == _tag)
    {
        _result.push_back(_tree);
    }

    for (const AbstractRenderTree::Ptr& child : _tree->GetAllChildren())
    {
        _GetNodesByTag(child, _tag, _result);
    }
}

//----------------------------------------------------------------------------------------------------------------------
template class RenderTree<SwitchRenderNode>;
template class RenderTree<LocalTransformRenderNode>;
template class RenderTree<WorldTransformRenderNode>;
template class RenderTree<ScreenTransformRenderNode>;
template class RenderTree<ShaderMaterialRenderNode>;
template class RenderTree<BasicDrawableRenderNode>;
template class RenderTree<ParticleRenderNode>;
template class RenderTree<SpriteRenderNode>;
template class RenderTree<AnimationRenderNode>;
template class RenderTree<DiscreteAnimationRenderNode>;

FRG__CLOSE_NAMESPACE(frg);
