#include <forge/Project.h>
#include <forge/engine/presentation/PresentationManager.h>

#include <forge/engine/presentation/DataAccessor.h>
#include <forge/engine/presentation/View.h>
#include <forge/engine/presentation/GUIBehavior.h>
#include <forge/engine/presentation/GUIWidget.h>
#include <forge/engine/rendering/drawable/Drawable.h>

#include <SFML/Graphics/Sprite.hpp>

FRG__OPEN_NAMESPACE(frg);

namespace static_utils { //---------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

static bool IsMouseOperation(const SystemEvent& _event)
{
    return _event.Type == _event.MouseWheelMoved        //!< The mouse wheel was scrolled (data in event.mouseWheel) (deprecated)
        || _event.Type == _event.MouseWheelScrolled     //!< The mouse wheel was scrolled (data in event.mouseWheelScroll)
        || _event.Type == _event.MouseButtonPressed     //!< A mouse button was pressed (data in event.mouseButton)
        || _event.Type == _event.MouseButtonReleased    //!< A mouse button was released (data in event.mouseButton)
        || _event.Type == _event.MouseMoved;             //!< The mouse cursor moved (data in event.mouseMove)
        // || _event.Type == _event.MouseEntered           //!< The mouse cursor entered the area of the window (no data)
        // || _event.Type == _event.MouseLeft              //!< The mouse cursor left the area of the window (no data)
}

static Vector2f GetMouseCoord(const SystemEvent& _event)
{
    if (_event.Type == _event.MouseWheelMoved)
    {
        return { static_cast<f32>(_event.MouseWheel.x), static_cast<f32>(_event.MouseWheel.y) };
    }
    else if (_event.Type == _event.MouseWheelScrolled)
    {
        return { static_cast<f32>(_event.MouseWheelScroll.x), static_cast<f32>(_event.MouseWheelScroll.y) };
    }
    else if (_event.Type == _event.MouseButtonPressed || _event.Type == _event.MouseButtonReleased)
    {
        return { static_cast<f32>(_event.MouseButton.x), static_cast<f32>(_event.MouseButton.y) };
    }
    else if (_event.Type == _event.MouseMoved)
    {
        return { static_cast<f32>(_event.MouseMove.x), static_cast<f32>( _event.MouseMove.y) };
    }
    else
    {
        FRG__ASSERT(false);
        return { 0, 0 };
    }
}

} // namespace static_utils

//----------------------------------------------------------------------------------------------------------------------
static constexpr u32 k_RenderTargetSize = 1080;

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::InitInstance()
{
    m_DataAccessorCounter.fill(0);

    m_Viewport = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::SetViewport(sf::RenderTarget& _target)
{
    m_Viewport = &_target;

    u32 width = static_cast<u32>(  static_cast<f32>(m_Viewport->getSize().x)
                                 / static_cast<f32>(m_Viewport->getSize().y)
                                 * static_cast<f32>(k_RenderTargetSize));

    m_InternalTarget.create(width, k_RenderTargetSize);

    m_RenderingScale.w =  static_cast<f32>(m_Viewport->getSize().x)
                        / static_cast<f32>(m_InternalTarget.getSize().x);

    m_RenderingScale.h =  static_cast<f32>(m_Viewport->getSize().y)
                        / static_cast<f32>(m_InternalTarget.getSize().y);
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f PresentationManager::ScreenToGUIPixel(const Vector2f& _coordScreen)
{
    return { _coordScreen.x / m_RenderingScale.w,
             _coordScreen.y / m_RenderingScale.h };
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f PresentationManager::ScreenToGUIRelative(const Vector2f& _coordScreen)
{
    return { _coordScreen.x / static_cast<f32>(m_Viewport->getSize().x) * 100.0f,
             _coordScreen.y / static_cast<f32>(m_Viewport->getSize().y) * 100.0f };
}

//----------------------------------------------------------------------------------------------------------------------
bool PresentationManager::HandleSystemEvent(const SystemEvent& _event)
{
    PresentationManager::BehaviorResult result{ false, false };

    for (View* const view : m_Views)
    {
        result |= CheckBehaviors(_event, view->GetWidgets());

        if (result.Triggered)
        {
            return true;
        }
    }

    return result.Captured;
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::RegisterDataAccessor(const AbstractDataAccessor& _dataAccessor)
{
    const ComponentSignature& sig = _dataAccessor.GetSignature();

    u64 debug = sig.to_ulong();

    m_DataProfile |= sig;

    for (u32 i = 0; i < sig.size(); ++i)
    {
        if (sig[i] == 0b1)
        {
            ComponentSignature flag = 0;
            flag[i] = 0b1;
            m_DataAccessorCounter[i] += 1;
            m_EntityDB[flag];
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::UnregisterDataAccessor(const AbstractDataAccessor& _dataAccessor)
{
    const ComponentSignature& sig = _dataAccessor.GetSignature();

    for (u32 i = 0; i < sig.size(); ++i)
    {
        if (sig[i] == 0b1)
        {
            m_DataAccessorCounter[i] -= 1;

            if (m_DataAccessorCounter[i] == 0)
            {
                ComponentSignature flag = 0;
                flag[i] = 0b1;
                m_DataProfile ^= flag;

                auto it = m_EntityDB.find(flag);

                if (it != m_EntityDB.cend())
                {
                    m_EntityDB.erase(it);
                }
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::FillEntityDB(const EntityMapType<ComponentSignature, Vector<Entity::Ptr>>& _entityMap)
{
    FRG__RETURN_IF(m_DataProfile == 0);

    for (Pair<const ComponentSignature, Vector<Entity::CPtr>>& mapEntry : m_EntityDB)
    {
        mapEntry.second.clear();
    }

    for (u32 i = 0; i < m_DataProfile.size(); ++i)
    {
        FRG__CONTINUE_IF(m_DataProfile[i] == 0b0);

        ComponentSignature flag = 0;
        flag[i] = 0b1;

        for (const Pair<ComponentSignature, Vector<Entity::Ptr>>& mapEntry : _entityMap)
        {
            const ComponentSignature& signature = mapEntry.first;

            if ((flag & signature) != 0)
            {
                Vector<Entity::CPtr>& column = m_EntityDB[flag];

                column.insert(column.end(), mapEntry.second.cbegin(),  mapEntry.second.cend());
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
const Vector<Entity::CPtr>& PresentationManager::GetEntityColumn(const ComponentSignature& _signature) const
{
    return m_EntityDB.at(_signature);
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::OpenView(View* _view)
{
    auto it = std::find(m_Views.cbegin(), m_Views.cend(), _view);

    FRG__RETURN_IF(it != m_Views.cend());

    _view->ComputeRenderedGeometry({ static_cast<f32>(m_InternalTarget.getSize().x),
                                     static_cast<f32>(m_InternalTarget.getSize().y) });
    m_Views.push_back(_view);
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::CloseView(View* _view)
{
    m_Views.erase(std::find(m_Views.cbegin(), m_Views.cend(), _view));
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::SafeOpenView(View* _view)
{
    m_ViewsToOpen.push_back(_view);
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::SafeCloseView(View* _view)
{
    m_ViewsToClose.push_back(_view);
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::OpenViews()
{
    for (View* view : m_ViewsToOpen)
    {
        OpenView(view);
    }

    m_ViewsToOpen.clear();
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::CloseViews()
{
    for (View* view : m_ViewsToClose)
    {
        CloseView(view);
    }

    m_ViewsToClose.clear();
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::RenderGUI()
{
    CloseViews();
    OpenViews();

    m_InternalTarget.clear(sf::Color(0, 0, 0, 0));

    for (View* const view : m_Views)
    {
        view->ComputeRenderedGeometry({ static_cast<f32>(m_InternalTarget.getSize().x),
                                        static_cast<f32>(m_InternalTarget.getSize().y) });
        RenderElements(view->GetWidgets());
    }

    m_InternalTarget.display();

    sf::Sprite internalTexture(m_InternalTarget.getTexture());
    internalTexture.setScale(m_RenderingScale.w, m_RenderingScale.h);

    m_Viewport->draw(internalTexture);
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationManager::RenderElements(const Vector<GUIWidget*>&  _elements)
{
    for (GUIWidget* element : _elements)
    {
        for (GUIWidget::AbstractGUIDrawable* drawable : element->GetDrawables())
        {
            m_InternalTarget.draw(drawable->GetDrawableData());
        }

        RenderElements(element->GetChildren());
    }
}

//----------------------------------------------------------------------------------------------------------------------
PresentationManager::BehaviorResult PresentationManager::CheckBehaviors(const SystemEvent& _event,
                                                                        const Vector<GUIWidget*>&  _elements)
{
    PresentationManager::BehaviorResult result{ false, false };

    for (GUIWidget* element : _elements)
    {
        for (AbstractGUIBehavior* behavior : element->GetBehaviors())
        {
            if (behavior->ShouldCaptureEvent(_event))
            {
                behavior->Trigger(_event);

                result.Captured = true;
                result.Triggered = true;
            }
        }

        result |= CheckBehaviors(_event, element->GetChildren());

        FRG__RETURN_IF(result.Triggered, result);

        if (!result.Captured && static_utils::IsMouseOperation(_event))
        {
            Vector2f guiScaledCoord = ScreenToGUIPixel(static_utils::GetMouseCoord(_event));

            result.Captured = element->ContainsPixel(guiScaledCoord);
        }
    }

    return result;
}

FRG__CLOSE_NAMESPACE(frg);