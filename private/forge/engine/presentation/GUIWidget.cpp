#include <forge/Project.h>
#include <forge/engine/presentation/GUIWidget.h>

#include <forge/engine/presentation/GUIBehavior.h>
#include <forge/engine/rendering/drawable/Drawable.h>

#include <algorithm>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
GUIWidget::GUIWidget()
    : m_Parent(nullptr)
{
}

//----------------------------------------------------------------------------------------------------------------------
GUIWidget::~GUIWidget()
{
    for (AbstractGUIBehavior* behavior : m_Behaviors)
    {
        delete behavior;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void GUIWidget::AddDrawable(AbstractGUIDrawable* _drawable)
{
    m_Drawables.push_back(_drawable);
}

//----------------------------------------------------------------------------------------------------------------------
void GUIWidget::AddChild(GUIWidget* _child)
{
    _child->SetParent(this);
    m_Children.push_back(_child);
}

//----------------------------------------------------------------------------------------------------------------------
void GUIWidget::RemoveChild(GUIWidget* _child)
{
    auto childIt = std::find(m_Children.begin(), m_Children.end(), _child);

    if (childIt != m_Children.end())
    {
        (*childIt)->SetParent(nullptr);
        m_Children.erase(childIt);
    }
}

//----------------------------------------------------------------------------------------------------------------------
void GUIWidget::ClearChildren()
{
    for (GUIWidget* child : m_Children)
    {
        child->SetParent(nullptr);
    }

    m_Children.clear();
}

//----------------------------------------------------------------------------------------------------------------------
void GUIWidget::ComputeRenderedGeometry(const Vector2f& _parentPosition,
                                         const Vector2f& _parentSize)
{
    bool wasdirty = ComputePixelData(_parentPosition, _parentSize);
    OnRenderedGeometryComputed(GetPixelPosition(), GetPixelSize());

    for (GUIWidget::AbstractGUIDrawable* drawable : m_Drawables)
    {
        drawable->OnElementGeometryComputed(GetPixelPosition(), GetPixelSize());
    }

    for (GUIWidget* child : m_Children)
    {
        if (wasdirty)
        {
            child->SetDirty();
        }

        child->ComputeRenderedGeometry(GetPixelPosition(), GetPixelSize());
    }
}

//----------------------------------------------------------------------------------------------------------------------
void GUIWidget::OnRenderedGeometryComputed(const Vector2f& _position,
                                           const Vector2f& _size)
{
}

//----------------------------------------------------------------------------------------------------------------------
GUIWidget::AbstractGUIDrawable::AbstractGUIDrawable()
    : m_Drawable(nullptr)
    , Gravity(GUIGravity::Center)
    , RelativePadding({ 0.0f, 0.0f })
{
}

//----------------------------------------------------------------------------------------------------------------------
GUIWidget::AbstractGUIDrawable::~AbstractGUIDrawable()
{
}

//----------------------------------------------------------------------------------------------------------------------
void GUIWidget::AbstractGUIDrawable::OnElementGeometryComputed(const Vector2f& _parentPixelPosition,
                                                            const Vector2f& _parentPixelSize) const
{
    Vector2f pixelPadding = { _parentPixelSize.x * 0.01f * RelativePadding.x,
                              _parentPixelSize.y * 0.01f * RelativePadding.y };

    Vector2f pixelSize = m_Drawable->GetOriginalSize();

    f32 xCoord = ((_parentPixelSize.x - pixelSize.w) * 0.5f) + _parentPixelPosition.x;
    f32 yCoord = ((_parentPixelSize.y - pixelSize.h) * 0.5f) + _parentPixelPosition.y;

    if (Gravity != GUIGravity::Center)
    {
        if ((Gravity & GUIGravity::Top) != 0)
        {
            yCoord = pixelPadding.y + _parentPixelPosition.y;
        }
        else if ((Gravity & GUIGravity::Bottom) != 0)
        {
            yCoord = _parentPixelSize.h - pixelSize.h - pixelPadding.y + _parentPixelPosition.y;
        }

        if ((Gravity & GUIGravity::Left) != 0)
        {
            xCoord = pixelPadding.x + _parentPixelPosition.x;
        }
        else if ((Gravity & GUIGravity::Right) != 0)
        {
            xCoord = _parentPixelSize.w - pixelSize.w - pixelPadding.x + _parentPixelPosition.x;
        }
    }

    m_Drawable->SetScreenCoord(xCoord, yCoord);
}

//----------------------------------------------------------------------------------------------------------------------
const GUIWidget::AbstractGUIDrawable::DrawableData& GUIWidget::AbstractGUIDrawable::GetDrawableData() const
{
    return m_Drawable->GetDrawableData();
}

FRG__CLOSE_NAMESPACE(frg);