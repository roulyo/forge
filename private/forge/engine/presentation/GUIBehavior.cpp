#include <forge/Project.h>
#include <forge/engine/presentation/GUIBehavior.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
AbstractGUIBehavior::AbstractGUIBehavior(const BehaviorCallback& _callback)
    : m_Callback(_callback)
{
}

//----------------------------------------------------------------------------------------------------------------------
AbstractGUIBehavior::~AbstractGUIBehavior()
{
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractGUIBehavior::Trigger(const SystemEvent& _event)
{
    m_Callback(m_Owner, _event);
}

FRG__CLOSE_NAMESPACE(frg);
