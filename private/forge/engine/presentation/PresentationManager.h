#pragma once

#include <bitset>

#include <forge/engine/ecs/Component.h>
#include <forge/engine/ecs/Entity.h>

#include <SFML/Graphics/RenderTexture.hpp>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class RenderTarget;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class... Cs> class DataAccessor;
typedef DataAccessor<>    AbstractDataAccessor;

//----------------------------------------------------------------------------------------------------------------------
class GUIWidget;
class SystemEvent;
class View;

//----------------------------------------------------------------------------------------------------------------------
class PresentationManager : public rtb::Singleton<PresentationManager>
{
    FRG__DECL_SINGLETON(PresentationManager);

public:
    void InitInstance() override;

    void SetViewport(sf::RenderTarget& _target);

    Vector2f ScreenToGUIPixel(const Vector2f& _coordScreen);
    Vector2f ScreenToGUIRelative(const Vector2f& _coordScreen);

    bool HandleSystemEvent(const SystemEvent& _event);

    void RegisterDataAccessor(const AbstractDataAccessor& _dataAccessor);
    void UnregisterDataAccessor(const AbstractDataAccessor& _dataAccessor);

    void FillEntityDB(const EntityMapType<ComponentSignature, Vector<Entity::Ptr>>& _entityMap);

    const Vector<Entity::CPtr>& GetEntityColumn(const ComponentSignature& _signature) const;

    void OpenView(View* _view);
    void CloseView(View* _view);
    void SafeOpenView(View* _view);
    void SafeCloseView(View* _view);
    void OpenViews();
    void CloseViews();

    void RenderGUI();

private:
    struct BehaviorResult
    {
        bool Captured;
        bool Triggered;

        BehaviorResult& operator|=(const BehaviorResult& _other)
        {
            Captured |= _other.Captured;
            Triggered |= _other.Triggered;

            return *this;
        }
    };

private:
    BehaviorResult CheckBehaviors(const SystemEvent& _event, const Vector<GUIWidget*>& _elements);
    void RenderElements(const Vector<GUIWidget*>& _elements);

public:
    FRG__CLASS_ATTR___(Vector2f, RenderingScale);
    FRG__CLASS_ATTR___(Vector<View*>, Views);
    FRG__CLASS_ATTR___(Vector<View*>, ViewsToOpen);
    FRG__CLASS_ATTR___(Vector<View*>, ViewsToClose);

private:
    Map<ComponentSignature, Vector<Entity::CPtr>>   m_EntityDB;
    ComponentSignature                              m_DataProfile;
    Array<u32, 64>                                  m_DataAccessorCounter;

private:
    sf::RenderTexture   m_InternalTarget;
    sf::RenderTarget*   m_Viewport;

};

FRG__CLOSE_NAMESPACE(frg);