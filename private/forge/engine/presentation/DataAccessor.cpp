#include <forge/Project.h>
#include <forge/engine/presentation/DataAccessor.h>

#include <forge/engine/presentation/PresentationManager.h>

FRG__OPEN_NAMESPACE(frg);

// //----------------------------------------------------------------------------------------------------------------------
// DataAccessor<>::ReadOnlyEntity::ReadOnlyEntity(const Entity::Ptr& _entity)
//     : m_Entity(*_entity)
// {
// }

// //----------------------------------------------------------------------------------------------------------------------
// DataAccessor<>::ReadOnlyEntity::ReadOnlyEntity(const ReadOnlyEntity& _entity)
//     : m_Entity(_entity.m_Entity)
// {
// }

// //----------------------------------------------------------------------------------------------------------------------
// const DataAccessor<>::ReadOnlyEntity& DataAccessor<>::ReadOnlyEntity::operator=(ReadOnlyEntity _entity)
// {
//     const_cast<Entity&>(m_Entity) = _entity.m_Entity;
//     return *this;
// }

// //----------------------------------------------------------------------------------------------------------------------
// const Entity& DataAccessor<>::ReadOnlyEntity::Access() const
// {
//     return m_Entity;
// }

//----------------------------------------------------------------------------------------------------------------------
DataAccessor<>::DataAccessor()
    : m_Signature(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
DataAccessor<>::~DataAccessor()
{
    PresentationManager::GetInstance().UnregisterDataAccessor(*this);
}

//----------------------------------------------------------------------------------------------------------------------
void DataAccessor<>::Init() const
{
    PresentationManager::GetInstance().RegisterDataAccessor(*this);
}

//----------------------------------------------------------------------------------------------------------------------
bool DataAccessor<>::DoesTarget(const ComponentSignature& _signature) const
{
    return (m_Signature & _signature) != 0;
}

//----------------------------------------------------------------------------------------------------------------------
const Vector<Entity::CPtr>& DataAccessor<>::GetColumn(const ComponentSignature& _signature) const
{
    FRG__ASSERT(DoesTarget(_signature));

    return PresentationManager::GetInstance().GetEntityColumn(_signature);
}

FRG__CLOSE_NAMESPACE(frg);