#include <forge/Project.h>
#include <forge/engine/presentation/View.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
View::View()
{
}

//----------------------------------------------------------------------------------------------------------------------
void View::AddWidget(GUIWidget* _widget)
{
    m_Widgets.push_back(_widget);
}

//----------------------------------------------------------------------------------------------------------------------
void View::OnViewOpened()
{
}

//----------------------------------------------------------------------------------------------------------------------
void View::OnViewClosed()
{
}

//----------------------------------------------------------------------------------------------------------------------
void View::ComputeRenderedGeometry(const Vector2f& _targetSize)
{
    bool wasDirty = ComputePixelData({ 0.0f, 0.0f }, _targetSize);

    for (GUIWidget* element : m_Widgets)
    {
        if (wasDirty)
        {
            element->SetDirty();
        }

        element->ComputeRenderedGeometry(GetPixelPosition(), GetPixelSize());
    }
}

FRG__CLOSE_NAMESPACE(frg);
