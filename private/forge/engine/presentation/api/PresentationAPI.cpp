#include <forge/Project.h>
#include <forge/engine/presentation/api/PresentationAPI.h>

#include <forge/engine/presentation/PresentationManager.h>
#include <forge/engine/window/WindowManager.h>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
Vector2f PresentationAPI::GetCursorPosition()
{
    return WindowManager::GetInstance().GetCursorPosition();
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f PresentationAPI::ScreenToGUIPixel(const Vector2f& _coordScreen)
{
    return PresentationManager::GetInstance().ScreenToGUIPixel(_coordScreen);
}

//----------------------------------------------------------------------------------------------------------------------
Vector2f PresentationAPI::ScreenToGUIRelative(const Vector2f& _coordScreen)
{
    return PresentationManager::GetInstance().ScreenToGUIRelative(_coordScreen);
}

//----------------------------------------------------------------------------------------------------------------------
void PresentationAPI::SetCursor(const Cursor::CPtr& _cursor)
{
    WindowManager::GetInstance().SetCursor(_cursor);
}

FRG__CLOSE_NAMESPACE(frg)
