#include <forge/Project.h>
#include <forge/engine/presentation/ViewController.h>

#include <forge/engine/presentation/PresentationManager.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
void AbstractViewController::Start()
{
    OnStart();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractViewController::Stop()
{
    OnStop();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractViewController::OnStart()
{}

//----------------------------------------------------------------------------------------------------------------------
void AbstractViewController::OnStop()
{}

//----------------------------------------------------------------------------------------------------------------------
void AbstractViewController::OpenView(View* _view) const
{
    _view->SetOwner(this);
    PresentationManager::GetInstance().OpenView(_view);
    _view->OnViewOpened();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractViewController::CloseView(View* _view) const
{
    _view->OnViewClosed();
    PresentationManager::GetInstance().CloseView(_view);
    _view->SetOwner(nullptr);
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractViewController::SafeOpenView(View* _view) const
{
    _view->SetOwner(this);
    PresentationManager::GetInstance().SafeOpenView(_view);
    _view->OnViewOpened();
}

//----------------------------------------------------------------------------------------------------------------------
void AbstractViewController::SafeCloseView(View* _view) const
{
    _view->OnViewClosed();
    PresentationManager::GetInstance().SafeCloseView(_view);
    _view->SetOwner(nullptr);
}

FRG__CLOSE_NAMESPACE(frg);