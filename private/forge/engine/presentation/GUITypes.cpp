#include <forge/Project.h>
#include <forge/engine/presentation/GUITypes.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
GUISpatializable::GUISpatializable()
    : m_Gravity(GUIGravity::TopLeft)
    , m_RelativePadding({ 0.0f, 0.0f })
    , m_RelativeSize({ 100.0f, 100.0f })
    , m_IsSizeRelative(true)
    , m_IsPaddingRelative(true)
    , m_IsReady(false)
{
}

//----------------------------------------------------------------------------------------------------------------------
void GUISpatializable::SetDirty()
{
    m_IsReady = false;
}

//----------------------------------------------------------------------------------------------------------------------
bool GUISpatializable::ContainsPixel(const frg::Vector2f& _pxCoord) const
{
    FRG__RETURN_IF(!IsReady(), false);

    return m_RenderedGeometry.Contains(_pxCoord);
}

//----------------------------------------------------------------------------------------------------------------------
void GUISpatializable::SetGravity(GUIGravity _gravity)
{
    m_Gravity = _gravity;

    m_IsReady = false;
}

//----------------------------------------------------------------------------------------------------------------------
void GUISpatializable::SetRelativeSize(const Vector2f& _relSize)
{
    m_RelativeSize = _relSize;

    m_IsReady = false;
    m_IsSizeRelative = true;
}

//----------------------------------------------------------------------------------------------------------------------
void GUISpatializable::SetRelativePadding(const Vector2f& _relPadding)
{
    m_RelativePadding = _relPadding;

    m_IsReady = false;
    m_IsPaddingRelative = true;
}

//----------------------------------------------------------------------------------------------------------------------
void GUISpatializable::SetPixelSize(const Vector2f& _pxSize)
{
    m_PixelSize = _pxSize;

    m_IsReady = false;
    m_IsSizeRelative = false;
}

//----------------------------------------------------------------------------------------------------------------------
void GUISpatializable::SetPixelPadding(const Vector2f& _pxPadding)
{
    m_PixelPadding = _pxPadding;

    m_IsReady = false;
    m_IsPaddingRelative = false;
}

//----------------------------------------------------------------------------------------------------------------------
GUIGravity GUISpatializable::GetGravity() const
{
    return m_Gravity;
}

//----------------------------------------------------------------------------------------------------------------------
const Vector2f& GUISpatializable::GetRelativeSize() const
{
    FRG__ASSERT(m_IsSizeRelative || m_IsReady);

    return m_RelativeSize;
}

//----------------------------------------------------------------------------------------------------------------------
const Vector2f& GUISpatializable::GetRelativePadding() const
{
    FRG__ASSERT(m_IsPaddingRelative || m_IsReady);

    return m_RelativePadding;
}

//----------------------------------------------------------------------------------------------------------------------
const Vector2f& GUISpatializable::GetRelativePosition() const
{
    FRG__ASSERT(m_IsReady);

    return m_RelativePosition;
}

//----------------------------------------------------------------------------------------------------------------------
const Vector2f& GUISpatializable::GetPixelSize() const
{
    FRG__ASSERT(!m_IsSizeRelative || m_IsReady);

    return m_PixelSize;
}

//----------------------------------------------------------------------------------------------------------------------
const Vector2f& GUISpatializable::GetPixelPadding() const
{
    FRG__ASSERT(!m_IsPaddingRelative || m_IsReady);

    return m_PixelPadding;
}

//----------------------------------------------------------------------------------------------------------------------
const Vector2f& GUISpatializable::GetPixelPosition() const
{
    FRG__ASSERT(m_IsReady);

    return m_PixelPosition;
}

//----------------------------------------------------------------------------------------------------------------------
bool GUISpatializable::ComputePixelData(const Vector2f& _parentPosition,
                                        const Vector2f& _parentSize)

{
    FRG__RETURN_IF(m_IsReady, false);

    if (m_IsSizeRelative)
    {
        m_PixelSize = { _parentSize.x * 0.01f * m_RelativeSize.w,
                        _parentSize.y * 0.01f * m_RelativeSize.h };
    }
    else
    {
        m_RelativeSize = { 100.0f * m_PixelSize.w / _parentSize.x,
                           100.0f * m_PixelSize.h / _parentSize.y };
    }

    if (m_IsPaddingRelative)
    {
        m_PixelPadding = { _parentSize.x * 0.01f * m_RelativePadding.x,
                           _parentSize.y * 0.01f * m_RelativePadding.y };
    }
    else
    {
        m_RelativePadding = { 100.0f * m_PixelPadding.w / _parentSize.x,
                              100.0f * m_PixelPadding.h / _parentSize.y };
    }

    Vector2f relativePos = { 50.0f - m_RelativeSize.w * 0.5f,
                             50.0f - m_RelativeSize.h * 0.5f };

    m_PixelPosition = { _parentSize.x * 0.01f * relativePos.x + _parentPosition.x,
                        _parentSize.y * 0.01f * relativePos.y + _parentPosition.y };

    if (m_Gravity != GUIGravity::Center)
    {
        if ((m_Gravity & GUIGravity::Top) != 0)
        {
            m_RelativePosition.y = m_RelativePadding.y;
            m_PixelPosition.y = m_PixelPadding.y + _parentPosition.y;
        }
        else if ((m_Gravity & GUIGravity::Bottom) != 0)
        {
            m_RelativePosition.y = 100.0f - m_RelativeSize.h - m_RelativePadding.y;
            m_PixelPosition.y = _parentSize.h - m_PixelSize.h - m_PixelPadding.y + _parentPosition.y;
        }

        if ((m_Gravity & GUIGravity::Left) != 0)
        {
            m_RelativePosition.x = m_RelativePadding.x;
            m_PixelPosition.x = m_PixelPadding.x + _parentPosition.x;
        }
        else if ((m_Gravity & GUIGravity::Right) != 0)
        {
            m_RelativePosition.x = 100.0f - m_RelativeSize.w - m_RelativePadding.x;
            m_PixelPosition.x = _parentSize.w - m_PixelSize.w - m_PixelPadding.x + _parentPosition.x;
        }
    }
    else
    {
        // m_RelativePosition.x = 50.0f + m_RelativePadding.x;
        // m_PixelPosition.x = _parentSize.w * 0.5f + m_PixelPadding.x + _parentPosition.x;
        // m_RelativePosition.y = 50.0f + m_RelativePadding.y;
        // m_PixelPosition.y = _parentSize.h * 0.5f + m_PixelPadding.y + _parentPosition.y;
    }

    m_RenderedGeometry = { m_PixelPosition, m_PixelSize };

    m_IsReady = true;

    return true;
}

//----------------------------------------------------------------------------------------------------------------------
bool GUISpatializable::IsReady() const
{
    return m_IsReady;
}


FRG__CLOSE_NAMESPACE(frg);
