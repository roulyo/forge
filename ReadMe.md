![](project_template/assets/forge.png)

# Introduction

## FORGE: Obviously a Reliable Game Engine

Forge is a C++ framework to help you make video games. It is a programmer-oriented and editor-free engine, because who needs a GUI amaright.

The engine is based on the [Entity-Component-System](https://en.wikipedia.org/wiki/Entity_component_system) paradigm. It comes with a number of builtin components and systems but is heavily extensible by writing you own sets, which can be generic (and shared between projects) or very game specific integrating gameplay logic.

Though it is *Obviously Reliable*(tm), it is aimed at small 2D sprite-based projects (but can handle a 3D world). It was first made to have fun game jaming with full tech control, and kept this spirit along development.

# Setting up

#### Downloading direct dependencies

As you use FORGE you will notice that the tooling around the engine is UNIX oriented. Therefore if you are on a Windows system you will want to download something like [Cygwin](http://cygwin.com/) or [cmder with git](https://cmder.net/). You will also need a [ruby](https://www.ruby-lang.org/) interpreter in order to use the FORGE data toolchain. And [CMake](https://cmake.org).

Once you get the necessary software on your machine, you can start your FORGE journey by downloading the engine source code and its direct dependencies, which are set as git submodules in the extern* folder and updated with the **forge_setup.sh** script.

    git clone https://bitbucket.org/roulyo/forge.git <path_to_forge>

    cd <path_to_forge>

    ./forge_setup.sh


#### Installing SFML dependencies

FORGE relies on [SFML](https://www.sfml-dev.org) for the rendering and system code, and was downloaded as a submodule, but SFML depends on a few other libraries, so before starting to configure you must have their development files installed.

On Windows and macOS, all the required dependencies are provided alongside SFML so you won't have to download/install anything else. Building will work out of the box.

On Linux however, nothing is provided. SFML relies on you to install all of its dependencies on your own. Here is a list of what you need to install before building SFML:

- freetype
- x11
- xrandr
- udev
- opengl
- flac
- ogg
- vorbis
- vorbisenc
- vorbisfile
- openal
- pthread

The exact name of the packages may vary from distribution to distribution. Once those packages are installed, don't forget to install their development headers as well.

# Game developping

#### Creating a new project

Once this is done you can create a new project with **new_project.sh**

    cd <somewhere>

    ./path_to_forge/new_project.sh <project_name>

This will create a new **project_name** folder (in the current one), containing a new game project -- using simple builtins -- and will be ready to be tailored to your wish.

To access the engine (sources and tools), the project will use the path where FORGE was located when you ran the script. It will be embedded in the game project's cmake so you can easily view/modify FORGE's source from within your favorite IDE alongside your game. Each project will compile its FORGE libraries, so it is easy to fork the whole engine for a game if necessary.

#### Configuring the project

For the project to compile correctly you need to process the data in YAML to generate the right C++ code. In the sample project there is a few of the data under **src/data/def/** for you to have a look or use as a starting point for you data definitions. Once the data is processed, you can update the CMakeLists.txt files. Hopefully we got two scripts for these two tasks.

    cd <path_to_project>

    ./data_gen.sh

    ./cmake_gen.sh


#### Building the project

Now that the project is properly configured we need to generate the makefile (or IDE solution) with CMake. It is configured so that you need an out-of-source build folder in order to clean the source clean.

    cd <path_to_project>

    mkdir build

    cd build

    cmake ..

With you makefile (or IDE solution) now generated, it is time to get to work.

Note that for the game to load the assets you will need to set your working directory to the project's folder, or run the executable from there.

#### Getting further

While you are waiting for thorough and comprehensive documentation of the engine you can dig in its source code. Particularly you can start by looking at the **builtin** folder and the
**SimpleGameState**[.h](public/include/forge/builtin/freeroaming/SimpleGameState.h)|[.cpp](private/forge/builtin/freeroaming/SimpleGameState.cpp) and its systems
**SimpleControlSystem**[.h](public/include/forge/builtin/3Cs/SimpleControlSystem.h)|[.cpp](private/forge/builtin/3Cs/SimpleControlSystem.cpp),
**SimplePhysicsSystem**[.h](public/include/forge/builtin/physics/SimplePhysicsSystem.h)|[.cpp](private/forge/builtin/physics/SimplePhysicsSystem.cpp),
**SimpleRenderingSystem**[.h](private/forge/builtin/rendering/SimpleRenderingSystem.h)|[.cpp](private/forge/builtin/rendering/SimpleRenderingSystem.cpp).
