#include <%name%/Precomp.h>
#include <%name%/%Name%.h>

int main(void)
{
    %Name% game;
    frg::ForgeGameParam param;

    param.GameName = "%name%";
    param.Camera = frg::CameraType::Elevation;
    param.Resolution = { 1280, 720 };

    game.Init(param);
    game.MainLoop();
    game.Quit();

}
