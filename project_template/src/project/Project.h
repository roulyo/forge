#pragma once

#include <forge/engine/game/Game.h>

//----------------------------------------------------------------------------------------------------------------------
class %Name% : public frg::AbstractForgeGame
{
public:
    %Name%();
    ~%Name%() override;

    void OnInit() override;
    void OnQuit() override;

};

