#include <%name%/Precomp.h>
#include <%name%/%Name%.h>

#include <forge/engine/camera/api/CameraAPI.h>
#include <forge/engine/data/api/DataAPI.h>

#include <forge/builtin/freeroaming/SimpleGameState.h>

#include <%name%/data/DataList.h>
#include <%name%/data/EntityCatalog.h>
#include <%name%/data/SpriteCatalog.h>
#include <%name%/data/TextureCatalog.h>

//----------------------------------------------------------------------------------------------------------------------
%Name%::%Name%()
{
}

//----------------------------------------------------------------------------------------------------------------------
%Name%::~%Name%()
{
}

//----------------------------------------------------------------------------------------------------------------------
void %Name%::OnInit()
{
    RegisterCatalogType<EntityCatalog>();
    RegisterCatalogType<SpriteCatalog>();
    RegisterCatalogType<TextureCatalog>();

    m_World.Init(100.f, 100.f);

    frg::Entity::Ptr mc = frg::DataAPI::GetDataFrom<EntityCatalog>(DataList::Entity::PlayableCharacter);
    mc->SetPosition(50.f, 50.f, 0.f);
    m_World.AddEntity(mc);

    frg::CameraAPI::SetLookAt({ 50.f, 50.f, 0.f });
    frg::CameraAPI::SetDistance(10);
    frg::CameraAPI::SetFieldOfView(90);
    frg::CameraAPI::SetFilmedWorld(GetWorld());

    RegisterGameState<frg::bi::SimpleGameState>();
    RequestState(frg::bi::SimpleGameState::Id);
}

//----------------------------------------------------------------------------------------------------------------------
void %Name%::OnQuit()
{
}
