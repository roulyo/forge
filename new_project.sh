#!/bin/bash

NAME="$1"
SCRIPT=$(readlink -f "$0")
FORGE=$(dirname "$SCRIPT")

PROJECT_DIR=$NAME
PROJECT_SRC=$NAME/src/$NAME
PROJECT_CPP=$PROJECT_SRC/${NAME^}.cpp
PROJECT_H=$PROJECT_SRC/${NAME^}.h

mkdir $PROJECT_DIR
cp -r $FORGE/project_template/* "$PROJECT_DIR/"

echo "SET( FORGE_FOLDER_PATH \"$FORGE\" )" > $PROJECT_DIR/CMake/Config.cmake

sed -i "s;%forge%;${FORGE};g" ${NAME}/data_gen.sh
sed -i "s;%name%;${NAME};g" ${NAME}/data_gen.sh
sed -i "s;%name%;${NAME};g" ${NAME}/CMakeLists.txt

mv $NAME/src/project $PROJECT_SRC

sed -i "s;%name%;${NAME};g" $NAME/src/main.cpp
sed -i "s;%Name%;${NAME^};g" $NAME/src/main.cpp
sed -i "s;%name%;${NAME};g" $PROJECT_SRC/data/def/entity.yaml

mv $PROJECT_SRC/Project.cpp $PROJECT_CPP
sed -i "s;%name%;${NAME};g" $PROJECT_CPP
sed -i "s;%Name%;${NAME^};g" $PROJECT_CPP

mv $PROJECT_SRC/Project.h $PROJECT_H
sed -i "s;%name%;${NAME};g" $PROJECT_H
sed -i "s;%Name%;${NAME^};g" $PROJECT_H

#EOF
