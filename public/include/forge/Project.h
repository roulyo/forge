#pragma once

#include <forge/engine/ForgeConfig.h>
#include <forge/engine/ForgeMacros.h>

FRG__CREATE_NAMESPACE(frg);

FRG__OPEN_NAMESPACE(frg);
    FRG__CREATE_NAMESPACE(bi);

    FRG__OPEN_NAMESPACE(bi);
        FRG__CREATE_NAMESPACE(gui);
    FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);

#include <forge/engine/ForgeTypes.h>

#include <cstring>
#include <cfloat>
