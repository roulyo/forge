#pragma once

#if defined(FRG_USE_LOG)

#   include <rtb/logging/Logging.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
typedef rtb::IQuill         AbstractLogger;
typedef rtb::LogLevel       LogLevel;
typedef rtb::LogCategory    LogCategory;

inline void RegisterLogger(AbstractLogger& _logger)
{
    rtb::Logger::RegisterQuill(_logger);
}


FRG__CLOSE_NAMESPACE(frg);

#endif
