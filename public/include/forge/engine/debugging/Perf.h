#pragma once

#if defined(FRG_USE_PROFILING)

#   include <rtb/profiling/Profiling.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
typedef rtb::Profiler       Profiler;

FRG__CLOSE_NAMESPACE(frg);

#endif