#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/system/SfmlWrapper.h>

namespace sf
{
    class Shader;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
struct ShaderUniform
{
    ShaderUniform()
        : Name("")
        , Type(ShaderUniform::Type::Void)
    {}

    enum class Type
    {
        Void,
        Float,
        Color,
    };

    union Value
    {
        f32     Float;
        Color   RGBA{ 255, 255, 255, 255 };
    };

    String Name;
    Type Type;
    Value Value;

};

//----------------------------------------------------------------------------------------------------------------------
typedef VMap<String, ShaderUniform> UniformMap;

//----------------------------------------------------------------------------------------------------------------------
class Shader :  public Data<Shader>, public SFMLWrapper<sf::Shader>
{
public:
    Shader();
    ~Shader() override;

    void SetVertexFile(const String& _string);
    void SetPixelFile(const String& _string);
    void SetFiles(const String& _vertex, const String& _pixel);

    void LoadVertexCode(const String& _string);
    void LoadPixelCode(const String& _string);
    void LoadCodes(const String& _vertex, const String& _pixel);

    void SetUniform(const ShaderUniform& _param);
    void SetUniforms(const UniformMap& _params);

};

//----------------------------------------------------------------------------------------------------------------------
struct ShaderMaterial
{
    Shader::Ptr Program = nullptr;
    UniformMap Parameters;
};

FRG__CLOSE_NAMESPACE(frg);
