#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/rendering/resource/Texture.h>
#include <forge/engine/system/SfmlWrapper.h>
#include <forge/engine/math/Types.h>

namespace sf
{
    class Font;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class Font : public Data<Font>, public SFMLWrapper<sf::Font>
{
public:
    Font();
    Font(const String& _fontPath);
    ~Font() override;

    void SetFile(const String& _texPath);

    Texture::CPtr GetTexture(u32 _size) const;
    IntQuad GetGlyphRect(char32_t _char, u32 _size, bool _isBold = false, f32 _outlineThickness = 0) const;

};

FRG__CLOSE_NAMESPACE(frg);
