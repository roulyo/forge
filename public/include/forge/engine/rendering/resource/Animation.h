#pragma once

#include <forge/engine/data/Data.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
typedef DataNameId   AnimationId;

//----------------------------------------------------------------------------------------------------------------------
enum class AnimationClock
{
    Local,
    World,
};

//----------------------------------------------------------------------------------------------------------------------
class Animation : public Data<Animation>
{
public:
    Animation();
    Animation(u32 _x, u32 _y, u32 _keyframeCount, u32 _durationInMs);
    ~Animation();

    void SetStartFrameCoord(u32 _x, u32 _y);
    void SetKeyframeCount(u32 _count);
    void SetDurationMs(u32 _durationMs);

    Vector2u GetKeyframe(u32 _t) const;
    u32 GetDurationMs() const;

private:
    Vector2u    m_StartFrameCoord;
    u32         m_KeyframeCount;
    u32         m_DurationMs;
    u32         m_KeyframeDuration;

};

FRG__CLOSE_NAMESPACE(frg);
