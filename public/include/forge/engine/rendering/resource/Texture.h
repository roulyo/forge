#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/system/SfmlWrapper.h>

namespace sf
{
    class Image;
    class Texture;
}

FRG__OPEN_NAMESPACE(frg);
//----------------------------------------------------------------------------------------------------------------------
typedef u32 TextureHandle;

//----------------------------------------------------------------------------------------------------------------------
struct TextureOptions
{
    TextureOptions();
    TextureOptions(const Vector2u& _resolution, const Color& _clearColor);

    Vector2u    Resolution;
    Color       ClearColor;
};

//----------------------------------------------------------------------------------------------------------------------
class RAMTexture : public Sharable<RAMTexture>, public SFMLWrapper<sf::Image>
{
public:
    RAMTexture();
    ~RAMTexture() override;

    void Copy(const sf::Image& _source);
    void Move(const sf::Image&& _source);

    Color GetPixelColor(const Vector2u& _pixelCoord) const;

    Vector2u GetSize() const;

};

//----------------------------------------------------------------------------------------------------------------------
class Texture : public Data<Texture>, public SFMLWrapper<sf::Texture>
{
public:
    Texture();
    Texture(const Color& _color);
    Texture(const String& _texPath);
    Texture(sf::Texture* _sfTexture);
    ~Texture() override;

    void Bind(sf::Texture* _sfTexture);

    void SetFile(const String& _texPath);

    void SetRepeated(bool _repeated);

private:
    bool m_Bound;

};

FRG__CLOSE_NAMESPACE(frg);
