#pragma once


#include <forge/engine/rendering/drawable/Sprite.h>
#include <forge/engine/rendering/resource/Animation.h>
#include <forge/engine/rendering/resource/Shader.h>
#include <forge/engine/system/Sharable.h>
#include <forge/engine/time/Chrono.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class Transform;
    class Transformable;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class RenderTreeVisitor;
enum class TraversalOrder;

//----------------------------------------------------------------------------------------------------------------------
class AbstractRenderTree
{
public:
    typedef std::shared_ptr<AbstractRenderTree>         Ptr;
    typedef std::shared_ptr<const AbstractRenderTree>   CPtr;

public:
    void AddChild(const AbstractRenderTree::Ptr& _node);
    void RemoveChild(const AbstractRenderTree::Ptr& _child);
    void RemoveChildren();

    void InsertChild(const AbstractRenderTree::Ptr& _node);
    void ExtractChild(const AbstractRenderTree::Ptr& _node);

    virtual const Vector<AbstractRenderTree::Ptr>& GetAllChildren() const;

    virtual void Accept(RenderTreeVisitor& _visitor,
                        TraversalOrder _order) const = 0;

    template<class T>
    T& As()
    {
        return *(static_cast<T*>(this));
    }

protected:
    AbstractRenderTree(const String& _tag);
    virtual ~AbstractRenderTree();

protected:
    FRG__CLASS_ATTR_RW(String, Tag);

private:
    FRG__CLASS_ATTR_R_(Vector<AbstractRenderTree::Ptr>, Children);
    FRG__CLASS_ATTR___(Vector<AbstractRenderTree*>, Parents);

};

//----------------------------------------------------------------------------------------------------------------------
template<class NodeType>
class RenderTree : public AbstractRenderTree, public Sharable<NodeType>
{
public:
    using typename Sharable<NodeType>::Ptr;
    using typename Sharable<NodeType>::CPtr;

public:
    void Accept(RenderTreeVisitor& _visitor,
                TraversalOrder _order) const override;

protected:
    RenderTree(const String& _tag);

};

//----------------------------------------------------------------------------------------------------------------------
class BasicRenderNode : public RenderTree<BasicRenderNode>
{
public:
    BasicRenderNode();
    BasicRenderNode(const String& _tag);

};

//----------------------------------------------------------------------------------------------------------------------
class SwitchRenderNode : public RenderTree<SwitchRenderNode>
{
public:
    SwitchRenderNode();
    SwitchRenderNode(const String& _tag);

public:
    void AddChild(const AbstractRenderTree::Ptr& _node) = delete;
    void AddChild(const AbstractRenderTree::Ptr& _node, u32 _weight);

    bool IsSolved() const;
    void SolveChild(u32 _seed);

    const Vector<AbstractRenderTree::Ptr>& GetAllChildren() const override;

private:
    FRG__CLASS_ATTR___(Vector<AbstractRenderTree::Ptr>, ChildrenCandidate);
    FRG__CLASS_ATTR___(Vector<u32>,                     WeightedIndex);
    FRG__CLASS_ATTR___(bool,                            Solved);

};


//----------------------------------------------------------------------------------------------------------------------
class BaseTransformRenderNode
{
public:
    BaseTransformRenderNode();
    virtual ~BaseTransformRenderNode();

    void Rotate(f32 _rotation);
    void Scale(const Vector2f& _scale);
    void Translate(const Vector2f& _translation);

    void SetRotation(f32 _rotation);
    void SetScale(const Vector2f& _scale);
    void SetTranslation(const Vector2f& _translation);

    f32 GetRotation() const;
    Vector2f GetScale() const;
    Vector2f GetTranslation() const;

    const sf::Transform& GetTransform() const;

protected:
    sf::Transformable&  m_Transform;

};

//----------------------------------------------------------------------------------------------------------------------
class LocalTransformRenderNode : public BaseTransformRenderNode, public RenderTree<LocalTransformRenderNode>
{
public:
    LocalTransformRenderNode();
    LocalTransformRenderNode(const String& _tag);
    ~LocalTransformRenderNode();

};

//----------------------------------------------------------------------------------------------------------------------
class WorldTransformRenderNode : public BaseTransformRenderNode, public RenderTree<WorldTransformRenderNode>
{
public:
    WorldTransformRenderNode();
    WorldTransformRenderNode(const String& _tag);
    ~WorldTransformRenderNode();

    void Translate(const Vector3f& _translation);
    void SetTranslation(const Vector3f& _translation);

    // Unsupported
    void Rotate(f32 _rotation) = delete;
    void SetRotation(f32 _rotation) = delete;

private:
   FRG__CLASS_ATTR_R_(Vector3f, Translation3D);

};

//----------------------------------------------------------------------------------------------------------------------
class ScreenTransformRenderNode : public BaseTransformRenderNode, public RenderTree<ScreenTransformRenderNode>
{
public:
    ScreenTransformRenderNode();
    ScreenTransformRenderNode(const String& _tag);
    ~ScreenTransformRenderNode();

};

//----------------------------------------------------------------------------------------------------------------------
class ShaderMaterialRenderNode : public RenderTree<ShaderMaterialRenderNode>
{
public:
    ShaderMaterialRenderNode();
    ShaderMaterialRenderNode(const String& _tag);

public:
    FRG__CLASS_ATTR_RW(struct ShaderMaterial, ShaderMaterial);
    FRG__CLASS_ATTR_RW(bool, OverrideNextMaterials);

};

//----------------------------------------------------------------------------------------------------------------------
class BasicDrawableRenderNode : public RenderTree<BasicDrawableRenderNode>
{
public:
    BasicDrawableRenderNode();
    BasicDrawableRenderNode(const String& _tag);

public:
    FRG__CLASS_ATTR_RW(AbstractDrawable::IPtr, Drawable);

};

//----------------------------------------------------------------------------------------------------------------------
class GUIDrawableRenderNode : public RenderTree<GUIDrawableRenderNode>
{
public:
    GUIDrawableRenderNode();
    GUIDrawableRenderNode(const String& _tag);

public:
    FRG__CLASS_ATTR_RW(AbstractDrawable::IPtr, Drawable);

};

//----------------------------------------------------------------------------------------------------------------------
class ParticleRenderNode : public RenderTree<ParticleRenderNode>
{
public:
    ParticleRenderNode();
    ParticleRenderNode(const String& _tag);

public:
    FRG__CLASS_ATTR_RW(AbstractDrawable::IPtr, Drawable);

};

//----------------------------------------------------------------------------------------------------------------------
class SpriteRenderNode : public RenderTree<SpriteRenderNode>
{
public:
    SpriteRenderNode();
    SpriteRenderNode(const String& _tag);

public:
    FRG__CLASS_ATTR_RW(Sprite::Ptr, Sprite);

};

//----------------------------------------------------------------------------------------------------------------------
class BaseAnimationRenderNode
{
protected:
    BaseAnimationRenderNode();

public:
    void AddAnimation(const Animation::Ptr& _animation);
    virtual void PlayAnimation(const AnimationId& _animationId);
    void StopAnimation();

    Vector2u GetFrameModifier(u64 _t) const;
    u64 GetAnimationTimeMs() const;

protected:
    FRG__CLASS_ATTR_RW(AnimationClock, Clock);

    FRG__CLASS_ATTR_R_(bool, IsPlaying);
    FRG__CLASS_ATTR_R_(u64, StartTimeMs);

protected:
    VMap<AnimationId, Animation::Ptr>   m_Animations;
    const Animation*                    m_CurrentAnimation;

};

//----------------------------------------------------------------------------------------------------------------------
class AnimationRenderNode : public BaseAnimationRenderNode
                          , public RenderTree<AnimationRenderNode>
{
public:
    AnimationRenderNode();
    AnimationRenderNode(const String& _tag);

};

//----------------------------------------------------------------------------------------------------------------------
class DiscreteAnimationRenderNode : public BaseAnimationRenderNode
                                  , public RenderTree<DiscreteAnimationRenderNode>
{
public:
    DiscreteAnimationRenderNode();
    DiscreteAnimationRenderNode(const String& _tag);

    void PlayAnimation(const AnimationId& _animationId) override;

    void StartLoop();
    void StopLoop();

    void StartAnimationTimer();
    void StartIdleTimer();

private:
    FRG__CLASS_ATTR_RW(u32, APM);

    FRG__CLASS_ATTR_R_(bool, IsLooping);
    FRG__CLASS_ATTR_R_(u64, LoopStartTimeMs);
    FRG__CLASS_ATTR_R_(Chrono, Timer);

};

//----------------------------------------------------------------------------------------------------------------------
class RenderTreeUtils
{
    RenderTreeUtils() = delete;
    ~RenderTreeUtils() = delete;

public:
    static AbstractRenderTree::Ptr GetFirstNodeByTag(const AbstractRenderTree::Ptr& _tree,
                                                     const String& _tag);
    static Vector<AbstractRenderTree::Ptr> GetNodesByTag(const AbstractRenderTree::Ptr& _tree,
                                                         const String& _tag);

private:
    static void _GetNodesByTag(const AbstractRenderTree::Ptr& _tree,
                               const String& _tag,
                               Vector<AbstractRenderTree::Ptr>& _result);

};

FRG__CLOSE_NAMESPACE(frg);
