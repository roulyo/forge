#pragma once

#include <forge/engine/async/Future.h>
#include <forge/engine/math/Types.h>
#include <forge/engine/rendering/RenderTree.h>
#include <forge/engine/rendering/resource/Texture.h>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
class RenderingAgent
{
protected:
    const Vector2f& GetTargetResolution() const;

    TextureHandle CreateTexture(const TextureOptions& _options) const;
    Texture::Ptr GetTexture(TextureHandle _handle) const;
    Future<RAMTexture::Ptr> DownloadRAMTexture(TextureHandle _handle) const;

    void PushToRender(const AbstractRenderTree::CPtr& _renderTree,
                      const FloatBox& _spatialData = FloatBox()) const;

    void PushToTexture(TextureHandle _handle,
                       const AbstractRenderTree::CPtr& _renderTree,
                       const FloatBox& _spatialData = FloatBox()) const;

};

FRG__CLOSE_NAMESPACE(frg)
