#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/math/Types.h>
#include <forge/engine/rendering/drawable/Drawable.h>
#include <forge/engine/rendering/resource/Texture.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class Sprite;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class Sprite : public Drawable<sf::Sprite>, public Data<Sprite>
{
public:
    Sprite();
    Sprite(const Texture::Ptr _texture, const IntQuad& _texQuad);
    ~Sprite() override;

    void SetTexture(const Texture::CPtr& _texture);
    const Texture::CPtr& GetTexture() const;

    void SetTextureQuad(i32 _x, i32 _y, i32 _w, i32 _h);
    void SetTextureQuad(const IntQuad& _texQuad);
    IntQuad GetTextureQuad() const;

    void SetColor(const Color& _color);
    Color GetColor() const;

    void SetOpacity(u8 _opacity);
    u8 GetOpacity() const;

    void SetKeyframeModifier(const Vector2u& _keyframeMod);

    void SetPadding(i32 _x, i32 _y);

    const DrawableData& GetDrawableData() const override;

public:
    FRG__CLASS_ATTR_RW(Vector2i, Padding);

private:
    Texture::CPtr   m_Texture;
    Vector2i        m_SpriteCoord;
    Vector2u        m_KeyframeMod;

    mutable bool    m_IsDirty;

};

//----------------------------------------------------------------------------------------------------------------------
class PlainSprite : public Sprite
{
public:
    PlainSprite(const Color& _color);

public:
    static PlainSprite::Ptr RGB(const Color& _color)
    {
        return Sharable<PlainSprite>::MakePtr(_color);
    }

    static const PlainSprite::Ptr White;
    static const PlainSprite::Ptr Black;
    static const PlainSprite::Ptr Red;
    static const PlainSprite::Ptr Green;
    static const PlainSprite::Ptr Blue;
    static const PlainSprite::Ptr Cyan;
    static const PlainSprite::Ptr Purple;
    static const PlainSprite::Ptr Yellow;
    static const PlainSprite::Ptr Orange;
    static const PlainSprite::Ptr Brown;

};

FRG__CLOSE_NAMESPACE(frg);
