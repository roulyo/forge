#pragma once

#include <forge/engine/math/Types.h>
#include <forge/engine/rendering/drawable/Sprite.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class CircleShape;
    class RectangleShape;
}

//----------------------------------------------------------------------------------------------------------------------
#define FRG__DECL_SHAPE(T)                                                                  \
    public:                                                                                 \
        void SetTexture(const Texture::CPtr& _texture, bool _resetRect = false) override;   \
        void SetTextureQuad(const IntQuad& _texQuad) override;                              \
        void SetSprite(const Sprite::CPtr& _sprite) override;                               \
        void SetFillColor(const Color& _color) override;                                    \
        void SetOutlineColor(const Color& _color) override;                                 \
        void SetOutlineThickness(f32 _thickness) override;                                  \
        Color GetFillColor() const override;                                                \
        Color GetOutlineColor() const override;                                             \
        f32 GetOutlineThickness() const override;


FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractShape
{
public:
    virtual void SetTexture(const Texture::CPtr& _texture, bool _resetRect = false) = 0;
    virtual void SetTextureQuad(const IntQuad& _texQuad) = 0;
    virtual void SetSprite(const Sprite::CPtr& _sprite) = 0;
    virtual void SetFillColor(const Color& _color) = 0;
    virtual void SetOutlineColor(const Color& _color) = 0;
    virtual void SetOutlineThickness(f32 _thickness) = 0;

    virtual Color GetFillColor() const = 0;
    virtual Color GetOutlineColor() const = 0;
    virtual f32 GetOutlineThickness() const = 0;

protected:
    Texture::CPtr   m_Texture;
    IntQuad         m_TexQuad;

};

//----------------------------------------------------------------------------------------------------------------------
class CircleShape : public AbstractShape, public Drawable<sf::CircleShape>
{
    FRG__DECL_SHAPE(CircleShape);

public:
    CircleShape();
    ~CircleShape() override;

public:
    void SetRadius(f32 _radius);
    f32 GetRadius() const;

};

//----------------------------------------------------------------------------------------------------------------------
class QuadShape : public AbstractShape, public Drawable<sf::RectangleShape>
{
    FRG__DECL_SHAPE(QuadShape);

public:
    QuadShape();
    ~QuadShape() override;

    void SetSize(const Vector2f& _size);
    Vector2f GetSize() const;

};

FRG__CLOSE_NAMESPACE(frg);
