#pragma once

#include <forge/engine/rendering/resource/Shader.h>
#include <forge/engine/system/SfmlWrapper.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class Drawable;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractDrawable
{
public:
    typedef std::shared_ptr<AbstractDrawable>   IPtr;
    typedef sf::Drawable                        DrawableData;

public:
    AbstractDrawable();
    virtual ~AbstractDrawable() = 0;

    virtual void SetScreenCoord(f32 _x, f32 _y) = 0;
    virtual const DrawableData& GetDrawableData() const = 0;
    virtual Vector2f GetOriginalPosition() const = 0;
    virtual Vector2f GetRenderedPosition() const = 0;
    virtual Vector2f GetOriginalSize() const = 0;
    virtual Vector2f GetRenderedSize() const = 0;

public:
    bool operator<(const AbstractDrawable& _other) const;

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class  Drawable : public AbstractDrawable, public SFMLWrapper<T>
{
public:
    virtual ~Drawable();

    void SetScreenCoord(f32 _x, f32 _y) override;
    const DrawableData& GetDrawableData() const override;
    Vector2f GetOriginalPosition() const override;
    Vector2f GetRenderedPosition() const override;
    Vector2f GetOriginalSize() const override;
    Vector2f GetRenderedSize() const override;

};

FRG__CLOSE_NAMESPACE(frg);
