#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/rendering/drawable/Drawable.h>
#include <forge/engine/rendering/resource/Font.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class Text;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class Text : public Drawable<sf::Text>, public Data<Text>
{
public:
    enum class Style
    {
        Regular         = 0,
        Bold            = 1 << 0,
        Italic          = 1 << 1,
        Underlined      = 1 << 2,
        StrikeThrough   = 1 << 3
    };

public:
    Text();
    Text(const Font::Ptr _font);
    ~Text() override;

    void SetString(const String& _string);
    void SetString(const std::wstring& _string);
    void SetFillColor(const Color& _color);
    void SetFont(const Font::CPtr& _font);
    void SetSize(u32 _size);
    void SetStyle(Style _style);
    void SetLineSpacing(f32 _spacing);

private:
    Font::CPtr  m_Font;

};

FRG__CLOSE_NAMESPACE(frg);
