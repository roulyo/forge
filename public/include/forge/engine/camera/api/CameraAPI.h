#pragma once


FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
class World;

//----------------------------------------------------------------------------------------------------------------------
class CameraAPI
{
public:
    CameraAPI() = delete;
    ~CameraAPI() = delete;

public:
    static void SetFilmedWorld(const World& _world);

    static void SetLookAt(const Vector3f& _coordWorld);
    static Vector3f GetLookAt();

    static void SetDistance(f32 _distance);
    static f32 GetDistance();
    static void SetFieldOfView(f32 _fov);
    static f32 GetFieldOfView();

    static const Vector3f& GetCameraPosition();

    static f32 GetPPU();

    static Vector2f WorldPointToScreen(const Vector3f& _coordWorld);
    static Vector3f ScreenPointToWorld(const Vector2f& _coordScreen);

    static bool IsDimetric();

};

FRG__CLOSE_NAMESPACE(frg)
