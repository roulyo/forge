#pragma once

#include <forge/engine/math/Types.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class World;

//----------------------------------------------------------------------------------------------------------------------
enum class CameraType
{
    Elevation,
    Dimetric,
};

//----------------------------------------------------------------------------------------------------------------------
class AbstractCamera
{
public:
    AbstractCamera(const Vector2f& _resolution);
    virtual ~AbstractCamera();

    virtual CameraType GetCameraType() const = 0;

    void SetDistance(f32 _distance);
    void SetFieldOfView(f32 _FoV);
    virtual void SetFocus(const Vector3f& _coordWorld);


    virtual Vector2f WorldPointToScreen(const Vector3f& _coordWorld) const = 0;
    virtual Vector3f ScreenPointToWorld(const Vector2f& _coordScreen) const = 0;


protected:
    virtual void ComputeFrustum();

public:
    FRG__CLASS_ATTR__W(const World*, FilmedWorld);

    FRG__CLASS_ATTR_RW(Vector2f, Resolution);

    FRG__CLASS_ATTR_R_(f32, Height);
    FRG__CLASS_ATTR_R_(f32, FoV);
    FRG__CLASS_ATTR_R_(Vector3f, LookAt);
    FRG__CLASS_ATTR_R_(Vector3f, Position);
    FRG__CLASS_ATTR_R_(FloatQuad, Frustum);
    FRG__CLASS_ATTR_R_(f32, PPU);

};

FRG__CLOSE_NAMESPACE(frg);
