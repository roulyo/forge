#pragma once

#include <forge/engine/ecs/Entity.h>
#include <forge/engine/event/Event.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class EntityAddedEvent : public Event<EntityAddedEvent>
{
    FRG__DECL_EVENT(EntityAddedEvent);

public:
    EntityAddedEvent(const Entity::CPtr& _entity)
        : m_Entity(_entity)
    {}

private:
    FRG__CLASS_ATTR_R_(Entity::CPtr, Entity);

};

//----------------------------------------------------------------------------------------------------------------------
class EntityRemovedEvent : public Event<EntityRemovedEvent>
{
    FRG__DECL_EVENT(EntityRemovedEvent);

public:
    EntityRemovedEvent(const Entity::CPtr& _entity)
        : m_Entity(_entity)
    {}

private:
    FRG__CLASS_ATTR_R_(Entity::CPtr, Entity);

};

#if defined(FRG_USE_DEBUG_INFO)

//----------------------------------------------------------------------------------------------------------------------
class EntityDebugInfoEvent : public Event<EntityDebugInfoEvent>
{
    FRG__DECL_EVENT(EntityDebugInfoEvent);

public:
    EntityDebugInfoEvent(u32 _entityTypeCount, u32 _entityCount)
        : m_EntityTypeCount(_entityTypeCount)
        , m_EntityCount(_entityCount)
    {}

private:
    FRG__CLASS_ATTR_R_(u32, EntityTypeCount);
    FRG__CLASS_ATTR_R_(u32, EntityCount);

};

#endif

FRG__CLOSE_NAMESPACE(frg);
