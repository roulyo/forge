#pragma once

#include <forge/engine/ecs/Component.h>
#include <forge/engine/ecs/Entity.h>
#include <forge/engine/math/Types.h>
#include <forge/engine/math/spacetree/Octree.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class EntityControlAgent;

//----------------------------------------------------------------------------------------------------------------------
class World
{
public:
    World();
    ~World();

    void Init(f32 _width, f32 _height);
    void Fill(Octree<Entity::Ptr>* _spacetree);
    void Update();
    void Destroy();

    void RegisterEntityControlAgent(EntityControlAgent& _ecc);
    void UnregisterEntityControlAgent(EntityControlAgent& _ecc);

    const Entity::Ptr& AddStaticEntity(const Entity::Ptr& _entity);
    bool RemoveStaticEntity(const Entity::Ptr& _entity);

    const Entity::Ptr& AddEntity(const Entity::Ptr& _entity);
    bool UpdateEntity(const Entity::Ptr& _entity);
    bool RemoveEntity(const Entity::CPtr& _entity);

    u32 GetStaticEntities(AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const;

    u32 GetAllEntities(AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const;

    template<class Collider>
    u32 GetEntitiesInRange(const Collider& _aabb,
                           AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const;
    template<class Collider>
    u32 GetEntitiesInRange(const Vector<Collider>& _range,
                           AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const;

    u32 GetFirstRaycastCollision(const LineEquation& _ray,
                                 RaycastResult<Entity::Ptr>& _result) const;
    u32 GetAllRaycastCollisions(const LineEquation& _ray,
                                Vector<RaycastResult<Entity::Ptr>>& _result) const;

private:
    FRG__CLASS_ATTR_R_(f32, Width);
    FRG__CLASS_ATTR_R_(f32, Depth);

    Vector<Entity::Ptr>         m_StaticEntities;
    Octree<Entity::Ptr>*        m_Entities;
    List<EntityControlAgent*>   m_ECAs;

};

//----------------------------------------------------------------------------------------------------------------------
template<class Collider>
u32 World::GetEntitiesInRange(const Collider& _aabb,
                              AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const
{
    if (m_Entities == nullptr)
        return 0;

    return m_Entities->QueryRange<Collider>(_aabb, _result);
}

//----------------------------------------------------------------------------------------------------------------------
template<class Collider>
u32 World::GetEntitiesInRange(const Vector<Collider>& _range,
                              AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const
{
    if (m_Entities == nullptr)
        return 0;

    return m_Entities->QueryRange<Collider>(_range, _result);
}

FRG__CLOSE_NAMESPACE(frg);
