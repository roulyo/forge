#pragma once

#include <forge/engine/ecs/Entity.h>
#include <forge/engine/math/Types.h>
#include <forge/engine/math/spacetree/SpacetreeQueryResult.h>
// fix me: try not to include World here
#include <forge/engine/world/World.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
struct LineEquation;
template<class T> struct RaycastResult;
class World;

//----------------------------------------------------------------------------------------------------------------------
class EntityControlAgent
{
    friend class World;

public:
    EntityControlAgent();
    ~EntityControlAgent();

    void Clear();

    void RequestAddEntity(const Entity::Ptr& _entity);
    void RequestUpdateEntity(const Entity::Ptr& _entity);
    void RequestRemoveEntity(const Entity::CPtr& _entity);

    template<class Collider>
    u32 GetEntitiesInRange(const Collider& _aabb, AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const;
    template<class Collider>
    u32 GetEntitiesInRange(const Vector<Collider>& _range, AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const;

    u32 GetFirstRaycastCollision(const LineEquation& _ray, RaycastResult<Entity::Ptr>& _result) const;
    u32 GetAllRaycastCollisions(const LineEquation& _ray, Vector<RaycastResult<Entity::Ptr>>& _result) const;

private:
    static void SetActiveWorld(World* _world);

    void Update();

private:
    static World*   m_World;

    Vector<Entity::Ptr>     m_AddRequests;
    Vector<Entity::Ptr>     m_UpdateRequests;
    Vector<Entity::CPtr>    m_RemoveRequests;

};

//----------------------------------------------------------------------------------------------------------------------
template<class Collider>
u32 EntityControlAgent::GetEntitiesInRange(const Collider& _aabb,
                                           AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const
{
    return m_World->GetEntitiesInRange(_aabb, _result);
}

//----------------------------------------------------------------------------------------------------------------------
template<class Collider>
u32 EntityControlAgent::GetEntitiesInRange(const Vector<Collider>& _range,
                                           AbstractSpacetreeQueryResult<Entity::Ptr>& _result) const
{
    return m_World->GetEntitiesInRange(_range, _result);
}


FRG__CLOSE_NAMESPACE(frg);
