#pragma once

#include <forge/engine/system/Sharable.h>

#include <system_error>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
struct BaseAsyncReturnValue
{
    typedef std::shared_ptr<BaseAsyncReturnValue> Ptr;

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
struct AsyncReturnValue : public BaseAsyncReturnValue, public Sharable<AsyncReturnValue<T>>
{
    using Ptr = typename Sharable<AsyncReturnValue<T>>::Ptr;

    typedef T Type;

    FRG__CLASS_ATTR_RW(T, Value);

};

//----------------------------------------------------------------------------------------------------------------------
class AsyncResult
{
public:
    enum StatusCode
    {
        OK = 0,
        Cancelled = -1,
        GenericError = 1
    };

public:
    AsyncResult();

    bool IsSuccessful() const;
    bool WasCancelled() const;

    void SetErrorCode(u32 _errorCode);

    template<class T>
    const T& GetReturnValue() const
    {
        return static_cast<AsyncReturnValue<T>*>(m_ReturnValue.get())->GetValue();
    }

private:
    FRG__CLASS_ATTR_RW(BaseAsyncReturnValue::Ptr, ReturnValue);

    std::error_condition            m_Error;

};

FRG__CLOSE_NAMESPACE(frg);
