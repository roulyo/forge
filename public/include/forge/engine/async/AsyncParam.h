#pragma once

#include <forge/engine/system/Sharable.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class BaseAsyncParam
{
public:
    typedef std::shared_ptr<BaseAsyncParam>  Ptr;

public:
    bool    UseWorkerThread = false;
    u32     Timeout = 0;

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class AsyncParam : public BaseAsyncParam, public Sharable<T>
{
public:
    using Ptr = typename Sharable<T>::Ptr;

};

FRG__CLOSE_NAMESPACE(frg);
