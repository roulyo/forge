#pragma once

#include <forge/engine/async/AsyncResult.h>
#include <forge/engine/system/Sharable.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
enum class AsyncState
{
    Pending,
    Running,
    Returning,
    Complete,
    Cancelled
};

//----------------------------------------------------------------------------------------------------------------------
class AsyncContext : public Sharable<AsyncContext>
{
public:
    AsyncContext();

private:
    FRG__CLASS_ATTR_RW(AsyncState, State);
    FRG__CLASS_ATTR_RW(AsyncResult, Result);

};

FRG__CLOSE_NAMESPACE(frg);
