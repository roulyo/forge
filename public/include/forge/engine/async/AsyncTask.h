#pragma once

#include <forge/engine/async/AsyncContext.h>
#include <forge/engine/async/AsyncParam.h>
#include <forge/engine/async/AsyncResult.h>
#include <forge/engine/system/Sharable.h>

#include <atomic>

#define FRG__TASK_PARAM (static_cast<const Param&>(*m_TaskParam.get()))
#define FRG__SET_TASK_PARAM(_param) public: typedef _param Param;
#define FRG__SET_TASK_RETURNVALUE(_rv) public: typedef frg::AsyncReturnValue<_rv> ReturnValue;

FRG__OPEN_NAMESPACE(frg);

//------------------------------------------------------------------------------------------------------------------------
class BaseAsyncTask
{
    FRG__SET_TASK_PARAM(BaseAsyncParam);
    FRG__SET_TASK_RETURNVALUE(void);

public:
    typedef std::shared_ptr<BaseAsyncTask>  Ptr;

public:
    BaseAsyncTask();

    virtual void OnStart();
    virtual void OnUpdate();
    virtual void OnFinish();

    bool IsDone() const;


protected:
    void SetAsDone();

    void Return();
    void Return(const BaseAsyncReturnValue::Ptr& _rv);
    void Exit(u32 _errorCode);


protected:
    FRG__CLASS_ATTR_RW(BaseAsyncParam::Ptr, TaskParam);


private:
    FRG__CLASS_ATTR_R_(AsyncResult, Result);

    std::atomic_bool    m_IsDone;

};

//------------------------------------------------------------------------------------------------------------------------
template<class T>
class AsyncTask : public BaseAsyncTask, public Sharable<T>
{
public:
    using Ptr = typename Sharable<T>::Ptr;

};

FRG__CLOSE_NAMESPACE(frg);
