#pragma once

#include <forge/engine/async/AsyncContext.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AsyncResult;

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class Future
{
public:
    void Bind(const AsyncContext::Ptr& _context);
    void Release();

    bool IsBound() const;
    bool IsReady() const;

    const AsyncResult& GetResult() const;
    const T& GetReturnValue() const;

private:
    AsyncContext::Ptr    m_Context = nullptr;

};

FRG__CLOSE_NAMESPACE(frg);

#include "Future.inl"
