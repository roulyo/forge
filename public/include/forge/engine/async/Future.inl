#include <forge/engine/async/AsyncResult.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Future<T>::Bind(const AsyncContext::Ptr& _context)
{
    m_Context = _context;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Future<T>::Release()
{
    m_Context = nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Future<T>::IsBound() const
{
    return m_Context != nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Future<T>::IsReady() const
{
    return m_Context->GetState() == AsyncState::Complete
        || m_Context->GetState() == AsyncState::Cancelled;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
const AsyncResult& Future<T>::GetResult() const
{
    return m_Context->GetResult();
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
const T& Future<T>::GetReturnValue() const
{
    return m_Context->GetResult().GetReturnValue<T>();
}

FRG__CLOSE_NAMESPACE(frg);
