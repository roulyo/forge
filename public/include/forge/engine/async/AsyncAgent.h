#pragma once

#include <forge/engine/async/AsyncContext.h>
#include <forge/engine/async/AsyncTask.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AsyncAgent
{
protected:
    AsyncContext::Ptr PushAsyncTask(const BaseAsyncTask::Ptr& _task) const;

    template<class Task>
    AsyncContext::Ptr PushAsyncTask(const typename Task::Param::Ptr& _task) const;

};

//----------------------------------------------------------------------------------------------------------------------
template<class Task>
AsyncContext::Ptr AsyncAgent::PushAsyncTask(const typename Task::Param::Ptr& _param) const
{
    typename Task::Ptr task = Task::MakePtr();
    task->SetTaskParam(_param);

    return PushAsyncTask(task);
}

FRG__CLOSE_NAMESPACE(frg);
