#pragma once

#include <forge/engine/ecs/Component.h>
#include <forge/engine/ecs/Entity.h>
#include <forge/engine/math/spacetree/SpacetreeQueryResult.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractForgeGame;
class InputMappingScheme;
class AbstractViewController;

//----------------------------------------------------------------------------------------------------------------------
template<class... Cs> class System;
typedef System<>    AbstractSystem;

//----------------------------------------------------------------------------------------------------------------------
typedef rtb::StringId GameStateId;

//----------------------------------------------------------------------------------------------------------------------
class EntityMapSQR : public AbstractSpacetreeQueryResult<Entity::Ptr>
{
public:
    void Add(const Entity::Ptr& _elt) override;
    void ClearEntities();

    const EntityMapType<ComponentSignature, Vector<Entity::Ptr>>& GetEntityMap() const;

private:
    EntityMapType<ComponentSignature, Vector<Entity::Ptr>> m_EntityMap;

};

//----------------------------------------------------------------------------------------------------------------------
class AbstractGameState
{
public:
    AbstractGameState(const AbstractForgeGame& _game);
    virtual ~AbstractGameState() = 0;

    void PlayFrame(const u64& _dt);

    void Start();
    void Stop();

    virtual void OnStart();
    virtual void OnStop();

    void AddSystem(AbstractSystem* _system);
    void AddViewController(AbstractViewController* _viewController);

    void SetInputMapping(const InputMappingScheme& _inputMapping) const;
    void ResetInputMapping() const;

public:
    static constexpr GameStateId Id = GameStateId("INVALID_GAMESTATE_ID");
    virtual const GameStateId& GetId () const { return Id; }

private:
    u32 FillEntityMap();

protected:
    const AbstractForgeGame&        m_Game;

    Vector<AbstractSystem*>         m_Systems;
    Vector<AbstractViewController*> m_ViewControllers;

    EntityMapSQR                    m_EntityMapSQR;

};

FRG__CLOSE_NAMESPACE(frg);


#define FRG__DECL_GAMESTATE(name)                               \
    public:                                                     \
        static constexpr frg::GameStateId Id = frg::GameStateId(#name);\
        const frg::GameStateId& GetId () const override { return this->Id; }
