#pragma once

#include <forge/engine/camera/Camera.h>
#include <forge/engine/game/GameState.h>
#include <forge/engine/utils/HierarchicalAutomata.h>
#include <forge/engine/world/World.h>


FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractDataCatalog;
class InternalCloseEvent;

//----------------------------------------------------------------------------------------------------------------------
typedef rtb::StringId CatalogId;

//----------------------------------------------------------------------------------------------------------------------
struct ForgeGameParam
{
    String GameName = "YAFG";
    CameraType Camera = CameraType::Elevation;
    Vector2u Resolution{ 800, 600 };
    bool Fullsreen = false;
};

//----------------------------------------------------------------------------------------------------------------------
class AbstractForgeGame : public HierarchicalAutomata
{
public:
    AbstractForgeGame();
    virtual ~AbstractForgeGame() = 0;

    void Init(const ForgeGameParam& _param);
    void MainLoop();
    void Quit();

    u32 GetSeed() const;
    const World& GetWorld() const;
    const Entity::Ptr& GetGameEntity() const;

protected:
    template<class T>
    void RegisterCatalogType() const;

    template<class T>
    void RegisterGameState();
    template<class T>
    void RegisterSubGameState(GameStateId _parentStateId);
    template<class T>
    void EnterState();
    template<class T>
    void UpdateState();
    template<class T>
    void ExitState();

    void Stop();

    virtual void OnInit() {}
    virtual void OnQuit() {}

    void ResetGameEntity();

    virtual void OnGameEntityCreated() {}

private:
    void BeginFrame(const u64& _dt);
    void PlayFrame(const u64& _dt);
    void EndFrame(const u64& _dt);

    void OnCloseEvent(const InternalCloseEvent& _event);

    void RegisterCatalog(const CatalogId& _id, const AbstractDataCatalog* _catalog) const;


protected:
    u32         m_Seed;
    World       m_World;
    Entity::Ptr m_GameEntity;

private:
    Map<GameStateId, AbstractGameState*>  m_States;

    u64         m_LastTick;
    u64         m_DT;
    bool        m_Running;

};

FRG__CLOSE_NAMESPACE(frg);

#include "Game.inl"
