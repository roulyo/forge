FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void AbstractForgeGame::RegisterCatalogType() const
{
    RegisterCatalog(T::Id, new T());
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void AbstractForgeGame::RegisterGameState()
{
    RegisterState(static_cast<int>(T::Id),
                  NullStateId,
                  std::bind(&AbstractForgeGame::EnterState<T>, this),
                  std::bind(&AbstractForgeGame::UpdateState<T>, this),
                  std::bind(&AbstractForgeGame::ExitState<T>, this));
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void AbstractForgeGame::RegisterSubGameState(GameStateId _parentStateId)
{
    RegisterState(static_cast<int>(T::Id),
                  static_cast<int>(_parentStateId),
                  std::bind(AbstractForgeGame::EnterState<T>(), this),
                  std::bind(AbstractForgeGame::UpdateState<T>(), this),
                  std::bind(AbstractForgeGame::ExitState<T>()), this);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void AbstractForgeGame::EnterState()
{
    T* state = new T(*this);

    m_States[T::Id] = state;
    state->Start();
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void AbstractForgeGame::UpdateState()
{
    m_States[T::Id]->PlayFrame(m_DT);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void AbstractForgeGame::ExitState()
{
    AbstractGameState* state = m_States[T::Id];
    state->Stop();

    m_States.erase(m_States.find(T::Id));

    delete state;
}



FRG__CLOSE_NAMESPACE(frg);
