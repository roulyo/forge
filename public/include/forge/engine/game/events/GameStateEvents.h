#pragma once

#include <forge/engine/event/Event.h>
#include <forge/engine/game/GameState.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class GameStateStartedEvent : public Event<GameStateStartedEvent>
{
    FRG__DECL_EVENT(GameStateStartedEvent);

public:
    GameStateStartedEvent(const GameStateId& _stateId)
        : m_StateId(_stateId)
    {}

private:
    FRG__CLASS_ATTR_R_(GameStateId, StateId);

};

//----------------------------------------------------------------------------------------------------------------------
class GameStateStoppedEvent : public Event<GameStateStoppedEvent>
{
    FRG__DECL_EVENT(GameStateStoppedEvent);

public:
    GameStateStoppedEvent(const GameStateId& _stateId)
        : m_StateId(_stateId)
    {}

private:
    FRG__CLASS_ATTR_R_(GameStateId, StateId);

};

FRG__CLOSE_NAMESPACE(frg);
