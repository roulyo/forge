#pragma once

#include <forge/engine/event/Event.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class PauseTimeRequestEvent : public Event<PauseTimeRequestEvent>
{
    FRG__DECL_EVENT(PauseTimeRequestEvent);

};

//----------------------------------------------------------------------------------------------------------------------
class ResumeTimeRequestEvent : public Event<ResumeTimeRequestEvent>
{
    FRG__DECL_EVENT(ResumeTimeRequestEvent);

};

FRG__CLOSE_NAMESPACE(frg);
