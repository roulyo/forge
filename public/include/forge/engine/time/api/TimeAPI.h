#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class TimeAPI
{
public:
    TimeAPI() = delete;
    ~TimeAPI() = delete;

public:
    static u64 GetGameTimeNanoseconds();
    static u64 GetGameTimeMicroseconds();
    static u64 GetGameTimeMilliseconds();
    static u64 GetGameTimeSeconds();

};

FRG__CLOSE_NAMESPACE(frg);
