#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class Chrono
{
public:
    Chrono();

    void Start(u32 _durationMs);
    void Stop();

    bool IsStarted() const;
    bool IsElapsed() const;

    u32 GetElapsedTimeMs() const;
    u32 GetRemainingTimeMs() const;
    f32 GetElapsedRatio() const;


private:
    u64 m_StartTimeMs;
    u32 m_DurationMs;

};

FRG__CLOSE_NAMESPACE(frg);
