#pragma once

#include <forge/engine/system/Sharable.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
typedef rtb::StringId DataNameId;

//----------------------------------------------------------------------------------------------------------------------
class AbstractData
{
public:
    typedef std::shared_ptr<AbstractData>   IPtr;

    const DataNameId& GetDataNameId() const;
    void SetDataNameId(const DataNameId& _id);

protected:
    const DataNameId* m_NameId;

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class Data : public AbstractData, public Sharable<T>
{
public:
    Data();
    virtual ~Data() = 0;

    void SetInstanceAddr(T** _instanceAddr);
    bool IsSingletonData() const;

private:
    T** m_InstanceAddr;

};

FRG__CLOSE_NAMESPACE(frg);

#include "Data.inl"
