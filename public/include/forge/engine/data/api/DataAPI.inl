FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
template<class T>
typename T::DataType::Ptr DataAPI::GetDataFrom(const DataNameId& _id)
{
    const T* catalog= static_cast<const T*>(GetCatalog(T::Id));

    if (catalog != nullptr)
    {
        return catalog->Get(_id);
    }

    return nullptr;
}

FRG__CLOSE_NAMESPACE(frg)
