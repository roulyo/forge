#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/data/DataCatalog.h>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
class DataAPI
{
public:
    DataAPI() = delete;
    ~DataAPI() = delete;

public:
    template<class T>
    static typename T::DataType::Ptr GetDataFrom(const DataNameId& _id);

private:
    static const AbstractDataCatalog* GetCatalog(const CatalogId& _catalogId);

};

FRG__CLOSE_NAMESPACE(frg)

#include "DataAPI.inl"
