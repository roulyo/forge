FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Data<T>::Data()
    : m_InstanceAddr(nullptr)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Data<T>::~Data()
{
    if (IsSingletonData())
    {
        *m_InstanceAddr = nullptr;
        m_InstanceAddr = nullptr;
    }
}

 //----------------------------------------------------------------------------------------------------------------------
 template<class T>
 void Data<T>::SetInstanceAddr(T** _instanceAddr)
 {
     m_InstanceAddr = _instanceAddr;
 }

 //----------------------------------------------------------------------------------------------------------------------
 template<class T>
 bool Data<T>::IsSingletonData() const
 {
     return m_InstanceAddr != nullptr;
 }

FRG__CLOSE_NAMESPACE(frg);
