#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/data/DataFactory.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
typedef rtb::StringId CatalogId;

//----------------------------------------------------------------------------------------------------------------------
class AbstractDataCatalog
{
public:
    void RegisterData(const DataNameId& _dataId, const AbstractDataFactory& _factory);
    virtual void CleanStaticResources() = 0;

public:
    static constexpr CatalogId Id = CatalogId("INVALID_CATALOG_ID");

protected:
    Map<DataNameId, const AbstractDataFactory*> m_Factories;

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class DataCatalog : public AbstractDataCatalog
{
public:
    typedef T   DataType;

    typename DataType::Ptr Get(const DataNameId& _dataId) const;

    void CleanStaticResources() override;

private:
    Map<DataType*, typename DataType::Ptr>   m_StaticResources;

};

FRG__CLOSE_NAMESPACE(frg);

#define FRG__DECL_CATALOG(name)                                 \
    public:                                                     \
        static constexpr frg::CatalogId Id = frg::CatalogId(#name);

#include "DataCatalog.inl"
