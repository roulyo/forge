#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractData;

//----------------------------------------------------------------------------------------------------------------------
class AbstractDataFactory
{
public:
    virtual AbstractData* Create() const = 0;

};

FRG__CLOSE_NAMESPACE(frg);
