FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
typename DataCatalog<T>::DataType::Ptr DataCatalog<T>::Get(const DataNameId& _dataId) const
{
    Map<DataNameId, const AbstractDataFactory*>::const_iterator factoryIt = m_Factories.find(_dataId);

    if (factoryIt != m_Factories.end())
    {
        DataType* data = static_cast<T*>(factoryIt->second->Create());

        if (data->IsSingletonData())
        {
            auto it = m_StaticResources.find(data);

            if (it == m_StaticResources.end())
            {
                typename DataType::Ptr dataPtr = typename DataType::Ptr(data);
                const_cast<DataCatalog<T>*>(this)->m_StaticResources[data] = dataPtr;

                return dataPtr;
            }

            return it->second;
        }
        else
        {
            return typename DataType::Ptr(data);
        }
    }
    else
    {
        return typename DataType::Ptr(new DataType());
    }
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void DataCatalog<T>::CleanStaticResources()
{
    auto it = m_StaticResources.begin();

    while (it != m_StaticResources.end())
    {
        if (it->second.use_count() == 1)
        {
            it = m_StaticResources.erase(it);
        }
        else
        {
            ++it;
        }

    }
}


FRG__CLOSE_NAMESPACE(frg);
