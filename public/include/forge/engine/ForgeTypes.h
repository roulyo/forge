#pragma once

#include <array>
#include <cstdint>
#include <list>
#include <memory>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include <rtb/utils/AutoLock.h>
#include <rtb/utils/Singleton.h>
#include <rtb/utils/StringId.h>
#include <rtb/utils/VMap.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;

typedef float       f32;
typedef double      f64;

typedef char        byte;

//----------------------------------------------------------------------------------------------------------------------
template<class T, u32 S> using Array    = std::array<T, S>;
template<class T> using List            = std::list<T>;
template<class K, class V> using Map    = std::unordered_map<K, V>;
template<class T, class L = std::less<T>> using Multiset
                                        = std::multiset<T, L>;
template<class K, class V> using Pair   = std::pair<K, V>;
template<class T> using Queue           = std::queue<T>;
template<class T> using Set             = std::unordered_set<T>;
template<class T> using Stack           = std::stack<T>;
using String                            = std::string;
using WString                           = std::wstring;
template<class T> using Vector          = std::vector<T>;

template<class T> using Singleton       = rtb::Singleton<T>;
using StringId                          = rtb::StringId;
template<class K, class V> using VMap   = rtb::VMap<K, V>;

//----------------------------------------------------------------------------------------------------------------------
template<class K, class V> using EntityMapType = VMap<K, V>;

//----------------------------------------------------------------------------------------------------------------------
template<class T>
struct Vector2
{
    union
    {
        T x, w;
    };

    union
    {
        T y, h;
    };

    bool operator==(const Vector2<T>& _rhs) const
    {
        return _rhs.x == x && _rhs.y == y;
    }

    bool operator!=(const Vector2<T>& _rhs) const
    {
        return !(_rhs == *this);
    }

    static const Vector2<T> Zero;
    static const Vector2<T> Identity;

};

template<class T> const Vector2<T> Vector2<T>::Zero = { 0, 0 };
template<class T> const Vector2<T> Vector2<T>::Identity = { 1, 1 };

//----------------------------------------------------------------------------------------------------------------------
typedef Vector2<i32>    Vector2i;
typedef Vector2<u32>    Vector2u;
typedef Vector2<f32>    Vector2f;

//----------------------------------------------------------------------------------------------------------------------
template<class T>
struct Vector3
{
    union
    {
        T x, w, r;
    };

    union
    {
        T y, d, g;
    };

    union
    {
        T z, h, b;
    };

    bool operator==(const Vector3<T>& _rhs) const
    {
        return _rhs.x == x && _rhs.y == y && _rhs.z == z;
    }

    bool operator!=(const Vector2<T>& _rhs) const
    {
        return !(_rhs == *this);
    }

    Vector2<T> xy() const
    {
        return { x, y };
    }

    void xy(const Vector2<T>& _vec2)
    {
        x = _vec2.x;
        y = _vec2.y;
    }

    static const Vector3<T> Zero;
    static const Vector3<T> Identity;

};

template<class T> const Vector3<T> Vector3<T>::Zero = { 0, 0, 0 };
template<class T> const Vector3<T> Vector3<T>::Identity = { 1, 1, 1 };

//----------------------------------------------------------------------------------------------------------------------
typedef Vector3<i32>    Vector3i;
typedef Vector3<u32>    Vector3u;
typedef Vector3<f32>    Vector3f;

//----------------------------------------------------------------------------------------------------------------------
struct Color
{
    u8 r, g, b;
    u8 a = 255;
};

inline constexpr Color operator+(const Color& _color, i32 _mod)
{
    return { static_cast<u8>(std::min(255, _color.r + _mod)),
             static_cast<u8>(std::min(255, _color.g + _mod)),
             static_cast<u8>(std::min(255, _color.b + _mod)) };
}

inline constexpr Color operator-(const Color& _color, i32 _mod)
{
    return { static_cast<u8>(std::max(0, (i32)_color.r - _mod)),
             static_cast<u8>(std::max(0, (i32)_color.g - _mod)),
             static_cast<u8>(std::max(0, (i32)_color.b - _mod)) };
}

namespace PlainColor
{
    static constexpr Color White        { 255, 255, 255 };
    static constexpr Color LightGrey    { 191, 191, 191 };
    static constexpr Color Grey         { 127, 127, 127 };
    static constexpr Color DarkGrey     {  63,  63,  63 };
    static constexpr Color Black        {   0,   0,   0 };
    static constexpr Color Red          { 255,   0,   0 };
    static constexpr Color Green        {   0, 255,   0 };
    static constexpr Color Blue         {   0,   0, 255 };
    static constexpr Color Cyan         {   0, 255, 255 };
    static constexpr Color Purple       { 255,   0, 255 };
    static constexpr Color Yellow       { 255, 255,   0 };
    static constexpr Color Orange       { 255, 127,   0 };
    static constexpr Color Brown        { 127,  63,   0 };
    static constexpr Color Transparent  {   0,   0,   0,   0 };

}

FRG__CLOSE_NAMESPACE(frg);
