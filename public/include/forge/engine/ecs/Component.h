#pragma once

#include <bitset>

FRG__OPEN_NAMESPACE(frg);

//------------------------------------------------------------------------
typedef rtb::StringId   ComponentId;
typedef std::bitset<64> ComponentSignature;

//------------------------------------------------------------------------
template<typename T>
class SignatureFactory
{
public:
    static ComponentSignature GetNewSignature()
    {
        FRG__LOG_ASSERT(n < 64,
            "Too many components. Increase the ComponentSignature size");

        return ComponentSignature(1) << n++;
    }

private:
    static u32 n;

};

template<typename T>
u32 SignatureFactory<T>::n = 0;

//------------------------------------------------------------------------
class AbstractComponent
{
public:
    static constexpr ComponentId Id = ComponentId("INVALID_COMPONENT_ID");
    static constexpr ComponentSignature Signature = 0;

};

FRG__CLOSE_NAMESPACE(frg);

#define FRG__DECL_COMPONENT(name)                                               \
    public:                                                                     \
        static constexpr frg::ComponentId Id = frg::ComponentId(#name);     \
        static const frg::ComponentSignature Signature;

#define FRG__IMPL_COMPONENT(name)                                               \
    const frg::ComponentSignature name::Signature                             \
        = frg::SignatureFactory<frg::AbstractComponent>::GetNewSignature();
