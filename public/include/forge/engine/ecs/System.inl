FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class C, class... Cs>
System<C, Cs...>::System()
    : System<Cs...>()
{
    System<C, Cs...>::m_Signature |= C::Signature;
}

FRG__CLOSE_NAMESPACE(frg);
