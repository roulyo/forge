#pragma once

#include <forge/engine/ecs/Component.h>
#include <forge/engine/ecs/Entity.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class... Cs>
class System
{
};

//----------------------------------------------------------------------------------------------------------------------
template<class C, class... Cs>
class System<C, Cs...> : public System<Cs...>
{
public:
    System();
};

//----------------------------------------------------------------------------------------------------------------------
template<>
class System<>
{
public:
    System();

    void Start();
    void Stop();

    void ExecuteForEach(const u64& _dt, const Vector<Entity::Ptr>& _entities);
    void ExecuteForEach(const u64& _dt, const EntityMapType<ComponentSignature, Vector<Entity::Ptr>>& _entityMap);

    virtual bool IsStandingBy() const;
    virtual void OnStart();
    virtual void OnFrameBegin();
    virtual void Execute(const u64& _dt, const Entity::Ptr& _entity) = 0;
    virtual void OnFrameEnd();
    virtual void OnStop();

    bool DoesTarget(const ComponentSignature& _signature) const;


public:
    FRG__CLASS_ATTR_R_(ComponentSignature, Signature);

};

//----------------------------------------------------------------------------------------------------------------------
typedef System<>    AbstractSystem;

FRG__CLOSE_NAMESPACE(frg);

#include "System.inl"

