#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/ecs/Component.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class EntityRuntimeIdAuthority
{
public:
    static u32 GetNewRuntimeId();

private:
    static u32  m_Id;

};

//----------------------------------------------------------------------------------------------------------------------
class Entity : public Data<Entity>
{
public:
    Entity();

    template<class Component>
    bool HasComponent() const;

    template<class Component>
    Component& GetComponent();

    template<class Component>
    const Component& GetComponent() const;

    template<class Component>
    Component& AddComponent();

    template<class Component>
    Component& TakeComponentFrom(Entity& _entity);

    template<class Component>
    void RemoveComponent();

    AbstractComponent& GetComponentById(ComponentId _id) const;

    void SetPosition(f32 _x, f32 _y, f32 _z);
    void SetSize(f32 _width, f32 _depth, f32 _height);

private:
    FRG__CLASS_ATTR_RW(String, DisplayName);
    FRG__CLASS_ATTR_RW(Vector3f, Position);
    FRG__CLASS_ATTR_RW(Vector3f, Size);
    FRG__CLASS_ATTR_R_(ComponentSignature, Signature);
    FRG__CLASS_ATTR_R_(u32, RuntimeId);

protected:
    Map<ComponentId, AbstractComponent*>  m_Components;

};

FRG__CLOSE_NAMESPACE(frg);

#include "Entity.inl"
