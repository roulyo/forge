FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class Component>
bool Entity::HasComponent() const
{
    return m_Components.find(Component::Id) != m_Components.end();
}

//----------------------------------------------------------------------------------------------------------------------
template<class Component>
Component& Entity::GetComponent()
{
    return static_cast<Component&>(GetComponentById(Component::Id));
}

//----------------------------------------------------------------------------------------------------------------------
template<class Component>
const Component& Entity::GetComponent() const
{
    return static_cast<const Component&>(GetComponentById(Component::Id));
}

//----------------------------------------------------------------------------------------------------------------------
template<class Component>
Component& Entity::AddComponent()
{
    FRG__ASSERT(m_Components.find(Component::Id) == m_Components.end());

    Component* c = new Component();

    m_Components[Component::Id] = c;
    m_Signature |= Component::Signature;

    return *c;
}

//----------------------------------------------------------------------------------------------------------------------
template<class Component>
Component& Entity::TakeComponentFrom(Entity& _entity)
{
    Component& c = _entity.GetComponent<Component>();

    m_Components[Component::Id] = &c;
    m_Signature |= Component::Signature;

    _entity.m_Components.erase(Component::Id);
    _entity.m_Signature ^= Component::Signature;

    return c;
}

//----------------------------------------------------------------------------------------------------------------------
template<class Component>
void Entity::RemoveComponent()
{
    Component& c = GetComponent<Component>();

    m_Components.erase(Component::Id);
    m_Signature ^= Component::Signature;

    delete &c;
}

FRG__CLOSE_NAMESPACE(frg);
