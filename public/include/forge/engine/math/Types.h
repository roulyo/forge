#pragma once

#include <cmath>
#include <math.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
static constexpr f32 PI32 = static_cast<f32>(M_PI);
static constexpr f64 PI64 = M_PI;

//----------------------------------------------------------------------------------------------------------------------
template<class T> class AAQuad;
template<class T> class AABox;

//----------------------------------------------------------------------------------------------------------------------
struct LineEquation
{
    Vector3f Point;
    Vector3f Vector;
};

//----------------------------------------------------------------------------------------------------------------------
struct LinearFunctionEquation
{
    f32 a;
    f32 b;

    f32 operator()(f32 _x) const { return (a * _x) + b; }
};

//----------------------------------------------------------------------------------------------------------------------
struct QuadraticFunctionEquation
{
    f32 a;
    f32 b;
    f32 c;

    f32 operator()(f32 _x) const { return (a * _x * _x) + (b * _x) + c; }
};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
struct RaycastResult
{
    Vector3f CollisionPoint { NAN, NAN, NAN };
    T        CollidedElement;
};


//----------------------------------------------------------------------------------------------------------------------
template<class T>
class Sphere
{
public:
    Sphere();
    Sphere(const Vector3<T>& _center, T _radius);

    bool Contains(const Vector3<T>& _point) const;
    bool Contains(const AABox<T>& _box) const;
    bool Intersects(const AABox<T>& _box) const;

public:
    FRG__CLASS_ATTR_RW(Vector3<T>, Center);
    FRG__CLASS_ATTR_RW(T, Radius);

};

//----------------------------------------------------------------------------------------------------------------------
typedef Sphere<i32> IntSphere;
typedef Sphere<f32> FloatSphere;


//----------------------------------------------------------------------------------------------------------------------
template<class T>
class ManhattanSphere : public Sphere<T>
{
public:
    ManhattanSphere();
    ManhattanSphere(const Vector3<T>& _center, T _radius);

    bool Contains(const Vector3<T>& _point) const;
    bool Contains(const AABox<T>& _box) const;
    bool Intersects(const AABox<T>& _box) const;

};

//----------------------------------------------------------------------------------------------------------------------
typedef ManhattanSphere<i32> IntManhattanSphere;
typedef ManhattanSphere<f32> FloatManhattanSphere;

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class Cylinder
{
public:
    Cylinder();
    Cylinder(const Vector3<T>& _base, T _radius);

    bool Contains(const Vector3<T>& _point) const;
    bool Contains(const AABox<T>& _point) const;
    bool Intersects(const AABox<T>& _box) const;

public:
    FRG__CLASS_ATTR_RW(Vector3<T>, Base);
    FRG__CLASS_ATTR_RW(T, Radius);

};

//----------------------------------------------------------------------------------------------------------------------
typedef Cylinder<i32> IntCylinder;
typedef Cylinder<f32> FloatCylinder;


//----------------------------------------------------------------------------------------------------------------------
template<class T>
class ManhattanCylinder : public Cylinder<T>
{
public:
    ManhattanCylinder();
    ManhattanCylinder(const Vector3<T>& _base, T _radius);

    bool Contains(const Vector3<T>& _point) const;
    bool Contains(const AABox<T>& _box) const;
    bool Intersects(const AABox<T>& _box) const;

};

//----------------------------------------------------------------------------------------------------------------------
typedef ManhattanCylinder<i32> IntManhattanCylinder;
typedef ManhattanCylinder<f32> FloatManhattanCylinder;


//----------------------------------------------------------------------------------------------------------------------
template<class T>
class AAQuad
{
public:
    AAQuad();
    AAQuad(T _x, T _y, T _width, T _height);
    AAQuad(const Vector2<T>& _position, const Vector2<T>& _size);
    ~AAQuad();

    bool Contains(const Vector2<T>& _point) const;
    bool Contains(const AAQuad<T>& _quad) const;
    bool Intersects(const AAQuad<T>& _quad) const;

    Vector2<T> GetCenter() const;

public:
    FRG__CLASS_ATTR_RW(Vector2<T>, Position);
    FRG__CLASS_ATTR_RW(Vector2<T>, Size);

};

//----------------------------------------------------------------------------------------------------------------------
typedef AAQuad<i32> IntQuad;
typedef AAQuad<f32> FloatQuad;


//----------------------------------------------------------------------------------------------------------------------
template<class T>
class AABox
{
public:
    AABox();
    AABox(T _x, T _y, T _z, T _width, T _depth, T _height);
    AABox(const Vector3<T>& _position, const Vector3<T>& _size);
    ~AABox();

    bool Contains(const Vector3<T>& _point) const;
    bool Contains(const AABox<T>& _box) const;
    bool Intersects(const AABox<T>& _box) const;
    Vector3f Intersects(const LineEquation& _line) const;

    Vector3<T> GetCenter() const;

public:
    FRG__CLASS_ATTR_RW(Vector3<T>, Position);
    FRG__CLASS_ATTR_RW(Vector3<T>, Size);

};

//----------------------------------------------------------------------------------------------------------------------
typedef AABox<i32> IntBox;
typedef AABox<f32> FloatBox;


FRG__CLOSE_NAMESPACE(frg);

#include "Types.inl"
