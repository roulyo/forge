#pragma once

#include <cmath>
#include <math.h>

#include <forge/engine/math/Types.h>

FRG__OPEN_NAMESPACE(frg);

namespace math_utils
{
//----------------------------------------------------------------------------------------------------------------------
inline f32 Radian(f32 _degree)
{
    return _degree * PI32 / 180.0f;
}

//----------------------------------------------------------------------------------------------------------------------
template<typename T>
inline f32 Distance(const Vector2<T>& _a, const Vector2<T>& _b)
{
    return std::sqrt(  std::pow(_b.x - _a.x, static_cast<T>(2))
                     + std::pow(_b.y - _a.y, static_cast<T>(2)));
}


//----------------------------------------------------------------------------------------------------------------------
template<typename T>
inline f32 Distance(const Vector3<T>& _a, const Vector3<T>& _b)
{
    return std::sqrt(  std::pow(_b.x - _a.x, static_cast<T>(2))
                     + std::pow(_b.y - _a.y, static_cast<T>(2))
                     + std::pow(_b.z - _a.z, static_cast<T>(2)));
}

//----------------------------------------------------------------------------------------------------------------------
template<typename T>
inline T ManhattanDistance(const Vector2<T>& _a, const Vector2<T>& _b)
{
    return   std::abs(_b.x - _a.x)
           + std::abs(_b.y - _a.y);
}


//----------------------------------------------------------------------------------------------------------------------
template<typename T>
inline T ManhattanDistance(const Vector3<T>& _a, const Vector3<T>& _b)
{
    return   std::abs(_b.x - _a.x)
           + std::abs(_b.y - _a.y)
           + std::abs(_b.z - _a.z);
}

//----------------------------------------------------------------------------------------------------------------------
template<typename T>
inline Vector2<T> RoundV(const Vector2<T>& _vec)
{
    return { std::round(_vec.x), std::round(_vec.y) };
}

//----------------------------------------------------------------------------------------------------------------------
template<typename T>
inline Vector3<T> RoundV(const Vector3<T>& _vec)
{
    return { std::round(_vec.x), std::round(_vec.y), std::round(_vec.z) };
}
}

FRG__CLOSE_NAMESPACE(frg);
