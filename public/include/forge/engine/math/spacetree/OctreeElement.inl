FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
OctreeElement<T>::OctreeElement(const T& _data, const FloatBox& _box)
    : m_BoundingBox(_box)
    , m_Data(_data)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
OctreeElement<T>::~OctreeElement()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool OctreeElement<T>::operator==(const OctreeElement<T>& _rhs) const
{
    return m_Data == _rhs.GetData();
}

FRG__CLOSE_NAMESPACE(frg);
