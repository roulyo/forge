#pragma once

#include <forge/engine/math/spacetree/QuadtreeElement.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class Quadtree
{
public:
    Quadtree();
    Quadtree(const FloatQuad& _box);
    ~Quadtree();

    void Reset(const FloatQuad& _box);
    void Clear();

    bool Insert(const T& _elt, const FloatQuad& _spaceData);

    bool Remove(const T& _elt, const FloatQuad& _approxArea);
    bool Remove(const T& _elt);

    bool Update(const T& _elt, const FloatQuad& _spaceData);

    u32 QueryRange(const FloatQuad& _range, Vector<T>& _result) const;

private:
    void Subdivide();

    bool RemovePrecise(const T& _elt, const FloatQuad& _approxArea);
    bool UpdatePrecise(const T& _elt, const FloatQuad& _spaceData);

    bool IsLeaf() const;

private:
    Vector<QuadtreeElement<T>>   m_Elements;

    FloatQuad       m_BoundingBox;

    Quadtree<T>*    m_TopLeft;
    Quadtree<T>*    m_TopRight;
    Quadtree<T>*    m_BottomRight;
    Quadtree<T>*    m_BottomLeft;

private:
    static const u32 QT_NODE_CAPACITY = 128;

};

FRG__CLOSE_NAMESPACE(frg);

#include "Quadtree.inl"
