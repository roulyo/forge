#include <algorithm>

#include <forge/engine/math/spacetree/SpacetreeQueryResult.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
namespace utils
{
    template<class Collider>
    static bool CheckRangeIntersects(const Vector<Collider>& _range, const FloatBox& _box)
    {
        for (const Collider& range : _range)
        {
            if (range.Intersects(_box))
                return true;
        }

        return false;
    }

    template<class Collider>
    static bool CheckRangeContains(const Vector<Collider>& _range, const FloatBox& _box)
    {
        for (const Collider& range : _range)
        {
            if (range.Contains(_box))
                return true;
        }

        return false;
    }
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Octree<T>::Octree()
    : m_BoundingBox()
    , m_000(nullptr)
    , m_100(nullptr)
    , m_110(nullptr)
    , m_010(nullptr)
    , m_001(nullptr)
    , m_101(nullptr)
    , m_111(nullptr)
    , m_011(nullptr)
{}


//----------------------------------------------------------------------------------------------------------------------
template<class T>
Octree<T>::Octree(const FloatBox& _box)
    : m_BoundingBox(_box)
    , m_000(nullptr)
    , m_100(nullptr)
    , m_110(nullptr)
    , m_010(nullptr)
    , m_001(nullptr)
    , m_101(nullptr)
    , m_111(nullptr)
    , m_011(nullptr)
{}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Octree<T>::~Octree()
{
    Clear();
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Octree<T>::Reset(const FloatBox& _box)
{
    Clear();

    m_BoundingBox = _box;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Octree<T>::Clear()
{
    if (!IsLeaf())
    {
        delete m_000;
        m_000 = nullptr;
        delete m_100;
        m_100 = nullptr;
        delete m_110;
        m_110 = nullptr;
        delete m_010;
        m_010 = nullptr;
        delete m_001;
        m_001 = nullptr;
        delete m_101;
        m_101 = nullptr;
        delete m_111;
        m_111 = nullptr;
        delete m_011;
        m_011 = nullptr;
    }

    m_Elements.clear();
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Octree<T>::Insert(const T& _elt, const FloatBox& _spaceData)
{
    if (m_BoundingBox.Contains(_spaceData))
    {
        if (m_Elements.size() < QT_NODE_CAPACITY)
        {
            m_Elements.push_back(OctreeElement<T>(_elt, _spaceData));
        }
        else
        {
            if (IsLeaf())
            {
                Subdivide();
            }

            if (   !m_000->Insert(_elt, _spaceData)
                && !m_100->Insert(_elt, _spaceData)
                && !m_110->Insert(_elt, _spaceData)
                && !m_010->Insert(_elt, _spaceData)
                && !m_001->Insert(_elt, _spaceData)
                && !m_101->Insert(_elt, _spaceData)
                && !m_111->Insert(_elt, _spaceData)
                && !m_011->Insert(_elt, _spaceData))
            {
                // The node is full but the element we're trying to insert
                // cannot be contained in children
                m_Elements.push_back(OctreeElement<T>(_elt, _spaceData));
            }
        }

        return true;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Octree<T>::Remove(const T& _elt, const FloatBox& _approxArea)
{
    FRG__RETURN_IF(RemovePrecise(_elt, _approxArea), true);

    return Remove(_elt);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Octree<T>::Remove(const T& _elt)
{
    typename Vector<OctreeElement<T>>::const_iterator it
        = std::find_if(m_Elements.cbegin(), m_Elements.cend(), [&](const auto& _v) {
            return _v.GetData() == _elt;
        });

    if (it != m_Elements.cend())
    {
        m_Elements.erase(it);

        return true;
    }
    else
    {
        FRG__RETURN_IF(IsLeaf(), false);

        return m_000->Remove(_elt)
            || m_100->Remove(_elt)
            || m_110->Remove(_elt)
            || m_010->Remove(_elt)
            || m_001->Remove(_elt)
            || m_101->Remove(_elt)
            || m_111->Remove(_elt)
            || m_011->Remove(_elt);
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Octree<T>::Update(const T& _elt, const FloatBox& _spaceData)
{
    FRG__RETURN_IF(UpdatePrecise(_elt, _spaceData), true);
    FRG__RETURN_IF(!Remove(_elt), false);

    return Insert(_elt, _spaceData);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
u32 Octree<T>::GetAllElements(AbstractSpacetreeQueryResult<T>& _result) const
{
    u32 insertCount = static_cast<u32>(m_Elements.size());

    _result.AddRange(m_Elements.cbegin(), m_Elements.cend());

    if (!IsLeaf())
    {
        insertCount += m_000->GetAllElements(_result);
        insertCount += m_100->GetAllElements(_result);
        insertCount += m_110->GetAllElements(_result);
        insertCount += m_010->GetAllElements(_result);
        insertCount += m_001->GetAllElements(_result);
        insertCount += m_101->GetAllElements(_result);
        insertCount += m_111->GetAllElements(_result);
        insertCount += m_011->GetAllElements(_result);
    }

    return insertCount;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
template<class Collider>
u32 Octree<T>::QueryRange(const Collider& _range, AbstractSpacetreeQueryResult<T>& _result) const
{
    u32 insertCount = 0;

    if (_range.Contains(m_BoundingBox))
    {
        insertCount += GetAllElements(_result);
    }
    else if (_range.Intersects(m_BoundingBox))
    {
        for (const OctreeElement<T>& elt : m_Elements)
        {
            if (_range.Intersects(elt.GetBoundingBox()))
            {
                _result.Add(elt.GetData());
                ++insertCount;
            }
        }

        if (!IsLeaf())
        {
            insertCount += m_000->QueryRange(_range, _result);
            insertCount += m_100->QueryRange(_range, _result);
            insertCount += m_110->QueryRange(_range, _result);
            insertCount += m_010->QueryRange(_range, _result);
            insertCount += m_001->QueryRange(_range, _result);
            insertCount += m_101->QueryRange(_range, _result);
            insertCount += m_111->QueryRange(_range, _result);
            insertCount += m_011->QueryRange(_range, _result);
        }
    }

    return insertCount;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
template<class Collider>
u32 Octree<T>::QueryRange(const Vector<Collider>& _range, AbstractSpacetreeQueryResult<T>& _result) const
{
    u32 insertCount = 0;

    if (utils::CheckRangeContains(_range, m_BoundingBox))
    {
        insertCount += GetAllElements(_result);
    }
    else if (utils::CheckRangeIntersects(_range, m_BoundingBox))
    {
        for (const OctreeElement<T>& elt : m_Elements)
        {
            if (utils::CheckRangeIntersects(_range, elt.GetBoundingBox()))
            {
                _result.Add(elt.GetData());
                ++insertCount;
            }
        }

        if (!IsLeaf())
        {
            insertCount += m_000->QueryRange(_range, _result);
            insertCount += m_100->QueryRange(_range, _result);
            insertCount += m_110->QueryRange(_range, _result);
            insertCount += m_010->QueryRange(_range, _result);
            insertCount += m_001->QueryRange(_range, _result);
            insertCount += m_101->QueryRange(_range, _result);
            insertCount += m_111->QueryRange(_range, _result);
            insertCount += m_011->QueryRange(_range, _result);
        }
    }

    return insertCount;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
u32 Octree<T>::GetFirstRaycastCollision(const LineEquation& _ray, RaycastResult<T>& _result) const
{
    bool found = false;

    if (!isnan(m_BoundingBox.Intersects(_ray).x))
    {
        for (const OctreeElement<T>& elt : m_Elements)
        {
            Vector3f raycastResult = elt.GetBoundingBox().Intersects(_ray);

            if (!isnan(raycastResult.x))
            {
                _result.CollidedElement = elt.GetData();
                _result.CollisionPoint = raycastResult;
                found = true;
                break;
            }
        }

        if (!found && !IsLeaf())
        {
            // TODO: Consider vector's direction.
            // For now, let's start from the closest box to the isometric camera.
            return (u32)(   (bool)m_111->GetFirstRaycastCollision(_ray, _result)
                         || (bool)m_101->GetFirstRaycastCollision(_ray, _result)
                         || (bool)m_011->GetFirstRaycastCollision(_ray, _result)
                         || (bool)m_001->GetFirstRaycastCollision(_ray, _result)
                         || (bool)m_110->GetFirstRaycastCollision(_ray, _result)
                         || (bool)m_100->GetFirstRaycastCollision(_ray, _result)
                         || (bool)m_010->GetFirstRaycastCollision(_ray, _result)
                         || (bool)m_000->GetFirstRaycastCollision(_ray, _result));
        }
    }

    return found ? 1 : 0;
}

//----------------------------------------------------------------------------------------------------------------------
template <class T>
u32 Octree<T>::GetAllRaycastCollisions(const LineEquation& _ray, Vector<RaycastResult<T>>& _result) const
{
    u32 insertCount = 0;

    if (!isnan(m_BoundingBox.Intersects(_ray).x))
    {
        for (const OctreeElement<T>& elt : m_Elements)
        {
            Vector3f raycastResult = elt.GetBoundingBox().Intersects(_ray);

            if (!isnan(raycastResult.x))
            {
                RaycastResult<T> result;
                result.CollidedElement = elt.GetData();
                result.CollisionPoint = raycastResult;

                _result.push_back(result);
                ++insertCount;
            }
        }

        if (!IsLeaf())
        {
            insertCount += m_000->GetAllRaycastCollisions(_ray, _result);
            insertCount += m_100->GetAllRaycastCollisions(_ray, _result);
            insertCount += m_110->GetAllRaycastCollisions(_ray, _result);
            insertCount += m_010->GetAllRaycastCollisions(_ray, _result);
            insertCount += m_001->GetAllRaycastCollisions(_ray, _result);
            insertCount += m_101->GetAllRaycastCollisions(_ray, _result);
            insertCount += m_111->GetAllRaycastCollisions(_ray, _result);
            insertCount += m_011->GetAllRaycastCollisions(_ray, _result);
        }
    }

    return insertCount;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Octree<T>::Subdivide()
{
    f32 newWidth = m_BoundingBox.GetSize().w / 2.0f;
    f32 newDepth = m_BoundingBox.GetSize().d / 2.0f;
    f32 newHeight = m_BoundingBox.GetSize().h / 2.0f;

    m_000 = new Octree<T>(FloatBox(m_BoundingBox.GetPosition().x, m_BoundingBox.GetPosition().y, m_BoundingBox.GetPosition().z,
                                   newWidth, newDepth, newHeight));
    m_100 = new Octree<T>(FloatBox(m_BoundingBox.GetPosition().x + newWidth, m_BoundingBox.GetPosition().y, m_BoundingBox.GetPosition().z,
                                   newWidth, newDepth, newHeight));
    m_010 = new Octree<T>(FloatBox(m_BoundingBox.GetPosition().x, m_BoundingBox.GetPosition().y + newDepth, m_BoundingBox.GetPosition().z,
                                   newWidth, newDepth, newHeight));
    m_110 = new Octree<T>(FloatBox(m_BoundingBox.GetPosition().x + newWidth, m_BoundingBox.GetPosition().y + newDepth, m_BoundingBox.GetPosition().z,
                                   newWidth, newDepth, newHeight));
    m_001 = new Octree<T>(FloatBox(m_BoundingBox.GetPosition().x, m_BoundingBox.GetPosition().y, m_BoundingBox.GetPosition().z + newHeight,
                                   newWidth, newDepth, newHeight));
    m_101 = new Octree<T>(FloatBox(m_BoundingBox.GetPosition().x + newWidth, m_BoundingBox.GetPosition().y, m_BoundingBox.GetPosition().z + newHeight,
                                   newWidth, newDepth, newHeight));
    m_111 = new Octree<T>(FloatBox(m_BoundingBox.GetPosition().x + newWidth, m_BoundingBox.GetPosition().y + newDepth, m_BoundingBox.GetPosition().z + newHeight,
                                   newWidth, newDepth, newHeight));
    m_011 = new Octree<T>(FloatBox(m_BoundingBox.GetPosition().x, m_BoundingBox.GetPosition().y + newDepth, m_BoundingBox.GetPosition().z + newHeight,
                                   newWidth, newDepth, newHeight));
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Octree<T>::RemovePrecise(const T& _elt, const FloatBox& _spaceData)
{
    if (m_BoundingBox.Contains(_spaceData))
    {
        typename Vector<OctreeElement<T>>::const_iterator it
            = std::find_if(m_Elements.cbegin(), m_Elements.cend(), [&](const auto& _v) {
                return _v.GetData() == _elt;
            });

        if (it != m_Elements.cend())
        {
            m_Elements.erase(it);

            return true;
        }
        else
        {
            FRG__RETURN_IF(IsLeaf(), false);

            return m_000->RemovePrecise(_elt, _spaceData)
                || m_100->RemovePrecise(_elt, _spaceData)
                || m_110->RemovePrecise(_elt, _spaceData)
                || m_010->RemovePrecise(_elt, _spaceData)
                || m_001->RemovePrecise(_elt, _spaceData)
                || m_101->RemovePrecise(_elt, _spaceData)
                || m_111->RemovePrecise(_elt, _spaceData)
                || m_011->RemovePrecise(_elt, _spaceData);
        }
    }

    return false;
}


//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Octree<T>::UpdatePrecise(const T& _elt, const FloatBox& _spaceData)
{
    if (m_BoundingBox.Contains(_spaceData))
    {
        typename Vector<OctreeElement<T>>::iterator it
            = std::find_if(m_Elements.begin(), m_Elements.end(), [&](const auto& _v) {
                return _v.GetData() == _elt;
            });

        if (it != m_Elements.end())
        {
            it->SetBoundingBox(_spaceData);

            return true;
        }
        else
        {
            FRG__RETURN_IF(IsLeaf(), false);

            return m_000->UpdatePrecise(_elt, _spaceData)
                || m_100->UpdatePrecise(_elt, _spaceData)
                || m_110->UpdatePrecise(_elt, _spaceData)
                || m_010->UpdatePrecise(_elt, _spaceData)
                || m_001->UpdatePrecise(_elt, _spaceData)
                || m_101->UpdatePrecise(_elt, _spaceData)
                || m_111->UpdatePrecise(_elt, _spaceData)
                || m_011->UpdatePrecise(_elt, _spaceData);
        }
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Octree<T>::IsLeaf() const
{
    return m_000 == nullptr;
}

FRG__CLOSE_NAMESPACE(frg);
