#pragma once

#include <forge/engine/math/Types.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class OctreeElement
{
public:
    OctreeElement(const T& _data, const FloatBox& _box);
    ~OctreeElement();

    bool operator==(const OctreeElement<T>& _rhs) const;

private:
    FRG__CLASS_ATTR_RW(FloatBox, BoundingBox);
    FRG__CLASS_ATTR_RW(T, Data);

};

FRG__CLOSE_NAMESPACE(frg);

#include "OctreeElement.inl"
