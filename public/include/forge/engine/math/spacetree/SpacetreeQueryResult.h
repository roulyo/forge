#pragma once

#include <forge/engine/math/spacetree/OctreeElement.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class AbstractSpacetreeQueryResult
{
public:
    virtual void Add(const T& _elt) = 0;

    void AddRange(typename Vector<OctreeElement<T>>::const_iterator _first,
                  typename Vector<OctreeElement<T>>::const_iterator _last);

    template<class InputIt>
    void AddRange(InputIt _first, InputIt _last);

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class VectorSQR : public AbstractSpacetreeQueryResult<T>
{
public:
    void Add(const T& _elt) override;

private:
    FRG__CLASS_ATTR_R_(Vector<T>, Container);

};

FRG__CLOSE_NAMESPACE(frg);

#include "SpacetreeQueryResult.inl"
