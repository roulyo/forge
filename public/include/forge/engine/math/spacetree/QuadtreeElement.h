#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class QuadtreeElement
{
public:
    QuadtreeElement(const T& _data, const FloatQuad& _box);
    ~QuadtreeElement();

    bool operator==(const QuadtreeElement<T>& _rhs) const;

private:
    FRG__CLASS_ATTR_RW(FloatQuad, BoundingBox);
    FRG__CLASS_ATTR_RW(T, Data);

};

FRG__CLOSE_NAMESPACE(frg);

#include "QuadtreeElement.inl"
