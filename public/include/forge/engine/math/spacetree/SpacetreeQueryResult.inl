FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void AbstractSpacetreeQueryResult<T>::Add(const T& _elt)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void AbstractSpacetreeQueryResult<T>::AddRange(typename Vector<OctreeElement<T>>::const_iterator _first,
                                               typename Vector<OctreeElement<T>>::const_iterator _last)
{
    for (; _first != _last; ++_first)
        Add(_first->GetData());
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
template<class InputIt>
void AbstractSpacetreeQueryResult<T>::AddRange(InputIt _first, InputIt _last)
{
    for (; _first != _last; ++_first)
        Add(*_first);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void VectorSQR<T>::Add(const T& _elt)
{
    m_Container.push_back(_elt);
}

FRG__CLOSE_NAMESPACE(frg);
