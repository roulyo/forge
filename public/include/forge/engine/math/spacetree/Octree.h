#pragma once

#include <forge/engine/math/Types.h>
#include <forge/engine/math/spacetree/OctreeElement.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T> class AbstractSpacetreeQueryResult;

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class Octree
{
public:
    Octree();
    Octree(const FloatBox& _box);
    ~Octree();

    void Reset(const FloatBox& _box);
    void Clear();

    bool Insert(const T& _elt, const FloatBox& _spaceData);

    bool Remove(const T& _elt, const FloatBox& _approxArea);
    bool Remove(const T& _elt);

    bool Update(const T& _elt, const FloatBox& _spaceData);

    u32 GetAllElements(AbstractSpacetreeQueryResult<T>& _result) const;

    template<class Collider>
    u32 QueryRange(const Collider& _range,
                   AbstractSpacetreeQueryResult<T>& _result) const;
    template<class Collider>
    u32 QueryRange(const Vector<Collider>& _range,
                   AbstractSpacetreeQueryResult<T>& _result) const;

    u32 GetFirstRaycastCollision(const LineEquation& _ray, RaycastResult<T>& _result) const;
    u32 GetAllRaycastCollisions(const LineEquation& _ray, Vector<RaycastResult<T>>& _result) const;

private:
    void Subdivide();

    bool RemovePrecise(const T& _elt, const FloatBox& _approxArea);
    bool UpdatePrecise(const T& _elt, const FloatBox& _spaceData);

    bool IsLeaf() const;

private:
    Vector<OctreeElement<T>>    m_Elements;

    FloatBox    m_BoundingBox;

    Octree<T>*  m_000;
    Octree<T>*  m_100;
    Octree<T>*  m_110;
    Octree<T>*  m_010;

    Octree<T>*  m_001;
    Octree<T>*  m_101;
    Octree<T>*  m_111;
    Octree<T>*  m_011;

private:
    static const u32 QT_NODE_CAPACITY = 32;

};

FRG__CLOSE_NAMESPACE(frg);

#include "Octree.inl"
