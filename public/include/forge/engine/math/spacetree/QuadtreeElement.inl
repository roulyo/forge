FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
QuadtreeElement<T>::QuadtreeElement(const T& _data, const FloatQuad& _box)
    : m_BoundingBox(_box)
    , m_Data(_data)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
QuadtreeElement<T>::~QuadtreeElement()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool QuadtreeElement<T>::operator==(const QuadtreeElement<T>& _rhs) const
{
    return m_Data == _rhs.GetData();
}

FRG__CLOSE_NAMESPACE(frg);
