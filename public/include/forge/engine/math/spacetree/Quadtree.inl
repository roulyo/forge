#include <algorithm>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Quadtree<T>::Quadtree()
    : m_BoundingBox()
    , m_TopLeft(nullptr)
    , m_TopRight(nullptr)
    , m_BottomRight(nullptr)
    , m_BottomLeft(nullptr)
{}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Quadtree<T>::Quadtree(const FloatQuad& _box)
    : m_BoundingBox(_box)
    , m_TopLeft(nullptr)
    , m_TopRight(nullptr)
    , m_BottomRight(nullptr)
    , m_BottomLeft(nullptr)
{}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Quadtree<T>::~Quadtree()
{
    Clear();
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Quadtree<T>::Reset(const FloatQuad& _box)
{
    Clear();

    m_BoundingBox = _box;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Quadtree<T>::Clear()
{
    if (!IsLeaf())
    {
        delete m_TopLeft;
        m_TopLeft = nullptr;
        delete m_TopRight;
        m_TopRight = nullptr;
        delete m_BottomRight;
        m_BottomRight = nullptr;
        delete m_BottomLeft;
        m_BottomLeft = nullptr;
    }

    m_Elements.clear();
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Quadtree<T>::Insert(const T& _elt, const FloatQuad& _spaceData)
{
    if (m_BoundingBox.Contains(_spaceData))
    {
        if (m_Elements.size() < QT_NODE_CAPACITY)
        {
            m_Elements.push_back(QuadtreeElement<T>(_elt, _spaceData));
        }
        else
        {
            if (IsLeaf())
            {
                Subdivide();
            }

            if (   !m_TopLeft->Insert(_elt, _spaceData)
                && !m_TopRight->Insert(_elt, _spaceData)
                && !m_BottomRight->Insert(_elt, _spaceData)
                && !m_BottomLeft->Insert(_elt, _spaceData))
            {
                // The node is full but the element we're trying to insert
                // cannot be contained in children
                m_Elements.push_back(QuadtreeElement<T>(_elt, _spaceData));
            }
        }

        return true;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Quadtree<T>::Remove(const T& _elt, const FloatQuad& _approxArea)
{
    FRG__RETURN_IF(RemovePrecise(_elt, _approxArea), true);

    return Remove(_elt);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Quadtree<T>::Remove(const T& _elt)
{
    typename Vector<QuadtreeElement<T>>::const_iterator it
        = std::find_if(m_Elements.cbegin(), m_Elements.cend(), [&](const auto& _v) {
            return _v.GetData() == _elt;
        });

    if (it != m_Elements.end())
    {
        m_Elements.erase(it);

        return true;
    }
    else
    {
        if (IsLeaf())
            return false;

        return m_TopLeft->Remove(_elt)
            || m_TopRight->Remove(_elt)
            || m_BottomRight->Remove(_elt)
            || m_BottomLeft->Remove(_elt);
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Quadtree<T>::Update(const T& _elt, const FloatQuad& _spaceData)
{
    FRG__RETURN_IF(UpdatePrecise(_elt, _spaceData), true);
    FRG__RETURN_IF(!Remove(_elt), false);

    return Insert(_elt, _spaceData);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
u32 Quadtree<T>::QueryRange(const FloatQuad& _range, Vector<T>& _result) const
{
    i32 insertCount = 0;

    if (_range.Intersects(m_BoundingBox))
    {
        for (const QuadtreeElement<T>& elt : m_Elements)
        {
            if (_range.Intersects(elt.GetBoundingBox()))
            {
                _result.push_back(elt.GetData());
                ++insertCount;
            }
        }

        if (!IsLeaf())
        {
            insertCount += m_TopLeft->QueryRange(_range, _result);
            insertCount += m_TopRight->QueryRange(_range, _result);
            insertCount += m_BottomRight->QueryRange(_range, _result);
            insertCount += m_BottomLeft->QueryRange(_range, _result);
        }
    }

    return insertCount;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void Quadtree<T>::Subdivide()
{
    f32 newWidth = m_BoundingBox.GetSize().w / 2.0f;
    f32 newHeight = m_BoundingBox.GetSize().h / 2.0f;

    m_TopLeft = new Quadtree<T>(FloatQuad(m_BoundingBox.GetPosition().x, m_BoundingBox.GetPosition().y, newWidth, newHeight));
    m_TopRight = new Quadtree<T>(FloatQuad(m_BoundingBox.GetPosition().x + newWidth, m_BoundingBox.GetPosition().y, newWidth, newHeight));
    m_BottomRight = new Quadtree<T>(FloatQuad(m_BoundingBox.GetPosition().x + newWidth, m_BoundingBox.GetPosition().y + newHeight, newWidth, newHeight));
    m_BottomLeft = new Quadtree<T>(FloatQuad(m_BoundingBox.GetPosition().x, m_BoundingBox.GetPosition().y + newHeight, newWidth, newHeight));
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Quadtree<T>::RemovePrecise(const T& _elt, const FloatQuad& _spaceData)
{
    if (m_BoundingBox.Contains(_spaceData))
    {
        typename Vector<QuadtreeElement<T>>::const_iterator it
            = std::find_if(m_Elements.cbegin(), m_Elements.cend(), [&](const auto& _v) {
                return _v.GetData() == _elt;
            });

        if (it != m_Elements.cend())
        {
            m_Elements.erase(it);

            return true;
        }
        else
        {
            FRG__RETURN_IF(IsLeaf(), false);

            return m_TopLeft->RemovePrecise(_elt, _spaceData)
                || m_TopRight->RemovePrecise(_elt, _spaceData)
                || m_BottomRight->RemovePrecise(_elt, _spaceData)
                || m_BottomLeft->RemovePrecise(_elt, _spaceData);
        }
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Quadtree<T>::UpdatePrecise(const T& _elt, const FloatQuad& _spaceData)
{
    if (m_BoundingBox.Contains(_spaceData))
    {
        typename Vector<QuadtreeElement<T>>::iterator it
            = std::find_if(m_Elements.begin(), m_Elements.end(), [&](const auto& _v) {
                return _v.GetData() == _elt;
            });

        if (it != m_Elements.end())
        {
            it->SetBoundingBox(_spaceData);

            return true;
        }
        else
        {
            FRG__RETURN_IF(IsLeaf(), false);

            return m_TopLeft->UpdatePrecise(_elt, _spaceData)
                || m_TopRight->UpdatePrecise(_elt, _spaceData)
                || m_BottomRight->UpdatePrecise(_elt, _spaceData)
                || m_BottomLeft->UpdatePrecise(_elt, _spaceData);
        }
    }

    return false;
}


//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Quadtree<T>::IsLeaf() const
{
    return m_TopLeft == nullptr;
}

FRG__CLOSE_NAMESPACE(frg);
