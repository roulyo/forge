FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Sphere<T>::Sphere()
    : m_Center{ static_cast<T>(0), static_cast<T>(0), static_cast<T>(0)}
    , m_Radius(static_cast<T>(0))
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Sphere<T>::Sphere(const Vector3<T>& _center, T _radius)
    : m_Center(_center)
    , m_Radius(_radius)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Sphere<T>::Contains(const Vector3<T>& _point) const
{
    return static_cast<T>(std::sqrt(  std::pow(_point.x - m_Center.x, 2)
                                    + std::pow(_point.y - m_Center.y, 2)
                                    + std::pow(_point.z - m_Center.z, 2)))
                          <= m_Radius;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Sphere<T>::Contains(const AABox<T>& _box) const
{
    const Vector3f pos = _box.GetPosition();
    const Vector3f size = _box.GetSize();

    return Contains(pos)
        && Contains({ pos.x, pos.y + size.d, pos.z })
        && Contains({ pos.x, pos.y, pos.z + size.h })
        && Contains({ pos.x, pos.y + size.d, pos.z + size.h })
        && Contains({ pos.x + size.w, pos.y , pos.z })
        && Contains({ pos.x + size.w, pos.y + size.d , pos.z })
        && Contains({ pos.x + size.w, pos.y , pos.z + size.h })
        && Contains({ pos.x + size.w, pos.y + size.d, pos.z + size.h });
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Sphere<T>::Intersects(const AABox<T>& _box) const
{
    if (GetRadius() == 0)
        return false;

    T distSq = std::pow(GetRadius(), static_cast<T>(2));

    if (GetCenter().x < _box.GetPosition().x)
        distSq -= std::pow(GetCenter().x - _box.GetPosition().x, static_cast<T>(2));
    else if (GetCenter().x > _box.GetPosition().x + _box.GetSize().w)
        distSq -= std::pow(GetCenter().x - (_box.GetPosition().x + _box.GetSize().w), static_cast<T>(2));

    if (GetCenter().y < _box.GetPosition().y)
        distSq -= std::pow(GetCenter().y - _box.GetPosition().y, static_cast<T>(2));
    else if (GetCenter().y > _box.GetPosition().y + _box.GetSize().d)
        distSq -= std::pow(GetCenter().y - (_box.GetPosition().y + _box.GetSize().d), static_cast<T>(2));

    if (GetCenter().z < _box.GetPosition().z)
        distSq -= std::pow(GetCenter().z - _box.GetPosition().z, static_cast<T>(2));
    else if (GetCenter().z > _box.GetPosition().z + _box.GetSize().h)
        distSq -= std::pow(GetCenter().z - (_box.GetPosition().z + _box.GetSize().h), static_cast<T>(2));

    return distSq > 0;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
ManhattanSphere<T>::ManhattanSphere()
    : Sphere<T>()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
ManhattanSphere<T>::ManhattanSphere(const Vector3<T>& _center, T _radius)
    : Sphere<T>(_center, _radius)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool ManhattanSphere<T>::Contains(const Vector3<T>& _point) const
{
    return   std::abs(_point.x - this->m_Center.x)
           + std::abs(_point.y - this->m_Center.y)
           + std::abs(_point.z - this->m_Center.z)
           <= this->m_Radius;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool ManhattanSphere<T>::Contains(const AABox<T>& _box) const
{
    const Vector3f pos = _box.GetPosition();
    const Vector3f size = _box.GetSize();

    return Contains(pos)
        && Contains({ pos.x, pos.y + size.d, pos.z })
        && Contains({ pos.x, pos.y, pos.z + size.h })
        && Contains({ pos.x, pos.y + size.d, pos.z + size.h })
        && Contains({ pos.x + size.w, pos.y , pos.z })
        && Contains({ pos.x + size.w, pos.y + size.d , pos.z })
        && Contains({ pos.x + size.w, pos.y , pos.z + size.h })
        && Contains({ pos.x + size.w, pos.y + size.d, pos.z + size.h });
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool ManhattanSphere<T>::Intersects(const AABox<T>& _box) const
{
    if (this->m_Radius == 0)
        return false;

    T dist = this->m_Radius;

    if (this->m_Center.x < _box.GetPosition().x)
        dist -= std::abs(this->m_Center.x - _box.GetPosition().x);
    else if (this->m_Center.x > _box.GetPosition().x + _box.GetSize().w)
        dist -= std::abs(this->m_Center.x - (_box.GetPosition().x + _box.GetSize().w));

    if (this->m_Center.y < _box.GetPosition().y)
        dist -= std::abs(this->m_Center.y - _box.GetPosition().y);
    else if (this->m_Center.y > _box.GetPosition().y + _box.GetSize().d)
        dist -= std::abs(this->m_Center.y - (_box.GetPosition().y + _box.GetSize().d));

    if (this->m_Center.z < _box.GetPosition().z)
        dist -= std::abs(this->m_Center.z - _box.GetPosition().z);
    else if (this->m_Center.z > _box.GetPosition().z + _box.GetSize().h)
        dist -= std::abs(this->m_Center.z - (_box.GetPosition().z + _box.GetSize().h));

    return dist > 0;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Cylinder<T>::Cylinder()
    : m_Base{ static_cast<T>(0), static_cast<T>(0), static_cast<T>(0)}
    , m_Radius(static_cast<T>(0))
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Cylinder<T>::Cylinder(const Vector3<T>& _base, T _radius)
    : m_Base(_base)
    , m_Radius(_radius)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Cylinder<T>::Contains(const Vector3<T>& _point) const
{
    return static_cast<T>(std::sqrt(  std::pow(_point.x - m_Base.x, 2)
                                    + std::pow(_point.y - m_Base.y, 2)))
                          <= m_Radius;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Cylinder<T>::Contains(const AABox<T>& _box) const
{
    const Vector3f pos = _box.GetPosition();
    const Vector3f size = _box.GetSize();

    return Contains(pos)
        && Contains({ pos.x, pos.y + size.d, pos.z })
        && Contains({ pos.x + size.w, pos.y , pos.z })
        && Contains({ pos.x + size.w, pos.y + size.d , pos.z });
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool Cylinder<T>::Intersects(const AABox<T>& _box) const
{
    if (_box.Contains({ m_Base.x, m_Base.y, _box.GetCenter().z }))
        return true;

    if (m_Radius == 0)
        return false;

    T distSq = std::pow(m_Radius, static_cast<T>(2));

    if (m_Base.x < _box.GetPosition().x)
        distSq -= std::pow(m_Base.x - _box.GetPosition().x, static_cast<T>(2));
    else if (m_Base.x > _box.GetPosition().x + _box.GetSize().w)
        distSq -= std::pow(m_Base.x - (_box.GetPosition().x + _box.GetSize().w), static_cast<T>(2));

    if (m_Base.y < _box.GetPosition().y)
        distSq -= std::pow(m_Base.y - _box.GetPosition().y, static_cast<T>(2));
    else if (m_Base.y > _box.GetPosition().y + _box.GetSize().d)
        distSq -= std::pow(m_Base.y - (_box.GetPosition().y + _box.GetSize().d), static_cast<T>(2));

    return distSq > 0;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
ManhattanCylinder<T>::ManhattanCylinder()
    : Cylinder<T>()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
ManhattanCylinder<T>::ManhattanCylinder(const Vector3<T>& _base, T _radius)
    : Cylinder<T>(_base, _radius)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool ManhattanCylinder<T>::Contains(const Vector3<T>& _point) const
{
    return        std::abs(_point.x - this->m_Base.x)
                + std::abs(_point.y - this->m_Base.y)
                <= this->m_Radius;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool ManhattanCylinder<T>::Contains(const AABox<T>& _box) const
{
    const Vector3f pos = _box.GetPosition();
    const Vector3f size = _box.GetSize();

    return Contains(pos)
        && Contains({ pos.x, pos.y + size.d, pos.z })
        && Contains({ pos.x + size.w, pos.y , pos.z })
        && Contains({ pos.x + size.w, pos.y + size.d , pos.z });
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool ManhattanCylinder<T>::Intersects(const AABox<T>& _box) const
{
    if (_box.Contains({ this->m_Base.x, this->m_Base.y, _box.GetCenter().z }))
        return true;

    if (this->m_Radius == 0)
        return false;

    T dist = this->m_Radius;

    if (this->m_Base.x < _box.GetPosition().x)
        dist -= std::abs(this->m_Base.x - _box.GetPosition().x);
    else if (this->m_Base.x > _box.GetPosition().x + _box.GetSize().w)
        dist -= std::abs(this->m_Base.x - (_box.GetPosition().x + _box.GetSize().w));

    if (this->m_Base.y < _box.GetPosition().y)
        dist -= std::abs(this->m_Base.y - _box.GetPosition().y);
    else if (this->m_Base.y > _box.GetPosition().y + _box.GetSize().d)
        dist -= std::abs(this->m_Base.y - (_box.GetPosition().y + _box.GetSize().d));

    return dist > 0;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
AAQuad<T>::AAQuad()
    : m_Position({ static_cast<T>(0), static_cast<T>(0) })
    , m_Size({ static_cast<T>(0), static_cast<T>(0) })
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
AAQuad<T>::AAQuad(T _x, T _y, T _width, T _height)
    : m_Position({ _x, _y })
    , m_Size({ _width, _height })
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
AAQuad<T>::AAQuad(const Vector2<T>& _position, const Vector2<T>& _size)
    : m_Position(_position)
    , m_Size(_size)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
AAQuad<T>::~AAQuad()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool AAQuad<T>::Contains(const Vector2<T>& _point) const
{
    return (_point.x >= m_Position.x)
        && (_point.x <= (m_Position.x + m_Size.w))
        && (_point.y >= m_Position.y)
        && (_point.y <= (m_Position.y + m_Size.h));
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool AAQuad<T>::Contains(const AAQuad<T>& _quad) const
{
    return (_quad.GetPosition().x >= m_Position.x)
        && (_quad.GetPosition().x + _quad.GetSize().w <= (m_Position.x + m_Size.w))
        && (_quad.GetPosition().y >= m_Position.y)
        && (_quad.GetPosition().y + _quad.GetSize().h <= (m_Position.y + m_Size.h));
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool AAQuad<T>::Intersects(const AAQuad<T>& _quad) const
{
    T interLeft = std::max(_quad.GetPosition().x, m_Position.x);
    T interTop = std::max(_quad.GetPosition().y, m_Position.y);
    T interRight = std::min(_quad.GetPosition().x + _quad.GetSize().w, m_Position.x + m_Size.w);
    T interBottom = std::min(_quad.GetPosition().y + _quad.GetSize().h, m_Position.y + m_Size.h);

    return ((interLeft < interRight) && (interTop < interBottom));
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Vector2<T> AAQuad<T>::GetCenter() const
{
    return Vector2<T>(m_Position.x + (m_Size.w / static_cast<T>(2)),
                      m_Position.y + (m_Size.h / static_cast<T>(2)));
}


//----------------------------------------------------------------------------------------------------------------------
template<class T>
AABox<T>::AABox()
    : m_Position({ static_cast<T>(0), static_cast<T>(0), static_cast<T>(0) })
    , m_Size({ static_cast<T>(0), static_cast<T>(0), static_cast<T>(0) })
{
}


//----------------------------------------------------------------------------------------------------------------------
template<class T>
AABox<T>::AABox(T _x, T _y, T _z, T _width, T _depth, T _height)
    : m_Position({ _x, _y, _z })
    , m_Size({ _width, _depth, _height })
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
AABox<T>::AABox(const Vector3<T>& _position, const Vector3<T>& _size)
    : m_Position(_position)
    , m_Size(_size)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
AABox<T>::~AABox()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool AABox<T>::Contains(const Vector3<T>& _point) const
{
    return (_point.x >= m_Position.x)
        && (_point.x <= (m_Position.x + m_Size.w))
        && (_point.y >= m_Position.y)
        && (_point.y <= (m_Position.y + m_Size.d))
        && (_point.z >= m_Position.z)
        && (_point.z <= (m_Position.z + m_Size.h));
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool AABox<T>::Contains(const AABox<T>& _box) const
{
    return (_box.GetPosition().x >= m_Position.x)
        && (_box.GetPosition().x + _box.GetSize().w <= (m_Position.x + m_Size.w))
        && (_box.GetPosition().y >= m_Position.y)
        && (_box.GetPosition().y + _box.GetSize().d <= (m_Position.y + m_Size.d))
        && (_box.GetPosition().z >= m_Position.z)
        && (_box.GetPosition().z + _box.GetSize().h <= (m_Position.z + m_Size.h));
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
bool AABox<T>::Intersects(const AABox<T>& _box) const
{
    T interLeft = std::max(_box.GetPosition().x, m_Position.x);
    T interRight = std::min(_box.GetPosition().x + _box.GetSize().w, m_Position.x + m_Size.w);

    T interTop = std::max(_box.GetPosition().y, m_Position.y);
    T interBottom = std::min(_box.GetPosition().y + _box.GetSize().d, m_Position.y + m_Size.d);

    T interFront = std::max(_box.GetPosition().z, m_Position.z);
    T interBack = std::min(_box.GetPosition().z + _box.GetSize().h, m_Position.z + m_Size.h);

    return (interLeft <= interRight) && (interTop <= interBottom) && (interFront <= interBack);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Vector3f AABox<T>::Intersects(const LineEquation& _line) const
{
    f32 k, x, y, z;

#define FRG__FIND_LINE_PLANE_INTERSECTION(constantAxis, constantValue, Axis1, Axis2)    \
    constantAxis = (constantValue);                                                     \
    k = ((constantValue) - _line.Point. constantAxis) / _line.Vector. constantAxis;     \
    Axis1 = _line.Vector. Axis1 * k + _line.Point. Axis1;                               \
    Axis2 = _line.Vector. Axis2 * k + _line.Point. Axis2;                               \
    if (   Axis1 >= m_Position. Axis1 && Axis1 <= m_Position. Axis1 + m_Size. Axis1     \
        && Axis2 >= m_Position. Axis2 && Axis2 <= m_Position. Axis2 + m_Size. Axis2)    \
    return { x, y, z };

    FRG__FIND_LINE_PLANE_INTERSECTION(z, m_Position.z + m_Size.h, x, y);
    FRG__FIND_LINE_PLANE_INTERSECTION(y, m_Position.y + m_Size.d, x, z);
    FRG__FIND_LINE_PLANE_INTERSECTION(x, m_Position.x + m_Size.w, y, z);
    FRG__FIND_LINE_PLANE_INTERSECTION(z, m_Position.z, x, y);
    FRG__FIND_LINE_PLANE_INTERSECTION(y, m_Position.y, x, z);
    FRG__FIND_LINE_PLANE_INTERSECTION(x, m_Position.x, y, z);

#undef FRG__FIND_LINE_PLANE_INTERSECTION

    return { NAN, NAN, NAN };

}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
Vector3<T> AABox<T>::GetCenter() const
{
    return Vector3<T>({ m_Position.x + (m_Size.w / static_cast<T>(2)),
                        m_Position.y + (m_Size.d / static_cast<T>(2)),
                        m_Position.z + (m_Size.h / static_cast<T>(2)) });
}

FRG__CLOSE_NAMESPACE(frg);
