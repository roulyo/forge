#pragma once

#include <functional>

#include <forge/engine/event/SystemEvent.h>
#include <forge/engine/presentation/GUIWidget.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractGUIBehavior
{
public:
    virtual ~AbstractGUIBehavior();
    virtual bool ShouldCaptureEvent(const SystemEvent& _event) = 0;

    void Trigger(const SystemEvent& _event);

protected:
    AbstractGUIBehavior(const BehaviorCallback& _callback);

public:
    FRG__CLASS_ATTR_RW(GUIWidget*, Owner);
    FRG__CLASS_ATTR___(BehaviorCallback, Callback);

};

FRG__CLOSE_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
#define FRG__BIND_BEHAVIOR(C, F, O)                         \
    (std::bind(&C::F, (C*)(O), (std::placeholders::_1),     \
                               (std::placeholders::_2)))
