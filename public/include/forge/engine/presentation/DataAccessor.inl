FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class C, class... Cs>
DataAccessor<C, Cs...>::DataAccessor()
    : DataAccessor<Cs...>()
{
    DataAccessor<C, Cs...>::m_Signature |= C::Signature;
}

//----------------------------------------------------------------------------------------------------------------------
template<class C>
const Vector<Entity::CPtr>& DataAccessor<>::GetColumn() const
{
    FRG__ASSERT(DoesTarget(C::Signature));

    return GetColumn(C::Signature);
}

FRG__CLOSE_NAMESPACE(frg);
