#pragma once

#include <functional>

#include <forge/engine/event/SystemEvent.h>
#include <forge/engine/math/Types.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class GUIWidget;
typedef std::function<void(GUIWidget*, const SystemEvent&)>  BehaviorCallback;

//----------------------------------------------------------------------------------------------------------------------
enum GUIGravity
{
    Center          = 0,
    Top             = 1,
    Right           = 1 << 1,
    Bottom          = 1 << 2,
    Left            = 1 << 3,
    TopLeft         = Top | Left,
    TopRight        = Top | Right,
    BottomRight     = Bottom | Right,
    BottomLeft      = Bottom | Left,
};

//----------------------------------------------------------------------------------------------------------------------
class GUISpatializable
{
public:
    GUISpatializable();

    void SetDirty();

    bool ContainsPixel(const frg::Vector2f& _pxCoord) const;

    void SetGravity(GUIGravity _gravity);
    void SetRelativeSize(const Vector2f& _relSize);
    void SetRelativePadding(const Vector2f& _relPadding);
    void SetPixelSize(const Vector2f& _pxSize);
    void SetPixelPadding(const Vector2f& _pxPadding);

    GUIGravity GetGravity() const;
    const Vector2f& GetRelativeSize() const;
    const Vector2f& GetRelativePadding() const;
    const Vector2f& GetRelativePosition() const;
    const Vector2f& GetPixelSize() const;
    const Vector2f& GetPixelPadding() const;
    const Vector2f& GetPixelPosition() const;

protected:
    bool ComputePixelData(const Vector2f& _parentPosition,
                          const Vector2f& _parentSize);

    bool IsReady() const;

private:
    GUIGravity  m_Gravity;

    Vector2f    m_RelativePosition;
    Vector2f    m_RelativePadding;
    Vector2f    m_RelativeSize;

    Vector2f    m_PixelPosition;
    Vector2f    m_PixelPadding;
    Vector2f    m_PixelSize;

    FloatQuad   m_RenderedGeometry;

    bool        m_IsSizeRelative;
    bool        m_IsPaddingRelative;
    bool        m_IsReady;

};


FRG__CLOSE_NAMESPACE(frg);
