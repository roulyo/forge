#pragma once

#include <forge/engine/presentation/GUIWidget.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractViewController;

//----------------------------------------------------------------------------------------------------------------------
class View : public GUISpatializable
{
public:
    View();

    void AddWidget(GUIWidget* _widget);

    virtual void OnViewOpened();
    virtual void OnViewClosed();

    void ComputeRenderedGeometry(const Vector2f& _targetSize);

public:
    FRG__CLASS_ATTR_RW(Vector<GUIWidget*>, Widgets);

    FRG__CLASS_ATTR_RW(const AbstractViewController*, Owner);

};

FRG__CLOSE_NAMESPACE(frg);