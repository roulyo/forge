#pragma once

#include <forge/engine/ecs/Component.h>
#include <forge/engine/ecs/Entity.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class... Cs>
class DataAccessor
{
};

//----------------------------------------------------------------------------------------------------------------------
template<class C, class... Cs>
class DataAccessor<C, Cs...> : public DataAccessor<Cs...>
{
public:
    DataAccessor();
};

//----------------------------------------------------------------------------------------------------------------------
template<>
class DataAccessor<>
{
public:
    DataAccessor();
    ~DataAccessor();

    void Init() const;

protected:
    template<class T>
    const Vector<Entity::CPtr>& GetColumn() const;

private:
    const Vector<Entity::CPtr>& GetColumn(const ComponentSignature& _signature) const;
    bool DoesTarget(const ComponentSignature& _signature) const;

public:
    FRG__CLASS_ATTR_R_(ComponentSignature, Signature);

};

//----------------------------------------------------------------------------------------------------------------------
typedef DataAccessor<>    AbstractDataAccessor;

FRG__CLOSE_NAMESPACE(frg);

#include "DataAccessor.inl"
