#pragma once

#include <forge/engine/presentation/View.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractViewController
{
public:
    virtual void Update() = 0;

    void Start();
    void Stop();

protected:
    virtual void OnStart();
    virtual void OnStop();

    void OpenView(View* _view) const;
    void CloseView(View* _view) const;
    void SafeOpenView(View* _view) const;
    void SafeCloseView(View* _view) const;
};

FRG__CLOSE_NAMESPACE(frg);
