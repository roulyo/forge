#pragma once

#include <functional>

#include <forge/engine/presentation/GUITypes.h>

//----------------------------------------------------------------------------------------------------------------------
namespace sf
{
    class Drawable;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractDrawable;
class AbstractGUIBehavior;

//----------------------------------------------------------------------------------------------------------------------
class GUIWidget : public GUISpatializable
{
public:
    class AbstractGUIDrawable
    {
    public:
        typedef sf::Drawable    DrawableData;

    public:
        AbstractGUIDrawable();
        virtual ~AbstractGUIDrawable() = 0;
        void OnElementGeometryComputed(const Vector2f& _position, const Vector2f& _size) const;
        const DrawableData& GetDrawableData() const;
        virtual AbstractDrawable* operator->() = 0;
        virtual AbstractDrawable& operator*() = 0;

    public:
        GUIGravity  Gravity;
        Vector2f    RelativePadding;

    protected:
        AbstractDrawable*   m_Drawable;

    };

    template<class T>
    class GUIDrawable : public AbstractGUIDrawable
    {
    public:
        GUIDrawable();
        ~GUIDrawable() override;
        T* operator->() override;
        T& operator*() override;

    private:
        T m_Drawable;

    };


public:
    GUIWidget();
    virtual ~GUIWidget();

    void AddDrawable(AbstractGUIDrawable* _drawable);

    template<class T>
    void AddBehavior(const BehaviorCallback& _callback);

    void AddChild(GUIWidget* _child);
    void RemoveChild(GUIWidget* _child);
    void ClearChildren();

    void ComputeRenderedGeometry(const Vector2f& _position, const Vector2f& _size);
    virtual void OnRenderedGeometryComputed(const Vector2f& _position, const Vector2f& _size);


public:
    FRG__CLASS_ATTR_R_(Vector<AbstractGUIDrawable*>, Drawables);
    FRG__CLASS_ATTR_R_(Vector<AbstractGUIBehavior*>, Behaviors);

    FRG__CLASS_ATTR_RW(GUIWidget*, Parent);
    FRG__CLASS_ATTR_R_(Vector<GUIWidget*>, Children);

};

FRG__CLOSE_NAMESPACE(frg);

#include "GUIWidget.inl"
