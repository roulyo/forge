#pragma once

#include <forge/engine/window/Cursor.h>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
class PresentationAPI
{
public:
    PresentationAPI() = delete;
    ~PresentationAPI() = delete;

public:
    static Vector2f GetCursorPosition();

    static Vector2f ScreenToGUIPixel(const Vector2f& _coordScreen);
    static Vector2f ScreenToGUIRelative(const Vector2f& _coordScreen);

    static void SetCursor(const Cursor::CPtr& _cursor);

};

FRG__CLOSE_NAMESPACE(frg)
