#include <forge/engine/event/SystemEvent.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
GUIWidget::GUIDrawable<T>::GUIDrawable()
{
    AbstractGUIDrawable::m_Drawable = &m_Drawable;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
GUIWidget::GUIDrawable<T>::~GUIDrawable()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
T* GUIWidget::GUIDrawable<T>::operator->()
{
    return &m_Drawable;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
T& GUIWidget::GUIDrawable<T>::operator*()
{
    return m_Drawable;
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
void GUIWidget::AddBehavior(const BehaviorCallback& _callback)
{
    T* behavior = new T(_callback);
    behavior->SetOwner(this);
    m_Behaviors.push_back(behavior);
}

FRG__CLOSE_NAMESPACE(frg);
