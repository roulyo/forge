#pragma once

#if defined(FRG_DEBUG) || defined(FRG_DEBUG_OPTI)
#   define FRG_USE_LOG
#   define FRG_USE_ASSERT
#   define FRG_USE_PROFILING
#   define FRG_USE_DEBUG_INFO

#   define RTB_LOGGING_ENABLED
#   define RTB_PROFILING_ENABLED
#elif defined(FRG_RELEASE)
#endif

#if defined (_HAS_EXCEPTIONS)
#   undef _HAS_EXCEPTIONS
#endif

#define _HAS_EXCEPTIONS 0

#define _USE_MATH_DEFINES
