#pragma once

#include <cassert>

#include <rtb/logging/Logging.h>
#include <rtb/profiling/Profiling.h>

//----------------------------------------------------------------------------------------------------------------------
#define FRG__RETURN_IF(_cond_, ...)     if (_cond_) return __VA_ARGS__;
#define FRG__BREAK_IF(_cond_)           if (_cond_) break;
#define FRG__CONTINUE_IF(_cond_)        if (_cond_) continue;


//----------------------------------------------------------------------------------------------------------------------
#define FRG__CLASS_ATTR___(_type_, _name_)      FRG__CLASS_ATTR(_type_, _name_, protected, protected)
#define FRG__CLASS_ATTR_R_(_type_, _name_)      FRG__CLASS_ATTR(_type_, _name_, public, protected)
#define FRG__CLASS_ATTR__W(_type_, _name_)      FRG__CLASS_ATTR(_type_, _name_, protected, public)
#define FRG__CLASS_ATTR_RW(_type_, _name_)      FRG__CLASS_ATTR(_type_, _name_, public, public)

#define FRG__CLASS_ATTR(_type_, _name_, _rMod_, _wMod_)\
    protected: _type_ m_##_name_;\
    _rMod_: _type_ const& Get##_name_() const { return m_##_name_; }\
    _wMod_: _type_ & _name_() { return m_##_name_; }\
    _wMod_: void Set##_name_(_type_ const& _value) { m_##_name_  = _value; }\
    private:


//----------------------------------------------------------------------------------------------------------------------
#if defined(FRG_USE_LOG)
#   define FRG__LOG_DEBUG(Category, str, ...)       rtb_LogDebug(Category, str, ##__VA_ARGS__)
#   define FRG__LOG_MESSAGE(Category, str, ...)     rtb_LogInfo(Category, str, ##__VA_ARGS__)
#   define FRG__LOG_WARNING(Category, str, ...)     rtb_LogWarning(Category, str, ##__VA_ARGS__)
#   define FRG__LOG_ERROR(Category, str, ...)       rtb_LogError(Category, str, ##__VA_ARGS__)
#else
#   define FRG__LOG_DEBUG(Category, str, ...)       ((void) 0)
#   define FRG__LOG_MESSAGE(Category, str, ...)     ((void) 0)
#   define FRG__LOG_WARNING(Category, str, ...)     ((void) 0)
#   define FRG__LOG_ERROR(Category, str, ...)       ((void) 0)
#endif

//----------------------------------------------------------------------------------------------------------------------
#if defined(FRG_USE_PROFILING)
#   define FRG__START_FRAME_PROFILING()             rtb_StartProfiling()
#   define FRG__PROFILE_POINT(tag)                  rtb_Profile(tag)
#   define FRG__STOP_FAME_PROFILING()               rtb_StopProfiling()
#else
#   define FRG__START_FRAME_PROFILING()             ((void) 0)
#   define FRG__PROFILE_POINT(tag)                  ((void) 0)
#   define FRG__STOP_FAME_PROFILING()               ((void) 0)
#endif

//----------------------------------------------------------------------------------------------------------------------
#if defined (FRG_USE_ASSERT)
#   define FRG__ASSERT(_cond_)                      CrashIfFalse(_cond_);
#   define FRG__LOG_ASSERT(_cond_, str)             LogAndCrashIfFalse(_cond_, str);

#pragma warning(disable: 4312)

inline void CrashIfFalse(bool _cond)
{
    if (!_cond)
    {
        int* p = (int*)0xd34db33f;
        int i = *p;
        (void)i;
    }
}

inline void LogAndCrashIfFalse(bool _cond, const char* _msg)
{
    if (!_cond)
    {
        FRG__LOG_ERROR(0, _msg);
        int* p = (int*)0xd34db33f;
        int i = *p;
        (void)i;
    }
}

#pragma warning(default: 4312)

#else
#   define FRG__ASSERT(_cond_)                      ((void) 0)
#   define FRG__LOG_ASSERT(_cond_, str)             ((void) 0)
#endif


//----------------------------------------------------------------------------------------------------------------------
#if defined(FRG_USE_DEBUG_INFO)

#   define FRG__CREATE_NAMESPACE(ns) enum { namespace_##ns = false };                                \
                                namespace ns { enum { namespace_##ns = true }; }

#   define FRG__OPEN_NAMESPACE(ns)   static_assert(!namespace_##ns, "namespace already opened");     \
                                namespace ns {

#   define FRG__CLOSE_NAMESPACE(ns)  }                                                               \
                                static_assert(!namespace_##ns, "not closing right namespace");

#else

#   define FRG__CREATE_NAMESPACE(ns)
#   define FRG__OPEN_NAMESPACE(ns)   namespace ns {
#   define FRG__CLOSE_NAMESPACE(ns)  }

#endif

//----------------------------------------------------------------------------------------------------------------------
#define FRG__DECL_SINGLETON(class)  rtb_DeclSingleton(class)

//----------------------------------------------------------------------------------------------------------------------
#define FRG__SCOPED_LOCK(mtx)  rtb_ScopedLock(mtx)

//----------------------------------------------------------------------------------------------------------------------
#define FRG__ALLOCA(T)  static_cast<T*>(alloca(sizeof (T)))
