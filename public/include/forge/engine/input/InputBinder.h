#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class BaseInputBinder
{
public:
    // Keyboard
    virtual void OnKeyPress() const;
    virtual void OnKeyRelease() const;

    // Mouse
    virtual void OnMouseButtonPress(i32 _x, i32 _y) const;
    virtual void OnMouseButtonRelease(i32 _x, i32 _y) const;
    virtual void OnMouseScroll(i32 _x, i32 _y, f32 _delta) const;
    virtual void OnMouseMove(i32 _x, i32 _y) const;

};

//----------------------------------------------------------------------------------------------------------------------
template<class KeyPressEvent, class KeyReleaseEvent>
class KeyBinder : public BaseInputBinder
{
public:
    void OnKeyPress() const override;
    void OnKeyRelease() const override;

};

//----------------------------------------------------------------------------------------------------------------------
template<class MouseButtonPressEvent, class MouseButtonReleaseEvent>
class MouseButtonBinder : public BaseInputBinder
{
public:
    void OnMouseButtonPress(i32 _x, i32 _y) const override;
    void OnMouseButtonRelease(i32 _x, i32 _y) const override;

};

//----------------------------------------------------------------------------------------------------------------------
template<class MouseScrollEvent>
class MouseScrollBinder : public BaseInputBinder
{
public:
    void OnMouseScroll(i32 _x, i32 _y, f32 _delta) const override;

};

//----------------------------------------------------------------------------------------------------------------------
template<class MouseMoveEvent>
class MouseMoveBinder : public BaseInputBinder
{
public:
    void OnMouseMove(i32 _x, i32 _y) const override;

};

FRG__CLOSE_NAMESPACE(frg);

#include "InputBinder.inl"
