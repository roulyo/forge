#pragma once

#include <forge/engine/input/InputTypes.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class BaseInputBinder;

//----------------------------------------------------------------------------------------------------------------------
class InputMappingScheme
{
public:
    InputMappingScheme();

public:
    void RegisterInputBinder(Keyboard::Key _key, const BaseInputBinder* _binder);
    void RegisterInputBinder(Mouse::Button _mouseButton, const BaseInputBinder* _binder);
    void RegisterInputBinder(Mouse::Wheel _mouseWheel, const BaseInputBinder* _binder);
    void RegisterInputBinder(Mouse::Move _mouseMove, const BaseInputBinder* _binder);

    const BaseInputBinder* GetInputBinder(u32 _inputIndex) const;

private:
    void RegisterInputBinder(u32 _inputIndex, const BaseInputBinder* _binder);

private:
    const BaseInputBinder* m_InputBinders[Input::Count];

};

FRG__CLOSE_NAMESPACE(frg);
