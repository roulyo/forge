FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class KeyPressEvent, class KeyReleaseEvent>
void KeyBinder<KeyPressEvent, KeyReleaseEvent>::OnKeyPress() const
{
    KeyPressEvent::Broadcast();
}

//----------------------------------------------------------------------------------------------------------------------
template<class KeyPressEvent, class KeyReleaseEvent>
void KeyBinder<KeyPressEvent, KeyReleaseEvent>::OnKeyRelease() const
{
    KeyReleaseEvent::Broadcast();
}

//----------------------------------------------------------------------------------------------------------------------
template<class MouseButtonPressEvent, class MouseButtonReleaseEvent>
void MouseButtonBinder<MouseButtonPressEvent, MouseButtonReleaseEvent>::OnMouseButtonPress(i32 _x, i32 _y) const
{
    MouseButtonPressEvent::Broadcast(_x, _y);
}

//----------------------------------------------------------------------------------------------------------------------
template<class MouseButtonPressEvent, class MouseButtonReleaseEvent>
void MouseButtonBinder<MouseButtonPressEvent, MouseButtonReleaseEvent>::OnMouseButtonRelease(i32 _x, i32 _y) const
{
    MouseButtonReleaseEvent::Broadcast(_x, _y);
}

//----------------------------------------------------------------------------------------------------------------------
template<class MouseScrollEvent>
void MouseScrollBinder<MouseScrollEvent>::OnMouseScroll(i32 _x, i32 _y, f32 _delta) const
{
    MouseScrollEvent::Broadcast(_x, _y, _delta);
}

//----------------------------------------------------------------------------------------------------------------------
template<class MouseMoveEvent>
void MouseMoveBinder<MouseMoveEvent>::OnMouseMove(i32 _x, i32 _y) const
{
    MouseMoveEvent::Broadcast(_x, _y);
}

FRG__CLOSE_NAMESPACE(frg);
