FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
inline static uintptr_t CantorPairing(uintptr_t _k1, uintptr_t _k2)
{
    return (((_k1 + _k2) * (_k1 + _k2 + 1)) / 2) + _k2;
}

//----------------------------------------------------------------------------------------------------------------------
// EventHandler
//----------------------------------------------------------------------------------------------------------------------
template<class N>
EventHandler<N>::EventHandler()
{}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
EventHandler<N>::EventHandler(uintptr_t _hash)
    : AbstractEventHandler(_hash)
{}



//----------------------------------------------------------------------------------------------------------------------
// EventHandlerMethod
//----------------------------------------------------------------------------------------------------------------------
template<class N, class Target>
EventHandlerMethod<N, Target>::EventHandlerMethod(Target* _t, callback_type _f)
    : m_T(_t)
    , m_F(_f)
{
    uintptr_t t = reinterpret_cast<uintptr_t>(_t);
    // hacky cast to get raw address of member function
    uintptr_t f = reinterpret_cast<uintptr_t>((void*&)_f);

    AbstractEventHandler::m_Hash = CantorPairing(t, f);
}

//----------------------------------------------------------------------------------------------------------------------
template<class N, class Target>
EventHandlerMethod<N, Target>::EventHandlerMethod(uintptr_t _hash, Target* _t, callback_type _f)
    : EventHandler<N>(_hash)
    , m_T(_t)
    , m_F(_f)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class N, class Target>
void EventHandlerMethod<N, Target>::operator()(const AbstractEvent& _n) const
{
    (m_T->*m_F)(static_cast<const N&>(_n));
}

//----------------------------------------------------------------------------------------------------------------------
template<class N, class Target>
const EventHandlerMethod<N, Target>* EventHandlerMethod<N, Target>::Clone() const
{
    return new EventHandlerMethod<N, Target>(AbstractEventHandler::m_Hash, m_T, m_F);
}



//----------------------------------------------------------------------------------------------------------------------
// EventHandlerFunction
//----------------------------------------------------------------------------------------------------------------------
template<class N>
EventHandlerFunction<N>::EventHandlerFunction(callback_type _f)
    : m_F(_f)
{
    AbstractEventHandler::m_Hash = CantorPairing(0, reinterpret_cast<uintptr_t>(_f));
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
EventHandlerFunction<N>::EventHandlerFunction(uintptr_t _hash, callback_type _f)
    : EventHandler<N>(_hash)
    , m_F(_f)
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
void EventHandlerFunction<N>::operator()(const AbstractEvent& _n) const
{
    (*m_F)(static_cast<const N&>(_n));
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
const EventHandlerFunction<N>* EventHandlerFunction<N>::Clone() const
{
    return new EventHandlerFunction<N>(AbstractEventHandler::m_Hash, m_F);
}



//----------------------------------------------------------------------------------------------------------------------
// EventHandlersProxy
//----------------------------------------------------------------------------------------------------------------------
template<class N>
void EventHandlersProxy<N>::operator+=(const handler_type& _handler)
{
    RegisterHandler(N::Id, _handler);
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
void EventHandlersProxy<N>::operator+=(const callback_type& _callback)
{
    RegisterHandler(N::Id, N::Handler(_callback));
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
void EventHandlersProxy<N>::operator-=(const handler_type& _handler)
{
    UnregisterHandler(N::Id, _handler);
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
void EventHandlersProxy<N>::operator-=(const callback_type& _callback)
{
    UnregisterHandler(N::Id, N::Handler(_callback));
}

FRG__CLOSE_NAMESPACE(frg);
