#pragma once

FRG__OPEN_NAMESPACE(frg);

//------------------------------------------------------------------------
typedef rtb::StringId EventId;

//----------------------------------------------------------------------------------------------------------------------
class AbstractEvent
{
public:
    typedef std::shared_ptr<AbstractEvent>  Ptr;

    virtual EventId GetEventId() const = 0;

protected:
    static void BroadcastRelay(Ptr _event);

};

FRG__CLOSE_NAMESPACE(frg);
