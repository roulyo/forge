#pragma once

#include <forge/engine/event/EventTypes.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class AbstractEventHandler
{
public:
    AbstractEventHandler();
    virtual ~AbstractEventHandler();

    virtual void operator()(const AbstractEvent&) const = 0;
    virtual const AbstractEventHandler* Clone() const = 0;

    bool operator==(const AbstractEventHandler& _h) const;
    bool operator!=(const AbstractEventHandler& _h) const;

protected:
    AbstractEventHandler(uintptr_t _hash);

protected:
    u64 m_Hash;

};

//----------------------------------------------------------------------------------------------------------------------
template<class N>
class EventHandler : public AbstractEventHandler
{
public:
    EventHandler();

protected:
    EventHandler(uintptr_t _hash);

};

//----------------------------------------------------------------------------------------------------------------------
template<class N, class Target>
class EventHandlerMethod : public EventHandler<N>
{
public:
    typedef void(Target::*callback_type)(const N&);

    EventHandlerMethod(Target* _t, callback_type _f);

    void operator()(const AbstractEvent& _n) const override;
    const EventHandlerMethod<N, Target>* Clone() const override;

private:
    EventHandlerMethod(uintptr_t _hash, Target* _t, callback_type _f);

private:
    Target*         m_T;
    callback_type   m_F;

};

//----------------------------------------------------------------------------------------------------------------------
template<class N>
class EventHandlerFunction : public EventHandler<N>
{
public:
    typedef void(*callback_type)(const N&);

    EventHandlerFunction(callback_type _f);

    void operator()(const AbstractEvent& _n) const override;
    const EventHandlerFunction<N>* Clone() const override;

private:
    EventHandlerFunction(uintptr_t _hash, callback_type _f);

private:
    callback_type   m_F;

};

//----------------------------------------------------------------------------------------------------------------------
class EventHandlersBroker
{
protected:
    void RegisterHandler(EventId _id, const AbstractEventHandler& _handler) const;
    void UnregisterHandler(EventId _id, const AbstractEventHandler& _handler) const;

};

//----------------------------------------------------------------------------------------------------------------------
template<class N>
class EventHandlersProxy : public EventHandlersBroker
{
public:
    typedef EventHandler<N> handler_type;
    typedef void(*callback_type)(const N&);

    void operator+=(const handler_type& _handler);
    void operator+=(const callback_type& _callback);

    void operator-=(const handler_type& _handler);
    void operator-=(const callback_type& _callback);

};

FRG__CLOSE_NAMESPACE(frg);

#include "EventHandler.inl"
