#pragma once

#include <forge/engine/event/EventTypes.h>
#include <forge/engine/event/EventHandler.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class N>
class Event : public AbstractEvent
{
public:
    template<auto... _args>
    class TArgs
    {
    public:
        static constexpr frg::EventId Id = N::Id;

        template<typename... Ts>
        static void Broadcast(Ts&&... _params)
        {
            Event<N>::Broadcast(std::forward<Ts>(_params)..., _args...);
        }
    };

public:
    static EventHandlersProxy<N> Handlers;

public:
    template<class... U>
    static void Broadcast(U&& ... _u);

    template<class Target>
    static EventHandlerMethod<N, Target> Handler(Target* _t, typename EventHandlerMethod<N, Target>::callback_type _f);
    static EventHandlerFunction<N> Handler(typename EventHandlerFunction<N>::callback_type _f);

protected:
    template<class... U>
    static Ptr MakeEvent(U&& ... _u);

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
concept EventConcept = requires(T e)
{
    { T::Broadcast() };
    { T::Id };
};

//----------------------------------------------------------------------------------------------------------------------
class VoidEvent
{
public:
    template<class... U>
    static void Broadcast(U&& ... _u);

};

FRG__CLOSE_NAMESPACE(frg);

#define FRG__DECL_EVENT(name)                                       \
    public:                                                         \
        static constexpr frg::EventId Id = frg::EventId(#name); \
        frg::EventId GetEventId() const override                  \
        {                                                           \
            return Id;                                              \
        }

#define FRG__MK_EVT_HANDLR_M(evt, method, object)                   \
    (evt::Handlers += evt::Handler(object, method));

#define FRG__RM_EVT_HANDLR_M(evt, method, object)                   \
    (evt::Handlers -= evt::Handler(object, method));

#define FRG__MK_EVT_HANDLR_F(evt, func)                             \
    (evt::Handlers += evt::Handler(func));

#define FRG__RM_EVT_HANDLR_F(evt, func)                             \
    (evt::Handlers -= evt::Handler(func));

#include "Event.inl"
