FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class N>
EventHandlersProxy<N> Event<N>::Handlers;

//----------------------------------------------------------------------------------------------------------------------
template<class N>
template<class... U>
typename Event<N>::Ptr Event<N>::MakeEvent(U&&... _u)
{
    return Event<N>::Ptr(new N(std::forward<U>(_u)...));
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
template<class... U>
void Event<N>::Broadcast(U&&... _u)
{
    typename Event<N>::Ptr evt = Event<N>::MakeEvent(std::forward<U>(_u)...);
    BroadcastRelay(evt);
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
template<class Target>
EventHandlerMethod<N, Target> Event<N>::Handler(Target* _t, typename EventHandlerMethod<N, Target>::callback_type _f)
{
    return EventHandlerMethod<N, Target>(_t, _f);
}

//----------------------------------------------------------------------------------------------------------------------
template<class N>
EventHandlerFunction<N> Event<N>::Handler(typename EventHandlerFunction<N>::callback_type _f)
{
    return EventHandlerFunction<N>(_f);
}

//----------------------------------------------------------------------------------------------------------------------
template<class... U>
void VoidEvent::Broadcast(U&&... _u)
{
}


FRG__CLOSE_NAMESPACE(frg);
