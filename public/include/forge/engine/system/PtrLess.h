#pragma once

FRG__OPEN_NAMESPACE(frg);

template<class T>
struct PtrLess
{
    bool operator()(T const& _left, T const& _right) const
    {
        return *_left < *_right;
    }

};

FRG__CLOSE_NAMESPACE(frg);
