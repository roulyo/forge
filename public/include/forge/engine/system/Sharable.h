#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class Sharable
{
public:
    typedef std::shared_ptr<T>          Ptr;
    typedef std::shared_ptr<const T>    CPtr;

    template<class... U>
    static Ptr MakePtr(U&& ... _u)
    {
        return Sharable<T>::Ptr(new T(std::forward<U>(_u)...));
    }

};

FRG__CLOSE_NAMESPACE(frg);