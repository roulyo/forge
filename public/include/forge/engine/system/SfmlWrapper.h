#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class SFMLWrapper
{
public:
    typedef T   ValueType;

    const ValueType& GetSFMLObject() const
    {
        return *m_Impl;
    }

protected:
    /*
    ** Deletion of m_Impl should be done in child's
    ** desctructor to avoid incomplete type error.
    */
    SFMLWrapper()
        : m_Impl(new ValueType())
    {}

    SFMLWrapper(const SFMLWrapper& _other) = delete;
    SFMLWrapper& operator=(const SFMLWrapper& _other) = delete;
    SFMLWrapper(SFMLWrapper&& _other) = delete;
    SFMLWrapper& operator=(SFMLWrapper&& _other) = delete;

    virtual ~SFMLWrapper() = 0;

protected:
    ValueType* m_Impl;

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
SFMLWrapper<T>::~SFMLWrapper()
{
}

FRG__CLOSE_NAMESPACE(frg);
