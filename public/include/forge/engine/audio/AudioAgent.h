#pragma once

#include <forge/engine/audio/playable/Music.h>
#include <forge/engine/audio/playable/Sound.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
typedef u32 AudioDesc;

//----------------------------------------------------------------------------------------------------------------------
class AudioAgent
{
public:
    AudioDesc PlayMusic(const frg::Music::Ptr& _music, bool _repeat = false);
    void StopMusic(AudioDesc _musicDescriptor);
    void SetGlobalMusicVolume(f32 _volume);

    AudioDesc PlaySound(const frg::Sound::Ptr& _sound, bool _repeat = false);
    void SetGlobalSoundVolume(f32 _volume);

};

FRG__CLOSE_NAMESPACE(frg);
