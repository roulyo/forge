#pragma once

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
enum class AudioStatus
{
    Stopped,
    Paused,
    Playing,

    Invalid
};

FRG__CLOSE_NAMESPACE(frg);
