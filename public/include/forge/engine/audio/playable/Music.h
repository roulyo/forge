#pragma once

#include <forge/engine/audio/AudioTypes.h>
#include <forge/engine/data/Data.h>
#include <forge/engine/system/SfmlWrapper.h>

namespace sf
{
    class Music;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class Music : public Data<Music>, public SFMLWrapper<sf::Music>
{
public:
    Music();
    Music(const String& _musicPath);
    ~Music() override;

    void SetFile(const String& _musicPath);
    void SetLoop(bool _loop);

    void SetVolume(f32 _volume);

    void Play() const;
    void Pause() const;
    void Stop() const;

    AudioStatus GetStatus() const;

};

FRG__CLOSE_NAMESPACE(frg);
