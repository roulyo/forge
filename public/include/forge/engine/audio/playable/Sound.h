#pragma once

#include <forge/engine/audio/AudioTypes.h>
#include <forge/engine/audio/resource/SoundBuffer.h>
#include <forge/engine/data/Data.h>
#include <forge/engine/system/SfmlWrapper.h>

namespace sf
{
    class Sound;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class Sound : public Data<Sound>, public SFMLWrapper<sf::Sound>
{
public:
    Sound();
    Sound(const SoundBuffer::Ptr& _soundBuffer);
    ~Sound() override;

    void SetSoundBuffer(const SoundBuffer::Ptr& _soundBuffer);
    void SetLoop(bool _loop) const;
    void Play() const;

    f32 GetPitch() const;
    void SetPitch(f32 _p);

    void SetVolume(f32 _volume);

    AudioStatus GetStatus() const;

private:
    SoundBuffer::Ptr    m_BufferPtr;

};

FRG__CLOSE_NAMESPACE(frg);
