#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/system/SfmlWrapper.h>

namespace sf
{
    class SoundBuffer;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class SoundBuffer : public Data<SoundBuffer>, public SFMLWrapper<sf::SoundBuffer>
{
    friend class Sound;

public:
    SoundBuffer();
    SoundBuffer(const String& _bufferPath);
    ~SoundBuffer() override;

    void SetFile(const String& _bufferPath);

};

FRG__CLOSE_NAMESPACE(frg);
