#pragma once

#include <functional>

#include <forge/engine/ecs/Entity.h>
#include <forge/engine/navigation/NavigationOptions.h>
#include <forge/engine/navigation/NavigationTypes.h>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class Navmesh;
class World;

//----------------------------------------------------------------------------------------------------------------------
class NavigationProvider
{
public:
    NavigationProvider();
    ~NavigationProvider();

    void SetNavmeshHooks(const IsNavigableHook& _navHook,
                         const IsAccessibleHook& _accessHook,
                         const IsFreeHook& _freeSpaceCheckHook);

    void GenerateNavmesh(const World* _world, f32 _cellSize);
    bool FindPath(const NavNode& _start,
                  const NavNode& _goal,
                  NavPath& _path,
                  const NavigationOptions& _options = NavigationOptions()) const;

private:
    Navmesh* m_Navmesh;

};

FRG__CLOSE_NAMESPACE(frg);
