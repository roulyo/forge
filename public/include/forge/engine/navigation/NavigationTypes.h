#pragma once

#include <functional>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
using IsAccessibleHook  = std::function<bool(const Vector3f&, const Vector3f&)>;
using IsFreeHook        = std::function<bool(const Vector3f&)>;
using IsNavigableHook   = std::function<bool(const Vector3f&)>;

using NavNode = Vector2f;
using NavPath = Vector<NavNode>;

struct NavResult
{
    bool    Success;
    NavPath Path;
};

FRG__CLOSE_NAMESPACE(frg);
