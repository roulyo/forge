#pragma once

#include <cmath>

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
struct NavigationOptions
{
    struct
    {
        Vector2f    Center { NAN, NAN };
        f32         Radius = NAN;
    } LookupArea;

    bool    CanMoveDiagonaly = true;
    bool    IgnoreDynamicObstacle = false;
    u32     MaxStepCount = -1;
    u32     StopAheadStepCount = 0;

};

FRG__CLOSE_NAMESPACE(frg);
