#pragma once

#include <forge/engine/data/Data.h>
#include <forge/engine/system/SfmlWrapper.h>

namespace sf
{
    class Cursor;
}

FRG__OPEN_NAMESPACE(frg);

//----------------------------------------------------------------------------------------------------------------------
class Cursor : public Data<Cursor>, public SFMLWrapper<sf::Cursor>
{
public:
    static const Cursor::CPtr Arrow;
    static const Cursor::CPtr Hand;
    static const Cursor::CPtr Cross;

public:
    Cursor();
    Cursor(u32 _cursorId);
    ~Cursor() override;

    void SetFile(const String& _file);
    void SetSize(u32 _x, u32 _y);
    void SetHotspot(u32 _x, u32 _y);

    void Load() const;
    bool IsLoaded() const;

private:
    mutable bool    m_Loaded;

    u32             m_Id;
    u8*             m_Pixels;
    Vector2u        m_Size;
    Vector2u        m_Hotspot;

};

FRG__CLOSE_NAMESPACE(frg);
