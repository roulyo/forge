#pragma once

#include <rtb/utils/HierarchicalAutomata.h>

FRG__OPEN_NAMESPACE(frg);

using HierarchicalAutomata = rtb::HierarchicalAutomata;

FRG__CLOSE_NAMESPACE(frg);
