#pragma once

#include <forge/engine/event/Event.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class SimplePhysicsCollisionEvent : public frg::Event<SimplePhysicsCollisionEvent>
{
    FRG__DECL_EVENT(SimplePhysicsCollisionEvent);

public:
    SimplePhysicsCollisionEvent(const Entity::CPtr& _collider, const Entity::CPtr& _collided)
        : m_Collider(_collider)
        , m_Collided(_collided)
    {}

private:
    FRG__CLASS_ATTR_R_(Entity::CPtr, Collider);
    FRG__CLASS_ATTR_R_(Entity::CPtr, Collided);

};


FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
