#pragma once

#include <forge/engine/ecs/Component.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class PhysicableComponent : public AbstractComponent
{
    FRG__DECL_COMPONENT(PhysicableComponent);

public:
    bool        IsDirty = false;

    bool        IsBlocking = false;
    bool        SuffersGravity = true;

    Vector2f    Speed = { 0.0f, 0.0f };

};


FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
