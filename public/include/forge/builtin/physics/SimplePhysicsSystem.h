#pragma once

#include <forge/engine/ecs/System.h>
#include <forge/engine/world/EntityControlAgent.h>

#include <forge/builtin/physics/PhysicableComponent.h>

FRG__OPEN_NAMESPACE(frg);

template<class T>
class Quadtree;

class EntityAddedEvent;
class EntityRemovedEvent;

FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class SimplePhysicsSystem : public System<PhysicableComponent>, public EntityControlAgent
{
public:
    void OnStart() override;
    void OnStop() override;
    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

private:
    f32 GetGroundElevation(f32 _x, f32 _y) const;
    Vector<Entity::CPtr> GetCollisions(const FloatQuad& _aabb);

    void OnEntityAdded(const EntityAddedEvent& _event);
    void OnEntityRemoved(const EntityRemovedEvent& _event);

private:
    FRG__CLASS_ATTR___(Quadtree<Entity::CPtr>*, PhysicsWorld);

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
