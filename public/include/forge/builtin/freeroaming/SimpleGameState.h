#pragma once

#include <forge/engine/game/GameState.h>

#include <forge/builtin/freeroaming/FreeRoamingInputs.h>
#include <forge/builtin/physics/SimplePhysicsSystem.h>
#include <forge/builtin/3Cs/SimpleControlSystem.h>
#include <forge/builtin/rendering/SimpleRenderingSystem.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class SimpleGameState : public AbstractGameState
{
    FRG__DECL_GAMESTATE(SimpleGameState);

public:
    static void AddPluginSystem(AbstractSystem* _system);
    static void AddPluginViewController(AbstractViewController* _vc);

public:
    SimpleGameState(const AbstractForgeGame& _game);

    void OnStart() override;
    void OnStop() override;

private:
    static Vector<AbstractSystem*>          ms_SystemPlugins;
    static Vector<AbstractViewController*>  ms_ViewControllerPlugins;

private:
    FRG__CLASS_ATTR___(class FreeRoamingInputScheme, FreeRoamingInputScheme);

    FRG__CLASS_ATTR_R_(SimpleControlSystem, PlayerControlSystem);
    FRG__CLASS_ATTR_R_(SimplePhysicsSystem, PhysicsSystem);
    FRG__CLASS_ATTR_R_(SimpleRenderingSystem, RenderingSystem);

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
