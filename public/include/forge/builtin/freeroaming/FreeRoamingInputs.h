#pragma once

#include <forge/engine/input/InputMappingScheme.h>
#include <forge/engine/input/InputBinder.h>

#include <forge/builtin/3Cs/events/MoveEvent.h>
#include <forge/builtin/3Cs/events/MouseEvent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
typedef KeyBinder<MoveEvent::TArgs<true, MoveDirection::Up>,
                  MoveEvent::TArgs<false, MoveDirection::Up>>
    MoveUpBinder;

typedef KeyBinder<MoveEvent::TArgs<true, MoveDirection::Down>,
                  MoveEvent::TArgs<false, MoveDirection::Down>>
    MoveDownBinder;

typedef KeyBinder<MoveEvent::TArgs<true, MoveDirection::Left>,
                  MoveEvent::TArgs<false, MoveDirection::Left>>
    MoveLeftBinder;

typedef KeyBinder<MoveEvent::TArgs<true, MoveDirection::Right>,
                  MoveEvent::TArgs<false, MoveDirection::Right>>
    MoveRightBinder;

//----------------------------------------------------------------------------------------------------------------------
typedef MouseButtonBinder<MouseClickEvent::TArgs<true, Mouse::Button::Left>,
                          MouseClickEvent::TArgs<false, Mouse::Button::Left>>
    LeftClickBinder;

//----------------------------------------------------------------------------------------------------------------------
typedef MouseButtonBinder<MouseClickEvent::TArgs<true, Mouse::Button::Right>,
                          MouseClickEvent::TArgs<false, Mouse::Button::Right>>
    RightClickBinder;

typedef MouseScrollBinder<MouseWheelEvent>
    CameraZoomBinder;

typedef MouseMoveBinder<MouseMoveEvent>
    MouseMovementBinder;

//----------------------------------------------------------------------------------------------------------------------
class FreeRoamingInputScheme : public InputMappingScheme
{
public:
    FreeRoamingInputScheme();

private:
    MoveUpBinder    m_MoveUpBinder;
    MoveDownBinder  m_MoveDownBinder;
    MoveLeftBinder  m_MoveLeftBinder;
    MoveRightBinder m_MoveRightBinder;

    LeftClickBinder     m_LeftClickBinder;
    RightClickBinder    m_RightClickBinder;
    MouseMovementBinder m_MouseMoveBinder;
    CameraZoomBinder    m_CameraZoomBinder;

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
