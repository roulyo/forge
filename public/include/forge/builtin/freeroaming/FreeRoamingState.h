#pragma once

#include <forge/engine/game/GameState.h>

#include <forge/builtin/freeroaming/FreeRoamingInputs.h>
#include <forge/builtin/physics/SimplePhysicsSystem.h>
#include <forge/builtin/3Cs/CameraFollowSystem.h>
#include <forge/builtin/3Cs/ScreenMappingSystem.h>
#include <forge/builtin/3Cs/PlayerControlSystem.h>
#include <forge/builtin/rendering/SimpleRenderingSystem.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class FreeRoamingState : public AbstractGameState
{
    FRG__DECL_GAMESTATE(FreeRoamingState);

public:
    static void AddPluginSystem(AbstractSystem* _system);
    static void AddPluginViewController(AbstractViewController* _vc);

public:
    FreeRoamingState(const AbstractForgeGame& _game);

    void OnStart() override;
    void OnStop() override;

private:
    static Vector<AbstractSystem*>          ms_SystemPlugins;
    static Vector<AbstractViewController*>  ms_ViewControllerPlugins;

private:
    FRG__CLASS_ATTR___(class FreeRoamingInputScheme, FreeRoamingInputScheme);

    FRG__CLASS_ATTR_R_(class PlayerControlSystem, PlayerControlSystem);
    FRG__CLASS_ATTR_R_(SimplePhysicsSystem, PhysicsSystem);
    FRG__CLASS_ATTR_R_(SimpleRenderingSystem, RenderingSystem);
    FRG__CLASS_ATTR_R_(class ScreenMappingSystem, ScreenMappingSystem);
    FRG__CLASS_ATTR_R_(class CameraFollowSystem, CameraFollowSystem);

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
