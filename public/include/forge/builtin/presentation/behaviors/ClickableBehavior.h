#pragma once

#include <forge/engine/presentation/GUIBehavior.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class ClickableBehavior : public frg::AbstractGUIBehavior
{
public:
    ClickableBehavior(const BehaviorCallback& _callback);

    bool ShouldCaptureEvent(const SystemEvent& _event) override;

private:
    bool m_IsPressed;

};


FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
