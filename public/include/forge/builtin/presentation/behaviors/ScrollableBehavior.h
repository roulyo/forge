#pragma once

#include <forge/engine/presentation/GUIBehavior.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class ScrollableBehavior : public frg::AbstractGUIBehavior
{
public:
    ScrollableBehavior(const BehaviorCallback& _callback);

    bool ShouldCaptureEvent(const SystemEvent& _event) override;

};


FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
