#pragma once

#include <forge/engine/presentation/GUIBehavior.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class HoverableBehavior : public frg::AbstractGUIBehavior
{
public:
    HoverableBehavior(const BehaviorCallback& _callback);

    bool ShouldCaptureEvent(const SystemEvent& _event) override;

private:
    bool m_IsHovered;

};


FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
