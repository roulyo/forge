#pragma once

#include <forge/engine/presentation/GUIWidget.h>
#include <forge/engine/rendering/drawable/Shapes.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);
FRG__OPEN_NAMESPACE(gui);

//----------------------------------------------------------------------------------------------------------------------
class AbstractPanel : public GUIWidget
{
public:
    ~AbstractPanel() = 0;

    virtual AbstractGUIDrawable& Background() { return *m_Background; }

protected:
    AbstractGUIDrawable*  m_Background;

};


//----------------------------------------------------------------------------------------------------------------------
template<class T>
class SpecializedPanel : virtual public AbstractPanel
{
public:
    SpecializedPanel();
    ~SpecializedPanel() override;

    void OnRenderedGeometryComputed(const Vector2f& _position,
                                    const Vector2f& _size) override;

    GUIDrawable<T>& Background() override { return *static_cast<GUIDrawable<T>*>(m_Background); }

};

FRG__CLOSE_NAMESPACE(gui);
FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);

#include "Panel.inl"

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);
FRG__OPEN_NAMESPACE(gui);

template<>
void SpecializedPanel<QuadShape>::OnRenderedGeometryComputed(const Vector2f& _position,
                                                             const Vector2f& _size);

template<>
void SpecializedPanel<CircleShape>::OnRenderedGeometryComputed(const Vector2f& _position,
                                                               const Vector2f& _size);

using QuadPanel = SpecializedPanel<QuadShape>;
using DiscPanel = SpecializedPanel<CircleShape>;

FRG__CLOSE_NAMESPACE(gui);
FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
