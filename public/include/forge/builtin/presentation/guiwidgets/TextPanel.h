#pragma once

#include <forge/engine/rendering/drawable/Text.h>

#include <forge/builtin/presentation/guiwidgets/Panel.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);
FRG__OPEN_NAMESPACE(gui);

//----------------------------------------------------------------------------------------------------------------------
class AbstractTextPanel : virtual public AbstractPanel
{
public:
    AbstractTextPanel();

    GUIWidget::GUIDrawable<Text>& Text() { return m_Text; }

protected:
    GUIWidget::GUIDrawable<frg::Text>   m_Text;

};

//----------------------------------------------------------------------------------------------------------------------
template<class T>
class SpecializedTextPanel : public SpecializedPanel<T>, public AbstractTextPanel
{
public:
    using Shape = T;
    using SpecializedPanel<T>::Background;

};

using QuadTextPanel = SpecializedTextPanel<QuadShape>;
using DiscTextPanel = SpecializedTextPanel<CircleShape>;

FRG__CLOSE_NAMESPACE(gui);
FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
