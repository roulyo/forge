FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);
FRG__OPEN_NAMESPACE(gui);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
SpecializedPanel<T>::SpecializedPanel()
{
    m_Background = new GUIDrawable<T>();

    AddDrawable(m_Background);
}

//----------------------------------------------------------------------------------------------------------------------
template<class T>
SpecializedPanel<T>::~SpecializedPanel()
{
    delete m_Background;
}

FRG__CLOSE_NAMESPACE(gui);
FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
