#pragma once

#include <forge/engine/presentation/GUIWidget.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);
FRG__OPEN_NAMESPACE(gui);

//----------------------------------------------------------------------------------------------------------------------
using Div = GUIWidget;

FRG__CLOSE_NAMESPACE(gui);
FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
