#pragma once

#include <forge/engine/ecs/Component.h>

#include <forge/engine/rendering/RenderTree.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class RenderableComponent : public AbstractComponent
{
    FRG__DECL_COMPONENT(RenderableComponent);

public:
    RenderableComponent();

    void AddPrefixNode(const AbstractRenderTree::Ptr& _node);
    void RemovePrefixNode(const AbstractRenderTree::CPtr& _node);

    AbstractRenderTree::CPtr GetRenderTreeRoot() const;

public:
    FRG__CLASS_ATTR_R_(AbstractRenderTree::Ptr, PrefixTree);
    FRG__CLASS_ATTR_R_(AbstractRenderTree::Ptr, SceneTree);

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
