#pragma once

#include <forge/engine/ecs/System.h>
#include <forge/engine/rendering/RenderingAgent.h>

#include <forge/builtin/rendering/ParticlesComponent.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class SimpleParticlesSystem : public System<ParticlesComponent>, public RenderingAgent
{
public:
    SimpleParticlesSystem();

    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

private:
    Particle CreateParticle(const ParticlesComponent& _comp) const;
    void HandleParticle(const Particle& _particle, const ParticlesComponent& _comp) const;

private:
    f32 m_zOffset;

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
