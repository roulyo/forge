#pragma once

#include <functional>

#include <forge/engine/ecs/Component.h>
#include <forge/engine/rendering/RenderTree.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
struct Particle
{
    Vector2f                        Speed;
    u32                             TTLMs;
    f32                             Size;
    f32                             Fading;

    Vector2f                        Offset;
    WorldTransformRenderNode::Ptr   TransformNode;

    u64                             StartTimeMs;
};

//----------------------------------------------------------------------------------------------------------------------
using ParticleTreeFactory = std::function<AbstractRenderTree::Ptr(void)>;

//----------------------------------------------------------------------------------------------------------------------
class ParticlesComponent : public AbstractComponent
{
    FRG__DECL_COMPONENT(ParticlesComponent);

public:
    ParticlesComponent();

public:
    FRG__CLASS_ATTR_RW(frg::bi::ParticleTreeFactory, ParticleTreeFactory);
    // FRG__CLASS_ATTR_RW(ShaderMaterial, Material);

    FRG__CLASS_ATTR_RW(Vector2f, SpawnAreaSize);
    FRG__CLASS_ATTR_RW(Vector2f, SpawnAreaOffset);

    FRG__CLASS_ATTR_RW(f32, Spread);            // [0.0; 360.0] Angle of spreading (split over upward Y axis)

    FRG__CLASS_ATTR_RW(f32, Density);           // ]0.0; inf[   nb of particles per second
    FRG__CLASS_ATTR_RW(f32, DensityVariation);  // [0.0; inf[   Next particle in (Density / (1.0 + rand(-Variation, Variation))) sec

    FRG__CLASS_ATTR_RW(u32, TTLMs);             // ]  0; inf[   Time of life of a single particle
    FRG__CLASS_ATTR_RW(f32, TTLMsVariation);    // [0.0; inf[   Particle.TTL = TTL * (1.0 + rand(-Variation, Variation))

    FRG__CLASS_ATTR_RW(f32, Fading);            // [0.0; 1.0]   Proportion of TTL fading
    FRG__CLASS_ATTR_RW(f32, FadingVariation);   // [0.0; inf[   Particle.Fading = Fading * (1.0 + rand(-Variation, Variation))

    FRG__CLASS_ATTR_RW(f32, Size);              // ]0.0, inf[   Default size of a particle
    FRG__CLASS_ATTR_RW(f32, SizeVariation);     // [0.0; inf[   Particle.Size = Size * (1.0 + rand(-Variation, Variation))

    FRG__CLASS_ATTR_RW(f32, Speed);             // ]0.0; inf[   u.s^-1
    FRG__CLASS_ATTR_RW(f32, SpeedVariation);    // [0.0; inf[   Particle.Speed = Speed * (1.0f + rand(-Variation, Variation))

    FRG__CLASS_ATTR_RW(f32, Gravity);           // Whether particles suffer speed loss from system's arbitry gravity or not

    FRG__CLASS_ATTR_RW(u64, StartTimeMs);
    FRG__CLASS_ATTR_RW(u64, LastSpawnTimeMs);
    FRG__CLASS_ATTR_RW(bool, IsActive);

    FRG__CLASS_ATTR_RW(List<Particle>, Particles);

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
