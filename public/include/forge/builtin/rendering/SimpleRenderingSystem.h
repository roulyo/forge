#pragma once

#include <forge/engine/ecs/System.h>
#include <forge/engine/rendering/RenderingAgent.h>

#include <forge/builtin/rendering/RenderableComponent.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class SimpleRenderingSystem : public System<RenderableComponent>, public RenderingAgent
{
public:
    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
