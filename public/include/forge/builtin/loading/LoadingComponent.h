#pragma once

#include <forge/engine/ecs/Component.h>

#include <forge/engine/async/AsyncTask.h>
#include <forge/engine/game/GameState.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class LoadingComponent : public AbstractComponent
{
    FRG__DECL_COMPONENT(LoadingComponent);

public:
    u32 AddTask(const BaseAsyncTask::Ptr& _task);
    bool HasTasks() const;
    BaseAsyncTask::Ptr GetNextTask();

    void Lock();
    void Unlock();

private:
    FRG__CLASS_ATTR_RW(GameStateId, PostLoadingState);

    List<BaseAsyncTask::Ptr>    m_Tasks;
    bool                        m_Locked;

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
