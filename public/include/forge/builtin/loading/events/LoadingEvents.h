#pragma once

#include <forge/engine/async/AsyncResult.h>
#include <forge/engine/event/Event.h>
#include <forge/engine/game/GameState.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class LoadingTasksDoneEvent : public frg::Event<LoadingTasksDoneEvent>
{
    FRG__DECL_EVENT(LoadingTasksDoneEvent);

public:
    LoadingTasksDoneEvent(GameStateId _reqState, const Vector<AsyncResult>& _results)
        : m_PostLoadingRequestedState(_reqState)
        , m_Results(_results)
    {}

private:
    FRG__CLASS_ATTR_R_(GameStateId, PostLoadingRequestedState);
    FRG__CLASS_ATTR_R_(Vector<AsyncResult>, Results);

};


FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
