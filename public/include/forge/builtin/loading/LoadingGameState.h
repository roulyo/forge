#pragma once

#include <forge/engine/game/GameState.h>

#include <forge/builtin/loading/LoadingSystem.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class LoadingGameState : public frg::AbstractGameState
{
    FRG__DECL_GAMESTATE(LoadingGameState);

public:
    static void AddPluginSystem(AbstractSystem* _system);
    static void AddPluginViewController(AbstractViewController* _vc);

public:
    LoadingGameState(const AbstractForgeGame& _game);

    void OnStart() override;
    void OnStop() override;

private:
    static Vector<AbstractSystem*>          ms_SystemPlugins;
    static Vector<AbstractViewController*>  ms_ViewControllerPlugins;

private:
    LoadingSystem   m_LoadingSystem;

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
