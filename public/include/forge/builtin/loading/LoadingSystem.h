#pragma once

#include <forge/engine/async/AsyncAgent.h>
#include <forge/engine/async/AsyncContext.h>
#include <forge/engine/ecs/System.h>

#include <forge/builtin/loading/LoadingComponent.h>


FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class LoadingSystem : public System<LoadingComponent>, public AsyncAgent
{
public:
    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

private:
    void Load(LoadingComponent& _loadComp);
    void HandleLoading(LoadingComponent& _loadComp);
    void HandleLoadingCompleted(LoadingComponent& _loadComp);

    bool IsLoadingComplete() const;

private:
    List<AsyncContext::Ptr> m_Contexts;

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
