#pragma once

#include <functional>

#include <forge/engine/ecs/System.h>
#include <forge/engine/navigation/NavigationOptions.h>

#include <forge/builtin/3Cs/NavigatorComponent.h>
#include <forge/builtin/3Cs/NavigatorSystemToolkit.h>
#include <forge/builtin/3Cs/PlayableCharacterComponent.h>
#include <forge/builtin/physics/PhysicableComponent.h>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
class NavigationProvider;


FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class EntityClickedEvent;
class NavigationRequestEvent;

//----------------------------------------------------------------------------------------------------------------------
class PlayerControlSystem : public System<PlayableCharacterComponent, NavigatorComponent, PhysicableComponent>
{
public:
    typedef std::function<void(const Entity::Ptr&, const Vector2f&)> OnEntityMovedHook;

public:
    static void SetOnEntityMovedHook(const OnEntityMovedHook& _hook);

public:
    PlayerControlSystem();
    ~PlayerControlSystem();

    void OnStart() override;
    void OnStop() override;

    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

    void OnEntityClickedEvent(const EntityClickedEvent& _event);
    void OnNavigationRequestEvent(const NavigationRequestEvent& _event);

private:
    void UpdateDirection(const Entity::Ptr& _navEntity) const;
    void UpdateSpeed(const Entity::Ptr& _navEntity) const;

private:
    static OnEntityMovedHook ms_OnEntityMovedHook;

    NavigatorSystemToolkit  m_NavigatorToolkit;

private:
    FRG__CLASS_ATTR_RW(NavigationOptions, DefaultNavOptions);
    FRG__CLASS_ATTR___(NavigationOptions, NavOptions);
    FRG__CLASS_ATTR___(Vector2f, NavRequest);
    FRG__CLASS_ATTR___(bool, WasMoving);

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
