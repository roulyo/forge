#pragma once

#include <forge/engine/event/Event.h>
#include <forge/engine/navigation/NavigationOptions.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
enum class MoveDirection
{
    Up = 0b0001,
    Down = 0b0010,
    Left = 0b0100,
    Right = 0b1000
};

//----------------------------------------------------------------------------------------------------------------------
class MoveEvent : public Event<MoveEvent>
{
    FRG__DECL_EVENT(MoveEvent);

public:
    MoveEvent(bool _isMoving, MoveDirection _direction);

    bool IsMoving() const;
    MoveDirection GetDirection() const;

private:
    bool            m_IsMoving;
    MoveDirection   m_Direction;

};

//----------------------------------------------------------------------------------------------------------------------
class NavigationRequestEvent : public Event<NavigationRequestEvent>
{
    FRG__DECL_EVENT(NavigationRequestEvent);

public:
    NavigationRequestEvent(const Vector2f& _dest)
        : m_Destination(_dest)
        , m_Options()
        , m_HasOptions(false)
    {}

    NavigationRequestEvent(const Vector2f& _dest, const NavigationOptions& _options)
        : m_Destination(_dest)
        , m_Options(_options)
        , m_HasOptions(true)
    {}

private:
    FRG__CLASS_ATTR_R_(Vector2f, Destination);
    FRG__CLASS_ATTR_R_(NavigationOptions, Options);
    FRG__CLASS_ATTR_R_(bool, HasOptions);

};

//----------------------------------------------------------------------------------------------------------------------
class NavigationStartedEvent : public Event<NavigationStartedEvent>
{
    FRG__DECL_EVENT(NavigationStartedEvent);

public:
    NavigationStartedEvent(const Vector2f& _dest)
        : m_Destination(_dest)
    {}

private:
    FRG__CLASS_ATTR_R_(Vector2f, Destination);

};

//----------------------------------------------------------------------------------------------------------------------
class NavigationStoppedEvent : public Event<NavigationStoppedEvent>
{
    FRG__DECL_EVENT(NavigationStoppedEvent);

public:
    NavigationStoppedEvent(const Vector2f& _dest, bool _interrupted)
        : m_Destination(_dest)
        , m_Interrupted(_interrupted)
    {}

private:
    FRG__CLASS_ATTR_R_(Vector2f, Destination);
    FRG__CLASS_ATTR_R_(bool, Interrupted);

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
