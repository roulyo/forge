#pragma once

#include <forge/engine/ecs/Entity.h>
#include <forge/engine/event/Event.h>

#include <forge/builtin/3Cs/Interaction.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class InteractableClickedEvent : public Event<InteractableClickedEvent>
{
    FRG__DECL_EVENT(InteractableClickedEvent);

public:
    InteractableClickedEvent(const Entity::CPtr& _entity, const Vector<AbstractInteraction::CPtr>& _interactions)
        : m_Interactable(_entity)
        , m_Interactions(_interactions)
    {}

public:
    FRG__CLASS_ATTR_R_(Entity::CPtr, Interactable);
    FRG__CLASS_ATTR_R_(Vector<AbstractInteraction::CPtr>, Interactions);

};


//----------------------------------------------------------------------------------------------------------------------
class InteractionChosenEvent : public Event<InteractionChosenEvent>
{
    FRG__DECL_EVENT(InteractionChosenEvent);

public:
    InteractionChosenEvent(const Entity::CPtr& _entity, const AbstractInteraction::CPtr& _interaction)
        : m_Interactable(_entity)
        , m_Interaction(_interaction)
    {}

public:
    FRG__CLASS_ATTR_R_(Entity::CPtr, Interactable);
    FRG__CLASS_ATTR_R_(AbstractInteraction::CPtr, Interaction);

};


FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
