#pragma once

#include <forge/engine/ecs/Entity.h>
#include <forge/engine/event/Event.h>
#include <forge/engine/input/InputTypes.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class MouseClickEvent : public Event<MouseClickEvent>
{
    FRG__DECL_EVENT(MouseClickEvent);

public:
    MouseClickEvent(i32 _x, i32 _y, bool _isPressed, Mouse::Button _button);

public:
    FRG__CLASS_ATTR_R_(bool, IsPressed);
    FRG__CLASS_ATTR_R_(i32, X);
    FRG__CLASS_ATTR_R_(i32, Y);
    FRG__CLASS_ATTR_R_(Mouse::Button, MouseButton);

};


//----------------------------------------------------------------------------------------------------------------------
class MouseWheelEvent : public Event<MouseWheelEvent>
{
    FRG__DECL_EVENT(MouseWheelEvent);

public:
    MouseWheelEvent(i32 _x, i32 _y, f32 _delta);

public:
    FRG__CLASS_ATTR_R_(f32, Delta);

};

//----------------------------------------------------------------------------------------------------------------------
class MouseMoveEvent : public Event<MouseMoveEvent>
{
    FRG__DECL_EVENT(MouseMoveEvent);

public:
    MouseMoveEvent(i32 _x, i32 _y);

public:
    FRG__CLASS_ATTR_R_(i32, X);
    FRG__CLASS_ATTR_R_(i32, Y);

};

//----------------------------------------------------------------------------------------------------------------------
class EntityClickedEvent : public Event<EntityClickedEvent>
{
    FRG__DECL_EVENT(EntityClickedEvent);

public:
    EntityClickedEvent(bool _isPressed, bool _isRightClick, const Entity::CPtr& _entity)
        : m_IsPressed(_isPressed)
        , m_IsRightClick(_isRightClick)
        , m_Entity(_entity)
    {}

    bool GetIsLeftClick() const { return !GetIsRightClick(); }

private:
    FRG__CLASS_ATTR_R_(bool, IsPressed);
    FRG__CLASS_ATTR_R_(bool, IsRightClick);
    FRG__CLASS_ATTR_R_(Entity::CPtr, Entity);

};

//----------------------------------------------------------------------------------------------------------------------
class EntityHoveredEnterEvent : public Event<EntityHoveredEnterEvent>
{
    FRG__DECL_EVENT(EntityHoveredEnterEvent);

public:
    EntityHoveredEnterEvent(const Entity::Ptr& _entity)
        : m_Entity(_entity)
    {}

private:
    FRG__CLASS_ATTR_R_(Entity::Ptr, Entity);

};

//----------------------------------------------------------------------------------------------------------------------
class EntityHoveredExitEvent : public Event<EntityHoveredExitEvent>
{
    FRG__DECL_EVENT(EntityHoveredExitEvent);

public:
    EntityHoveredExitEvent(const Entity::Ptr& _entity)
        : m_Entity(_entity)
    {}

private:
    FRG__CLASS_ATTR_R_(Entity::Ptr, Entity);

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
