#include <forge/Project.h>
#include <forge/builtin/3Cs/NavigatorSystemToolkit.h>

#include <cmath>

#include <forge/engine/navigation/NavigationProvider.h>

#include <forge/builtin/3Cs/NavigatorComponent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
NavigationProvider* NavigatorSystemToolkit::ms_NavigationProvider = nullptr;

//----------------------------------------------------------------------------------------------------------------------
void NavigatorSystemToolkit::SetNavigationProvider(NavigationProvider* _navProvider)
{
    ms_NavigationProvider = _navProvider;
}

//----------------------------------------------------------------------------------------------------------------------
NavResult NavigatorSystemToolkit::FindPath(const NavNode& _start,
                                           const NavNode& _goal,
                                           const NavigationOptions& _options) const
{
    NavResult result;

    result.Success = ms_NavigationProvider->FindPath({ roundf(_start.x), roundf(_start.y) },
                                                     { roundf(_goal.x), roundf(_goal.y) },
                                                     result.Path,
                                                     _options);

    return result;
}

//----------------------------------------------------------------------------------------------------------------------
NavResult NavigatorSystemToolkit::FindPath(const Entity::Ptr& _navEntity,
                                           const NavNode& _goal,
                                           const NavigationOptions& _options) const
{
    return FindPath(_navEntity->GetPosition().xy(), _goal, _options);
}

//----------------------------------------------------------------------------------------------------------------------
bool NavigatorSystemToolkit::Navigate(const Entity::Ptr& _navEntity,
                                      const NavNode& _goal,
                                      const NavigationOptions& _options) const
{
    FRG__ASSERT(ms_NavigationProvider != nullptr);

    NavResult result = FindPath(_navEntity, _goal, _options);

    if (result.Success)
    {
        _navEntity->GetComponent<NavigatorComponent>().SetPath(result.Path);
    }

    return result.Success;
}

//----------------------------------------------------------------------------------------------------------------------
bool NavigatorSystemToolkit::UpdateNavigation(const Entity::Ptr& _navEntity) const
{
    NavigatorComponent& navComp = _navEntity->GetComponent<NavigatorComponent>();
    const Vector3f& position = _navEntity->GetPosition();

    if (navComp.GetPath().empty())
        return true; // true or false ?

    NavPath path = navComp.GetPath();
    const NavNode& nextNode = path[0];

    f32 prevDx = nextNode.x - navComp.GetLastKnownPosition().x;
    f32 dx = nextNode.x - position.x;

    f32 prevDy = nextNode.y - navComp.GetLastKnownPosition().y;
    f32 dy = nextNode.y - position.y;

    bool endReached = false;
    // if sign of dx are different then we passed/reached the goal
    if (   (dx == 0.0f && dy == 0.0f)
        || (dx * prevDx < 0.0f) || (dy * prevDy < 0.0f))
    {
        path.erase(path.begin());

        if (path.empty())
        {
            endReached = true;
        }

        navComp.SetPath(path);
    }

    // - /!\ warning Roulyo /!\ -
    // use nextNode instead of dxy because it is the new path[0] after erase
    // and that's where the entity is headed (dxy might be opposite direction)
    navComp.SetDirection({ endReached ? 0.0f : nextNode.x - position.x,
                           endReached ? 0.0f : nextNode.y - position.y });

    NormalizeDirection(navComp);

    navComp.SetLastKnownPosition(position);

    return endReached;
}

//----------------------------------------------------------------------------------------------------------------------
void NavigatorSystemToolkit::TerminateNavigation(const Entity::Ptr& _navEntity) const
{
    NavigatorComponent& navComp = _navEntity->GetComponent<NavigatorComponent>();
    const Vector3f& position = _navEntity->GetPosition();

    navComp.SetPath(frg::NavPath());
    navComp.SetDirection(Vector2f::Zero);
    navComp.SetLastKnownPosition(position);
}

//----------------------------------------------------------------------------------------------------------------------
void NavigatorSystemToolkit::NormalizeDirection(NavigatorComponent& _navComp) const
{
    Vector2f direction = _navComp.GetDirection();

    if (direction.x != 0.0f || direction.y != 0.0f)
    {
        f32 length = sqrt((direction.x * direction.x) + (direction.y * direction.y));

        direction.x /= length;
        direction.y /= length;

        _navComp.SetDirection(direction);
    }
}

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
