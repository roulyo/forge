#pragma once

#include <forge/engine/ecs/Component.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class PlayableCharacterComponent : public AbstractComponent
{
    FRG__DECL_COMPONENT(PlayableCharacterComponent);
};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
