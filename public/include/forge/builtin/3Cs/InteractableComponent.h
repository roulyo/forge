#pragma once

#include <forge/engine/ecs/Component.h>
#include <forge/engine/system/Sharable.h>

#include <forge/builtin/3Cs/Interaction.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class InteractableComponent : public AbstractComponent
{
    FRG__DECL_COMPONENT(InteractableComponent);

public:
    InteractableComponent();

    void AddInteraction(const AbstractInteraction::CPtr& _interaction);

private:
    FRG__CLASS_ATTR_RW(bool, AlwaysUseDefault);
    FRG__CLASS_ATTR_RW(AbstractInteraction::CPtr, DefaultInteraction);
    FRG__CLASS_ATTR_RW(Vector<AbstractInteraction::CPtr>, Interactions);

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
