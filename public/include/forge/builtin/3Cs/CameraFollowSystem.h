#pragma once

#include <forge/engine/ecs/System.h>

#include <forge/builtin/3Cs/PlayableCharacterComponent.h>
#include <forge/builtin/3Cs/events/MouseEvent.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class MoveEvent;

//----------------------------------------------------------------------------------------------------------------------
class CameraFollowSystem : public System<PlayableCharacterComponent>
{
public:
    CameraFollowSystem();
    ~CameraFollowSystem();

    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

private:
    void OnMouseWheelEvent(const MouseWheelEvent& _event);

private:
    Vector3f    m_Speed;
    f32         m_DefaultFOV;

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
