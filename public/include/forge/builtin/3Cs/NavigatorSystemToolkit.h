#pragma once

#include <forge/engine/ecs/Entity.h>
#include <forge/engine/navigation/NavigationOptions.h>
#include <forge/engine/navigation/NavigationTypes.h>

FRG__OPEN_NAMESPACE(frg)

//----------------------------------------------------------------------------------------------------------------------
class NavigationProvider;

FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class NavigatorComponent;

//----------------------------------------------------------------------------------------------------------------------
class NavigatorSystemToolkit
{
public:
    static void SetNavigationProvider(NavigationProvider* _provider);

public:
    NavResult FindPath(const NavNode& _start,
                       const NavNode& _goal,
                       const NavigationOptions& _options = NavigationOptions()) const;
    NavResult FindPath(const Entity::Ptr& _navEntity,
                       const NavNode& _goal,
                       const NavigationOptions& _options = NavigationOptions()) const;
    bool Navigate(const Entity::Ptr& _navEntity,
                  const NavNode& _goal,
                  const NavigationOptions& _options = NavigationOptions()) const;
    bool UpdateNavigation(const Entity::Ptr& _navEntity) const;
    void TerminateNavigation(const Entity::Ptr& _navEntity) const;

    void NormalizeDirection(NavigatorComponent& _navComp) const;

private:
    static NavigationProvider* ms_NavigationProvider;

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
