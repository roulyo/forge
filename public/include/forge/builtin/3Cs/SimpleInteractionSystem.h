#pragma once

#include <forge/engine/ecs/System.h>
#include <forge/engine/world/EntityControlAgent.h>

#include <forge/builtin/3Cs/InteractableComponent.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class EntityClickedEvent;
class EntityHoveredEnterEvent;
class EntityHoveredExitEvent;
class InteractionChosenEvent;
class NavigationStartedEvent;
class NavigationStoppedEvent;

//----------------------------------------------------------------------------------------------------------------------
class SimpleInteractionSystem : public System<InteractableComponent>
                              , public EntityControlAgent
{
public:
    SimpleInteractionSystem();
    ~SimpleInteractionSystem();

    void OnStart() override;
    void OnStop() override;

    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

private:
    void ExecuteInteraction(const Entity::CPtr& _interactable, const AbstractInteraction::CPtr& _interaction);

    void OnEntityClickedEvent(const EntityClickedEvent& _event);
    void OnEntityHoveredEnterEvent(const EntityHoveredEnterEvent& _event);
    void OnEntityHoveredExitEvent(const EntityHoveredExitEvent& _event);
    void OnInteractionChosenEvent(const InteractionChosenEvent& _event);

    void OnNavigationStartedEvent(const NavigationStartedEvent& _event);
    void OnNavigationStoppedEvent(const NavigationStoppedEvent& _event);

private:
    struct
    {
        Vector2f                    Destination;
        AbstractInteraction::CPtr   Interaction;
        Entity::CPtr                Interactable;

        void Reset()
        {
            Interactable = nullptr;
            Interaction = nullptr;
            Destination = Vector2f::Zero;
        }
    } m_Context;

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
