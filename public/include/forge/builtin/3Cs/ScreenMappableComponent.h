#pragma once

#include <forge/engine/ecs/Component.h>
#include <forge/engine/rendering/RenderTree.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class ScreenMappableComponent : public AbstractComponent
{
    FRG__DECL_COMPONENT(ScreenMappableComponent);

public:
    ScreenMappableComponent();

    void SetRenderable(const AbstractRenderTree::Ptr& _renderTree);

private:
    FRG__CLASS_ATTR_RW(ShaderMaterialRenderNode::Ptr, MaterialNode);

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
