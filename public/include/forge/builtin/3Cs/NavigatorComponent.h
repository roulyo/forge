#pragma once

#include <forge/engine/ecs/Component.h>

#include <forge/engine/navigation/NavigationTypes.h>

FRG__OPEN_NAMESPACE(frg)
FRG__OPEN_NAMESPACE(bi)

//----------------------------------------------------------------------------------------------------------------------
class NavigatorComponent : public AbstractComponent
{
    FRG__DECL_COMPONENT(NavigatorComponent);

public:
    NavigatorComponent();

public:
    FRG__CLASS_ATTR_RW(frg::NavPath, Path);
    FRG__CLASS_ATTR_RW(frg::Vector3f, LastKnownPosition);
    FRG__CLASS_ATTR_RW(frg::Vector2f, Direction);
    FRG__CLASS_ATTR_RW(f32, MaxSpeed);
    FRG__CLASS_ATTR_RW(u32, MoveCapacity);

};

FRG__CLOSE_NAMESPACE(bi)
FRG__CLOSE_NAMESPACE(frg)
