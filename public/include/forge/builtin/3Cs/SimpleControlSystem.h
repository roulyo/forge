#pragma once

#include <functional>

#include <forge/engine/ecs/System.h>

#include <forge/builtin/3Cs/PlayableCharacterComponent.h>
#include <forge/builtin/physics/PhysicableComponent.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class MoveEvent;

//----------------------------------------------------------------------------------------------------------------------
class SimpleControlSystem : public System<PlayableCharacterComponent, PhysicableComponent>
{
public:
    typedef std::function<void(const Entity::Ptr&, const Vector2f&)> OnEntityMovedHook;

public:
    static f32 MoveSpeedUpS;

public:
    static void SetOnEntityMovedHook(const OnEntityMovedHook& _hook);

public:
    SimpleControlSystem();
    ~SimpleControlSystem();

    void OnStart() override;
    void OnStop() override;

    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

    void OnMoveEvent(const MoveEvent& _event);

private:
    void UpdateMoveVector();
    void UpdateSpeed(const Entity::Ptr& _navEntity) const;

private:
    static OnEntityMovedHook ms_OnEntityMovedHook;

private:
    frg::Vector2f m_MoveVector;
    u8              m_Direction;

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
