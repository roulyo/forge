#pragma once

#include <forge/engine/ecs/Entity.h>
#include <forge/engine/system/Sharable.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
template<class T>
concept InteractionEventConcept = requires(T)
{
    { requires(const Entity::CPtr& e) { T::Broadcast(e); } };
    { T::Id };
};

//----------------------------------------------------------------------------------------------------------------------
class AbstractInteraction
{
public:
    friend class SimpleInteractionSystem;

    typedef std::shared_ptr<AbstractInteraction>        Ptr;
    typedef std::shared_ptr<const AbstractInteraction>  CPtr;

public:
    AbstractInteraction()
        : m_DisplayString("")
        , m_ConsumesEntityOnInteract(false)
        , m_NeedsCloseRange(false)
    {}

    virtual const StringId& GetId() const = 0;

protected:
    virtual void Interact(const Entity::CPtr& _interactable) const = 0;

public:
    FRG__CLASS_ATTR_RW(String, DisplayString);
    FRG__CLASS_ATTR_RW(bool, ConsumesEntityOnInteract);
    FRG__CLASS_ATTR_RW(bool, NeedsCloseRange);

};

//----------------------------------------------------------------------------------------------------------------------
template<InteractionEventConcept Event>
class Interaction : public AbstractInteraction, public Sharable<Interaction<Event>>
{
public:
    using typename Sharable<Interaction<Event>>::Ptr;
    using typename Sharable<Interaction<Event>>::CPtr;

public:
    const StringId& GetId() const override { return Event::Id; }

private:
    void Interact(const Entity::CPtr& _interactable) const override { Event::Broadcast(_interactable); };

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
