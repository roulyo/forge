#pragma once

#include <forge/engine/async/Future.h>
#include <forge/engine/ecs/Entity.h>
#include <forge/engine/ecs/System.h>
#include <forge/engine/rendering/RenderingAgent.h>
#include <forge/engine/rendering/resource/Shader.h>
#include <forge/engine/rendering/resource/Texture.h>
#include <forge/engine/time/Chrono.h>

#include <forge/builtin/rendering/RenderableComponent.h>
#include <forge/builtin/3Cs/ScreenMappableComponent.h>

FRG__OPEN_NAMESPACE(frg);
FRG__OPEN_NAMESPACE(bi);

//----------------------------------------------------------------------------------------------------------------------
class MouseClickEvent;
class MouseMoveEvent;

//----------------------------------------------------------------------------------------------------------------------
class ScreenMappingSystem : public System<ScreenMappableComponent>,
                            public RenderingAgent
{
public:
    ScreenMappingSystem();
    ~ScreenMappingSystem();

    bool IsStandingBy() const override;

    void OnStart() override;
    void OnStop() override;
    void OnFrameBegin() override;
    void Execute(const u64& _dt, const Entity::Ptr& _entity) override;

    void OnMouseClickEvent(const MouseClickEvent& _event);
    void OnMouseMoveEvent(const MouseMoveEvent& _event);

private:
    enum class State
    {
        Waiting,
        Mapping,
        Downloading
    }   m_State;

    void SetState(State _state) { m_State = _state; }
    bool IsInState(State _state) const { return m_State == _state; }

private:
    static Shader::Ptr          ms_ScreenMappingShader;
    static TextureHandle        ms_ScreenMapHandle;
    static TextureHandle        ms_ScreenMapBackBuffer;

    Array<Entity::Ptr, 65535>   m_EntityMap;

    RAMTexture::Ptr             m_ScreenMap;

    Future<RAMTexture::Ptr>     m_DLReq;
    Chrono                      m_DLReqTimer;

    u16                         m_CurrentColor;

    Entity::Ptr                 m_HoveredEntity;

};

FRG__CLOSE_NAMESPACE(bi);
FRG__CLOSE_NAMESPACE(frg);
